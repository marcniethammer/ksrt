#include "ksrtParallelSegmentation.h"
#include <string>
#include<vector>
int n =0;

int main(int argc, char **argv)
{
        int rank, size;
        char hostname[256];
        MPI_Init(&argc,&argv);
        MPI_Comm_size( MPI_COMM_WORLD, &size );
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        struct timeval tim, timeOverall;
        double t1, t1Overall;

        if (size%numTestImg!=0 || size < 2*numTestImg ){
                cerr << "Error: # of processes MUST be a multiplier of  # of Test Images & large than twice of # of Test Imgs!" << endl;
                MPI_Finalize();

                return 0;
        }

        int groupSize = (int) size/numTestImg;
        gethostname(hostname,255);

        if (rank == 0){
                testMkdir(DataDIR + test_orig_DIR);
                testMkdir(DataDIR + test_DIR);
                testMkdir(DataDIR + move_bone_DIR);
                testMkdir(DataDIR + move_cart_DIR);

                string rsfilename = DataDIR + "/Test/patch-*.txt";
                runCommand("rm " + rsfilename);


                gettimeofday(&tim, NULL);
                t1=tim.tv_sec+(tim.tv_usec/1000000.0);
                t1Overall=tim.tv_sec+(tim.tv_usec/1000000.0);
        }

<<<<<<< HEAD
	if (rank == 0){
		testMkdir(DataDIR + train_orig_DIR);
		testMkdir(DataDIR + train_DIR);
		gettimeofday(&tim, NULL);
	        t1=tim.tv_sec+(tim.tv_usec/1000000.0);
	}
	
	MPI_Barrier(MPI_COMM_WORLD);
=======
        MPI_Barrier(MPI_COMM_WORLD);

        if(rank % groupSize == 0){ // Root of each group!
                lowLevel( rank, size, groupSize, 0);
        }
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0){
                        printTime(t1, 0);
                        gettimeofday(&tim, NULL);
                        t1=tim.tv_sec+(tim.tv_usec/1000000.0);
>>>>>>> a6c33da8946d26aed70408b919785ef6394d8314

        }



        lowLevel (rank, size, groupSize, 1);
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0){

                        printTime(t1, 1);
                        gettimeofday(&tim, NULL);
                        t1=tim.tv_sec+(tim.tv_usec/1000000.0);

        }


        if(rank % groupSize == 0){ // Root of each group!
                n = 0;
                string testFile1 = DataDIR + "/Test/move-bone/affine-";
                string testFile2 = DataDIR + "/Test/move-bone/bspline-";
                string inputFileFemur = DataDIR + "/Test/femur-fusion-";
                string addFileFemur = DataDIR + "/Test/move-bone/femur-";
                string outputFileFemur = DataDIR + "/Test/femur-fusion-";;
                string inputFileTibia = DataDIR + "/Test/tibia-fusion-";
                string addFileTibia = DataDIR + "/Test/move-bone/tibia-";
                string outputFileTibia =  DataDIR + "/Test/tibia-fusion-";

                printInFile(rank, size, groupSize, testFile1, testFile2, inputFileFemur, addFileFemur, outputFileFemur, inputFileTibia, addFileTibia, outputFileTibia, ".tfm");
                lowLevel ( rank, size, groupSize, 2);

        } // end of if

        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0){
                        printTime(t1, 2);
                        gettimeofday(&tim, NULL);
                        t1=tim.tv_sec+(tim.tv_usec/1000000.0);
        }

        lowLevel ( rank, size, groupSize, 3);
        MPI_Barrier(MPI_COMM_WORLD);
        if (rank == 0){
                        printTime(t1, 3);
                        gettimeofday(&tim, NULL);
                        t1=tim.tv_sec+(tim.tv_usec/1000000.0);
        }

        if(rank % groupSize == 0){ // Root of each group!
                n = 0;
                string testFile1 = DataDIR + "/Test/move-cart/affine-femur-";
                string testFile2 = DataDIR + "/Test/move-cart/affine-tibia-";
                string inputFileFemur = DataDIR + "/Test/fem-fusion-";
                string addFileFemur = DataDIR + "/Test/move-cart/fem-";
                string outputFileFemur = DataDIR + "/Test/fem-fusion-";;
                string inputFileTibia = DataDIR + "/Test/tib-fusion-";
                string addFileTibia = DataDIR + "/Test/move-cart/tib-";
                string outputFileTibia =  DataDIR + "/Test/tib-fusion-";

                printInFile(rank, size, groupSize, testFile1, testFile2, inputFileFemur, addFileFemur, outputFileFemur, inputFileTibia, addFileTibia, outputFileTibia, ".tfm");

        } // end of if
        MPI_Barrier(MPI_COMM_WORLD);

        if (rank == 0){
                        printTime(t1, 4);
                        gettimeofday(&tim, NULL);
                        t1=tim.tv_sec+(tim.tv_usec/1000000.0);
        }


        lowLevel ( rank, size, groupSize, 4);
        MPI_Barrier(MPI_COMM_WORLD);

        if (rank == 0){
                        printTime(t1, 5);
                        gettimeofday(&tim, NULL);
                        t1=tim.tv_sec+(tim.tv_usec/1000000.0);

        }

        if(rank % groupSize == 0){ // Root of each group!
                lowLevel ( rank, size, groupSize, 5);
                lowLevel ( rank, size, groupSize, 6);
        }

        MPI_Barrier(MPI_COMM_WORLD);

        if (rank == 0){
                        printTime(t1, 6);
                        printTime(t1Overall, -1);
        }


        MPI_Finalize();

        return 0;

}

void printTime(double temp, int phaseID){

        struct timeval tim2;
        gettimeofday(&tim2, NULL);
        double t2=tim2.tv_sec+(tim2.tv_usec/1000000.0);

        printf("\n\n <============================ Phase: %d -> %.6lf seconds elapsed =====================================> \n\n", phaseID,  t2-temp);

}


void printInFile(int rank, int size, int groupSize, string testFile1, string testFile2, string inputFileFemur, string addFileFemur, string outputFileFemur, string inputFileTibia, string addFileTibia, string outputFileTibia, string ext){
        cout << "<-------------------------------- Printing Phase --------------------------------------->"<< endl;

        int lineNum=0, lineNumAtlas=0;
<<<<<<< HEAD
        string filename = "Train.txt";
=======
        string filename = "Test.txt";
>>>>>>> a6c33da8946d26aed70408b919785ef6394d8314
        char * sFilename = new char[filename.length() + 1];
        std::strcpy(sFilename,filename.c_str());

        ifstream read(sFilename);

        string line, ATLAS, ID, FLIP, switches, inputFilename, outputFilename, outputbiasFilename;
        std::vector<std::string> splitLine;

        while( getline(read,line) ) {
                if (lineNum  == (rank / groupSize)){
                        splitLine = split(line, ' ');
                        ID = splitLine[0];
<<<<<<< HEAD
			FLIP = splitLine[1];
		
			ksrtCopy(DataDIR + "/image-" + ID + ".mhd ", DataDIR + "/Train-orig/image-" + ID + ".nhdr");
			ksrtCopy(DataDIR + "/labels-" + ID + ".mhd ", DataDIR + "/Train-orig/labels-" + ID + ".nhdr");
			
	/*		if (checkFileExists(DataDIR + "/roi-image-"  + ID + ".mhd") == 0 )
				ksrtCopy(DataDIR + "/roi-image-" + ID + ".mhd ", DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr");

			if (checkFileExists(DataDIR + "/roi-image-"  + ID + ".mhd") == 0)
				ksrtCopy(DataDIR + "/Train-orig/roi-image-" + ID + ".mhd ", DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr");
			ImageMath(DataDIR + "/Train-orig/roi-image-" + ID + ".mhd "," -changeOrig 0,0,0 ", "outfile " + DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr ");
	*/

			ImageMath(DataDIR + "/Train-orig/image-" + ID + ".nhdr ", " -changeOrig 0,0,0 ", " -outfile " + DataDIR + "/Train-orig/image-" + ID + ".nhdr ");
			ImageMath(DataDIR + "/Train-orig/labels-" + ID + ".nhdr ", " -changeOrig 0,0,0 ", " -outfile " + DataDIR + "/Train-orig/labels-" + ID + ".nhdr ");

	/*		if ( checkFileExists(DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr ") == 0)
        			ImageMath( DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr ", " -changeOrig 0,0,0 ", " -outfile " + DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr "); 
	*/

			ksrtFlip( DataDIR + "/Train-orig/image-"  + ID + ".nhdr ", " -f 0,0," + FLIP + " " , DataDIR + "/Train/image-" + ID + ".nhdr ");
			
			ksrtFlip( DataDIR + "/Train-orig/labels-" + ID + ".nhdr ", " -f 0,0," + FLIP + " " , DataDIR + "/Train/labels-" + ID + ".nhdr ");


	/*		if ( checkFileExists( DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr ") == 0)
			        ksrtFlip(DataDIR + "/Train-orig/roi-image-" + ID + ".nhdr " , " -f 0,0," + FLIP + " ", DataDIR + "/Train/roi-image-" + ID + ".nhdr ");
	*/

			N4ITKBiasFieldCorrection( DataDIR + "/Train/image-" + ID + ".nhdr ", DataDIR + "/Train/image-" + ID + "-correct.nhdr ", DataDIR + "/Train/bias-" + ID + ".nhdr ");
			remove( DataDIR + "/Train/bias-" + ID + ".*");
			
			ksrtScaleIntensities(DataDIR + "/Train/image-" + ID + "-correct.nhdr ",  DataDIR + "/Train/image-" + ID + "-correct-scale.nhdr ", " -m 100");
			
			ImageMath(DataDIR + "/Train/image-" + ID + "-correct-scale.nhdr "," -smooth -curveEvol -iter 20 ", " -outfile " + DataDIR + "/Train/image-" + ID + "-correct-scale-smooth.nhdr");
			ksrtResample(DataDIR + "/Train/image-" + ID + "-correct-scale-smooth.nhdr ", " -r newSize -v 100,100,100 ", DataDIR + "/Train/image-" + ID + "-correct-scale-smooth-small.nhdr");

			ImageMath(DataDIR + "/Train/labels-" + ID + ".nhdr ", " -extractLabel 1 ", " -outfile " + DataDIR + "/Train/femur-" + ID + ".nhdr");
			ImageMath(DataDIR + "/Train/labels-" + ID + ".nhdr ", " -extractLabel 2 ", " -outfile " + DataDIR + "/Train/fem-" + ID + ".nhdr");
			ImageMath(DataDIR + "/Train/labels-" + ID + ".nhdr ", " -extractLabel 3 ", " -outfile " + DataDIR + "/Train/tibia-" + ID + ".nhdr");
			ImageMath(DataDIR + "/Train/labels-" + ID + ".nhdr ", " -extractLabel 4 ", " -outfile " + DataDIR + "/Train/tib-" + ID + ".nhdr");
			
			ksrtCutFemur(DataDIR + "/Train/tibia-" + ID + ".nhdr ", " 45 ", DataDIR + "/Train/tibia-" + ID + "-cut.nhdr ");
			ImageMath (DataDIR + "/Train/tibia-" + ID + ".nhdr ", " -mul "+ DataDIR + "/Train/tibia-" + ID + "-cut.nhdr ", " -outfile " + DataDIR + "/Train/tibia-" + ID +"-cut.nhdr");
			ksrtCutTibia( DataDIR + "/Train/femur-" + ID + ".nhdr ", " 45 ", DataDIR + "/Train/femur-" + ID + "-cut.nhdr");
			ImageMath(DataDIR + "/Train/femur-" + ID + ".nhdr ", " -mul " + DataDIR + "/Train/femur-" + ID + "-cut.nhdr ", " -outfile " + DataDIR + "/Train/femur-" + ID + "-cut.nhdr");

                } // end of outer if 
=======
                        FLIP = splitLine[1];

                        string filenameAtlas = "atlas.txt";
                        char * sFilenameAtlas = new char[filenameAtlas.length() + 1];
                        std::strcpy(sFilenameAtlas,filenameAtlas.c_str());

                        ifstream readAtlas(sFilenameAtlas);

                        while( getline(readAtlas, ATLAS) ) {
                                if (checkFileExists(testFile1 + ID + '-' + ATLAS + ext ) ==0 && checkFileExists(testFile2 + ID + '-' + ATLAS + ext) ==0){
                                        cout << "My Rank: " << rank <<" ATLAS: " << ATLAS << " ADDED" << endl;
                                        ImageMath(inputFileFemur  + ID + ".nhdr", " -add " + addFileFemur + ID + '-' + ATLAS + ".nhdr",  " -outfile " + outputFileFemur  + ID + ".nhdr -type float");
                                        ImageMath(inputFileTibia  + ID + ".nhdr", " -add " + addFileTibia + ID + '-' + ATLAS + ".nhdr",  " -outfile " + outputFileTibia  + ID + ".nhdr -type float");
                                        n++;
                                }// end of check file if
                                else{
                                        cout << "File "<<testFile1 + ID + '-' + ATLAS + ".tfm does not exist!" << endl;
                                        cout << "File "<<testFile2 + ID + '-' + ATLAS + ".tfm does not exist!" << endl;
                                }

                                lineNumAtlas++;

                        } //end of inner while

                } // end of outer if
>>>>>>> a6c33da8946d26aed70408b919785ef6394d8314

                lineNum++;
        } // end of outer while
        cout << "<-------------------------------- End of Printing Phase --------------------------------------->"<< endl;
} // end of printing file

void lowLevel ( int rank, int size, int groupSize, int phase){

        int lineNum=0, lineNumAtlas=0;
        string filename = "Test.txt";
        char * sFilename = new char[filename.length() + 1];
        std::strcpy(sFilename,filename.c_str());

        ifstream read(sFilename);

        string line, ATLAS, ID, FLIP, switches, inputFilename, outputFilename, outputbiasFilename;
        std::vector<std::string> splitLine;

        cout << "<--------------------------------Type Phase: " <<phase <<" --------------------------------------->"<< endl;

        while( getline(read,line) ) {
                if (lineNum  == (rank / groupSize)){
                        splitLine = split(line, ' ');
                        ID = splitLine[0];
                        FLIP = splitLine[1];

                        string filenameAtlas = "atlas.txt";
                        char * sFilenameAtlas = new char[filenameAtlas.length() + 1];
                        std::strcpy(sFilenameAtlas,filenameAtlas.c_str());

                        stringstream ss;

                        ifstream readAtlas(sFilenameAtlas);
                        if (phase == 0 || phase ==2 || phase ==6){
                                if(phase == 0){
                                        cout << "<--------------------------------Phase 0--------------------------------------->"<< endl;
                                        ksrtCopy(DataDIR + "/test-" + ID + ".mhd ", DataDIR + "/Test-orig/test-" + ID + ".nhdr");
                                        ImageMath(DataDIR + "/Test-orig/test-" + ID + ".nhdr ", "-changeOrig 0,0,0 ", " -outfile " + DataDIR + "/Test-orig/test-" + ID + ".nhdr");
                                        ksrtFlip(DataDIR + "/Test-orig/test-" + ID + ".nhdr", " -f 0,0," + FLIP + " ",  DataDIR + "/Test/test-" + ID + ".nhdr");
                                        N4ITKBiasFieldCorrection(DataDIR + "/Test/test-" + ID + ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct.nhdr ", DataDIR + "/Test/bias-" + ID + ".nhdr ");
                                        ksrtScaleIntensities(DataDIR + "/Test/test-" + ID + "-correct.nhdr ",  DataDIR + "/Test/test-" + ID + "-correct-scale.nhdr", " -m 100");
                                        ImageMath(DataDIR + "/Test/test-" + ID + "-correct-scale.nhdr", " -smooth -curveEvol -iter 20"," -outfile " + DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr");
                                        ksrtResample(DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr"," -r newSize -v 100,100,100 " , DataDIR + "/Test/test-" + ID + "-correct-scale-smooth-small.nhdr");
                                        ImageMath(DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr"," -sub " + DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr", " -outfile " + DataDIR + "/Test/femur-fusion-" + ID + ".nhdr -type float");
                                        ImageMath( DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr", " -sub " + DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr", " -outfile " + DataDIR + "/Test/tibia-fusion-" + ID + ".nhdr -type float");
                                        cout << "<-------------------------------- End Phase 0 --------------------------------------->"<< endl;

                                }
                                else if (phase ==2) {

                                        cout << "<-------------------------------- Phase 2 --------------------------------------->"<< endl;
                                        ss << n;
                                        ImageMath(DataDIR + "/Test/femur-fusion-" + ID + ".nhdr ", "-constOper 3,"+ ss.str() +  " -outfile ", DataDIR + "/Test/femur-fusion-" + ID + ".nhdr -type float");
                                        ImageMath(DataDIR + "/Test/tibia-fusion-" + ID + ".nhdr ", "-constOper 3," + ss.str() + " -outfile ", DataDIR + "/Test/tibia-fusion-" + ID + ".nhdr -type float");
                                        ksrtComputeBoneProbabilityUsingAssumption( DataDIR +"/Test/test-"+ ID +"-correct-scale-smooth.nhdr ", DataDIR + "/Test/pBone-" + ID + ".nhdr ");
                                        ksrtComputeBoneLabelingCost(DataDIR + "/Test/pBone-" + ID + ".nhdr ", DataDIR + "/Test/femur-fusion-" + ID + ".nhdr ", DataDIR + "/Test/tibia-fusion-" + ID + ".nhdr ", DataDIR + "/Test/femur-cost-" + ID + ".nhdr ", DataDIR + "/Test/bkgrd-cost-" + ID + ".nhdr ", DataDIR + "/Test/tibia-cost-" + ID + ".nhdr ");

                                        ksrtSegmentation_3label(DataDIR + "/Test/femur-cost-" + ID + ".nhdr ", DataDIR + "/Test/bkgrd-cost-" + ID + ".nhdr ", DataDIR + "/Test/tibia-cost-" + ID + ".nhdr " ,  DataDIR + "/Test/femur-seg-" + ID + ".nhdr ", DataDIR + "/Test/tibia-seg-" + ID + ".nhdr", " -g 0.5 ");

                                        ExtractLargestConnectedComponentFromBinaryImage(DataDIR + "/Test/femur-seg-" + ID + ".nhdr ", DataDIR + "/Test/femur-seg-" + ID + ".nhdr ");
                                        ExtractLargestConnectedComponentFromBinaryImage(DataDIR + "/Test/tibia-seg-" + ID + ".nhdr ", DataDIR + "/Test/tibia-seg-" + ID + ".nhdr ");

                                        ksrtCutFemur(DataDIR + "/Test/tibia-seg-" + ID + ".nhdr ", " 45 ", DataDIR + "/Test/tibia-seg-" + ID + "-cut.nhdr ");
                                        ImageMath(DataDIR + "/Test/tibia-seg-" + ID + ".nhdr ", " -mul " + DataDIR + "/Test/tibia-seg-" + ID + "-cut.nhdr ", " -outfile " + DataDIR + "/Test/tibia-seg-" + ID + "-cut.nhdr");
                                        ksrtCutTibia(DataDIR + "/Test/femur-seg-" + ID + ".nhdr ", + " 45 ", DataDIR + "/Test/femur-seg-" + ID + "-cut.nhdr ");
                                        ImageMath(DataDIR + "/Test/femur-seg-" + ID + ".nhdr ", " -mul " + DataDIR + "/Test/femur-seg-" + ID + "-cut.nhdr ", " -outfile " + DataDIR + "/Test/femur-seg-" + ID + "-cut.nhdr");                        ImageMath(DataDIR + "/Test/test-" + ID + "-correct.nhdr ", " -sub " + DataDIR + "/Test/test-" + ID + "-correct.nhdr", " -outfile " +DataDIR + "/Test/fem-fusion-"+ ID + ".nhdr -type float ");
                                        ImageMath(DataDIR + "/Test/test-" + ID + "-correct.nhdr -sub ", DataDIR + "/Test/test-" + ID + "-correct.nhdr ", " -outfile " + DataDIR + "/Test/tib-fusion-" + ID + ".nhdr -type float");

                                        cout << "<-------------------------------- End Phase 2 --------------------------------------->"<< endl;

                                } // end of phase 2
                                else if (phase==6) { // phase 6

                                        cout << "<-------------------------------- Phase 6 n: "<< n <<" rank: " << rank << " --------------------------------------->"<< endl;
                                        ss << n;
                                        ksrtPatchBasedLabelFusion(DataDIR + "/Test/test-" + ID + "-correct-scale.nhdr ", DataDIR + "/Test/patch-" + ID + ".txt ", DataDIR + "/Test/fem-fusion-"+ ID + "-patch.nhdr ", DataDIR + "/Test/tib-fusion-" + ID +"-patch.nhdr"," --patchSize 2 --neighborhoodSize 2 --nearestNeighbors 1");

                                        ImageMath( DataDIR + "/Test/fem-fusion-" + ID + ".nhdr ", " -constOper 3," + ss.str() , " -outfile " + DataDIR + "/Test/fem-fusion-" + ID + ".nhdr -type float");
                                        ImageMath( DataDIR + "/Test/tib-fusion-" + ID + ".nhdr "," -constOper 3," + ss.str() , " -outfile " + DataDIR + "/Test/tib-fusion-" + ID + ".nhdr -type float");
                                        ImageMath( DataDIR + "/Test/test-" + ID + "-correct-scale.nhdr"," -constOper 2,300", " -outfile " + DataDIR + "/Test/test-" + ID + "-correct-scale-30000.nhdr");

                                        ksrtExtractTestingFeatures_SVM ( DataDIR + "/Test/test-" + ID + "-correct-scale-30000.nhdr " ,  DataDIR + "/Test/fem-fusion-" + ID + ".nhdr " , DataDIR + "/Test/tib-fusion-" + ID + ".nhdr --scales 0.39,0.78,1.0 --threshold 0.0 " , DataDIR + "/SVM/f-" + ID);

                                        svm_predict (" -b 1 " , DataDIR + "/SVM/f-" + ID +  " " , DataDIR + "/SVM/train_new_small.model " , DataDIR + "/SVM/p-" + ID);

                                        ksrtComputeClassificationProbabilities_SVM(DataDIR + "/Test/fem-fusion-" + ID + ".nhdr " , DataDIR + "/Test/tib-fusion-" + ID + ".nhdr " , DataDIR + "/SVM/p-" + ID + " " ,  DataDIR+ "/Test/pFem-" + ID + "-svm.nhdr " , DataDIR + "/Test/pTib-" + ID + "-svm.nhdr"," -t 0.0");

                                        ksrtComputeNormalDirection(DataDIR + "/Test/femur-seg-" + ID + ".nhdr " , DataDIR + "/Test/tibia-seg-" + ID + ".nhdr " , DataDIR + "/Test/nx-" + ID + ".nhdr " , DataDIR + "/Test/ny-" + ID + ".nhdr " , DataDIR + "/Test/nz-" + ID + ".nhdr"," -u 1,1,1");

                                        ksrtComputeCartilageLabelingCost(DataDIR + "/Test/pFem-" + ID + "-svm.nhdr " ,  DataDIR + "/Test/pTib-" + ID + "-svm.nhdr " , DataDIR + "/Test/fem-fusion-" + ID + "-patch.nhdr " , DataDIR + "/Test/tib-fusion-" + ID + "-patch.nhdr " , DataDIR + "/Test/fem-cost-" + ID + ".nhdr " , DataDIR + "/Test/bkg-cost-" + ID + ".nhdr " , DataDIR + "/Test/tib-cost-" + ID + ".nhdr");

                                        ksrtSegmentation_3label_anisotropic(DataDIR + "/Test/fem-cost-" + ID + ".nhdr " , DataDIR + "/Test/bkg-cost-" + ID + ".nhdr " , DataDIR + "/Test/tib-cost-" + ID + ".nhdr " , DataDIR + "/Test/nx-" + ID + ".nhdr " , DataDIR + "/Test/ny-" + ID + ".nhdr " , DataDIR + "/Test/nz-" + ID + ".nhdr " , DataDIR + "/Test/fem-seg-" + ID + "-svm-patch-1.0-0.1.nhdr " , DataDIR + "/Test/tib-seg-" + ID + "-svm-patch-1.0-0.1.nhdr", " -g 1.0 -a 0.1");


                                        cout << "<-------------------------------- End Phase 6 --------------------------------------->"<< endl;
                                }// end of phase 6

                        } // end of phase 2
                        else {
                                while( getline(readAtlas, ATLAS) ) {
//                              if(phase == 1 || phase == 3 || phase == 4){
                                        if((lineNumAtlas % groupSize) == (rank % groupSize)){
                                                if (phase == 1){
                                                        cout << "<--------------------------------Phase 1------------------------------------------->"<< endl;
                                                        ksrtComputeAffineRegistration(DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ", DataDIR + "/Train/image-" + ATLAS + "-correct-scale-smooth.nhdr", " --metric MI ", DataDIR + "/Test/move-bone/affine-" + ID + '-' + ATLAS + ".nhdr ", DataDIR + "/Test/move-bone/affine-" + ID + '-' + ATLAS + ".tfm");

                                                        BSplineDeformableRegistration( DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ",    DataDIR + "/Test/move-bone/affine-" + ID + '-' + ATLAS + ".nhdr ", "--histogrambins 20 --spatialsamples 100000 --iterations 200 --constrain --maximumDeformation 10.0 --resampledmovingfilename ", DataDIR + "/Test/move-bone/bspline-" + ID + '-' + ATLAS + ".nhdr "," --outputtransform "+ DataDIR + "/Test/move-bone/bspline-" + ID + '-' + ATLAS + ".tfm");

                                                        if((checkFileExists(DataDIR + "/Test/move-bone/affine-" + ID + '-' + ATLAS + ".tfm") == 0) && (checkFileExists(DataDIR + "/Test/move-bone/bspline-" + ID + '-' + ATLAS + ".tfm") == 0)){

                                                                ksrtApplyAffineTransform(DataDIR + "/Test/move-bone/affine-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Train/femur-" + ATLAS+ ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ",DataDIR + "/Test/move-bone/femur-" + ID + '-' + ATLAS + ".nhdr");

                                                                ksrtApplyBSplineTransform(DataDIR + "/Test/move-bone/bspline-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Test/move-bone/femur-" + ID + '-' + ATLAS + ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ", DataDIR + "/Test/move-bone/femur-" + ID + '-' + ATLAS + ".nhdr");

                                                                ksrtApplyAffineTransform(DataDIR + "/Test/move-bone/affine-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Train/tibia-" + ATLAS + ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ", DataDIR + "/Test/move-bone/tibia-" + ID + '-' +ATLAS + ".nhdr");


                                                                ksrtApplyBSplineTransform(DataDIR + "/Test/move-bone/bspline-" + ID + '-' + ATLAS + ".tfm ", DataDIR +  "/Test/move-bone/tibia-" + ID + '-' + ATLAS + ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ", DataDIR + "/Test/move-bone/tibia-" + ID + '-' + ATLAS + ".nhdr");
                                                        cout << "<--------------------------------End Phase 1---------------------------------------->"<< endl;

                                                        }// end of check file if

                                                }// end of phase 1

                                                else if (phase == 3 || phase == 4) {
                                                        if (phase ==3){
                                                                cout << "<-------------------------------- Phase 3 --------------------------------------->"<< endl;
                                                                ksrtComputeAffineRegistration(DataDIR + "/Test/femur-seg-" + ID + "-cut.nhdr ", DataDIR + "/Train/femur-" + ATLAS + "-cut.nhdr", " --metric SSD ", DataDIR + "/Test/move-cart/femur-" + ID + '-' + ATLAS + ".nhdr ", DataDIR + "/Test/move-cart/affine-femur-" + ID + '-' + ATLAS + ".tfm");
                                                                ksrtComputeAffineRegistration(DataDIR + "/Test/tibia-seg-" + ID + "-cut.nhdr ", DataDIR + "/Train/tibia-" + ATLAS + "-cut.nhdr", " --metric SSD ", DataDIR + "/Test/move-cart/tibia-"+ ID + '-' + ATLAS + ".nhdr " , DataDIR + "/Test/move-cart/affine-tibia-" + ID + '-' + ATLAS + ".tfm");

                                                                if(checkFileExists(DataDIR + "/Test/move-cart/affine-femur-" + ID + '-' + ATLAS + ".tfm ")==0 && checkFileExists(DataDIR + "/Test/move-cart/affine-tibia-" + ID + '-' + ATLAS + ".tfm")==0) {

                                                                        ksrtApplyAffineTransform(DataDIR + "/Test/move-cart/affine-femur-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Train/fem-" + ATLAS + ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ", DataDIR + "/Test/move-cart/fem-" + ID + '-' + ATLAS + ".nhdr");
                                                                        ksrtApplyAffineTransform(DataDIR + "/Test/move-cart/affine-tibia-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Train/tib-" + ATLAS + ".nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr " , DataDIR + "/Test/move-cart/tib-" + ID + '-' + ATLAS + ".nhdr ");


                                                                }// end of check file if
                                                                cout << "<--------------------------------End Phase 3--------------------------------------->"<< endl;
                                                        } // end of phase 3
                                                        else if (phase == 4){
                                                                cout << "<-------------------------------- Phase 4 --------------------------------------->"<< endl;

                                                                ksrtApplyAffineTransform(DataDIR + "/Test/move-cart/affine-femur-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Train/image-" + ATLAS + "-correct-scale.nhdr ", DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ", DataDIR + "/Test/move-cart/mri-femur-" + ID + '-' + ATLAS + ".nhdr ");
                                                                ksrtApplyAffineTransform(DataDIR + "/Test/move-cart/affine-tibia-" + ID + '-' + ATLAS + ".tfm ", DataDIR + "/Train/image-" + ATLAS + "-correct-scale.nhdr ",  DataDIR + "/Test/test-" + ID + "-correct-scale-smooth.nhdr ",  DataDIR +  "/Test/move-cart/mri-tibia-" + ID + '-' + ATLAS + ".nhdr ");

                                                                                }
                                                }//end of phase 3 or 4

                                        } // end of if
                        //      }
                                if (phase == 5){
                                                                cout << "<-------------------------------- Phase 5 --------------------------------------->"<< endl;
                                                                string S1 = DataDIR + "/Test/move-cart/mri-femur-"+ ID + '-' + ATLAS + ".nhdr";
                                                                string S2 = DataDIR + "/Test/move-cart/fem-"+ ID + '-' + ATLAS + ".nhdr";
                                                                string S3 = DataDIR + "/Test/move-cart/mri-tibia-" + ID + '-' + ATLAS + ".nhdr";
                                                                string S4 = DataDIR + "/Test/move-cart/tib-" + ID + '-' + ATLAS + ".nhdr";
                                                                //echo "$S1 $S2 $S3 $S4" >> "$DataDIR"/Test/patch-"$ID".txt

                                                                string pFilename = DataDIR + "/Test/patch-" + ID + ".txt";
                                                                cout << pFilename << endl;
                                                                ofstream myfile(pFilename.c_str(), std::ofstream::out | std::ofstream::app);
                                                                if(myfile.is_open()){
                                                                        myfile << S1 + " " + S2 + " " + S3 + " " + S4 +  "\n";
                                                                        myfile.close();
                                                                }
                                                                else cout << "Unable to open file";

                                                                cout << "<--------------------------------End Phase 5 --------------------------------------->"<< endl;

                                        }

                                        lineNumAtlas++;

                                } //end of inner while
                        } //Phase 2

                } // end of outer if

                lineNum++;

        } // end of outer while

}