#include "ksrtParallelSVM.h" 
#include <string>
#include<vector>
void compute_SVM(int rank, int size){
	cout << "<-------------------------------- SVM Phase --------------------------------------->"<< endl;

	int lineNum=0, lineNumAtlas=0;
	string filename = "atlas.txt";
	char * sFilename = new char[filename.length() + 1];
	std::strcpy(sFilename,filename.c_str());

	ifstream read(sFilename);

	string line, ATLAS, ID, FLIP, switches, inputFilename, outputFilename, outputbiasFilename;
	std::vector<std::string> splitLine;

	while( getline(read,line) ) {
//		if (lineNum % size == rank){
			splitLine = split(line, ' ');
			ID = splitLine[0];
			FLIP = splitLine[1];

			runCommand(ksrtAppDIR+ "/ksrtComputeTrainingLabelImage " + DataDIR + "/Train/fem-" + ID + ".nhdr "+ DataDIR + "/Train/tib-" + ID + ".nhdr "+  DataDIR + "/Train/trainLabel-" + ID + ".nhdr -r 4 ");
			ImageMath(DataDIR + "/Train/image-" + ID + "-correct-scale.nhdr ", " -constOper 2,300 ", " -outfile " + DataDIR + "/Train/image-" + ID + "-correct-scale-30000.nhdr ");

//		} // end of outer if 

//		lineNum++;
	} // end of outer while
	read.close();
		
	MPI_Barrier(MPI_COMM_WORLD);

	lineNum = 0;
	ifstream read2(sFilename);
	if (rank == 0){
		while( getline(read2,line) ) {
	//		if (lineNum % size == rank){
				splitLine = split(line, ' ');
				ID = splitLine[0];
				FLIP = splitLine[1];
				runCommand(ksrtAppDIR + "/ksrtExtractTrainingFeaturesGroups_SVM "+ DataDIR + "/Train/image-" + ID + "-correct-scale-30000.nhdr " + DataDIR + "/Train/trainLabel-" + ID + ".nhdr  -s 0.39,0.78,1.0 " + DataDIR  + "/SVM/train_new_1 "+ DataDIR + "/SVM/train_new_2 " + DataDIR + "/SVM/train_new_3");

	//		} // end of outer if 

	//		lineNum++;
		} // end of outer while
		runCommand( PYTHON + " " + svmDIR + "/tools/subset.py " + DataDIR + "/SVM/train_new_1 66667 " + DataDIR + "/SVM/train_new_1_small");
		runCommand( PYTHON + " " + svmDIR + "/tools/subset.py " + DataDIR + "/SVM/train_new_2 66667 " + DataDIR + "/SVM/train_new_2_small");
		runCommand( PYTHON + " " + svmDIR + "/tools/subset.py " + DataDIR + "/SVM/train_new_3 66667 " + DataDIR + "/SVM/train_new_3_small");

		runCommand("cat " + DataDIR + "/SVM/train_new_3_small >> " + DataDIR + "/SVM/train_new_small");
		runCommand("cat " + DataDIR + "/SVM/train_new_2_small >> " + DataDIR + "/SVM/train_new_small");
		runCommand("cat " + DataDIR + "/SVM/train_new_1_small >> " + DataDIR + "/SVM/train_new_small");

		runCommand( svmDIR + "/svm-train -c 4 -e 0.1 -b 1 -m 32000 -h 0 " + DataDIR + "/SVM/train_new_small " +  DataDIR + "/SVM/train_new_small.model");	

		read2.close();
	cout << "<-------------------------------- End of SVM Phase --------------------------------------->"<< endl;
	}

} // end of compute

int main(int argc, char **argv)
{
	int rank, size;
	char hostname[256];
	MPI_Init(&argc,&argv);
	MPI_Comm_size( MPI_COMM_WORLD, &size );
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	struct timeval tim;
	double t1, t2;

	gethostname(hostname,255);

//	if (size<2 ){
//		cerr << "Error: # of processes MUST be larger than one!" << endl;
//		MPI_Finalize();
//		return 0;
//	}


	if (rank == 0){
		testMkdir(DataDIR + SVM_DIR);

		if (checkFileExists(DataDIR + "/SVM/train_new_small") == 0){
			remove(DataDIR + "/SVM/train_new_small");
			cout << "removing old training file: train_new_small" << endl;
		}


		if (checkFileExists(DataDIR + "/SVM/train_new_1") == 0){
			remove(DataDIR + "/SVM/train_new_1");
			cout << "removing old training file: train_new_1" << endl;
		}

		if (checkFileExists(DataDIR + "/SVM/train_new_2") == 0){
			remove(DataDIR + "/SVM/train_new_2");
			cout << "removing old training file: train_new_2" << endl;
		}

		if (checkFileExists(DataDIR + "/SVM/train_new_3") == 0){
			remove(DataDIR + "/SVM/train_new_3");
			cout << "removing old training file: train_new_3" << endl;
		}

		gettimeofday(&tim, NULL);
		t1=tim.tv_sec+(tim.tv_usec/1000000.0);
	}

	MPI_Barrier(MPI_COMM_WORLD);

	compute_SVM(rank, size);


	MPI_Barrier(MPI_COMM_WORLD);

	if (rank == 0){

		gettimeofday(&tim, NULL);
		t2=tim.tv_sec+(tim.tv_usec/1000000.0);
		printf("\nCompute Time: %.6lf seconds elapsed\n", t2-t1);
	}


	MPI_Finalize();

	return 0;

}



