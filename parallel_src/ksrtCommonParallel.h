#include <time.h>
#include <mpi.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <sys/time.h>
using namespace std;

const int numTestImg = 2;

const string  test_orig_DIR = "/Test-orig/";
const string  test_DIR = "/Test/";
<<<<<<< HEAD
const string move_bone_DIR = "/Test/move-bone/"; 
const string move_cart_DIR = "/Test/move-cart/"; 
=======
const string move_bone_DIR = "/Test/move-bone/";
const string move_cart_DIR = "/Test/move-cart/";
>>>>>>> a6c33da8946d26aed70408b919785ef6394d8314
const string  train_orig_DIR = "/Train-orig/";
const string  train_DIR = "/Train/";
const string SVM_DIR= "/SVM/";


/*Config directories */
const string ImgMathDIR="/home/abahman/Tools/KSRT/ksrt/build/lib/ImageMath";
const string ksrtAppDIR="/home/abahman/Tools/KSRT/ksrt/build/src/Application";
const string SlicerDIR="/home/abahman/Tools/KSRT/Slicer3-build";
const string ksrtLibDIR="/home/abahman/Tools/KSRT/ksrt/build/lib";
const string DataDIR="/home/abahman/Tools/KSRT/DataDIR_SKI";
const string ExtractLCCDIR="/home/abahman/Tools/KSRT/ksrt/build/lib/ExtractLargestConnectedComponentFromBinaryImage";
const string svmDIR="/home/abahman/Tools/KSRT/ksrt/lib/libsvm-3.18";
const string svmOutDIR="/home/abahman/Tools/KSRT/DataDIR/SVM";
const string PYTHON = "/home/abahman/bin/python3/bin/python";


int runCommand(string command);
int checkFileExists(string filename);

void ImageMath(string inputFile, string ops, string outputFile);
void testMkdir(string inputFilename);

void ksrtCopy( string inputFilename, string outputFilename);
void ksrtFlip( string inputFilename, string ops, string outputFilename);
void N4ITKBiasFieldCorrection();
void N4ITKBiasFieldCorrection(string inputFilename, string correctFilename, string biasFilename);
void ksrtScaleIntensities(string inputFilename, string outputFilename, string ops);


void ksrtResample(string inputFilename, string ops, string outputFilename);



void ksrtComputeBoneProbabilityUsingAssumption(string inputFilename, string outputFilename);

void ksrtComputeBoneLabelingCost( string pBoneFilename, string femurFusionFilename, string tibiaFusionFilename, string femurCostFilename, string bkgrdCostFilename, string tibiaCostFilename);


void ksrtSegmentation_3label( string femurCost, string bkgrdCost, string tibiaCost, string femurSeg, string tibiaSeg, string ops);


void ExtractLargestConnectedComponentFromBinaryImage(string inputFilename, string outputFilename);

void ksrtCutFemur( string inputFilename, string ops, string outputFilename);
void ksrtCutTibia( string inputFilename, string ops, string outputFilename);


void remove( string filename);
void ksrtPatchBasedLabelFusion(string inputFilename, string patchFilename, string femFusionFilename, string tibFusionFilename, string ops);

void ksrtExtractTestingFeatures_SVM (string inputTestfile , string  femFusion, string tibfusion , string fFilename);

void svm_predict ( string ops, string fFilename, string ModelFilename , string pFilename);
void ksrtComputeClassificationProbabilities_SVM ( string femFusion, string tibFusion, string pFilename , string pFemsvm, string pTibsvm, string ops);

void ksrtComputeNormalDirection( string femurSeg, string tibiaSeg, string nxFilename , string nyFilename, string nzFilename, string ops);

void ksrtComputeCartilageLabelingCost(string pFemsvm, string pTibsvm, string femFusionPatch, string tibFusionPatch, string femCost, string bkgCost, string tibCost);

void ksrtSegmentation_3label_anisotropic(string femCost, string bkgCost, string tibCost, string nxFilename, string nyFilename, string nzFilename, string femSegSvmPatch, string tibSegSvmPatch, string ops);

void ksrtComputeAffineRegistration( string inputFilename, string ATLASfilename, string ops, string affineATLAS, string affineATLAStfm);

void BSplineDeformableRegistration(string inputFilename, string affineATLAS, string ops, string bsplineATLAS, string outputtransform);

void ksrtApplyAffineTransform( string affineATLAS,  string ATLAStfm, string testFilename, string ATLAS);

void ksrtApplyBSplineTransform(string bsplineATLAStfm, string ATLAS1, string testFilename, string ATLAS2);


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);