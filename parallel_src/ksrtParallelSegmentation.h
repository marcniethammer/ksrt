#include "ksrtCommonParallel.h"

void printInFile(int rank, int size, int groupSize, string testFile1, string testFile2, string inputFileFemur, string addFileFemur, string outputFileFemur, string inputFileTibia, string addFileTibia, string outputFileTibia, string ext);
void printTime(double, int);
void lowLevel ( int rank, int size, int groupSize, int phase);
void Phase0(string string_n, string ID);
void Phase1(string ID, string ATLAS);
void Phase2(string string_n, string ID);
void Phase3(string ID, string ATLAS);
void Phase4(string ID, string ATLAS);
void Phase5(string ID, string ATLAS);
void Phase6(string string_n, string ID);
