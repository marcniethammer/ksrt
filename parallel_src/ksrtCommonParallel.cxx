#include "ksrtCommonParallel.h"

int checkFileExists(string filename){

        string SS = "[ -e " + filename +" ]";

        return  runCommand(SS);

};

int runCommand(string Command){

        struct timeval tim;
        gettimeofday(&tim, NULL);
        double t1=tim.tv_sec+(tim.tv_usec/1000000.0);

        char *sCommand = new char[Command.length() + 1];
        cout << "Command: " << Command << endl;
        std::strcpy(sCommand,Command.c_str());

        int val;
#ifdef PRINT_COMMANDS
                        val = 0;
#else
                        val = system(sCommand);
#endif



        gettimeofday(&tim, NULL);
        double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
        printf("%.6lf seconds elapsed\n", t2-t1);


        return val;
};


void testMkdir(string inputFilename){

        if (checkFileExists(inputFilename) != 0 ){
                runCommand ("mkdir " + inputFilename);
        }
};


void ImageMath(string inputFile, string ops, string outputFile){
        string SS = ImgMathDIR + "/ImageMath " + inputFile + ops + outputFile;
        runCommand(SS);
};



void ksrtCopy( string inputFilename, string outputFilename){
        string SS = ksrtAppDIR + "/ksrtCopy " + inputFilename + " " + outputFilename;
        runCommand(SS);
};

void ksrtFlip( string inputFilename, string ops, string outputFilename){

        string SS = ksrtAppDIR + "/ksrtFlip " + inputFilename + ops + outputFilename;
        runCommand(SS);
};

void N4ITKBiasFieldCorrection() {


};
void N4ITKBiasFieldCorrection(string inputFilename, string correctFilename, string biasFilename){
//      "$SlicerDIR"/lib/Slicer3/Plugins/N4ITKBiasFieldCorrection --inputimage "$DataDIR"/Test/test-"$ID".nhdr --outputimage "$DataDIR"/Test/test-"$ID"-correct.nhdr --outputbiasfield "$DataDIR"/Test/bias-"$ID".nhdr

        string SS = SlicerDIR + "/lib/Slicer3/Plugins/N4ITKBiasFieldCorrection " +" --inputimage  "  + inputFilename + " --outputimage " + correctFilename + " --outputbiasfield " + biasFilename;
        runCommand(SS);

};
void ksrtScaleIntensities(string inputFilename, string outputFilename, string ops){

        string SS = ksrtAppDIR + "/ksrtScaleIntensities " + inputFilename + outputFilename + ops;
        runCommand(SS);

} ;

void ksrtResample(string inputFilename, string ops, string outputFilename) {

        string SS = ksrtAppDIR + "/ksrtResample " + inputFilename + ops + outputFilename;
        runCommand(SS);
};

void ksrtComputeBoneProbabilityUsingAssumption(string inputFilename, string outputFilename) {

        string SS = ksrtAppDIR + "/ksrtComputeBoneProbabilityUsingAssumption " + inputFilename + outputFilename;
        runCommand(SS);

};

void ksrtComputeBoneLabelingCost( string pBoneFilename, string femurFusionFilename, string tibiaFusionFilename, string femurCostFilename, string bkgrdCostFilename, string tibiaCostFilename) {

        string SS = ksrtAppDIR + "/ksrtComputeBoneLabelingCost " +  pBoneFilename + femurFusionFilename + tibiaFusionFilename + femurCostFilename + bkgrdCostFilename + tibiaCostFilename;
        runCommand(SS);
};

void ksrtSegmentation_3label( string femurCost, string bkgrdCost, string tibiaCost, string femurSeg, string tibiaSeg, string ops) {

        string SS = ksrtAppDIR + "/ksrtSegmentation_3label " + femurCost + bkgrdCost + tibiaCost + femurSeg + tibiaSeg + ops;
        runCommand(SS);

};
void ExtractLargestConnectedComponentFromBinaryImage(string inputFilename, string outputFilename){

        string SS = ksrtLibDIR + "/ExtractLargestConnectedComponentFromBinaryImage/ExtractLargestConnectedComponentFromBinaryImage " + inputFilename + outputFilename;
        runCommand(SS);

};
void ksrtCutFemur( string inputFilename, string ops, string outputFilename){
        string SS = ksrtAppDIR + "/ksrtCutFemur " + inputFilename + ops + outputFilename;
        runCommand(SS);
};
void ksrtCutTibia( string inputFilename, string ops, string outputFilename){
        string SS = ksrtAppDIR + "/ksrtCutTibia " + inputFilename + ops + outputFilename;
        runCommand(SS);
};

void remove( string filename){

        string SS = "rm " + filename;
        runCommand(SS);

};

void ksrtPatchBasedLabelFusion(string inputFilename, string patchFilename, string femFusionFilename, string tibFusionFilename, string ops) {
        string SS = ksrtAppDIR + "/ksrtPatchBasedLabelFusion " + inputFilename +  patchFilename +  femFusionFilename + tibFusionFilename + ops;
        runCommand(SS);
};


void ksrtExtractTestingFeatures_SVM (string inputTestfile , string  femFusion, string tibfusion , string fFilename){

        string SS = ksrtAppDIR + "/ksrtExtractTestingFeatures_SVM " + inputTestfile + femFusion + tibfusion + fFilename;
        runCommand(SS);
};

void svm_predict ( string ops, string fFilename, string ModelFilename , string pFilename) {
        string SS = svmDIR + "/svm-predict " + ops + fFilename + ModelFilename + pFilename;
        runCommand(SS);
};
void ksrtComputeClassificationProbabilities_SVM ( string femFusion, string tibFusion, string pFilename , string pFemsvm, string pTibsvm, string ops) {
        string SS = ksrtAppDIR + "/ksrtComputeClassificationProbabilities_SVM " + femFusion + tibFusion + pFilename + pFemsvm + pTibsvm + ops;
        runCommand(SS);
};
void ksrtComputeNormalDirection( string femurSeg, string tibiaSeg, string nxFilename , string nyFilename, string nzFilename, string ops) {
        string SS = ksrtAppDIR + "/ksrtComputeNormalDirection " + femurSeg + tibiaSeg + nxFilename + nyFilename + nzFilename + ops;
        runCommand(SS);
};
void ksrtComputeCartilageLabelingCost(string pFemsvm, string pTibsvm, string femFusionPatch, string tibFusionPatch, string femCost, string bkgCost, string tibCost) {

        string SS = ksrtAppDIR + "/ksrtComputeCartilageLabelingCost " +  pFemsvm + pTibsvm + femFusionPatch + tibFusionPatch + femCost + bkgCost + tibCost;
        runCommand(SS);
};
void ksrtSegmentation_3label_anisotropic(string femCost, string bkgCost, string tibCost, string nxFilename, string nyFilename, string nzFilename, string femSegSvmPatch, string tibSegSvmPatch, string ops) {

        string SS = ksrtAppDIR + "/ksrtSegmentation_3label_anisotropic " + femCost + bkgCost + tibCost + nxFilename + nyFilename + nzFilename + femSegSvmPatch + tibSegSvmPatch + ops;
        runCommand(SS);

};
void ksrtComputeAffineRegistration( string inputFilename, string ATLASfilename, string ops, string affineATLAS, string affineATLAStfm) {
        string SS = ksrtAppDIR + "/ksrtComputeAffineRegistration " + inputFilename +  ATLASfilename + ops + affineATLAS + affineATLAStfm;
        runCommand(SS);
};
void BSplineDeformableRegistration(string inputFilename, string affineATLAS, string ops, string bsplineATLAS, string outputtransform) {
        string SS = SlicerDIR + "/lib/Slicer3/Plugins/BSplineDeformableRegistration " + inputFilename + affineATLAS + ops + bsplineATLAS + outputtransform;
        runCommand(SS);
};
void ksrtApplyAffineTransform( string affineATLAS,  string ATLAStfm, string testFilename, string ATLAS) {
        string SS = ksrtAppDIR + "/ksrtApplyAffineTransform " + affineATLAS + ATLAStfm + testFilename + ATLAS ;
        runCommand(SS);
};
void ksrtApplyBSplineTransform(string bsplineATLAStfm, string ATLAS1, string testFilename, string ATLAS2){
        string SS = ksrtAppDIR + "/ksrtApplyBSplineTransform " + bsplineATLAStfm + ATLAS1 + testFilename + ATLAS2;
        runCommand(SS);
};


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}