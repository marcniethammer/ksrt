#ifndef _ksrtBinarySegmentation_h
#define _ksrtBinarySegmentation_h

#include <Common/ksrtImageData.h>

#include <cmath>
#include <iostream>

class BinarySegmentation
{
public:
    BinarySegmentation();
    
    void SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 );
    void SetSpacing( float sp0, float sp1, float sp2 );
    
    void SetR( float * r );
    void SetG( float * g );
    void SetU( float * u );
    void SetPX( float * px );
    void SetPY( float * py );
    void SetPZ( float * pz );
    void SetInitialU( float * u );
    void SetTimeStep( float tau );
    void SetNumberOfIterations( unsigned int N );
    
    void SetGapThreshold( float gapThreshold );
    void SetDisplayFrequency( unsigned int dispFreq );
    
    void DoIt();
    
private:
    ImageData< float >     m_r;
    ImageData< float >     m_u;
    ImageData< float >     m_px;
    ImageData< float >     m_py;
    ImageData< float >     m_pz;
    ImageData< float >     m_g;
    
    float m_spacing[3];
    unsigned int m_size[3];
    
    unsigned int m_N;
    float m_tau;   
    
    unsigned int m_dispFreq;
    double m_gapThreshold;
    
};

#include "ksrtBinarySegmentation.cxx"

#endif//_ksrtBinarySegmentation_h