#ifndef _ksrtAnisotropicThreeLabelSegmentation_h
#define _ksrtAnisotropicThreeLabelSegmentation_h

#include <cmath>
#include <iostream>

#include <Common/ksrtImageData.h>

class AnisotropicThreeLabelSegmentation
{
public:
	/**
	 * Constructor 
	 */
	AnisotropicThreeLabelSegmentation(); 

	/**
	 * Set labeling cost for label 0.
	 */
	void SetR0( float * r0 );
	
	/**
	 * Set labeling cost for label 1.
	 */
	void SetR1( float * r1 );

	/**
	 * Set labeling cost for label 2.
	 */
	void SetR2( float * r2 );

	void SetD11( float * D11 ); 
	void SetD12( float * D12 );
	void SetD13( float * D13 );
	void SetD21( float * D21 );
	void SetD22( float * D22 );
	void SetD23( float * D23 );
	void SetD31( float * D31 );
	void SetD32( float * D32 );
	void SetD33( float * D33 );

	void SetPX1( float * px1 );
	void SetPX2( float * px2 );
	void SetPY1( float * py1 );
	void SetPY2( float * py2 );
	void SetPZ1( float * pz1 );
	void SetPZ2( float * pz2 );

	void SetQ0( float * q0 );
	void SetQ1( float * q1 );
	void SetQ2( float * q2 );
	
	void SetU1( float * u1 );
	void SetU2( float * u2 );

	/**
	 * Set the spacing. Default isotropic spacing = 1.0.
     * @param sp0 spacing in x-direction.
     * @param sp1 spacing in y-direction.
     * @param sp2 spacing in z-direction.
	 */
	void SetSpacing( float sp0, float sp1, float sp2 );
    
	/**
	 * Set the size. Default zero size.
     * @param sz0 size in the x-direction.
     * @param sz1 size in the y-direction.
     * @param sz2 size in the z-direction.
	 */
	void SetSize( unsigned int sz0, unsigned int sz1, unsigned sz2 );
    
	/** 
	 * Set the number of iterations. Default value = 500.
     * @param n number of iterations.
	 */
	void SetNumberOfIterations( unsigned int n );
    
	/**
	 * Set the time step. Default value = 0.2.
     * @param tau time step.
	 */
	void SetTimeStep( float tau );
    
	/**
	 * Set threshold of gap between primal and dual energy to stop iterations.
     * Default value = 0.01.
     * @param tau threshold of the energy gap.
	 */
	void SetGapThreshold( float gapThreshold );
	
	/**
	 * Set display frequency. Default value = 10.
     * @param dispFreq display frequency.
	 */
	void SetDisplayFrequency( unsigned int dispFreq );
	
	/**
	 * Start the iterative optimization.
	 */
	void DoIt();

private:


	ImageData< float > m_D11;
	ImageData< float > m_D12;
	ImageData< float > m_D13;
	ImageData< float > m_D21;
	ImageData< float > m_D22;
	ImageData< float > m_D23;
	ImageData< float > m_D31;
	ImageData< float > m_D32;
	ImageData< float > m_D33;

	ImageData<float> m_r0;
	ImageData<float> m_r1;
	ImageData<float> m_r2;

	ImageData<float> m_u1;
	ImageData<float> m_u2;
	
	ImageData<float> m_px1;
	ImageData<float> m_px2;

	ImageData<float> m_py1;
	ImageData<float> m_py2;

	ImageData<float> m_pz1;
	ImageData<float> m_pz2;

	ImageData<float> m_q0;
	ImageData<float> m_q1;
	ImageData<float> m_q2;

    /** 
	 * number of iterations
	 */
	unsigned int m_N; 
    
	/**
	 * time step
	 */
	float m_tau;
    
	/**
	 * Image spacing.
	 */
	float m_spacing[3];

	/**
	 * Image size.
	 */
	unsigned int m_size[3];

	/**
	 * Threshold of primal and dual energy to terminate iterations
	 */
	double m_gapThreshold;

	/**
	 * display frequency
	 */
	unsigned int m_dispFreq;
};

#include "ksrtAnisotropicThreeLabelSegmentation.cxx"
#endif //_ksrtAnisotropicThreeLabelSegmentation_h

