#ifndef _ksrtLongitudinalThreeLabelSegmentation_h
#define _ksrtLongitudinalThreeLabelSegmentation_h

#include <Common/ksrtImageData.h>

#include <cmath>
#include <iostream>
#include <vector>

class LongitudinalThreeLabelSegmentation
{
public:
    /** 
     * Constructor. Initialize the parameter to default values.
     */
    LongitudinalThreeLabelSegmentation();

	/**
	 * Set labeling cost for label 0.
	 */
	void SetR0( float * r0 );
	
	/**
	 * Set labeling cost for label 1.
	 */
	void SetR1( float * r1 );
    
	/**
	 * Set labeling cost for label 2.
	 */
	void SetR2( float * r2 );
    
    /**
     * Set spatial smoothing weight for the boundary between label 0 and 1.
     */
    void SetG1( float * g1 );
    
    /**
     * Set spatial smoothing weight for the boundary between label 1 and 2.
     */
    void SetG2( float * g2 );
    
    /**
     * Set temporal smoothing weight for the boundary between label 0 and 1.
     */
    void SetH1( float * h1 );
    
    /**
     * Set temporal smoothing weight for the boundary between label 1 and 2.
     */
    void SetH2( float * h2 );
    
    /**
     * Set the level function u
     */
    void SetU1( float * u1 );
    
    /**
     * Set the level function u.
     */
    void SetU2( float * u2 );
    
    /**
     * Set the dual variable p in the x-direction.
     */
    void SetPX1( float * px1 );
    
    /**
     * Set the dual variable p in the x-direction.
     */
    void SetPX2( float * px2 );
    
    /**
     * Set the dual variable p in the y-direction.
     */
    void SetPY1( float * py1 );
    
    /**
     * Set the dual variable p in the x-direction.
     */
    void SetPY2( float * py2 );
    
    /**
     * Set the dual variable p in the z-direction.
     */
    void SetPZ1( float * pz1 );
    
    /**
     * Set the dual variable p in the z-direction.
     */
    void SetPZ2( float * pz2 );
    
    /**
     * Set the dual variable q.
     */
    void SetQ0( float * q0 );
    
    /**
     * Set the dual variable q.
     */
    void SetQ1( float * q1 );
    
    /**
     * Set the dual variable q.
     */
    void SetQ2( float * q2 );
    
    /**
     * Set the dual variable s.
     */
    void SetS1( float * s1 );
    
    /**
     * Set the dual variable s.
     */
    void SetS2( float * s2 );
    
	/**
	 * Set spatial spacing. Default isotropic spacing = 1.0.
     * @param sp0 spacing in x-direction.
     * @param sp1 spacing in y-direction.
     * @param sp2 spacing in z-direction.
	 */
	void SetSpatialSpacing( float sp0, float sp1, float sp2 );
    
	/**
	 * Set spatial size. Default zero size.
     * @param sz0 size in the x-direction.
     * @param sz1 size in the y-direction.
     * @param sz2 size in the z-direction.
     * @param sz3 size in temporal direction.
	 */
	void SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2, unsigned int sz3 );
    
	/** 
	 * Set the number of iterations. Default value = 500.
     * @param n number of iterations.
	 */
	void SetNumberOfIterations( unsigned int n );
    
	/**
	 * Set the time step. Default value = 0.2.
     * @param tau time step.
	 */
	void SetTimeStep( float tau );
    
    /**
	 * Set time of visits.
     * @param time time of visits.
	 */
	void SetTime( float * times );
    
	/**
	 * Set threshold of gap between primal and dual energy to stop iterations.
     * Default value = 0.01.
     * @param tau threshold of the energy gap.
	 */
	void SetGapThreshold( float gapThreshold );
	
	/**
	 * Set display frequency. Default value = 10.
     * @param dispFreq display frequency.
	 */
	void SetDisplayFrequency( unsigned int dispFreq );
	
	/**
	 * Start the iterative optimization.
	 */
	void DoIt();

private:
    
    ImageData< float > m_u1;
    ImageData< float > m_u2;
    
    ImageData< float > m_r0;
    ImageData< float > m_r1;
    ImageData< float > m_r2;
    
    ImageData< float > m_px1;
    ImageData< float > m_px2;
    ImageData< float > m_py1;
    ImageData< float > m_py2;
    ImageData< float > m_pz1;
    ImageData< float > m_pz2;
    
    ImageData< float > m_q0;
    ImageData< float > m_q1;
    ImageData< float > m_q2;

    ImageData< float > m_s1;
    ImageData< float > m_s2;
    
    ImageData< float > m_g1;
    ImageData< float > m_g2;
    
    ImageData< float > m_h1;
    ImageData< float > m_h2;
    
    float * m_time;
    /**
     * Time step.
     */
    float m_tau;
    
    /** 
     * Number of iterations.
     */
    unsigned int m_N;
    
    /**
     * Image spacing.
     */
    float m_spacing[3];
    
    /**
     * Image size.
     */
    unsigned int m_size[4];
 	
    /**
	 * Threshold of primal and dual energy to terminate iterations
	 */
	double m_gapThreshold;
    
	/**
	 * display frequency
	 */
	unsigned int m_dispFreq;   
    
    
    
};

#include "ksrtLongitudinalThreeLabelSegmentation.cxx"

#endif //_ksrtLongitudinalThreeLabelSegmentation_h

