#ifndef _ksrtAnisotropicThreeLabelSegmentation_DoublePrecision_h
#define _ksrtAnisotropicThreeLabelSegmentation_DoublePrecision_h

#include <Common/ksrtImageData.h>

#include <cmath>
#include <iostream>

class AnisotropicThreeLabelSegmentation_DoublePrecision
{
public:
	/**
	 * Constructor 
	 */
	AnisotropicThreeLabelSegmentation_DoublePrecision(); 
    
	/**
	 * Set labeling cost for label 0.
	 */
	void SetR0( double * r0 );
	
	/**
	 * Set labeling cost for label 1.
	 */
	void SetR1( double * r1 );
    
	/**
	 * Set labeling cost for label 2.
	 */
	void SetR2( double * r2 );
    
	void SetD11( double * D11 ); 
	void SetD12( double * D12 );
	void SetD13( double * D13 );
	void SetD21( double * D21 );
	void SetD22( double * D22 );
	void SetD23( double * D23 );
	void SetD31( double * D31 );
	void SetD32( double * D32 );
	void SetD33( double * D33 );
    
	void SetPX1( double * px1 );
	void SetPX2( double * px2 );
	void SetPY1( double * py1 );
	void SetPY2( double * py2 );
	void SetPZ1( double * pz1 );
	void SetPZ2( double * pz2 );
    
	void SetQ0( double * q0 );
	void SetQ1( double * q1 );
	void SetQ2( double * q2 );
	
	void SetU1( double * u1 );
	void SetU2( double * u2 );
    
	/**
	 * Set the spacing. Default isotropic spacing = 1.0.
     * @param sp0 spacing in x-direction.
     * @param sp1 spacing in y-direction.
     * @param sp2 spacing in z-direction.
	 */
	void SetSpacing( double sp0, double sp1, double sp2 );
    
	/**
	 * Set the size. Default zero size.
     * @param sz0 size in the x-direction.
     * @param sz1 size in the y-direction.
     * @param sz2 size in the z-direction.
	 */
	void SetSize( unsigned int sz0, unsigned int sz1, unsigned sz2 );
    
	/** 
	 * Set the number of iterations. Default value = 500.
     * @param n number of iterations.
	 */
	void SetNumberOfIterations( unsigned int n );
    
	/**
	 * Set the time step. Default value = 0.2.
     * @param tau time step.
	 */
	void SetTimeStep( double tau );
    
	/**
	 * Set threshold of gap between primal and dual energy to stop iterations.
     * Default value = 0.01.
     * @param tau threshold of the energy gap.
	 */
	void SetGapThreshold( double gapThreshold );
	
	/**
	 * Set display frequency. Default value = 10.
     * @param dispFreq display frequency.
	 */
	void SetDisplayFrequency( unsigned int dispFreq );
	
	/**
	 * Start the iterative optimization.
	 */
	void DoIt();
    
private:
    
    
	ImageData< double > m_D11;
	ImageData< double > m_D12;
	ImageData< double > m_D13;
	ImageData< double > m_D21;
	ImageData< double > m_D22;
	ImageData< double > m_D23;
	ImageData< double > m_D31;
	ImageData< double > m_D32;
	ImageData< double > m_D33;
    
	ImageData<double> m_r0;
	ImageData<double> m_r1;
	ImageData<double> m_r2;
    
	ImageData<double> m_u1;
	ImageData<double> m_u2;
	
	ImageData<double> m_px1;
	ImageData<double> m_px2;
    
	ImageData<double> m_py1;
	ImageData<double> m_py2;
    
	ImageData<double> m_pz1;
	ImageData<double> m_pz2;
    
	ImageData<double> m_q0;
	ImageData<double> m_q1;
	ImageData<double> m_q2;
    
    /** 
	 * number of iterations
	 */
	unsigned int m_N; 
    
	/**
	 * time step
	 */
	double m_tau;
    
	/**
	 * Image spacing.
	 */
	double m_spacing[3];
    
	/**
	 * Image size.
	 */
	unsigned int m_size[3];
    
	/**
	 * Threshold of primal and dual energy to terminate iterations
	 */
	double m_gapThreshold;
    
	/**
	 * display frequency
	 */
	unsigned int m_dispFreq;
};

#include "ksrtAnisotropicThreeLabelSegmentation_DoublePrecision.cxx"
#endif //_ksrtAnisotropicThreeLabelSegmentation_DoublePrecision_h

