#ifndef _ksrtLongitudinalThreeLabelSegmentation_cxx
#define _ksrtLongitudinalThreeLabelSegmentation_cxx

#include "ksrtLongitudinalThreeLabelSegmentation.h"

LongitudinalThreeLabelSegmentation::LongitudinalThreeLabelSegmentation()
{
    m_size[0] = 0;
	m_size[1] = 0;
	m_size[2] = 0;
    
	m_spacing[0] = 1.0;
	m_spacing[1] = 1.0;
	m_spacing[2] = 1.0;
    
	m_gapThreshold = 1e-2;
	m_dispFreq = 10;
    m_tau = (float)0.2;
    m_N = 500;
    
}

void LongitudinalThreeLabelSegmentation::SetR0( float * r0 )
{
	m_r0.SetData( r0 );
}

void LongitudinalThreeLabelSegmentation::SetR1( float * r1 )
{
	m_r1.SetData( r1 );
}

void LongitudinalThreeLabelSegmentation::SetR2( float * r2 )
{
	m_r2.SetData( r2 );
}

void LongitudinalThreeLabelSegmentation::SetU1( float * u1 )
{
    m_u1.SetData( u1 );
}

void LongitudinalThreeLabelSegmentation::SetU2( float * u2 )
{
    m_u2.SetData( u2 );
}

void LongitudinalThreeLabelSegmentation::SetPX1( float * px1 )
{
    m_px1.SetData( px1);
}

void LongitudinalThreeLabelSegmentation::SetPX2( float * px2 )
{
    m_px2.SetData( px2 );
}

void LongitudinalThreeLabelSegmentation::SetPY1( float * py1 )
{
    m_py1.SetData( py1 );
}

void LongitudinalThreeLabelSegmentation::SetPY2( float * py2 )
{
    m_py2.SetData( py2 );
}

void LongitudinalThreeLabelSegmentation::SetPZ1( float * pz1 )
{
    m_pz1.SetData( pz1 );
}

void LongitudinalThreeLabelSegmentation::SetPZ2( float * pz2 )
{
    m_pz2.SetData( pz2 );
}

void LongitudinalThreeLabelSegmentation::SetQ0( float * q0 )
{
    m_q0.SetData( q0 );
}

void LongitudinalThreeLabelSegmentation::SetQ1( float * q1 )
{
    m_q1.SetData( q1 );
}

void LongitudinalThreeLabelSegmentation::SetQ2( float * q2 )
{
    m_q2.SetData( q2 );
}

void LongitudinalThreeLabelSegmentation::SetS1( float * s1 )
{
    m_s1.SetData( s1 );
}

void LongitudinalThreeLabelSegmentation::SetS2( float * s2 )
{
    m_s2.SetData( s2 );
}

void LongitudinalThreeLabelSegmentation::SetG1( float * g1 )
{
    m_g1.SetData( g1 );
}

void LongitudinalThreeLabelSegmentation::SetG2( float * g2 )
{
    m_g2.SetData( g2 );
}

void LongitudinalThreeLabelSegmentation::SetH1( float * h1 )
{
    m_h1.SetData( h1 );
}

void LongitudinalThreeLabelSegmentation::SetH2( float * h2 )
{
    m_h2.SetData( h2 );
}

void LongitudinalThreeLabelSegmentation::SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2, unsigned int sz3 )
{
	m_size[0] = sz0;
	m_size[1] = sz1;
	m_size[2] = sz2;
    m_size[3] = sz3;
    
	m_r0.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_r1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_r2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_u1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_u2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_g1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_g2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
    m_h1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_h2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_px1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_px2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_py1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_py2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_pz1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_pz2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_q0.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_q1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_q2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
    m_s1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_s2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
}

void LongitudinalThreeLabelSegmentation::SetSpatialSpacing( float sp0, float sp1, float sp2 )
{
	m_spacing[0] = sp0;
	m_spacing[1] = sp1;
	m_spacing[2] = sp2;
}

void LongitudinalThreeLabelSegmentation::SetNumberOfIterations( unsigned int n )
{
	m_N = n;
}

void LongitudinalThreeLabelSegmentation::SetTime( float * times )
{
	m_time = times;
}

void LongitudinalThreeLabelSegmentation::SetTimeStep( float tau )
{
	m_tau = tau;
}

void LongitudinalThreeLabelSegmentation::SetGapThreshold( float gapThreshold )
{
	m_gapThreshold = gapThreshold;
}

void LongitudinalThreeLabelSegmentation::SetDisplayFrequency( unsigned int dispFreq )
{
	m_dispFreq = dispFreq;
}

void LongitudinalThreeLabelSegmentation::DoIt()
{
    for (unsigned int n = 0; n < m_N; n++)
    {
        for (unsigned int t = 0; t < m_size[3]; t++)
        {
            for (unsigned int k = 0; k < m_size[2]; k++)
            {
                for (unsigned int j = 0; j < m_size[1]; j++)
                {
                    for (unsigned int i = 0; i < m_size[0]; i++)
                    {
                        //forward difference to update p and q
                        
                        float du1dx = (i < m_size[0]-1) ? m_u1(i+1,j,k,t) - m_u1(i,j,k,t) : 0;
                        du1dx /= m_spacing[0];
                        float du2dx = (i < m_size[0]-1) ? m_u2(i+1,j,k,t) - m_u2(i,j,k,t) : 0;
                        du2dx /= m_spacing[0];
                        
                        float du1dy = (j < m_size[1]-1) ? m_u1(i,j+1,k,t) - m_u1(i,j,k,t) : 0;
                        du1dy /= m_spacing[1];
                        float du2dy = (j < m_size[1]-1) ? m_u2(i,j+1,k,t) - m_u2(i,j,k,t) : 0;
                        du2dy /= m_spacing[1];
                        
                        float du1dz = (k < m_size[2]-1) ? m_u1(i,j,k+1,t) - m_u1(i,j,k,t) : 0;
                        du1dz /= m_spacing[2];
                        float du2dz = (k < m_size[2]-1) ? m_u2(i,j,k+1,t) - m_u2(i,j,k,t) : 0;
                        du2dz /= m_spacing[2];
                        
                        m_px1(i,j,k,t) -= m_tau * du1dx;
                        m_px2(i,j,k,t) -= m_tau * du2dx;
                        
                        m_py1(i,j,k,t) -= m_tau * du1dy;
                        m_py2(i,j,k,t) -= m_tau * du2dy;
                        
                        m_pz1(i,j,k,t) -= m_tau * du1dz;
                        m_pz2(i,j,k,t) -= m_tau * du2dz;
                        
                        float denomInv_p1 = (m_g1(i,j,k,t) > 0) ? 1.0 / std::max((float)1.0, (float)sqrt(m_px1(i,j,k,t)*m_px1(i,j,k,t)+m_py1(i,j,k,t)*m_py1(i,j,k,t)+m_pz1(i,j,k,t)*m_pz1(i,j,k,t))/m_g1(i,j,k,t)) : 0;
                        m_px1(i,j,k,t) *= denomInv_p1;
                        m_py1(i,j,k,t) *= denomInv_p1;
                        m_pz1(i,j,k,t) *= denomInv_p1;
                        
                        float denomInv_p2 = (m_g2(i,j,k,t) > 0) ? 1.0 / std::max((float)1.0, (float)sqrt(m_px2(i,j,k,t)*m_px2(i,j,k,t)+m_py2(i,j,k,t)*m_py2(i,j,k,t)+m_pz2(i,j,k,t)*m_pz2(i,j,k,t))/m_g2(i,j,k,t)) : 0;
                        m_px2(i,j,k,t) *= denomInv_p2;
                        m_py2(i,j,k,t) *= denomInv_p2;
                        m_pz2(i,j,k,t) *= denomInv_p2;
                        
                        float du0dl = m_u1(i,j,k,t);
                        float du1dl = m_u2(i,j,k,t) - m_u1(i,j,k,t);
                        float du2dl = 1 - m_u2(i,j,k,t);
                        
                        m_q0(i,j,k,t) -= m_tau * du0dl;
                        m_q1(i,j,k,t) -= m_tau * du1dl;
                        m_q2(i,j,k,t) -= m_tau * du2dl;
                        
                        float denomInv_q0 = 1 / std::max((float)1.0, (float)fabs(m_q0(i,j,k,t))/m_r0(i,j,k,t));
                        m_q0(i,j,k,t) *= denomInv_q0;
                        
                        float denomInv_q1 = 1 / std::max((float)1.0, (float)fabs(m_q1(i,j,k,t))/m_r1(i,j,k,t));
                        m_q1(i,j,k,t) *= denomInv_q1;
                        
                        float denomInv_q2 = 1 / std::max((float)1.0, (float)fabs(m_q2(i,j,k,t))/m_r2(i,j,k,t));
                        m_q2(i,j,k,t) *= denomInv_q2;
                        
                        float du1dt = (t < m_size[3]-1) ? (m_u1(i,j,k,t+1) - m_u1(i,j,k,t)) : 0;
                        float du2dt = (t < m_size[3]-1) ? (m_u2(i,j,k,t+1) - m_u2(i,j,k,t)) : 0;
                        
                        du1dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                        du2dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                        
                        m_s1(i,j,k,t) = m_s1(i,j,k,t) - m_tau * du1dt;
                        m_s2(i,j,k,t) = m_s2(i,j,k,t) - m_tau * du2dt;
                        
                        float denomInv_s1 = (m_h1(i,j,k,t) > 0) ? 1.0 / std::max((float)1.0, (float)fabs(m_s1(i,j,k,t))/m_h1(i,j,k,t)) : 0;
                        m_s1(i,j,k,t) *= denomInv_s1;
                        
                        float denomInv_s2 = (m_h2(i,j,k,t) > 0) ? 1.0 / std::max((float)1.0, (float)fabs(m_s2(i,j,k,t))/m_h2(i,j,k,t)) : 0;
                        m_s2(i,j,k,t) *= denomInv_s2;
                        
                    }
                }
            }
        }
              
        for (unsigned int t = 0; t < m_size[3]; t++)
        {
            for (unsigned int k = 0; k < m_size[2]; k++)
            {
                for (unsigned int j = 0; j < m_size[1]; j++)
                {
                    for (unsigned int i = 0; i < m_size[0]; i++)
                    {
                        //backward difference to update u
                        
                        float dpx1dx = ( (i < m_size[0]-1) ? m_px1(i,j,k,t) : 0 ) - ( (i > 0) ? m_px1(i-1,j,k,t) : 0 );
                        dpx1dx /= m_spacing[0];
                        float dpx2dx = ( (i < m_size[0]-1) ? m_px2(i,j,k,t) : 0 ) - ( (i > 0) ? m_px2(i-1,j,k,t) : 0 );
                        dpx2dx /= m_spacing[0];
                        
                        float dpy1dy = ( (j < m_size[1]-1) ? m_py1(i,j,k,t) : 0 ) - ( (j > 0) ? m_py1(i,j-1,k,t) : 0 );
                        dpy1dy /= m_spacing[1];
                        float dpy2dy = ( (j < m_size[1]-1) ? m_py2(i,j,k,t) : 0 ) - ( (j > 0) ? m_py2(i,j-1,k,t) : 0 );
                        dpy2dy /= m_spacing[1];
                        
                        float dpz1dz = ( (k < m_size[2]-1) ? m_pz1(i,j,k,t) : 0 ) - ( (k > 0) ? m_pz1(i,j,k-1,t) : 0 );
                        dpz1dz /= m_spacing[2];
                        float dpz2dz = ( (k < m_size[2]-1) ? m_pz2(i,j,k,t) : 0 ) - ( (k > 0) ? m_pz2(i,j,k-1,t) : 0 );
                        dpz2dz /= m_spacing[2];
                        
                        float dq1dl = m_q1(i,j,k,t) - m_q0(i,j,k,t);
                        float dq2dl = m_q2(i,j,k,t) - m_q1(i,j,k,t);
                        
                        float ds1dt = ( (t < m_size[3]-1) ? m_s1(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s1(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                        float ds2dt = ( (t < m_size[3]-1) ? m_s2(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s2(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                        

                        m_u1(i,j,k,t) -= m_tau * (dpx1dx + dpy1dy + dpz1dz + dq1dl + ds1dt);
                        m_u1(i,j,k,t) = (float)(m_u1(i,j,k,t) >= 1) * 1.0 + (float)(m_u1(i,j,k,t) <= 0) * 0.0 + (float)(m_u1(i,j,k,t) < 1 && m_u1(i,j,k,t) > 0) * m_u1(i,j,k,t);
                        
                        m_u2(i,j,k,t) -= m_tau * (dpx2dx + dpy2dy + dpz2dz + dq2dl + ds2dt);
                        m_u2(i,j,k,t) = (float)(m_u2(i,j,k,t) >= 1) * 1.0 + (float)(m_u2(i,j,k,t) <= 0) * 0.0 + (float)(m_u2(i,j,k,t) < 1 && m_u2(i,j,k,t) > 0) * m_u2(i,j,k,t);
                        
                    }
                }
            }
        }
        
        double gap = 100;
		if( n % m_dispFreq == 0 )
		{
			std::cout << "Iteration " << n << std::endl;
            
			double energy = 0;
            double dualEnergy = 0;
            for (unsigned int t = 0; t < m_size[3]; t++) 
            {
                for (unsigned int k = 0; k < m_size[2]; k++)
                {
                    for (unsigned int j = 0; j < m_size[1]; j++)
                    {
                        for (unsigned int i = 0; i < m_size[0]; i++)
                        {
                            float du1dx = (i < m_size[0]-1) ? m_u1(i+1,j,k,t) - m_u1(i,j,k,t) : 0;
                            du1dx /= m_spacing[0];
                            float du2dx = (i < m_size[0]-1) ? m_u2(i+1,j,k,t) - m_u2(i,j,k,t) : 0;
                            du2dx /= m_spacing[0];
                            
                            float du1dy = (j < m_size[1]-1) ? m_u1(i,j+1,k,t) - m_u1(i,j,k,t) : 0;
                            du1dy /= m_spacing[1];
                            float du2dy = (j < m_size[1]-1) ? m_u2(i,j+1,k,t) - m_u2(i,j,k,t) : 0;
                            du2dy /= m_spacing[1];
                            
                            float du1dz = (k < m_size[2]-1) ? m_u1(i,j,k+1,t) - m_u1(i,j,k,t) : 0;
                            du1dz /= m_spacing[2];
                            float du2dz = (k < m_size[2]-1) ? m_u2(i,j,k+1,t) - m_u2(i,j,k,t) : 0;
                            du2dz /= m_spacing[2];
                            
                            float du0dl = m_u1(i,j,k,t);
                            float du1dl = m_u2(i,j,k,t) - m_u1(i,j,k,t);
                            float du2dl = 1 - m_u2(i,j,k,t);
                            
                            float du1dt = (t < m_size[3]-1) ? (m_u1(i,j,k,t+1) - m_u1(i,j,k,t)) : 0;
                            float du2dt = (t < m_size[3]-1) ? (m_u2(i,j,k,t+1) - m_u2(i,j,k,t)) : 0;
                            
                            du1dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                            du2dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                            
                            energy += m_g1(i,j,k,t) * sqrt( du1dx * du1dx + du1dy * du1dy + du1dz * du1dz );
                            energy += m_g2(i,j,k,t) * sqrt( du2dx * du2dx + du2dy * du2dy + du2dz * du2dz );
                            energy += fabs(du0dl) * m_r0(i,j,k,t) + fabs(du1dl) * m_r1(i,j,k,t) + fabs(du2dl) * m_r2(i,j,k,t);
                            energy += m_h1(i,j,k,t) * fabs(du1dt) + m_h2(i,j,k,t) * fabs(du2dt);
                            
                            float dpx1dx = ( (i < m_size[0]-1) ? m_px1(i,j,k,t) : 0 ) - ( (i > 0) ? m_px1(i-1,j,k,t) : 0 );
                            dpx1dx /= m_spacing[0];
                            float dpx2dx = ( (i < m_size[0]-1) ? m_px2(i,j,k,t) : 0 ) - ( (i > 0) ? m_px2(i-1,j,k,t) : 0 );
                            dpx2dx /= m_spacing[0];
                            
                            float dpy1dy = ( (j < m_size[1]-1) ? m_py1(i,j,k,t) : 0 ) - ( (j > 0) ? m_py1(i,j-1,k,t) : 0 );
                            dpy1dy /= m_spacing[1];
                            float dpy2dy = ( (j < m_size[1]-1) ? m_py2(i,j,k,t) : 0 ) - ( (j > 0) ? m_py2(i,j-1,k,t) : 0 );
                            dpy2dy /= m_spacing[1];
                            
                            float dpz1dz = ( (k < m_size[2]-1) ? m_pz1(i,j,k,t) : 0 ) - ( (k > 0) ? m_pz1(i,j,k-1,t) : 0 );
                            dpz1dz /= m_spacing[2];
                            float dpz2dz = ( (k < m_size[2]-1) ? m_pz2(i,j,k,t) : 0 ) - ( (k > 0) ? m_pz2(i,j,k-1,t) : 0 );
                            dpz2dz /= m_spacing[2];
                            
                            float dq1dl = m_q1(i,j,k,t) - m_q0(i,j,k,t);
                            float dq2dl = m_q2(i,j,k,t) - m_q1(i,j,k,t);
                            float dq3dl = -m_q2(i,j,k,t);
                            
                            float ds1dt = ( (t < m_size[3]-1) ? m_s1(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s1(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                            float ds2dt = ( (t < m_size[3]-1) ? m_s2(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s2(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                            
                            float div_1 = dpx1dx + dpy1dy + dpz1dz + dq1dl + ds1dt;
                            float div_2 = dpx2dx + dpy2dy + dpz2dz + dq2dl + ds2dt;
                            dualEnergy += ((div_1 < 0) ? div_1 : 0) + ((div_2 < 0) ? div_2 : 0);  
                            dualEnergy += dq3dl;                        
                        }
                    }
                }
            }
			gap = fabs((energy - dualEnergy) / energy);		
			std::cout << "energy = " << energy << "	";
			std::cout << "dual energy = " << dualEnergy << "	";
			std::cout << "gap = " << gap << std::endl;
		}//end if
		if(gap < m_gapThreshold)
		{	
			std::cout << "Converge after "<< n << " iterations." << std::endl;
			break;
		}
    }//end for n
    
}


#endif //_ksrtLongitudinalThreeLabelSegmentation_cxx
