#ifndef _ksrtBinarySegmentation_cxx
#define _ksrtBinarySegmentation_cxx

#include "ksrtBinarySegmentation.h"

BinarySegmentation::BinarySegmentation()
{
    m_size[0] = 0;
    m_size[1] = 0;
    m_size[2] = 0;
    m_spacing[0] = 1.0;
    m_spacing[1] = 1.0;
    m_spacing[2] = 1.0;
    m_r.SetData( NULL );
    m_u.SetData( NULL );
    m_px.SetData( NULL );
    m_py.SetData( NULL );
    m_pz.SetData( NULL );
    m_N = 5000;
    m_tau = (float)0.2;
}

void BinarySegmentation::SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 )
{
    m_size[0] = sz0;
    m_size[1] = sz1;
    m_size[2] = sz2;
    m_u.SetSize( m_size[0], m_size[1], m_size[2] );
    m_r.SetSize( m_size[0], m_size[1], m_size[2] );
    m_g.SetSize( m_size[0], m_size[1], m_size[2] );
    m_px.SetSize( m_size[0], m_size[1], m_size[2] );
    m_py.SetSize( m_size[0], m_size[1], m_size[2] );
    m_pz.SetSize( m_size[0], m_size[1], m_size[2] );
}

void BinarySegmentation::SetSpacing( float sp0, float sp1, float sp2 )
{
    m_spacing[0] = sp0;
    m_spacing[1] = sp1;
    m_spacing[2] = sp2;
}

void BinarySegmentation::SetU( float * u )
{
    m_u.SetData( u );
}

void BinarySegmentation::SetG( float * g )
{
    m_g.SetData( g );
}

void BinarySegmentation::SetR( float * r )
{
    m_r.SetData( r );
}

void BinarySegmentation::SetPX( float * px )
{
    m_px.SetData( px );
}

void BinarySegmentation::SetPY( float * py )
{
    m_py.SetData( py );
}

void BinarySegmentation::SetPZ( float * pz )
{
    m_pz.SetData( pz );
}

void BinarySegmentation::SetTimeStep( float tau )
{
    m_tau = tau;
}

void BinarySegmentation::SetNumberOfIterations( unsigned int N )
{
    m_N = N;
}

void BinarySegmentation::SetGapThreshold( float gapThreshold )
{
	m_gapThreshold = gapThreshold;
}

void BinarySegmentation::SetDisplayFrequency( unsigned int dispFreq )
{
	m_dispFreq = dispFreq;
}

void BinarySegmentation::DoIt()
{
    for(unsigned int n = 0; n < m_N; n++)
	{
		for (unsigned int k = 0; k < m_size[2]; k++)
		{
			for (unsigned int j = 0; j < m_size[1]; j++)
			{
				for (unsigned int i = 0; i < m_size[0]; i++)
				{
					//forward difference to update p and q
                    
					float dudx = (i < m_size[0]-1) ? m_u(i+1,j,k) - m_u(i,j,k) : 0;
					dudx /= m_spacing[0];
                    
					float dudy = (j < m_size[1]-1) ? m_u(i,j+1,k) - m_u(i,j,k) : 0;
					dudy /= m_spacing[1];
                    
					float dudz = (k < m_size[2]-1) ? m_u(i,j,k+1) - m_u(i,j,k) : 0;
					dudz /= m_spacing[2];
                    
					m_px(i,j,k) -= m_tau * dudx;
					
					m_py(i,j,k) -= m_tau * dudy;
					
					m_pz(i,j,k) -= m_tau * dudz;
					
					float denomInv = (m_g(i,j,k) > 0) ? 1.0 / std::max((float)1.0, (float)sqrt(m_px(i,j,k)*m_px(i,j,k)+m_py(i,j,k)*m_py(i,j,k)+m_pz(i,j,k)*m_pz(i,j,k))/m_g(i,j,k)) : 0;
					m_px(i,j,k) *= denomInv;
					m_py(i,j,k) *= denomInv;
					m_pz(i,j,k) *= denomInv;
                    
				}
			}
		}
        
		for (unsigned int k = 0; k < m_size[2]; k++)
		{
			for (unsigned int j = 0; j < m_size[1]; j++)
			{
				for (unsigned int i = 0; i < m_size[0]; i++)
				{
					//backward difference to update u
                    
					float dpxdx = ( (i < m_size[0]-1) ? m_px(i,j,k) : 0 ) - ( (i > 0) ? m_px(i-1,j,k) : 0 );
					dpxdx /= m_spacing[0];
					
					float dpydy = ( (j < m_size[1]-1) ? m_py(i,j,k) : 0 ) - ( (j > 0) ? m_py(i,j-1,k) : 0 );
					dpydy /= m_spacing[1];
                    
					float dpzdz = ( (k < m_size[2]-1) ? m_pz(i,j,k) : 0 ) - ( (k > 0) ? m_pz(i,j,k-1) : 0 );
					dpzdz /= m_spacing[2];
                    
                    
					m_u(i,j,k) -= m_tau * (dpxdx + dpydy + dpzdz + m_r(i,j,k));
					m_u(i,j,k) = (float)(m_u(i,j,k) >= 1) * 1.0 + (float)(m_u(i,j,k) <= 0) * 0.0 + (float)(m_u(i,j,k) < 1 && m_u(i,j,k) > 0) * m_u(i,j,k);
                    
				}
			}
		}
        
        
		double gap = 100;
		if( n % m_dispFreq == 0 )
		{
			std::cout << "Iteration " << n << std::endl;
             
			double energy = 0, dualEnergy = 0;
			for (unsigned int k = 0; k < m_size[2]; k++)
			{
				for (unsigned int j = 0; j < m_size[1]; j++)
				{
					for (unsigned int i = 0; i < m_size[0]; i++)
					{
						float dudx = (i < m_size[0]-1) ? m_u(i+1,j,k) - m_u(i,j,k) : 0;
						dudx /= m_spacing[0];
                        
						float dudy = (j < m_size[1]-1) ? m_u(i,j+1,k) - m_u(i,j,k) : 0;
						dudy /= m_spacing[1];
                        
						float dudz = (k < m_size[2]-1) ? m_u(i,j,k+1) - m_u(i,j,k) : 0;
						dudz /= m_spacing[2];
                        
						energy += m_g(i,j,k) * sqrt( dudx * dudx + dudy * dudy + dudz * dudz );
						energy += m_u(i,j,k) * m_r(i,j,k);
                        
						float dpxdx = ( (i < m_size[0]-1) ? m_px(i,j,k) : 0 ) - ( (i > 0) ? m_px(i-1,j,k) : 0 );
						dpxdx /= m_spacing[0];
						
						float dpydy = ( (j < m_size[1]-1) ? m_py(i,j,k) : 0 ) - ( (j > 0) ? m_py(i,j-1,k) : 0 );
						dpydy /= m_spacing[1];
                        
						float dpzdz = ( (k < m_size[2]-1) ? m_pz(i,j,k) : 0 ) - ( (k > 0) ? m_pz(i,j,k-1) : 0 );
						dpzdz /= m_spacing[2];
                        
						float div = dpxdx + dpydy + dpzdz;
						dualEnergy += (div + m_r(i,j,k) < 0) ? div + m_r(i,j,k) : 0;  
					}
				}
			}
			gap = fabs((energy-dualEnergy)/energy);		
			std::cout << "energy = " << energy << "	";
			std::cout << "dual energy = " << dualEnergy << "	";
			std::cout << "gap = " << gap << std::endl;
		}//end if
		if(gap < m_gapThreshold)
		{	
			std::cout << "Converge after " << n << " iterations." << std::endl;
			break;
		}
	}//end for n
}
#endif//_ksrtBinarySegmentation_cxx
