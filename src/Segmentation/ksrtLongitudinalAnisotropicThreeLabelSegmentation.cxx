#ifndef _ksrtLongitudinalAnisotropicThreeLabelSegmentation_cxx
#define _ksrtLongitudinalAnisotropicThreeLabelSegmentation_cxx

#include "ksrtLongitudinalAnisotropicThreeLabelSegmentation.h"

LongitudinalAnisotropicThreeLabelSegmentation::LongitudinalAnisotropicThreeLabelSegmentation()
{
    m_size[0] = 0;
	m_size[1] = 0;
	m_size[2] = 0;
    
	m_spacing[0] = 1.0;
	m_spacing[1] = 1.0;
	m_spacing[2] = 1.0;
    
	m_gapThreshold = 1e-2;
	m_dispFreq = 10;
    m_tau = (float)0.2;
    m_N = 500;
    
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetR0( float * r0 )
{
	m_r0.SetData( r0 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetR1( float * r1 )
{
	m_r1.SetData( r1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetR2( float * r2 )
{
	m_r2.SetData( r2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetU1( float * u1 )
{
    m_u1.SetData( u1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetU2( float * u2 )
{
    m_u2.SetData( u2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetPX1( float * px1 )
{
    m_px1.SetData( px1);
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetPX2( float * px2 )
{
    m_px2.SetData( px2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetPY1( float * py1 )
{
    m_py1.SetData( py1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetPY2( float * py2 )
{
    m_py2.SetData( py2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetPZ1( float * pz1 )
{
    m_pz1.SetData( pz1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetPZ2( float * pz2 )
{
    m_pz2.SetData( pz2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetQ0( float * q0 )
{
    m_q0.SetData( q0 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetQ1( float * q1 )
{
    m_q1.SetData( q1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetQ2( float * q2 )
{
    m_q2.SetData( q2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetS1( float * s1 )
{
    m_s1.SetData( s1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetS2( float * s2 )
{
    m_s2.SetData( s2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetH1( float * h1 )
{
    m_h1.SetData( h1 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetH2( float * h2 )
{
    m_h2.SetData( h2 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD11( float * D11 )
{
	m_D11.SetData( D11 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD12( float * D12 )
{
	m_D12.SetData( D12 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD13( float * D13 )
{
	m_D13.SetData( D13 );
} 

void LongitudinalAnisotropicThreeLabelSegmentation::SetD21( float * D21 )
{
	m_D21.SetData( D21 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD22( float * D22 )
{
	m_D22.SetData( D22 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD23( float * D23 )
{
	m_D23.SetData( D23 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD31( float * D31 )
{
	m_D31.SetData( D31 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD32( float * D32 )
{
	m_D32.SetData( D32 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetD33( float * D33 )
{
	m_D33.SetData( D33 );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2, unsigned int sz3 )
{
	m_size[0] = sz0;
	m_size[1] = sz1;
	m_size[2] = sz2;
    m_size[3] = sz3;
    
	m_r0.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_r1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_r2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_u1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_u2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_g1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_g2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
    m_h1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_h2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_px1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_px2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_py1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_py2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_pz1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_pz2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_q0.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_q1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_q2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
    m_s1.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_s2.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
    
	m_D11.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D12.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D13.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D21.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D22.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D23.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D31.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D32.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
	m_D33.SetSize( m_size[0], m_size[1], m_size[2], m_size[3] );
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetSpatialSpacing( float sp0, float sp1, float sp2 )
{
	m_spacing[0] = sp0;
	m_spacing[1] = sp1;
	m_spacing[2] = sp2;
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetNumberOfIterations( unsigned int n )
{
	m_N = n;
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetTime( float * times )
{
	m_time = times;
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetTimeStep( float tau )
{
	m_tau = tau;
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetGapThreshold( float gapThreshold )
{
	m_gapThreshold = gapThreshold;
}

void LongitudinalAnisotropicThreeLabelSegmentation::SetDisplayFrequency( unsigned int dispFreq )
{
	m_dispFreq = dispFreq;
}

void LongitudinalAnisotropicThreeLabelSegmentation::DoIt()
{
	float * vx1_arr = new float[m_size[3]*m_size[2]*m_size[1]*m_size[0]];
	float * vx2_arr = new float[m_size[3]*m_size[2]*m_size[1]*m_size[0]];
	float * vy1_arr = new float[m_size[3]*m_size[2]*m_size[1]*m_size[0]];
	float * vy2_arr = new float[m_size[3]*m_size[2]*m_size[1]*m_size[0]];
	float * vz1_arr = new float[m_size[3]*m_size[2]*m_size[1]*m_size[0]];
	float * vz2_arr = new float[m_size[3]*m_size[2]*m_size[1]*m_size[0]];

	ImageData<float> vx1(m_size[0], m_size[1], m_size[2], m_size[3]);
	ImageData<float> vx2(m_size[0], m_size[1], m_size[2], m_size[3]);
	ImageData<float> vy1(m_size[0], m_size[1], m_size[2], m_size[3]);
	ImageData<float> vy2(m_size[0], m_size[1], m_size[2], m_size[3]);
	ImageData<float> vz1(m_size[0], m_size[1], m_size[2], m_size[3]);
	ImageData<float> vz2(m_size[0], m_size[1], m_size[2], m_size[3]);
	
	vx1.SetData( vx1_arr );
	vx2.SetData( vx2_arr );
	vy1.SetData( vy1_arr );
	vy2.SetData( vy2_arr );
	vz1.SetData( vz1_arr );
	vz2.SetData( vz2_arr );
	
    for (unsigned int n = 0; n < m_N; n++)
    {
        for (unsigned int t = 0; t < m_size[3]; t++)
        {
            for (unsigned int k = 0; k < m_size[2]; k++)
            {
                for (unsigned int j = 0; j < m_size[1]; j++)
                {
                    for (unsigned int i = 0; i < m_size[0]; i++)
                    {
                        //forward difference to update p and q
                        
                        float du1dx = (i < m_size[0]-1) ? m_u1(i+1,j,k,t) - m_u1(i,j,k,t) : 0;
                        du1dx /= m_spacing[0];
                        float du2dx = (i < m_size[0]-1) ? m_u2(i+1,j,k,t) - m_u2(i,j,k,t) : 0;
                        du2dx /= m_spacing[0];
                        
                        float du1dy = (j < m_size[1]-1) ? m_u1(i,j+1,k,t) - m_u1(i,j,k,t) : 0;
                        du1dy /= m_spacing[1];
                        float du2dy = (j < m_size[1]-1) ? m_u2(i,j+1,k,t) - m_u2(i,j,k,t) : 0;
                        du2dy /= m_spacing[1];
                        
                        float du1dz = (k < m_size[2]-1) ? m_u1(i,j,k+1,t) - m_u1(i,j,k,t) : 0;
                        du1dz /= m_spacing[2];
                        float du2dz = (k < m_size[2]-1) ? m_u2(i,j,k+1,t) - m_u2(i,j,k,t) : 0;
                        du2dz /= m_spacing[2];
                        
						m_px1(i,j,k,t) -= m_tau * ( m_D11(i,j,k,t)*du1dx + m_D12(i,j,k,t)*du1dy + m_D13(i,j,k,t)*du1dz );
						m_px2(i,j,k,t) -= m_tau * ( m_D11(i,j,k,t)*du2dx + m_D12(i,j,k,t)*du2dy + m_D13(i,j,k,t)*du2dz );

						m_py1(i,j,k,t) -= m_tau * ( m_D21(i,j,k,t)*du1dx + m_D22(i,j,k,t)*du1dy + m_D23(i,j,k,t)*du1dz );
						m_py2(i,j,k,t) -= m_tau * ( m_D21(i,j,k,t)*du2dx + m_D22(i,j,k,t)*du2dy + m_D23(i,j,k,t)*du2dz );

						m_pz1(i,j,k,t) -= m_tau * ( m_D31(i,j,k,t)*du1dx + m_D32(i,j,k,t)*du1dy + m_D33(i,j,k,t)*du1dz );
						m_pz2(i,j,k,t) -= m_tau * ( m_D31(i,j,k,t)*du2dx + m_D32(i,j,k,t)*du2dy + m_D33(i,j,k,t)*du2dz );
                        
						float denomInv_p1 = (float)1.0 / std::max( (float)1.0, (float)sqrt( m_px1(i,j,k,t)*m_px1(i,j,k,t) + m_py1(i,j,k,t)*m_py1(i,j,k,t) + m_pz1(i,j,k,t)*m_pz1(i,j,k,t) ) );
						m_px1(i,j,k,t) *= denomInv_p1;
						m_py1(i,j,k,t) *= denomInv_p1;
						m_pz1(i,j,k,t) *= denomInv_p1;
                        
						float denomInv_p2 = (float)1.0 / std::max( (float)1.0, (float)sqrt( m_px2(i,j,k,t)*m_px2(i,j,k,t) + m_py2(i,j,k,t)*m_py2(i,j,k,t) + m_pz2(i,j,k,t)*m_pz2(i,j,k,t) ) );			
						m_px2(i,j,k,t) *= denomInv_p2;
						m_py2(i,j,k,t) *= denomInv_p2;
						m_pz2(i,j,k,t) *= denomInv_p2;
                        
                        float du0dl = m_u1(i,j,k,t);
                        float du1dl = m_u2(i,j,k,t) - m_u1(i,j,k,t);
                        float du2dl = 1 - m_u2(i,j,k,t);
                        
                        m_q0(i,j,k,t) -= m_tau * du0dl;
                        m_q1(i,j,k,t) -= m_tau * du1dl;
                        m_q2(i,j,k,t) -= m_tau * du2dl;
                        
                        float denomInv_q0 = 1 / std::max((float)1.0, (float)fabs(m_q0(i,j,k,t))/m_r0(i,j,k,t));
                        m_q0(i,j,k,t) *= denomInv_q0;
                        
                        float denomInv_q1 = 1 / std::max((float)1.0, (float)fabs(m_q1(i,j,k,t))/m_r1(i,j,k,t));
                        m_q1(i,j,k,t) *= denomInv_q1;
                        
                        float denomInv_q2 = 1 / std::max((float)1.0, (float)fabs(m_q2(i,j,k,t))/m_r2(i,j,k,t));
                        m_q2(i,j,k,t) *= denomInv_q2;
                        
                        float du1dt = (t < m_size[3]-1) ? (m_u1(i,j,k,t+1) - m_u1(i,j,k,t)) : 0;
                        float du2dt = (t < m_size[3]-1) ? (m_u2(i,j,k,t+1) - m_u2(i,j,k,t)) : 0;
                        
                        du1dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                        du2dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                        
                        m_s1(i,j,k,t) = m_s1(i,j,k,t) - m_tau * du1dt;
                        m_s2(i,j,k,t) = m_s2(i,j,k,t) - m_tau * du2dt;
                        
                        float denomInv_s1 = (m_h1(i,j,k,t) > 0) ? 1.0 / std::max((float)1.0, (float)fabs(m_s1(i,j,k,t))/m_h1(i,j,k,t)) : 0;
                        m_s1(i,j,k,t) *= denomInv_s1;
                        
                        float denomInv_s2 = (m_h2(i,j,k,t) > 0) ? 1.0 / std::max((float)1.0, (float)fabs(m_s2(i,j,k,t))/m_h2(i,j,k,t)) : 0;
                        m_s2(i,j,k,t) *= denomInv_s2;

                    }
                }
            }
        }
              
        for (unsigned int t = 0; t < m_size[3]; t++)
        {
            for (unsigned int k = 0; k < m_size[2]; k++)
            {
                for (unsigned int j = 0; j < m_size[1]; j++)
                {
                    for (unsigned int i = 0; i < m_size[0]; i++)
                    {
                        //backward difference to update u
						vx1(i,j,k,t) = m_D11(i,j,k,t)*m_px1(i,j,k,t) + m_D12(i,j,k,t)*m_py1(i,j,k,t) + m_D13(i,j,k,t)*m_pz1(i,j,k,t);
						vx2(i,j,k,t) = m_D11(i,j,k,t)*m_px2(i,j,k,t) + m_D12(i,j,k,t)*m_py2(i,j,k,t) + m_D13(i,j,k,t)*m_pz2(i,j,k,t);
						vy1(i,j,k,t) = m_D21(i,j,k,t)*m_px1(i,j,k,t) + m_D22(i,j,k,t)*m_py1(i,j,k,t) + m_D23(i,j,k,t)*m_pz1(i,j,k,t);
						vy2(i,j,k,t) = m_D21(i,j,k,t)*m_px2(i,j,k,t) + m_D22(i,j,k,t)*m_py2(i,j,k,t) + m_D23(i,j,k,t)*m_pz2(i,j,k,t);
						vz1(i,j,k,t) = m_D31(i,j,k,t)*m_px1(i,j,k,t) + m_D32(i,j,k,t)*m_py1(i,j,k,t) + m_D33(i,j,k,t)*m_pz1(i,j,k,t);
						vz2(i,j,k,t) = m_D31(i,j,k,t)*m_px2(i,j,k,t) + m_D32(i,j,k,t)*m_py2(i,j,k,t) + m_D33(i,j,k,t)*m_pz2(i,j,k,t);
						
						float dvx1dx = ( (i < m_size[0]-1) ? vx1(i,j,k,t) : 0 ) - ( (i > 0) ? vx1(i-1,j,k,t) : 0 );
						dvx1dx /= m_spacing[0];
						float dvx2dx = ( (i < m_size[0]-1) ? vx2(i,j,k,t) : 0 ) - ( (i > 0) ? vx2(i-1,j,k,t) : 0 );
						dvx2dx /= m_spacing[0];
					
						float dvy1dy = ( (j < m_size[1]-1) ? vy1(i,j,k,t) : 0 ) - ( (j > 0) ? vy1(i,j-1,k,t) : 0 );
						dvy1dy /= m_spacing[1];
						float dvy2dy = ( (j < m_size[1]-1) ? vy2(i,j,k,t) : 0 ) - ( (j > 0) ? vy2(i,j-1,k,t) : 0 );
						dvy2dy /= m_spacing[1];

						float dvz1dz = ( (k < m_size[2]-1) ? vz1(i,j,k,t) : 0 ) - ( (k > 0) ? vz1(i,j,k-1,t) : 0 );
						dvz1dz /= m_spacing[2];
						float dvz2dz = ( (k < m_size[2]-1) ? vz2(i,j,k,t) : 0 ) - ( (k > 0) ? vz2(i,j,k-1,t) : 0 );
						dvz2dz /= m_spacing[2];
                        
                        float dq1dl = m_q1(i,j,k,t) - m_q0(i,j,k,t);
                        float dq2dl = m_q2(i,j,k,t) - m_q1(i,j,k,t);
                        
                        float ds1dt = ( (t < m_size[3]-1) ? m_s1(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s1(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                        float ds2dt = ( (t < m_size[3]-1) ? m_s2(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s2(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                        
                        m_u1(i,j,k,t) -= m_tau * (dvx1dx + dvy1dy + dvz1dz + dq1dl + ds1dt);
                        m_u1(i,j,k,t) = (float)(m_u1(i,j,k,t) >= 1) * 1.0 + (float)(m_u1(i,j,k,t) <= 0) * 0.0 + (float)(m_u1(i,j,k,t) < 1 && m_u1(i,j,k,t) > 0) * m_u1(i,j,k,t);
                        
                        m_u2(i,j,k,t) -= m_tau * (dvx2dx + dvy2dy + dvz2dz + dq2dl + ds2dt);
                        m_u2(i,j,k,t) = (float)(m_u2(i,j,k,t) >= 1) * 1.0 + (float)(m_u2(i,j,k,t) <= 0) * 0.0 + (float)(m_u2(i,j,k,t) < 1 && m_u2(i,j,k,t) > 0) * m_u2(i,j,k,t);

                    }
                }
            }
        }
        
        double gap = 100;
		if( n % m_dispFreq == 0 )
		{
			std::cout << "Iteration " << n << std::endl;
            
			double energy = 0;
            double dualEnergy = 0;
            for (unsigned int t = 0; t < m_size[3]; t++) 
            {
                for (unsigned int k = 0; k < m_size[2]; k++)
                {
                    for (unsigned int j = 0; j < m_size[1]; j++)
                    {
                        for (unsigned int i = 0; i < m_size[0]; i++)
                        {
                            float du1dx = (i < m_size[0]-1) ? m_u1(i+1,j,k,t) - m_u1(i,j,k,t) : 0;
                            du1dx /= m_spacing[0];
                            float du2dx = (i < m_size[0]-1) ? m_u2(i+1,j,k,t) - m_u2(i,j,k,t) : 0;
                            du2dx /= m_spacing[0];
                            
                            float du1dy = (j < m_size[1]-1) ? m_u1(i,j+1,k,t) - m_u1(i,j,k,t) : 0;
                            du1dy /= m_spacing[1];
                            float du2dy = (j < m_size[1]-1) ? m_u2(i,j+1,k,t) - m_u2(i,j,k,t) : 0;
                            du2dy /= m_spacing[1];
                            
                            float du1dz = (k < m_size[2]-1) ? m_u1(i,j,k+1,t) - m_u1(i,j,k,t) : 0;
                            du1dz /= m_spacing[2];
                            float du2dz = (k < m_size[2]-1) ? m_u2(i,j,k+1,t) - m_u2(i,j,k,t) : 0;
                            du2dz /= m_spacing[2];
							
                            float du0dl = m_u1(i,j,k,t);
                            float du1dl = m_u2(i,j,k,t) - m_u1(i,j,k,t);
                            float du2dl = 1 - m_u2(i,j,k,t);
                            
                            float du1dt = (t < m_size[3]-1) ? (m_u1(i,j,k,t+1) - m_u1(i,j,k,t)) : 0;
                            float du2dt = (t < m_size[3]-1) ? (m_u2(i,j,k,t+1) - m_u2(i,j,k,t)) : 0;
                            
                            du1dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                            du2dt /= (t < m_size[3]-1) ? (m_time[t+1] - m_time[t]) : 1;
                            
							energy += pow( pow( (float)( m_D11(i,j,k,t)*du1dx + m_D12(i,j,k,t)*du1dy + m_D13(i,j,k,t)*du1dz ) , (float)2.0 ) + pow( (float)( m_D21(i,j,k,t)*du1dx + m_D22(i,j,k,t)*du1dy + m_D23(i,j,k,t)*du1dz ), (float)2.0 ) + pow( (float)( m_D31(i,j,k,t)*du1dx + m_D32(i,j,k,t)*du1dy + m_D33(i,j,k,t)*du1dz ), (float)2.0 ), (float)0.5 );
							energy += pow( pow( (float)( m_D11(i,j,k,t)*du2dx + m_D12(i,j,k,t)*du2dy + m_D13(i,j,k,t)*du2dz ) , (float)2.0 ) + pow( (float)( m_D21(i,j,k,t)*du2dx + m_D22(i,j,k,t)*du2dy + m_D23(i,j,k,t)*du2dz ), (float)2.0 ) + pow( (float)( m_D31(i,j,k,t)*du2dx + m_D32(i,j,k,t)*du2dy + m_D33(i,j,k,t)*du2dz ), (float)2.0 ), (float)0.5 );
							
                            energy += fabs(du0dl) * m_r0(i,j,k,t) + fabs(du1dl) * m_r1(i,j,k,t) + fabs(du2dl) * m_r2(i,j,k,t);
                            energy += m_h1(i,j,k,t) * fabs(du1dt) + m_h2(i,j,k,t) * fabs(du2dt);
                            
							float dvx1dx = ( (i < m_size[0]-1) ? vx1(i,j,k,t) : 0 ) - ( (i > 0) ? vx1(i-1,j,k,t) : 0 );
							dvx1dx /= m_spacing[0];
							float dvx2dx = ( (i < m_size[0]-1) ? vx2(i,j,k,t) : 0 ) - ( (i > 0) ? vx2(i-1,j,k,t) : 0 );
							dvx2dx /= m_spacing[0];
						
							float dvy1dy = ( (j < m_size[1]-1) ? vy1(i,j,k,t) : 0 ) - ( (j > 0) ? vy1(i,j-1,k,t) : 0 );
							dvy1dy /= m_spacing[1];
							float dvy2dy = ( (j < m_size[1]-1) ? vy2(i,j,k,t) : 0 ) - ( (j > 0) ? vy2(i,j-1,k,t) : 0 );
							dvy2dy /= m_spacing[1];

							float dvz1dz = ( (k < m_size[2]-1) ? vz1(i,j,k,t) : 0 ) - ( (k > 0) ? vz1(i,j,k-1,t) : 0 );
							dvz1dz /= m_spacing[2];
							float dvz2dz = ( (k < m_size[2]-1) ? vz2(i,j,k,t) : 0 ) - ( (k > 0) ? vz2(i,j,k-1,t) : 0 );
							dvz2dz /= m_spacing[2];
							
                            float dq1dl = m_q1(i,j,k,t) - m_q0(i,j,k,t);
                            float dq2dl = m_q2(i,j,k,t) - m_q1(i,j,k,t);
                            float dq3dl = -m_q2(i,j,k,t);
                            
                            float ds1dt = ( (t < m_size[3]-1) ? m_s1(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s1(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                            float ds2dt = ( (t < m_size[3]-1) ? m_s2(i,j,k,t)/(m_time[t+1]-m_time[t]) : 0 ) - ( (t > 0) ? m_s2(i,j,k,t-1)/(m_time[t]-m_time[t-1]) : 0 );
                            
                            float div_1 = dvx1dx + dvy1dy + dvz1dz + dq1dl + ds1dt;
                            float div_2 = dvx2dx + dvy2dy + dvz2dz + dq2dl + ds2dt;
                            dualEnergy += ((div_1 < 0) ? div_1 : 0) + ((div_2 < 0) ? div_2 : 0);  
                            dualEnergy += dq3dl;     

                        }
                    }
                }
            }
			gap = fabs((energy - dualEnergy) / energy);		
			std::cout << "energy = " << energy << "	";
			std::cout << "dual energy = " << dualEnergy << "	";
			std::cout << "gap = " << gap << std::endl;
		}//end if
		if(gap < m_gapThreshold)
		{	
			std::cout << "Converge after "<< n << " iterations." << std::endl;
			break;
		}
    }//end for n
    
	delete [] vx1_arr;
	delete [] vx2_arr;
	delete [] vy1_arr;
	delete [] vy2_arr;
	delete [] vz1_arr;
	delete [] vz2_arr;
}


#endif //_ksrtLongitudinalAnisotropicThreeLabelSegmentation_cxx
