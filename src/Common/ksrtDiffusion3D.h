#ifndef _ksrtDiffusion3D_h
#define _ksrtDiffusion3D_h

#include "ksrtImageData.h"

/** 
 * This class implements 3-dimensional diffusion smoothing. 
 */

class Diffusion3D
{
public:

	/**
 	 * Constructor.
	 */
	Diffusion3D();

	/**
 	 * Set image data to be operated on.
	 */
	void SetImageData( float * data );
	
    /**
     * Set image size.
     */
    void SetSize( unsigned int sz0, unsigned int sz1, unsigned sz2 );
    
	/**
	 * Set image spacing.
	 */
	void SetSpacing( float sp0, float sp1, float sp2 );

	/**
	 * Set number of iterations.
	 */
	void SetN( unsigned int N );

	/**
	 * Set display frequency.
	 */
	void SetDisplayFrequency( unsigned int dispFreq );

	/**
	 * Set time step.
	 */
	void SetTimeStep( float tau );
	
	/**
	 * Set fixed region.
	 */
	void SetFixedRegion( short * fixed );

	/**
	 * Start iterations.
	 */
	void DoIt();
	
private:
	
	/**
	 * 3-dimensional image data.
	 */
	ImageData< float > m_data;
	
	/**
	 * Number of iterations.
	 */
	unsigned int m_N;

	/**
	 * Display frequency.
	 */
	unsigned int m_dispFreq;

	/**
	 * Time step.
	 */
	float m_tau;
	
	/**
	 * Image spacing.
	 */
	float m_spacing[3];

	/**
	 * Image size.
	 */
	unsigned int m_size[3];
	
	/**
	 * Fixed region.
	 */
	ImageData< short > m_fixed;

};

#include "ksrtDiffusion3D.cxx"

#endif //_ksrtDiffusion3D_h
