#ifndef _ksrtWriteImage_h
#define _ksrtWriteImage_h

#include <itkImage.h>
#include <itkImageFileWriter.h>

/**
 * A class to write an image to disk
 */

template< class TImage >
class WriteImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileWriter< TImage >     TWriter;
    typedef typename TWriter::Pointer                   TWriterPointer;
    
public:
	/**
	 * Write an image to disk
	 * @param fileName the location to write the image to.
	 * @param im the image to write.
	 */
    static int DoIt( const char * fileName, TImagePointer im, bool compression );
};

#include "ksrtWriteImage.cxx"

#endif //_ksrtWriteImage_h


