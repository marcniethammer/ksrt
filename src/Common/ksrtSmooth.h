#ifndef _ksrtSmooth_h
#define _ksrtSmooth_h

#include <itkImage.h>
#include <itkCurvatureFlowImageFilter.h>

template< class TImage >
class Smooth
{
public:
    typedef typename TImage::Pointer							TImagePointer;
    typedef itk::CurvatureFlowImageFilter< TImage, TImage >		TCurvatureFlow;
    typedef typename TCurvatureFlow::Pointer					TCurvatureFlowPointer;

    static TImagePointer DoIt( TImagePointer im, double tau, unsigned int N );
    
};

#include "ksrtSmooth.cxx"

#endif //_ksrtSmooth_h
