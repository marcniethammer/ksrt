#ifndef _ksrtLaplaceThickness_h
#define _ksrtLaplaceThickness_h

#include "ksrtImageData.h"
#include <iostream>
#include <cmath>

class LaplaceDistance
{
public:
    LaplaceDistance();
    void SetData( float * data );
    void SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 );
	void SetSpacing( float sp0, float sp1, float sp2 );
    void SetSolution( float * solution );
    void DoIt();
    void SetTimeStep( float tau );
private:
    
    ImageData< float > m_data;
    ImageData< float > m_solution;
    unsigned int m_size[3];
	float m_spacing[3];
    float m_tau;
};

#include "ksrtLaplaceThickness.cxx"

#endif //_ksrtLaplaceThickness_h
