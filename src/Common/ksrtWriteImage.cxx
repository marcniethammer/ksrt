#ifndef _ksrtWriteImage_cxx
#define _ksrtWriteImage_cxx

#include "ksrtWriteImage.h"
#include <iostream>

template< class TImage >
int WriteImage< TImage >::DoIt( const char * fileName, TImagePointer im, bool compression )
{
    TWriterPointer writer = TWriter::New();
    writer->SetInput( im );
    writer->SetFileName( fileName );
    writer->SetUseCompression( compression );
    
    try{
        writer->Update();
        std::cout << "Writing " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        exit( 1 );
    }
    return 0;
}

#endif //_ksrtWriteImage_cxx