#ifndef _ksrtWriteNrrdImage_h
#define _ksrtWriteNrrdImage_h

#include <itkImage.h>
#include <itkNrrdImageIO.h>
#include <itkImageFileWriter.h>

/**
 * A class to write an nrrd image to disk
 */

template< class TImage >
class WriteNrrdImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileWriter< TImage >     TWriter;
    typedef typename TWriter::Pointer                   TWriterPointer;
    typedef typename itk::NrrdImageIO                   TNrrdIO;
    typedef typename TNrrdIO::Pointer                   TNrrdIOPointer;
    
public:
	/**
	 * Write an image to disk
	 * @param fileName the location to write the image to.
	 * @param im the image to write.
	 */
    static int DoIt( const char * fileName, TImagePointer im );
};


#include "ksrtWriteNrrdImage.cxx"

#endif //_ksrtWriteNrrdImage_h


