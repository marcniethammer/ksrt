#ifndef _ksrtWriteMetaImage_cxx
#define _ksrtWriteMetaImage_cxx

#include "ksrtWriteMetaImage.h"
#include <iostream>

template< class TImage >
int WriteMetaImage< TImage >::DoIt( const char * fileName, TImagePointer im )
{
    TWriterPointer writer = TWriter::New();
    TMetaIOPointer MetaIO = TMetaIO::New();
    writer->SetImageIO( MetaIO );
    writer->SetInput( im );
    writer->SetFileName( fileName );
    try{
        writer->Update();
        std::cout << "Writing " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        exit( 1 );
    }
    return 0;
}

#endif //_ksrtWriteMetaImage_cxx