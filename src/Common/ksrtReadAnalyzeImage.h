#ifndef _ksrtReadAnalyzeImage_h
#define _ksrtReadAnalyzeImage_h

#include <itkImage.h>
#include <itkAnalyzeImageIO.h>
#include <itkImageFileReader.h>

/**
 * A class to read an analyze image from disk
 */
template< class TImage >
class ReadAnalyzeImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileReader< TImage >     TReader;
    typedef typename TReader::Pointer                   TReaderPointer;
    typedef typename itk::AnalyzeImageIO                TAnalyzeIO;
    typedef typename TAnalyzeIO::Pointer                TAnalyzeIOPointer;
    
public:
	/**
	 * Read an analyze image from disk.
	 * @param fileName the image to be read.
	 * @return an image pointer.
	 */
    static TImagePointer DoIt( const char * fileName );
};

#include "ksrtReadAnalyzeImage.cxx"

#endif //_ksrtReadAnalyzeImage_h
