#ifndef _ksrtLaplaceThickness_cxx
#define _ksrtLaplaceThickness_cxx

#include "ksrtLaplaceThickness.h"

LaplaceDistance::LaplaceDistance()
{
	m_size[0] = 0;
	m_size[1] = 0;
	m_size[2] = 0;
	m_tau = 0.0;
}

void LaplaceDistance::SetData( float * data )
{
    m_data.SetData( data );
}

void LaplaceDistance::SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 )
{
    m_size[0] = sz0;
    m_size[1] = sz1;
    m_size[2] = sz2;
    
    m_data.SetSize( m_size[0], m_size[1], m_size[2] );
    m_solution.SetSize( m_size[0], m_size[1], m_size[2] );
}

void LaplaceDistance::SetSpacing( float sp0, float sp1, float sp2 )
{
	m_spacing[0] = sp0;
	m_spacing[1] = sp1;
	m_spacing[2] = sp2;
}

void LaplaceDistance::SetSolution( float * solution )
{
    m_solution.SetData( solution );
}

void LaplaceDistance::SetTimeStep( float tau )
{
    m_tau = tau;;
}

void LaplaceDistance::DoIt()
{
    float lambda = 0.0;
    unsigned int iter = 0;

    ImageData< float > data2;
    data2.SetData( new float[ m_size[0]*m_size[1]*m_size[2] ] );
    data2.SetSize( m_size[0], m_size[1], m_size[2] );
    
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                data2(i,j,k) = m_data(i,j,k);
            }
        }
    }
    
    unsigned int numSolutionVoxels = 0;
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                numSolutionVoxels = ( m_solution(i,j,k) > 0 ) ? numSolutionVoxels + 1 : numSolutionVoxels;
            }
        }
    }
    
    while (true) {
        for (unsigned int k = 0; k < m_size[2]; k++)
        {
            for (unsigned int j = 0; j < m_size[1]; j++)
            {
                for (unsigned int i = 0; i < m_size[0]; i++)
                {
//					float sum_data = m_data(std::min(m_size[0]-1,i+1),j,k) + m_data(i,std::min(m_size[1]-1,j+1),k) + m_data(i,j,std::min(m_size[2]-1,k+1));
//
//					float sum_data2 = data2(std::max(1u, i)-1,j,k) + data2(i,std::max(1u, j)-1,k) + data2(i,j,std::max(1u, k)-1);
//					float delta = ( sum_data + sum_data2 ) / 6 - m_data(i,j,k);
//					data2(i,j,k) = (m_solution(i,j,k) > 0) ? m_data(i,j,k) + ( 1+ lambda ) * delta : m_data(i,j,k);
                    float tmpX = ( m_data(std::min(m_size[0]-1,i+1),j,k) + data2(std::max(1u,i)-1,j,k) ) / pow(m_spacing[0], 2.0f);
                    float tmpY = ( m_data(i,std::min(m_size[1]-1,j+1),k) + data2(i,std::max(1u,j)-1,k) ) / pow(m_spacing[1], 2.0f);
                    float tmpZ = ( m_data(i,j,std::min(m_size[2]-1,k+1)) + data2(i,j,std::max(1u,k)-1) ) / pow(m_spacing[2], 2.0f);
                    
                    float delta = (tmpX + tmpY + tmpZ) / (pow(m_spacing[0], -2.0f) + pow(m_spacing[1], -2.0f) + pow(m_spacing[2], -2.0f)) / 2.0f - m_data(i,j,k);
                    
                    data2(i,j,k) = (m_solution(i,j,k) > 0) ? m_data(i,j,k) + ( 1+ lambda ) * delta : m_data(i,j,k);
                }
            }
        }
        double error = 0.0;
        for (unsigned int k = 0; k < m_size[2]; k++)
        {
            for (unsigned int j = 0; j < m_size[1]; j++)
            {
                for (unsigned int i = 0; i < m_size[0]; i++)
                {
                    error += fabs(data2(i,j,k) - m_data(i,j,k));
                }
            }
        }
        for (unsigned int k = 0; k < m_size[2]; k++)
        {
            for (unsigned int j = 0; j < m_size[1]; j++)
            {
                for (unsigned int i = 0; i < m_size[0]; i++)
                {
                    m_data(i,j,k) = data2(i,j,k);
                }
            }
        }
        iter = iter + 1;
        if (error/numSolutionVoxels < 1e-4 )
        {
			std::cout << "Solving Laplace equation takes " << iter << " iterations to converge." << std::endl;         
			break;
        }
    }
    delete data2.GetData();
    
    ImageData< float > d1;
    d1.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    d1.SetSize( m_size[0], m_size[1], m_size[2] );
    
    ImageData< float > d2;
    d2.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    d2.SetSize( m_size[0], m_size[1], m_size[2] );
    
    ImageData< float > d1new;
    d1new.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    d1new.SetSize( m_size[0], m_size[1], m_size[2] );
    
    ImageData< float > d2new;
    d2new.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    d2new.SetSize( m_size[0], m_size[1], m_size[2] );
    
    ImageData< float > gradX;
    gradX.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    gradX.SetSize( m_size[0], m_size[1], m_size[2] );

    ImageData< float > gradY;
    gradY.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    gradY.SetSize( m_size[0], m_size[1], m_size[2] );
    
    ImageData< float > gradZ;
    gradZ.SetData( new float [ m_size[0]*m_size[1]*m_size[2] ] );
    gradZ.SetSize( m_size[0], m_size[1], m_size[2] );
    
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                d1(i,j,k) = 0;
                d2(i,j,k) = 0;
                d1new(i,j,k) = 0;
                d2new(i,j,k) = 0;
                gradX(i,j,k) = 0;
                gradY(i,j,k) = 0;
                gradZ(i,j,k) = 0;
            }
        }
    }
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 1; i < m_size[0]-1; i++)
            {
                gradX(i,j,k) = m_data(i+1,j,k) - m_data(i-1,j,k);
                gradX(i,j,k) /= 2 * m_spacing[0];
            }
        }
    }
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            gradX(0,j,k) = m_data(1,j,k) - m_data(0,j,k);
            gradX(0,j,k) /= m_spacing[0];
            gradX(m_size[0]-1,j,k) = m_data(m_size[0]-1,j,k) - m_data(m_size[0]-2,j,k);
            gradX(m_size[0]-1,j,k) /= m_spacing[0];
        }
    }
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 1; j < m_size[1]-1; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                gradY(i,j,k) = m_data(i,j+1,k) - m_data(i,j-1,k);
                gradY(i,j,k) /= 2 * m_spacing[1];
            }
        }
    }
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int i = 0; i < m_size[0]; i++)
        {
            gradY(i,0,k) = m_data(i,1,k) - m_data(i,0,k);
            gradY(i,0,k) /= m_spacing[1];
            gradY(i,m_size[1]-1,k) = m_data(i,m_size[1]-1,k) - m_data(i,m_size[1]-2,k);
            gradY(i,m_size[1]-1,k) /= m_spacing[1];
        }
    }
          
    for (unsigned int k = 1; k < m_size[2]-1; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                gradZ(i,j,k) = m_data(i,j,k+1) - m_data(i,j,k-1);
                gradZ(i,j,k) /= 2 * m_spacing[2];

            }
        }
    }
                           
    for (unsigned int j = 0; j < m_size[1]; j++)
    {
        for (unsigned int i = 0; i < m_size[0]; i++)
        {
            gradZ(i,j,0) = m_data(i,j,1) - m_data(i,j,0);
            gradZ(i,j,0) /= m_spacing[2];
            gradZ(i,j,m_size[2]-1) = m_data(i,j,m_size[2]-1) - m_data(i,j,m_size[2]-2);
            gradZ(i,j,m_size[2]-1) /= m_spacing[2];
        }
    }
    
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                float mag = pow( gradX(i,j,k), (float)2.0 ) + pow( gradY(i,j,k), (float)2.0 ) + pow( gradZ(i,j,k), (float)2.0);
                mag = pow( mag, (float)0.5 );
                gradX(i,j,k) = ( mag > 0 ) ? gradX(i,j,k) / mag : gradX(i,j,k);
                gradY(i,j,k) = ( mag > 0 ) ? gradY(i,j,k) / mag : gradY(i,j,k);
                gradZ(i,j,k) = ( mag > 0 ) ? gradZ(i,j,k) / mag : gradZ(i,j,k);
                
            }
        }
    }
    

    iter = 0;
    
    while (true) {
        for (unsigned int k = 0; k < m_size[2]; k++)
        {
            for (unsigned int j = 0; j < m_size[1]; j++)
            {
                for (unsigned int i = 0; i < m_size[0]; i++)
                {
                    float d1dxb = (i > 0) ? d1(i,j,k) - d1(i-1,j,k) : 0;
                    float d1dxf = (i < m_size[0]-1) ? d1(i+1,j,k) - d1(i,j,k) : 0;
                    float d1dx = (float)(gradX(i,j,k) > 0) * d1dxb + (float)(gradX(i,j,k) < 0) * d1dxf;
                    d1dx /= m_spacing[0];
                    
                    float d1dyb = (j > 0) ? d1(i,j,k) - d1(i,j-1,k) : 0;
                    float d1dyf = (j < m_size[1]-1) ? d1(i,j+1,k) - d1(i,j,k) : 0;
                    float d1dy = (float)(gradY(i,j,k) > 0) * d1dyb + (float)(gradY(i,j,k) < 0) * d1dyf;
                    d1dy /= m_spacing[1];
                    
                    float d1dzb = (k > 0) ? d1(i,j,k) - d1(i,j,k-1) : 0;
                    float d1dzf = (k < m_size[2]-1) ? d1(i,j,k+1) - d1(i,j,k) : 0;
                    float d1dz = (float)(gradZ(i,j,k) > 0) * d1dzb + (float)(gradZ(i,j,k) < 0) * d1dzf;
                    d1dz /= m_spacing[2];
                    
                    float d2dxb = (i > 0) ? d2(i,j,k) - d2(i-1,j,k) : 0;
                    float d2dxf = (i < m_size[0]-1) ? d2(i+1,j,k) - d2(i,j,k) : 0;
                    float d2dx = (float)(gradX(i,j,k) > 0) * d2dxf + (float)(gradX(i,j,k) < 0) * d2dxb;
                    d2dx /= m_spacing[0];
                    
                    float d2dyb = (j > 0) ? d2(i,j,k) - d2(i,j-1,k) : 0;
                    float d2dyf = (j < m_size[1]-1) ? d2(i,j+1,k) - d2(i,j,k) : 0;
                    float d2dy = (float)(gradY(i,j,k) > 0) * d2dyf + (float)(gradY(i,j,k) < 0) * d2dyb;
                    d2dy /= m_spacing[1];
                    
                    float d2dzb = (k > 0) ? d2(i,j,k) - d2(i,j,k-1) : 0;
                    float d2dzf = (k < m_size[2]-1) ? d2(i,j,k+1) - d2(i,j,k) : 0;
                    float d2dz = (float)(gradZ(i,j,k) > 0) * d2dzf + (float)(gradZ(i,j,k) < 0) * d2dzb;
                    d2dz /= m_spacing[2];
                    
                    d1new(i,j,k) = d1(i,j,k) + m_tau * ( 1 - gradX(i,j,k)*d1dx - gradY(i,j,k)*d1dy - gradZ(i,j,k)*d1dz );
                    d1new(i,j,k) = (m_solution(i,j,k) > 0) ? d1new(i,j,k) : 0;
                    d2new(i,j,k) = d2(i,j,k) + m_tau * ( 1 + gradX(i,j,k)*d2dx + gradY(i,j,k)*d2dy + gradZ(i,j,k)*d2dz );
                    d2new(i,j,k) = (m_solution(i,j,k) > 0) ? d2new(i,j,k) : 0;
                }
            }
        }
        
		if (iter % 10 == 0)
		{
			double err = 0.0;
			for (unsigned int k = 0; k < m_size[2]; k++)
			{
				for (unsigned int j = 0; j < m_size[1]; j++)
				{
					for (unsigned int i = 0; i < m_size[0]; i++)
					{
						err += fabs(d1new(i,j,k)-d1(i,j,k));
						err += fabs(d2new(i,j,k)-d2(i,j,k));
					}
				}
			}
			std::cout << "Iter = " << iter << ", difference = " << err << std::endl;
			if (err/numSolutionVoxels < 2e-4)
			{
				std::cout << "Solving transport equation takes " << iter << " iterations to converge." << std::endl;
				break;
			}
			if (iter > 1e+4)
			{
				std::cout << iter << " iterations." << std::endl;
				break;
			}
		}

		for (unsigned int k = 0; k < m_size[2]; k++)
        {
            for (unsigned int j = 0; j < m_size[1]; j++)
            {
                for (unsigned int i = 0; i < m_size[0]; i++)
                {
                    d1(i,j,k) = d1new(i,j,k);
                    d2(i,j,k) = d2new(i,j,k);
                }
            }
        }
        iter = iter + 1;
    }
    
    for (unsigned int k = 0; k < m_size[2]; k++)
    {
        for (unsigned int j = 0; j < m_size[1]; j++)
        {
            for (unsigned int i = 0; i < m_size[0]; i++)
            {
                m_solution(i,j,k) = d1(i,j,k) + d2(i,j,k);
            }
        }
    }
	
	delete gradX.GetData();
    delete gradY.GetData();
    delete gradZ.GetData();

    delete d1.GetData();
    delete d2.GetData();
    delete d1new.GetData();
    delete d2new.GetData();
}
#endif //_ksrtLaplaceThickness_cxx

