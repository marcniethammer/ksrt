#ifndef _ksrtReadAnalyzeImage_cxx
#define _ksrtReadAnalyzeImage_cxx

#include "ksrtReadAnalyzeImage.h"
#include <iostream>

template< class TImage >
typename ReadAnalyzeImage< TImage >::TImagePointer
ReadAnalyzeImage< TImage >::DoIt( const char * fileName )
{
    TReaderPointer reader = TReader::New();
    TAnalyzeIOPointer analyzeIO = TAnalyzeIO::New();
    reader->SetImageIO( analyzeIO );
    reader->SetFileName( fileName );
    try{
        reader->Update();
        std::cout << "Reading " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        return NULL;
    }
    return reader->GetOutput();
}

#endif //_ksrtReadAnalyzeImage_cxx
