#ifndef _ksrtGaussianConvolution_txx
#define _ksrtGaussianConvolution_txx

#include "ksrtGaussianConvolution.h"

template< class TImage >
typename GaussianConvolution< TImage >::TImagePointer
GaussianConvolution< TImage >::DoIt( TImagePointer im, unsigned int orderX, unsigned int orderY, unsigned int orderZ, float sigmaX, float sigmaY, float sigmaZ )
{
	TGaussianFilterPointer filterX = TGaussianFilter::New();
	TGaussianFilterPointer filterY = TGaussianFilter::New();
	TGaussianFilterPointer filterZ = TGaussianFilter::New();
	
	filterX->SetDirection(0);
	filterY->SetDirection(1);
	filterZ->SetDirection(2);
	
	switch( orderX ){
		case 0:
		{
			filterX->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::ZeroOrder );
			break;
		}
		case 1:
		{
			filterX->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::FirstOrder );
			break;
		}
		case 2:
		{
			filterX->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::SecondOrder );
			break;
		}
		default:
		std::cout << "only support ZeroOrder, FirstOrder and SecondOrder." << std::endl;
	}
	
	switch( orderY ){
		case 0:
		{
			filterY->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::ZeroOrder );
			break;
		}
		case 1:
		{
			filterY->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::FirstOrder );
			break;
		}
		case 2:
		{
			filterY->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::SecondOrder );
			break;
		}
		default:
		std::cout << "only support ZeroOrder, FirstOrder and SecondOrder." << std::endl;
	}
	
	switch( orderZ ){
		case 0:
		{
			filterZ->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::ZeroOrder );
			break;
		}
		case 1:
		{
			filterZ->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::FirstOrder );
			break;
		}
		case 2:
		{
			filterZ->SetOrder( itk::RecursiveGaussianImageFilter< TImage, TImage >::SecondOrder );
			break;
		}
		default:
		std::cout << "only support ZeroOrder, FirstOrder and SecondOrder." << std::endl;
	}
	
	filterX->SetNormalizeAcrossScale( false );
	filterY->SetNormalizeAcrossScale( false );
	filterZ->SetNormalizeAcrossScale( false );
	
	filterX->SetInput( im );
	filterY->SetInput( filterX->GetOutput() );
	filterZ->SetInput( filterY->GetOutput() );
	
	filterX->SetSigma( sigmaX );
	filterY->SetSigma( sigmaY );
	filterZ->SetSigma( sigmaZ );
	
	filterZ->Update();
	
	return filterZ->GetOutput();
	return NULL;
}
#endif //_ksrtGaussianConvolution_txx
