#ifndef _ksrtGaussianConvolution_h
#define _ksrtGaussianConvolution_h

#include "itkImage.h"
#include "itkRecursiveGaussianImageFilter.h"

/** 
 * A class to do Gaussian convolution.
 */

template< class TImage >
class GaussianConvolution
{
	typedef typename TImage::Pointer									TImagePointer;
	typedef itk::RecursiveGaussianImageFilter< TImage, TImage >			TGaussianFilter;
	typedef typename TGaussianFilter::Pointer 							TGaussianFilterPointer;

public:
	/**
	 * Do the Gaussian convolution
	 * @param im input image.
	 * @param orderX the order of Gaussian in x-direction, choose from 0 (Gaussian itself), 1 (first derivative of Gaussian) and 2 (second derivative of Gaussian).
	 * @param orderY the order of Gaussian in y-direction, choose from 0, 1 and 2.
	 * @param orderZ the order of Gaussian in z-direction, choose from 0, 1 and 2.
	 * @param sigmaX the sigma of Gaussian in x-direction
	 * @param sigmaY the sigma of Gaussian in y-direction
	 * @param sigmaZ the sigma of Gaussian in z-direction
	 * @return result image
	 */
	static TImagePointer DoIt(TImagePointer im, unsigned int orderX, unsigned int orderY, unsigned int orderZ, float sigmaX, float sigmaY, float sigmaZ);
	
};

#include "ksrtGaussianConvolution.cxx"

#endif //_ksrtGaussianConvolution_h
