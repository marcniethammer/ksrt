#ifndef _ksrtNew3DImage_cxx
#define _ksrtNew3DImage_cxx

#include <iostream>
#include "ksrtNew3DImage.h"

template< class TImage >
typename New3DImage< TImage >::TImagePointer
New3DImage< TImage >::DoIt( TSize sz, TSpacing sp, TDirection dir, TIndex start, TPoint origin, TPixel value )
{
    TImagePointer im = TImage::New();
    TRegion region;
    region.SetSize( sz );
    region.SetIndex( start );
    im->SetRegions( region );
    im->SetSpacing( sp );
    im->SetDirection( dir );
    im->SetOrigin( origin );
    im->Allocate();
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                TIndex idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                im->SetPixel( idx, value );
            }
        }
    }
    return im;
}


#endif //_ksrtNew3DImage_cxx
