
#ifndef _ksrtNew3DImage_h
#define _ksrtNew3DImage_h

template< class TImage >
class New3DImage
{
public:
    typedef typename TImage::PixelType      TPixel;
    typedef typename TImage::Pointer        TImagePointer;
    typedef typename TImage::SizeType       TSize;
    typedef typename TImage::SpacingType    TSpacing;
    typedef typename TImage::DirectionType  TDirection;
    typedef typename TImage::IndexType      TIndex;
    typedef typename TImage::PointType      TPoint;
    typedef typename TImage::RegionType     TRegion;
    
    static TImagePointer DoIt( TSize sz, TSpacing sp, TDirection dir, TIndex start, TPoint origin, TPixel value );
};

#include "ksrtNew3DImage.cxx"

#endif //_ksrtNew3DImage_h
