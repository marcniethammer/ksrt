#ifndef _ksrtResample_cxx
#define _ksrtResample_cxx

#include "ksrtResample.h"

#include <iostream>

template< class TImage >
Resample< TImage >::Resample()
{
    m_in = NULL;
    m_out = NULL;
    
    m_sz[0] = 0;
    m_sz[1] = 0;
    m_sz[2] = 0;
    
    m_sp[0] = 0;
    m_sp[1] = 0;
    m_sp[2] = 0;
    
    m_newSz[0] = 0;
    m_newSz[1] = 0;
    m_newSz[2] = 0;
    
    m_newSp[0] = 0;
    m_newSp[1] = 0;
    m_newSp[2] = 0;
    
    m_origin[0] = 0;
    m_origin[1] = 0;
    m_origin[2] = 0;
    
    m_method = Linear;
    
}

template< class TImage >
void Resample< TImage >::SetInput( typename Resample<TImage>::TImagePointer input )
{
    m_in = input;
    m_sz = m_in->GetRequestedRegion().GetSize();
    m_sp = m_in->GetSpacing();
    m_dir = m_in->GetDirection();
    m_origin = m_in->GetOrigin();
}

template< class TImage >
void Resample< TImage >::SetNewSize( TSize newSz )
{
    m_newSz[0] = newSz[0];
    m_newSz[1] = newSz[1];
    m_newSz[2] = newSz[2];
    
    m_newSp[0] = m_sp[0] * m_sz[0] / m_newSz[0];
    m_newSp[1] = m_sp[1] * m_sz[1] / m_newSz[1];
    m_newSp[2] = m_sp[2] * m_sz[2] / m_newSz[2];
}

template< class TImage >
void Resample< TImage >::SetInterpolateMethod( InterpolateMethod method )
{
    m_method = method;
}

template< class TImage >
typename Resample< TImage >::TImagePointer 
Resample< TImage >::GetOutput()
{
    if (m_newSz[0] == 0 || m_newSz[1] == 0 || m_newSz[2] == 0)
    {
        std::cerr << "Wrong output size" << std::endl;
        exit( 1 );
    }
 	TIndex start;
	start[0] = 0;
	start[1] = 0;
	start[2] = 0;
    
	TImagePointer m_out = New3DImage< TImage >::DoIt(m_newSz, m_newSp, m_dir, start, m_origin, 0);
    
    TTransformPointer transform = TTransform::New();
    transform->SetIdentity();
    
    TResamplerPointer resampler = TResampler::New();
    resampler->SetTransform( transform );
    resampler->SetInput( m_in );
	
    resampler->SetSize( m_newSz );
    resampler->SetOutputOrigin( m_origin );
    resampler->SetOutputSpacing( m_newSp );
    resampler->SetOutputDirection( m_dir );
    resampler->SetDefaultPixelValue( 0 );
    resampler->Update();
    m_out = resampler->GetOutput();
    
    switch (m_method) {
        case Linear:
        {
            TLinearInterpolatorPointer interpolator = TLinearInterpolator::New();
            resampler->SetInterpolator( interpolator );
            resampler->Update();
            m_out = resampler->GetOutput();
            break;
        }  
        case NearestNeighbor:
        {
            TNearestNeighborInterpolatorPointer interpolator = TNearestNeighborInterpolator::New();
            resampler->SetInterpolator( interpolator );
            resampler->Update();
            m_out = resampler->GetOutput();
            break;
        }
        default:
		{
			std::cerr << "Choose an interpolation method please.\n";
            break;
		}
    };
	
    return m_out;
}



#endif
