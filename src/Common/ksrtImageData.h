#ifndef _ksrtImageData_h
#define _ksrtImageData_h

#include <stdlib.h>

/**
 * A class to manipulate image data (up to 5 dimensions).
 */

template< class T >
class ImageData
{
	unsigned int m_n1;
	unsigned int m_n2;
	unsigned int m_n3;
	unsigned int m_n4;
    unsigned int m_n5;
	unsigned int m_dim;
	T * m_data;

public:

	/**
	 * Constructor.
	 * @param n1 size of the image in 1st dimension.
	 * @param n2 size of the image in 2nd dimension.
	 * @param n3 size of the image in 3rd dimension.
	 * @param n4 size of the image in 4th dimension.
     * @param n5 size of the image in 5th dimension.
	 */
	ImageData< T >( unsigned int n1 = 1, unsigned int n2 = 1, unsigned int n3 = 1, unsigned int n4 = 1, unsigned int n5 = 1 );

	/**
	 * Destructor.
	 */
	~ImageData< T >()
	{
		m_data = NULL;
	}

	/**
	 * Set the size of the image.
	 * @param n1 size of the image in 1st dimension.
	 * @param n2 size of the image in 2nd dimension.
	 * @param n3 size of the image in 3rd dimension.
	 * @param n4 size of the image in 4th dimension.
     * @param n5 size of the image in 5th dimension.
	 */
	void SetSize( unsigned int n1 = 1, unsigned int n2 = 1, unsigned int n3 = 1, unsigned int n4 = 1, unsigned int n5 = 1 );

	/**
	 * Set the pointer to the image data.
	 * @param data the pointer to the image data.
	 */
	void SetData( T * data );

	/**
	 * Get the dimension of the image data.
	 * @return the dimension of the image data.
	 */
	unsigned int   GetDimension();

	/**
	 * Gets the size of the image data.
	 * @return the size of the image data.
	 */
	unsigned int * GetSize();

	/**
	 * Get the pointer to the image data.
	 * @return the pointer to the image data.
	 */
	T * GetData();

	/**
	 * Get the pixel data from 1-dimensional image.
	 * @param i index
	 * @return data at pixel (i).
	 */
	inline T & operator()(unsigned int i);

	/**
	 * Get the pixel data from 2-dimensional image.
	 * @param i index in 1st dimension.
	 * @param j index in 2nd dimension.
	 * @return data at pixel (i,j).
	 */
	inline T & operator()(unsigned int i, unsigned int j);

	/**
	 * Get the pixel data from 3-dimensional image.
	 * @param i index in 1st dimension.
	 * @param j index in 2nd dimension.
	 * @param k index in 3rd dimension.
	 * @return data at pixel (i,j,k).
	 */
	inline T & operator()(unsigned int i, unsigned int j, unsigned int k);

	/**
	 * Get the pixel data from 4-dimensional image.
	 * @param i index in 1st dimension.
	 * @param j index in 2nd dimension.
	 * @param k index in 3rd dimension.
	 * @param l index in 4th dimension.
	 * @return data at pixel (i,j,k,l).
	 */
	inline T & operator()(unsigned int i, unsigned int j, unsigned int k, unsigned int l);

    /**
	 * Get the pixel data from 4-dimensional image.
	 * @param i index in 1st dimension.
	 * @param j index in 2nd dimension.
	 * @param k index in 3rd dimension.
	 * @param l index in 4th dimension.
     * @param p index in 5th dimension.
	 * @return data at pixel (i,j,k,l,p).
	 */
	inline T & operator()(unsigned int i, unsigned int j, unsigned int k, unsigned int l, unsigned int p);
    
	/**
	 * Add an image data to it.
	 * @param tmp the image data to be added.
	 */
	inline void operator += ( ImageData tmp );

	/**
	 * Subtract an image data from it.
	 * @param tmp the image data to be subtracted.
	 */
	inline void operator -= ( ImageData tmp );

	/**
	 * Multiply an image data to it.
	 * @param tmp the image data to be multiplied by.
	 */
	inline void operator *= ( ImageData tmp );

	/**
	 * Divide it by an image data.
	 * @param tmp the divisor image data.
	 */
	inline void operator /= ( ImageData tmp );
	
	/**
	 * Add a constant number to it.
	 * @param tmp the constant number to be added.
	 */
	inline void operator += ( T tmp );

	/**
	 * Subtract a constant number from it.
	 * @param tmp the constant number to be subtracted.
	 */
	inline void operator -= ( T tmp );

	/**
	 * Multiply a constant number to it.
	 * @param tmp the constant number to be multiplied by.
	 */
	inline void operator *= ( T tmp );

 	/**
 	 * Divide by a constant number.
 	 * @param tmp the constant divosor.
 	 */
	inline void operator /= ( T tmp );

};

#include "ksrtImageData.cxx"	

#endif //_ksrtImageData_h