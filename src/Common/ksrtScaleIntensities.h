#ifndef _ksrtScaleIntensities_h
#define _ksrtScaleIntensities_h

#include <itkImage.h>
#include "ksrtNew3DImage.h"

template< class TImage >
class ScaleIntensities
{
public:
	typedef typename TImage::PixelType							TPixel;
    typedef typename TImage::Pointer							TImagePointer;
	typedef typename TImage::IndexType							TIndex;
	typedef typename TImage::SizeType							TSize;
	typedef typename TImage::SpacingType						TSpacing;
	typedef typename TImage::PointType							TPoint;
	typedef typename TImage::DirectionType						TDirection;

	static void ThrowOutliers( TImagePointer im, double threshold );
	static void DoIt( TImagePointer im, TPixel newMaxValue );

};

#include "ksrtScaleIntensities.cxx"

#endif //_ksrtScaleIntensities_h
