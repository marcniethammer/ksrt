#ifndef _ksrtScaleIntensities_cxx
#define _ksrtScaleIntensities_cxx

#include "ksrtScaleIntensities.h"
#include <iostream>
#include <itkImageRegionIteratorWithIndex.h>

template< class TImage >
void ScaleIntensities< TImage >::ThrowOutliers( TImagePointer im, double cutThreshold )
{
	itk::ImageRegionIteratorWithIndex< TImage > iter( im, im->GetRequestedRegion() );
	TPixel maxValue = 0;
	TPixel minValue = 1000;
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		if( iter.Get() < 0 )
			iter.Set( 0 );
		if( iter.Get() > maxValue )
			maxValue = iter.Get();		
		if( iter.Get() < minValue )
			minValue = iter.Get();
	} 
	TPixel cutValue = minValue;
	double ratio = 0.0;
	unsigned int numOutliers = 0;
	TSize sz = im->GetRequestedRegion().GetSize();
	unsigned int numTotal = sz[0]*sz[1]*sz[2];
	do
	{
		numOutliers = 0;
		cutValue = (cutValue + maxValue)/2;
		for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
			numOutliers = (iter.Get() > cutValue) ? numOutliers+1 : numOutliers;
		ratio = (double)numOutliers / (double)numTotal;
	}
	while( ratio > cutThreshold );
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		TPixel tmp_value = (iter.Get() > cutValue) ? cutValue : iter.Get();
		iter.Set( tmp_value );
	}
}

template< class TImage >
void ScaleIntensities< TImage >::DoIt( TImagePointer im, TPixel newMaxValue )
{
	itk::ImageRegionIteratorWithIndex< TImage > iter( im, im->GetRequestedRegion() );
	TPixel maxValue = 0;
	TPixel minValue = 1000;
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		if( iter.Get() < 0 )
			iter.Set( 0 );
		if( iter.Get() > maxValue )
			maxValue = iter.Get();		
		if( iter.Get() < minValue )
			minValue = iter.Get();
	} 
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		TPixel oldValue = iter.Get();
		double newValue = (double)oldValue / (double)maxValue * (double)newMaxValue;
		iter.Set( (TPixel)newValue );
	}
}
#endif //_ksrtScaleIntensities_cxx
