#ifndef _ksrtReadMetaImage_cxx
#define _ksrtReadMetaImage_cxx

#include "ksrtReadMetaImage.h"
#include <iostream>

template< class TImage >
typename ReadMetaImage< TImage >::TImagePointer
ReadMetaImage< TImage >::DoIt( const char * fileName )
{
    TReaderPointer reader = TReader::New();
    TMetaIOPointer MetaIO = TMetaIO::New();
    reader->SetImageIO( MetaIO );
    reader->SetFileName( fileName );
    try{
        reader->Update();
        std::cout << "Reading " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        return NULL;
    }
    return reader->GetOutput();
}

#endif //_ksrtReadMetaImage_cxx