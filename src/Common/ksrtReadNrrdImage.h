#ifndef _ksrtReadNrrdImage_h
#define _ksrtReadNrrdImage_h

#include <itkImage.h>
#include <itkNrrdImageIO.h>
#include <itkImageFileReader.h>

/**
 * A class to read an nrrd image from disk
 */
template< class TImage >
class ReadNrrdImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileReader< TImage >     TReader;
    typedef typename TReader::Pointer                   TReaderPointer;
    typedef typename itk::NrrdImageIO                   TNrrdIO;
    typedef typename TNrrdIO::Pointer                   TNrrdIOPointer;
    
public:
	/**
	 * Read an nrrd image from disk.
	 * @param fileName the image to be read.
	 * @return an image pointer.
	 */
    static TImagePointer DoIt( const char * fileName );
};

#include "ksrtReadNrrdImage.cxx"

#endif //_ksrtReadNrrdImage_h
