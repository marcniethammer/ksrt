#ifndef _ksrtSmooth_cxx
#define _ksrtSmooth_cxx

#include "ksrtSmooth.h"
#include <iostream>

template< class TImage >
typename Smooth< TImage >::TImagePointer 
Smooth< TImage >::DoIt( TImagePointer im, double tau, unsigned int N )
{
    TCurvatureFlowPointer curvFlow = TCurvatureFlow::New();
    curvFlow->SetNumberOfIterations( N );
    curvFlow->SetTimeStep( tau );
    curvFlow->SetInput( im );
    try{
        curvFlow->Update();
    }
    catch( itk::ExceptionObject & err ){
        std::cerr << "ExceptionObject caught!" << std::endl;
        std::cerr << err << std::endl;
        exit( 1 );	
    }
    return curvFlow->GetOutput();
}
#endif //_ksrtSmooth_cxx
