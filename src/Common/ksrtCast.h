#ifndef _ksrtCast_h
#define _ksrtCast_h

#include <itkImage.h>
#include <itkCastImageFilter.h>

/** 
 * A class to do ITK image type casting. 
 */

template< class TInputImage, class TOutputImage>
class Cast
{
    typedef typename TInputImage::Pointer                                   TInputImagePointer;
    typedef typename TOutputImage::Pointer                                  TOutputImagePointer;
    
    typedef typename itk::CastImageFilter< TInputImage, TOutputImage >      TCastFilter;
    typedef typename TCastFilter::Pointer                                   TCastFilterPointer;
    
public:
	/**
	 * Do the type casting.
	 * @param input the input image to be converted to a new ITK type.
	 * @return an image with desired ITK type.
	 */
    static TOutputImagePointer DoIt( TInputImagePointer input );
    
};

#include "ksrtCast.cxx"

#endif


