#ifndef _ksrtWriteMetaImage_h
#define _ksrtWriteMetaImage_h

#include <itkImage.h>
#include <itkMetaImageIO.h>
#include <itkImageFileWriter.h>

/**
 * A class to write an image to disk
 */
template< class TImage >
class WriteMetaImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileWriter< TImage >     TWriter;
    typedef typename TWriter::Pointer                   TWriterPointer;
    typedef typename itk::MetaImageIO                   TMetaIO;
    typedef typename TMetaIO::Pointer                   TMetaIOPointer;
    
public:
	/**
	 * Write the image to disk
	 * @param fileName the location to write the image to.
	 * @param the image to write.
	 */
    static int DoIt( const char * fileName, TImagePointer im );
};

#include "ksrtWriteMetaImage.cxx"

#endif //_ksrtWriteMetaImage_h
