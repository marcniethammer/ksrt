#ifndef _ksrtDiffusion3D_cxx
#define _ksrtDiffusion3D_cxx

#include "ksrtDiffusion3D.h"
#include <iostream>

Diffusion3D::Diffusion3D()
{
    m_size[0] = 0;
    m_size[1] = 0;
    m_size[2] = 0;
	m_data.SetData( NULL );
	m_data.SetSize( m_size[0], m_size[1], m_size[2] );
	m_spacing[0] = 1.0;
	m_spacing[1] = 1.0;
	m_spacing[2] = 1.0;
	m_N = 0;
	m_tau = 0.0;
	m_dispFreq = 10;
	m_fixed.SetData( NULL );
	m_fixed.SetSize( m_size[0], m_size[1], m_size[2] );
}

void Diffusion3D::SetImageData( float * data )
{
	m_data.SetData( data );
}

void Diffusion3D::SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 )
{
    m_size[0] = sz0;
    m_size[1] = sz1;
    m_size[2] = sz2;
    m_data.SetSize( m_size[0], m_size[1], m_size[2] );
    m_fixed.SetSize( m_size[0], m_size[1], m_size[2] );
}

void Diffusion3D::SetDisplayFrequency( unsigned int dispFreq )
{
	m_dispFreq = dispFreq;
}

void Diffusion3D::SetSpacing( float sp0, float sp1, float sp2 )
{
	m_spacing[0] = sp0;
	m_spacing[1] = sp1;
	m_spacing[2] = sp2;
}

void Diffusion3D::SetN( unsigned int N )
{
	m_N = N;
}

void Diffusion3D::SetTimeStep( float tau )
{
	m_tau = tau;
}

void Diffusion3D::SetFixedRegion( short * fixed )
{
	m_fixed.SetData( fixed );
}
void Diffusion3D::DoIt()
{

	if (m_fixed.GetData() == NULL)
	{
		for (unsigned int n = 0; n < m_N; n++)
		{
			if ( n % m_dispFreq == 0)
				std::cout << "Iteration " << n << std::endl;

			for (unsigned int k = 0; k < m_size[2]; k++)
			{
				for (unsigned int j = 0; j < m_size[1]; j++)
				{
					for (unsigned int i = 0; i < m_size[0]; i++)
					{
						float mx = (i == 0)           ? m_data(0,j,k)           : m_data(i-1,j,k);
						float px = (i == m_size[0]-1) ? m_data(m_size[0]-1,j,k) : m_data(i+1,j,k);
						float my = (j == 0)           ? m_data(i,0,k)           : m_data(i,j-1,k);
						float py = (j == m_size[1]-1) ? m_data(i,m_size[1]-1,k) : m_data(i,j+1,k);
						float mz = (k == 0)           ? m_data(i,j,0)           : m_data(i,j,k-1);
						float pz = (k == m_size[2]-1) ? m_data(i,j,m_size[2]-1) : m_data(i,j,k+1);
				
						float Dxx = mx - 2*m_data(i,j,k) + px;
						Dxx /= m_spacing[0] * m_spacing[0];
						float Dyy = my - 2*m_data(i,j,k) + py;
						Dyy /= m_spacing[1] * m_spacing[1];
						float Dzz = mz - 2*m_data(i,j,k) + pz;	
						Dzz /= m_spacing[2] * m_spacing[2];
					
						m_data(i,j,k) += m_tau * (Dxx + Dyy + Dzz);
					}
				}	
			}
		}
	}
	else
	{
		for (unsigned int n = 0; n < m_N; n++)
		{
			if ( n % m_dispFreq == 0)
				std::cout << "Iteration " << n << std::endl;

			for (unsigned int k = 0; k < m_size[2]; k++)
			{
				for (unsigned int j = 0; j < m_size[1]; j++)
				{
					for (unsigned int i = 0; i < m_size[0]; i++)
					{
						float mx = (i == 0)           ? m_data(0,j,k)           : m_data(i-1,j,k);
						float px = (i == m_size[0]-1) ? m_data(m_size[0]-1,j,k) : m_data(i+1,j,k);
						float my = (j == 0)           ? m_data(i,0,k)           : m_data(i,j-1,k);
						float py = (j == m_size[1]-1) ? m_data(i,m_size[1]-1,k) : m_data(i,j+1,k);
						float mz = (k == 0)           ? m_data(i,j,0)           : m_data(i,j,k-1);
						float pz = (k == m_size[2]-1) ? m_data(i,j,m_size[2]-1) : m_data(i,j,k+1);
				
						float Dxx = mx - 2*m_data(i,j,k) + px;
						Dxx /= m_spacing[0] * m_spacing[0];
						float Dyy = my - 2*m_data(i,j,k) + py;
						Dyy /= m_spacing[1] * m_spacing[1];
						float Dzz = mz - 2*m_data(i,j,k) + pz;	
						Dzz /= m_spacing[2] * m_spacing[2];
					
						m_data(i,j,k) += (m_fixed(i,j,k) == 0) ? m_tau * (Dxx + Dyy + Dzz) : 0;
					}
				}	
			}
		}

	}
}
#endif //_ksrtDiffusion3D_cxx
