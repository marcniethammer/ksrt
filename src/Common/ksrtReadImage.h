#ifndef _ksrtReadImage_h
#define _ksrtReadImage_h

#include <itkImage.h>
#include <itkImageFileReader.h>

/**
 * A class to read an image from disk
 */
template< class TImage >
class ReadImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileReader< TImage >     TReader;
    typedef typename TReader::Pointer                   TReaderPointer;
    
public:
	/**
	 * Read an image from disk.
	 * @param fileName the image to be read.
	 * @return an image pointer.
	 */
    static TImagePointer DoIt( const char * fileName );
};

#include "ksrtReadImage.cxx"

#endif //_ksrtReadImage_h
