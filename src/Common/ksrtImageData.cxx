#ifndef _ksrtImageData_cxx
#define _ksrtImageData_cxx

#include "ksrtImageData.h"

template< class T >
ImageData< T >::ImageData( unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4, unsigned int n5 )
{
	m_n1 = n1;
	m_n2 = n2;
	m_n3 = n3;
	m_n4 = n4;
    m_n5 = n5;
	m_dim = 0;
	if (m_n1 == 1)
		m_dim = 0;
	else if (m_n2 == 1)
		m_dim = 1;
	else if (m_n3 == 1)
		m_dim = 2;
	else if (m_n4 == 1)
		m_dim = 3;
	else if (m_n5 == 1)
		m_dim = 4;
    else
        m_dim = 5;
}

template< class T >
void ImageData< T >::SetSize( unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4, unsigned int n5 )
{
	m_n1 = n1;
	m_n2 = n2;
	m_n3 = n3;
	m_n4 = n4;
    m_n5 = n5;
    
	m_dim = 0;

	if (m_n1 == 1)
		m_dim = 0;
	else if (m_n2 == 1)
		m_dim = 1;
	else if (m_n3 == 1)
		m_dim = 2;
	else if (m_n4 == 1)
		m_dim = 3;
	else if (m_n5 == 1)
		m_dim = 4;
    else
        m_dim = 5;
}

template< class T >
void ImageData< T >::SetData( T * data )
{
	m_data = data;
}

template< class T >
unsigned int ImageData< T >::GetDimension()
{
	return m_dim;
}

template< class T >
unsigned int * ImageData< T >::GetSize()
{
	unsigned int * sz;
	if (m_dim == 1)
	{
		sz = new unsigned int[1];
		sz[0] = m_n1;
	}
	else if (m_dim ==2)
	{
		sz = new unsigned int[2];
		sz[0] = m_n1;
		sz[1] = m_n2;
	}
	else if (m_dim == 3)
	{
		sz = new unsigned int [3];
		sz[0] = m_n1;
		sz[1] = m_n2;
		sz[2] = m_n3;
	}
	else if (m_dim == 4)
	{
		sz = new unsigned int [4];
		sz[0] = m_n1;
		sz[1] = m_n2;
		sz[2] = m_n3;
		sz[3] = m_n4;
	}
    else
    {
        sz = new unsigned int [5];
        sz[0] = m_n1;
        sz[1] = m_n2;
        sz[2] = m_n3;
        sz[3] = m_n4;
        sz[4] = m_n5;
    }
	return sz;
}

template< class T >
T * ImageData< T >::GetData()
{
	return m_data;
}

template< class T >
inline T & ImageData< T >::operator () ( unsigned int i )
{
	return m_data[i];
}

template< class T >
inline T & ImageData< T >::operator () ( unsigned int i, unsigned int j )
{
	return m_data[i + m_n1*j];
}

template< class T >
inline T & ImageData< T >::operator () ( unsigned int i, unsigned int j, unsigned int k )
{
	return m_data[i + m_n1*j + m_n1*m_n2*k];
}

template< class T >
inline T & ImageData< T >::operator () ( unsigned int i, unsigned int j, unsigned int k, unsigned int l )
{
	return m_data[i + m_n1*j + m_n1*m_n2*k + m_n1*m_n2*m_n3*l];
}

template< class T >
inline T & ImageData< T >::operator () ( unsigned int i, unsigned int j, unsigned int k, unsigned int l, unsigned int p )
{
	return m_data[i + m_n1*j + m_n1*m_n2*k + m_n1*m_n2*m_n3*l + m_n1*m_n2*m_n3*m_n4*p];
}

template< class T >
inline void ImageData< T >::operator += ( ImageData tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] += tmp(i);
}

template< class T >
inline void ImageData< T >::operator -= ( ImageData tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] -= tmp(i);
}

template< class T >
inline void ImageData< T >::operator *= ( ImageData tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] *= tmp(i);
}

template< class T >
inline void ImageData< T >::operator /= ( ImageData tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] /= tmp(i);
}

template< class T >
inline void ImageData< T >::operator += ( T tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] += tmp;
}

template< class T >
inline void ImageData< T >::operator -= ( T tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] -= tmp;
}

template< class T >
inline void ImageData< T >::operator *= ( T tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5; i++)
			m_data[i] *= tmp;
}

template< class T >
inline void ImageData< T >::operator /= ( T tmp )
{
	for (unsigned int i = 0; i < m_n1*m_n2*m_n3*m_n4*m_n5 ; i++)
			m_data[i] /= tmp;
}
#endif //_ksrtImageData_cxx