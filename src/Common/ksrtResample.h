#ifndef _ksrt_Resample_h
#define _ksrt_Resample_h

#include <itkImage.h>
#include <itkSimilarity3DTransform.h>
#include <itkAffineTransform.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkNearestNeighborInterpolateImageFunction.h>
#include <itkResampleImageFilter.h>


#include "ksrtNew3DImage.h"

enum InterpolateMethod { Linear, NearestNeighbor };

template< class TImage >
class Resample
{    
public:
    typedef typename TImage::PixelType                  TPixel;
    typedef typename TImage::Pointer                    TImagePointer;
    
    typedef typename TImage::SizeType                   TSize;
    typedef typename TImage::SpacingType                TSpacing;
    typedef typename TImage::DirectionType              TDirection;
    typedef typename TImage::PointType                  TPoint;
    typedef typename TImage::RegionType                 TRegion;
    typedef typename TImage::IndexType                  TIndex;
    
    typedef  itk::Similarity3DTransform< double >                   		TTransform;
    //typedef typename itk::AffineTransform< TPixel, 3 >						TTransform;		
	typedef typename TTransform::Pointer                                    TTransformPointer;
    typedef typename itk::LinearInterpolateImageFunction< TImage, double >  TLinearInterpolator;
    typedef typename TLinearInterpolator::Pointer                           TLinearInterpolatorPointer;
    typedef typename itk::NearestNeighborInterpolateImageFunction< TImage, double >
                                                                            TNearestNeighborInterpolator;
    typedef typename TNearestNeighborInterpolator::Pointer                  TNearestNeighborInterpolatorPointer;
    
	typedef typename itk::ResampleImageFilter< TImage, TImage, double >     TResampler;
	typedef typename TResampler::Pointer                                    TResamplerPointer;
    
    /** 
     *  Constructor.
     */
    Resample();
    
    /**
     * Destructor.
     */
    ~Resample(){}
    
    /**
     * Set the input image (must be float type 3D image).
     * @param input input image (3D float type).
     */
	void SetInput( TImagePointer input );
    
    /**
     * Set the desired size of the output image.
     * @param newSize the desired size of the output image.
     */
    void SetNewSize( TSize newSz );
    
    /**
     * Set the interpolation method.
     * @method the interpolation method, Choose from {Linear, NearestNeighbor}.
     */
    void SetInterpolateMethod( InterpolateMethod method );
    
    /**
     * Get the output image.
     * @return the result image.
     */
    TImagePointer GetOutput();
    
private:

    TImagePointer m_in;
    
    TImagePointer m_out;

    TSize m_sz;

    TSize m_newSz;
    
    TSpacing m_sp;
    
    TSpacing m_newSp;
    
    TDirection m_dir;
    
    TPoint m_origin;

    InterpolateMethod m_method; 
};

#include "ksrtResample.cxx"

#endif


