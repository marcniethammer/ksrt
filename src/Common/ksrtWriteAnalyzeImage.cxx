#ifndef _ksrtWriteAnalyzeImage_cxx
#define _ksrtWriteAnalyzeImage_cxx

#include "ksrtWriteAnalyzeImage.h"
#include <iostream>

template< class TImage >
int WriteAnalyzeImage< TImage >::DoIt( const char * fileName, TImagePointer im )
{
    TWriterPointer writer = TWriter::New();
    TAnalyzeIOPointer analyzeIO = TAnalyzeIO::New();
    writer->SetImageIO( analyzeIO );
    writer->SetInput( im );
    writer->SetFileName( fileName );
    try{
        writer->Update();
        std::cout << "Writing " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        exit( 1 );
    }
    return 0;
}

#endif //_ksrtWriteAnalyzeImage_cxx
