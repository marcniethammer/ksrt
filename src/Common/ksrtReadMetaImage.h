#ifndef _ksrtReadMetaImage_h
#define _ksrtReadMetaImage_h

#include <itkImage.h>
#include <itkMetaImageIO.h>
#include <itkImageFileReader.h>

/**
 * A class to read meta image.
 */
template< class TImage >
class ReadMetaImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileReader< TImage >     TReader;
    typedef typename TReader::Pointer                   TReaderPointer;
    typedef typename itk::MetaImageIO                   TMetaIO;
    typedef typename TMetaIO::Pointer                   TMetaIOPointer;
    
public:
	/**
	 * Read a meta image from disk.
	 * @param fileName the image name to be read.
	 * @return an image pointer.
	 */
    static TImagePointer DoIt( const char * fileName );
};

#include "ksrtReadMetaImage.cxx"

#endif //_ksrtReadMetaImage_h
