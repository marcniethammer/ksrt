#ifndef _ksrtWriteNrrdImage_cxx
#define _ksrtWriteNrrdImage_cxx

#include "ksrtWriteNrrdImage.h"
#include <iostream>

template< class TImage >
int WriteNrrdImage< TImage >::DoIt( const char * fileName, TImagePointer im )
{
    TWriterPointer writer = TWriter::New();
    TNrrdIOPointer nrrdIO = TNrrdIO::New();
    writer->SetImageIO( nrrdIO );
    writer->SetInput( im );
    writer->SetFileName( fileName );
    try{
        writer->Update();
        std::cout << "Writing " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        exit( 1 );
    }
    return 0;
}

#endif //_ksrtWriteNrrdImage_cxx