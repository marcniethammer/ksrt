#ifndef _ksrtFlip_cxx
#define _ksrtFlip_cxx

#include "ksrtFlip.h"
#include <iostream>

template< class TImage >
typename Flip< TImage >::TImagePointer 
Flip< TImage >::DoIt( TImagePointer im, bool flipX, bool flipY, bool flipZ )
{
	TSize sz = im->GetRequestedRegion().GetSize();
	TSpacing sp = im->GetSpacing();
	TDirection dir = im->GetDirection();
	TIndex start;
	start.Fill( 0 );
	TPoint origin = im->GetOrigin();
	TImagePointer imNew = New3DImage< TImage >::DoIt( sz, sp, dir, start, origin, 0 );

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				TIndex idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				TIndex idxNew;
				idxNew[0] = flipX ? sz[0]-1-idx[0] : idx[0];
				idxNew[1] = flipY ? sz[1]-1-idx[1] : idx[1];
				idxNew[2] = flipZ ? sz[2]-1-idx[2] : idx[2];
				 
				imNew->SetPixel( idxNew, im->GetPixel( idx ) );
			}
		}
	}

	return imNew;
}
#endif //_ksrtFlip_cxx
