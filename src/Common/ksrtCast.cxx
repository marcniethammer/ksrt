#ifndef _ksrtCast_cxx
#define _ksrtCast_cxx

#include "ksrtCast.h"

template< class TInputImage, class TOutputImage >
typename Cast< TInputImage, TOutputImage >::TOutputImagePointer 
Cast< TInputImage, TOutputImage >::DoIt( TInputImagePointer input )
{
    TCastFilterPointer castFilter = TCastFilter::New();
    castFilter->SetInput( input );
    castFilter->Update();
    return castFilter->GetOutput();
    
}

#endif