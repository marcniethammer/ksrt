#ifndef _ksrtReadNrrdImage_cxx
#define _ksrtReadNrrdImage_cxx

#include "ksrtReadNrrdImage.h"
#include <iostream>

template< class TImage >
typename ReadNrrdImage< TImage >::TImagePointer
ReadNrrdImage< TImage >::DoIt( const char * fileName )
{
    TReaderPointer reader = TReader::New();
    TNrrdIOPointer nrrdIO = TNrrdIO::New();
    reader->SetImageIO( nrrdIO );
    reader->SetFileName( fileName );
    try{
        reader->Update();
        std::cout << "Reading " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        return NULL;
    }
    return reader->GetOutput();
}

#endif //_ksrtReadNrrdImage_cxx