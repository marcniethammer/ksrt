#ifndef _ksrtWriteAnalyzeImage_h
#define _ksrtWriteAnalyzeImage_h

#include <itkImage.h>
#include <itkAnalyzeImageIO.h>
#include <itkImageFileWriter.h>

/**
 * A class to write an analyze image to disk
 */

template< class TImage >
class WriteAnalyzeImage
{
    typedef typename TImage::Pointer                    TImagePointer;
    typedef typename itk::ImageFileWriter< TImage >     TWriter;
    typedef typename TWriter::Pointer                   TWriterPointer;
    typedef typename itk::AnalyzeImageIO                TAnalyzeIO;
    typedef typename TAnalyzeIO::Pointer                TAnalyzeIOPointer;
    
public:
	/**
	 * Write an analyze image to disk
	 * @param fileName the location to write the image to.
	 * @param im the image to write.
	 */
    static int DoIt( const char * fileName, TImagePointer im );
};


#include "ksrtWriteAnalyzeImage.cxx"

#endif //_ksrtWriteAnalyzeImage_h


