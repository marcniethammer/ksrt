#ifndef _ksrtFlip_h
#define _ksrtFlip_h

#include <itkImage.h>
#include "ksrtNew3DImage.h"

template< class TImage >
class Flip
{
public:
    typedef typename TImage::Pointer							TImagePointer;
	typedef typename TImage::IndexType							TIndex;
	typedef typename TImage::SizeType							TSize;
	typedef typename TImage::SpacingType						TSpacing;
	typedef typename TImage::PointType							TPoint;
	typedef typename TImage::DirectionType						TDirection;

	static TImagePointer DoIt( TImagePointer im, bool flipX, bool flipY, bool flipZ );    
};

#include "ksrtFlip.cxx"

#endif //_ksrtFlip_h
