#ifndef _ksrtReadImage_cxx
#define _ksrtReadImage_cxx

#include "ksrtReadImage.h"
#include <iostream>

template< class TImage >
typename ReadImage< TImage >::TImagePointer
ReadImage< TImage >::DoIt( const char * fileName )
{
    TReaderPointer reader = TReader::New();
    reader->SetFileName( fileName );
    try{
        reader->Update();
        std::cout << "Reading " << fileName << std::endl;
    }
    catch( itk::ExceptionObject & e ){
        std::cerr << e.GetDescription() << std::endl;
        return NULL;
    }
    return reader->GetOutput();
}

#endif //_ksrtReadImage_cxx