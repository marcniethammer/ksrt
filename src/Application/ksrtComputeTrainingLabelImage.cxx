#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtImageData.h>
#include <Common/ksrtNew3DImage.h>

#include <itkBinaryDilateImageFilter.h>
#include <itkBinaryBallStructuringElement.h>


#include <itkPluginUtilities.h>

#include "ksrtComputeTrainingLabelImageCLP.h"

using namespace std;
using namespace itk;

template< class T > 
int DoIt( int argc, char * argv[], T )
{
    PARSE_ARGS;

	const unsigned int Dim = 3;
    typedef Image< T, Dim >                     InputImageType;
	
    typedef short 								ShortPixelType;
	typedef Image< ShortPixelType, Dim >		ShortImageType;
	typedef ShortImageType::Pointer				ShortImagePointerType;
	
    typedef ShortImageType::SizeType			SizeType;
	typedef ShortImageType::RegionType			RegionType;
	typedef ShortImageType::SpacingType			SpacingType;
	typedef ShortImageType::IndexType			IndexType;
	typedef ShortImageType::PointType 			PointType;

	typedef BinaryBallStructuringElement< ShortPixelType, Dim > 
	          									StructuringElementType;

	typedef BinaryDilateImageFilter< ShortImageType, ShortImageType, StructuringElementType > 
												DilateFilterType;

	typedef DilateFilterType::Pointer 			DialteFilterPointerType;

	ShortImagePointerType imFem = Cast< InputImageType, ShortImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputFemCart.c_str() ) );
	ShortImagePointerType imTib = Cast< InputImageType, ShortImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputTibCart.c_str() ) );
	SizeType sz = imFem->GetRequestedRegion().GetSize();
    IndexType start;
	start.Fill( 0 );

	ShortImagePointerType imCart = New3DImage< ShortImageType >::DoIt(sz, imFem->GetSpacing(), imFem->GetDirection(), start, imFem->GetOrigin(), 0);

	ImageData< ShortPixelType > dataFem( sz[0], sz[1], sz[2] );
	ImageData< ShortPixelType > dataTib( sz[0], sz[1], sz[2] );
	ImageData< ShortPixelType > dataCart( sz[0], sz[1], sz[2] );

	dataFem.SetData( imFem->GetBufferPointer() );
	dataTib.SetData( imTib->GetBufferPointer() );
	dataCart.SetData( imCart->GetBufferPointer() );

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				dataCart(i,j,k) = (dataFem(i,j,k)>0 || dataTib(i,j,k)>0) ? 1 : 0;
			}
		}
	}
	
	StructuringElementType structuringElement;
	structuringElement.SetRadius( radius );
	structuringElement.CreateStructuringElement();

	DialteFilterPointerType binaryDilate = DilateFilterType::New();
	binaryDilate->SetKernel( structuringElement );		
	binaryDilate->SetDilateValue( 1 );
	binaryDilate->SetInput( imCart );

	try{
		binaryDilate->Update();
	}catch( ExceptionObject & e ){
		cerr << e.GetDescription() << endl;
	}

	ShortImagePointerType imCartDilate = binaryDilate->GetOutput();

	ShortImagePointerType imOutput = New3DImage< ShortImageType >::DoIt(sz, imFem->GetSpacing(), imFem->GetDirection(), start, imFem->GetOrigin(), 0);

	ImageData< ShortPixelType > dataOutput( sz[0], sz[1], sz[2] );
	ImageData< ShortPixelType > dataCartDilate( sz[0], sz[1], sz[2] );

	dataOutput.SetData( imOutput->GetBufferPointer() );
	dataCartDilate.SetData( imCartDilate->GetBufferPointer() );

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				dataOutput(i,j,k) = (dataCartDilate(i,j,k) > 0) ? 3 : 0;
				dataOutput(i,j,k) = (dataFem(i,j,k) > 0) ? 1 : dataOutput(i,j,k);
				dataOutput(i,j,k) = (dataTib(i,j,k) > 0) ? 2 : dataOutput(i,j,k);
			}
		}
	}
	
	WriteImage< ShortImageType >::DoIt( outputResult.c_str(), imOutput, (bool)compression );

	return EXIT_SUCCESS;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType femurPixelType;
    ImageIOBase::IOComponentType femurComponentType;
    
    ImageIOBase::IOPixelType tibiaPixelType;
    ImageIOBase::IOComponentType tibiaComponentType;
    
    try
    {
        GetImageType (inputFemCart, femurPixelType, femurComponentType);
        GetImageType (inputTibCart, tibiaPixelType, tibiaComponentType);
        
        if (femurComponentType != tibiaComponentType)
        {
            std::cerr << "Input femur and tibia should have the same image type!" << std::endl;
            return EXIT_FAILURE;
        }
        switch (femurComponentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}