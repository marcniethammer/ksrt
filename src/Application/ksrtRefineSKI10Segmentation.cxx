#include <itkImage.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>

#include <itkPluginUtilities.h>

#include "ksrtRefineSKI10SegmentationCLP.h"

using namespace std;
using namespace itk;

int DoIt( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    typedef Image< unsigned char, 3 >                   ShortImageType;
    typedef ShortImageType::Pointer                     ShortImagePointerType;
    
    typedef Image< float, 3 >                           FloatImageType;
    typedef FloatImageType::Pointer                     FloatImagePointerType;
    

    typedef ShortImageType::SpacingType                 SpacingType;
    typedef ShortImageType::SizeType                    SizeType;
	typedef ShortImageType::DirectionType				DirectionType;
	typedef ShortImageType::IndexType					IndexType;
	typedef ShortImageType::PointType					PointType;

    ShortImagePointerType seg = ReadImage<ShortImageType>::DoIt(inputSegmentation.c_str());
    FloatImagePointerType dis = ReadImage<FloatImageType>::DoIt(inputDistanceMap.c_str());
    
    SizeType sz = seg->GetRequestedRegion().GetSize();
        
	float * sum = new float [sz[0]]; 
    
	for (unsigned int i = 0; i < sz[0]; i++)
	{
		sum[i] = 0;
	}
	
	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				sum[i] += (seg->GetPixel( idx ) == 2 ? 1 : 0);
			}
		}
	}
	unsigned int start = 0;;
    
	while(sum[start] <= 50)
	{
		start++;
	}
    start = std::max(0u,start - 20);
	unsigned int end = std::min((unsigned int)sz[0], (unsigned int)floor(start + 40));
    
    for (unsigned int i = start; i < end; i++)
	{
        for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int k = 0; k < sz[2]; k++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				if (seg->GetPixel( idx ) == 2 && dis->GetPixel( idx ) > 5.0)
                    seg->SetPixel(idx, 0);
                
			}
		}
	}
	WriteImage< ShortImageType >::DoIt( outputSegmentation.c_str(), seg, (bool)compression );
	return 0;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    return DoIt( argc, argv );

}