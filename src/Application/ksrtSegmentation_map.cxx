#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkImageFileReader.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <itkPluginUtilities.h>
#include "ksrtSegmentation_mapCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;


using namespace std;
using namespace itk;

template< class T >
int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef Image< T, 3 >              TImage;

    FloatImagePointerType imR0 = Cast< TImage, FloatImageType >::DoIt( ReadImage< TImage >::DoIt( inputFemurCost.c_str() ) );
    FloatImagePointerType imR1 = Cast< TImage, FloatImageType >::DoIt( ReadImage< TImage >::DoIt( inputBackgroundCost.c_str() ) );
    FloatImagePointerType imR2 = Cast< TImage, FloatImageType >::DoIt( ReadImage< TImage >::DoIt( inputTibiaCost.c_str() ) );
    
    SizeType sz = imR0->GetRequestedRegion().GetSize();
    SpacingType sp = imR0->GetSpacing();
    DirectionType dir = imR0->GetDirection();
    PointType origin = imR0->GetOrigin();
    IndexType start;
    start.Fill(0);
    
    ImageData< float > r0;
    r0.SetData( imR0->GetBufferPointer() );
    r0.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > r1;
    r1.SetData( imR1->GetBufferPointer() );
    r1.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > r2;
    r2.SetData( imR2->GetBufferPointer() );
    r2.SetSize( sz[0], sz[1], sz[2] );
    
    ShortImagePointerType imFemur = New3DImage< ShortImageType >::DoIt(sz, sp, dir, start, origin, 0);
    ShortImagePointerType imTibia = New3DImage< ShortImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    ImageData< short > femur;
    ImageData< short > tibia;
    femur.SetData( imFemur->GetBufferPointer() );
    femur.SetSize( sz[0], sz[1], sz[2] );
    tibia.SetData( imTibia->GetBufferPointer() );
    tibia.SetSize( sz[0], sz[1], sz[2] );
    
    
    for (unsigned int k = 0; k < sz[2]; k++) {
        for (unsigned int j = 0; j < sz[1]; j++) {
            for (unsigned int i = 0; i < sz[0]; i++) {
                femur(i,j,k) = (short)( r0(i,j,k) < r1(i,j,k) && r0(i,j,k) < r2(i,j,k) );
                tibia(i,j,k) = (short)( r2(i,j,k) < r0(i,j,k) && r2(i,j,k) < r1(i,j,k) );
            }
        }
    }
    
    WriteImage< ShortImageType >::DoIt( outputFemurSegmentation.c_str(), imFemur, (bool)compression );
    WriteImage< ShortImageType >::DoIt( outputTibiaSegmentation.c_str(), imTibia, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
     
    ImageIOBase::IOPixelType pixelType_femur;
    ImageIOBase::IOComponentType componentType_femur;
    
    ImageIOBase::IOPixelType pixelType_background;
    ImageIOBase::IOComponentType componentType_background;
    
    ImageIOBase::IOPixelType pixelType_tibia;
    ImageIOBase::IOComponentType componentType_tibia;
    
    try
    {
        GetImageType (inputFemurCost, pixelType_femur, componentType_femur);
        GetImageType (inputBackgroundCost, pixelType_background, componentType_background);
        GetImageType (inputTibiaCost, pixelType_tibia, componentType_tibia);
        
        if ((componentType_femur != componentType_background) || (componentType_femur != componentType_tibia))
        {
            cerr << "Input shape images must have the same image type!" << endl;
            return EXIT_FAILURE;
        }
         
        switch (componentType_femur)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}