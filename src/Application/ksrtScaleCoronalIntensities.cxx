#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtCast.h>

#include <itkPluginUtilities.h>

#include "ksrtScaleCoronalIntensitiesCLP.h"

using namespace std;
using namespace itk;


typedef itk::Image< float, 3 >              FloatImageType;
typedef FloatImageType::Pointer             FloatImagePointerType;
typedef itk::Image< short, 3 >              ShortImageType;
typedef ShortImageType::Pointer             ShortImagePointerType;
typedef FloatImageType::SizeType            SizeType;
typedef FloatImageType::SpacingType         SpacingType;
typedef FloatImageType::DirectionType       DirectionType;   
typedef FloatImageType::PointType           PointType;
typedef FloatImageType::IndexType           IndexType;

void filterOutliers( FloatImagePointerType im );
void doIntervalScaling( FloatImagePointerType im, unsigned int p1, unsigned int p2, float maxValue );

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef itk::Image< T, 3 >              ImageType;   
    
    FloatImagePointerType im = Cast< ImageType, FloatImageType >::DoIt( ReadImage< ImageType >::DoIt( inputVolume.c_str() ) );
    
    filterOutliers( im );
    
	//for coronal T1
	unsigned int p1 = 11;
	unsigned int p2 = 73;
    
	//for sagittal T1
	//unsigned int p1 = 10;
	//unsigned int p2 = 79;
    
	//for sagittal T2
	//unsigned int p1 = 6;
	//unsigned int p2 = 82;
    
	doIntervalScaling(im, p1, p2, maxValue);

    WriteImage< ShortImageType >::DoIt( outputVolume.c_str(), Cast< FloatImageType, ShortImageType >::DoIt( im ), (bool)compression );
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void filterOutliers( FloatImagePointerType im )
{
	double thresh = 1e-3;
	itk::ImageRegionIterator< FloatImageType > iter( im, im->GetRequestedRegion() );
	float maxValue = 0;
	float minValue = 1000;
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		
		if( iter.Get() < 0 )
			iter.Set( 0 );
		if( iter.Get() > maxValue )
			maxValue = iter.Get();		
		if( iter.Get() < minValue )
			minValue = iter.Get();
	} 
	float cutValue = maxValue;
	double ratio = 0.0;
	unsigned int numOutliers = 0;
	SizeType size = im->GetRequestedRegion().GetSize();
	unsigned int numTotal = size[0]*size[1]*size[2];
	do
	{
		numOutliers = 0;
		cutValue = (cutValue + minValue)/20*19;
		for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
			numOutliers = (iter.Get() > cutValue) ? numOutliers+1 : numOutliers;
		ratio = (double)numOutliers / (double)numTotal;
	}
	while( ratio < thresh );
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		float tmp_value = (iter.Get() > cutValue) ? 99.0 : iter.Get()/cutValue*99.0;
		iter.Set( tmp_value );
	}
}

void doIntervalScaling( FloatImagePointerType im, unsigned int p1, unsigned int p2, float maxValue )
{
	int hist[100] = {0};	
	itk::ImageRegionIterator< FloatImageType > iter( im, im->GetRequestedRegion() );
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		double tmp = iter.Get() * (iter.Get() > 0);
		const int idx_tmp = (int) floor( tmp );
		hist[idx_tmp]++;
	}
	int max_idx_1 = 0; 
	int max_sz_1 = 0;
	int max_idx_2 = 0;
	int max_sz_2 = 0;
	for(unsigned int i = 1; i < 50; i++ )
	{
		if( hist[i] > max_sz_1 )
		{
			max_sz_1 = hist[i];
			max_idx_1 = i;
		}
	}
	for(unsigned int i = 50; i < 100; i++ )
	{
		if( hist[i] > max_sz_2 )
		{
			max_sz_2 = hist[i];
			max_idx_2 = i;
		}
	}
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		int id_1 = (iter.Get() <= max_idx_1);
		int id_2 = (iter.Get() > max_idx_1) * (iter.Get() <= max_idx_2);
		int id_3 = (iter.Get() > max_idx_2);
		double value_1 = iter.Get() / max_idx_1 * p1; 
		double value_2 = p1 + (iter.Get() - max_idx_1) * (p2 - p1) / (max_idx_2 - max_idx_1);
		double value_3 = p2 + (iter.Get() - max_idx_2) * (99 - p2) / (99 - max_idx_2);
		iter.Set( (id_1 * value_1 + id_2 * value_2 + id_3 * value_3) / 99 * maxValue );
	}
    
}
