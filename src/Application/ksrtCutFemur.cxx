#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtCast.h>

#include <itkPluginUtilities.h>

#include "ksrtCutFemurCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;


typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

FloatImagePointerType cutFemur( FloatImagePointerType femur, float len );

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef    T        InputPixelType;
    typedef    T        OutputPixelType;
    
    typedef typename itk::Image< InputPixelType,  3 >     InputImageType;
    typedef typename itk::Image< OutputPixelType, 3 >     OutputImageType;
    
    typedef typename InputImageType::Pointer         InputImagePointerType;
    typedef typename OutputImageType::Pointer        OutputImagePointerType;
    
    FloatImagePointerType femur = Cast< InputImageType, FloatImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputVolume.c_str() ) );
    FloatImagePointerType femur_cut_mask  = cutFemur( femur, length );
    WriteImage< OutputImageType >::DoIt( outputVolume.c_str(), Cast<FloatImageType, OutputImageType>::DoIt( femur_cut_mask ), (bool)compression );
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


FloatImagePointerType cutFemur( FloatImagePointerType femur, float len )
{
    SizeType sz = femur->GetRequestedRegion().GetSize();
    
	DirectionType dir = femur->GetDirection();
	SpacingType sp = femur->GetSpacing();
    IndexType start;
    start.Fill(0);
    
    
	FloatImagePointerType femur_cut_mask = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, femur->GetOrigin(), 0);
    
	float * sum_femur = new float [sz[1]]; 
    
	for (unsigned int i = 0; i < sz[1]; i++)
	{
		sum_femur[i] = 0;
	}
	
	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				sum_femur[j] += femur->GetPixel( idx );
			}
		}
	}
    
	
	unsigned int min_femur =  sz[1]/4;
    
	while(sum_femur[min_femur] <= 1)
	{
		min_femur++;
	}
    
	unsigned int jmax = std::min((unsigned int)sz[1], (unsigned int)floor(min_femur + len/sp[1]));
    
	for (unsigned int i = 0; i < sz[0]; i++)
	{
//		for (unsigned int j = min_femur; j < jmax; j++)
        for (unsigned int j = 0; j < jmax; j++)
		{
			for (unsigned int k = 0; k < sz[2]; k++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				femur_cut_mask->SetPixel(idx, 1);
			}
		}
	}
    return femur_cut_mask;
}