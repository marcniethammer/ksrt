#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtCast.h>

#include <itkPluginUtilities.h>

#include "ksrtCutTibiaCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;


typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

FloatImagePointerType cutTibia( FloatImagePointerType tibia, float len );

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef    T        InputPixelType;
    typedef    T        OutputPixelType;
    
    typedef typename itk::Image< InputPixelType,  3 >     InputImageType;
    typedef typename itk::Image< OutputPixelType, 3 >     OutputImageType;
    
    typedef typename InputImageType::Pointer         InputImagePointerType;
    typedef typename OutputImageType::Pointer        OutputImagePointerType;
    
    FloatImagePointerType tibia = Cast< InputImageType, FloatImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputVolume.c_str() ) );
    FloatImagePointerType tibia_cut_mask  = cutTibia( tibia, length );
    WriteImage< OutputImageType >::DoIt( outputVolume.c_str(), Cast<FloatImageType, OutputImageType>::DoIt( tibia_cut_mask ), (bool)compression );
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


FloatImagePointerType cutTibia( FloatImagePointerType tibia, float len )
{
    SizeType sz = tibia->GetRequestedRegion().GetSize();
    
	DirectionType dir = tibia->GetDirection();
	SpacingType sp = tibia->GetSpacing();
    IndexType start;
    start.Fill(0);
    
    
	FloatImagePointerType tibia_cut_mask = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, tibia->GetOrigin(), 0);
    
    
    float * sum_tibia = new float [sz[1]];
	
	for (unsigned int i = 0; i < sz[1]; i++)
	{
		sum_tibia[i] = 0;
	}
	
	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				sum_tibia[j] += tibia->GetPixel( idx );
			}
		}
	}
	
	unsigned int max_tibia = 3*sz[1]/4;
    
	while(sum_tibia[max_tibia] <= 1)
	{
		max_tibia--;
	}
    
	
	unsigned int jmin = std::max((int)0, (int)ceil(max_tibia - len/sp[1]));
    
    
	for (unsigned int k = 0; k < sz[2]; k++)
	{
//		for (unsigned int j = jmin; j < max_tibia; j++)
        for (unsigned int j = jmin; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				tibia_cut_mask->SetPixel(idx, 1);
			}
		}
	}
    return tibia_cut_mask;
    
}



