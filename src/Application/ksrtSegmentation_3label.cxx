#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkImageFileReader.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <Segmentation/ksrtThreeLabelSegmentation.h>

#include <itkPluginUtilities.h>
#include "ksrtSegmentation_3labelCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;


using namespace std;
using namespace itk;

template< class T >
int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef Image< T, 3 >              TImage;
    
    FloatImagePointerType imR0 = Cast< TImage, FloatImageType >::DoIt( ReadImage< TImage >::DoIt( inputFemurCost.c_str() ) );
    FloatImagePointerType imR1 = Cast< TImage, FloatImageType >::DoIt( ReadImage< TImage >::DoIt( inputBackgroundCost.c_str() ) );
    FloatImagePointerType imR2 = Cast< TImage, FloatImageType >::DoIt( ReadImage< TImage >::DoIt( inputTibiaCost.c_str() ) );    
 
    SpacingType sp = imR0->GetSpacing();
    SizeType sz = imR0->GetRequestedRegion().GetSize();
    DirectionType dir = imR0->GetDirection();
    
    IndexType start;
    start.Fill(0);
    
    PointType origin = imR0->GetOrigin();
    
    FloatImagePointerType imU1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0.33);
    FloatImagePointerType imU2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0.67);
    
    FloatImagePointerType imPX1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imPX2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    FloatImagePointerType imPY1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imPY2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    FloatImagePointerType imPZ1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imPZ2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    FloatImagePointerType imQ0 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imQ1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imQ2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    FloatImagePointerType imG1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, g);
    FloatImagePointerType imG2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, g);
    
    ShortImagePointerType imFemur = New3DImage< ShortImageType >::DoIt(sz, sp, dir, start, origin, 0);
    ShortImagePointerType imTibia = New3DImage< ShortImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    ThreeLabelSegmentation segmentation;
    
    segmentation.SetSize( sz[0], sz[1], sz[2] );
    segmentation.SetSpacing( sp[0], sp[1], sp[2]);
    
    segmentation.SetG1( imG1->GetBufferPointer() );
    segmentation.SetG2( imG2->GetBufferPointer() );
    
    segmentation.SetR0( imR0->GetBufferPointer() );
    segmentation.SetR1( imR1->GetBufferPointer() );
    segmentation.SetR2( imR2->GetBufferPointer() );
    
    segmentation.SetU1( imU1->GetBufferPointer() );
    segmentation.SetU2( imU2->GetBufferPointer() );
    
    segmentation.SetQ0( imQ0->GetBufferPointer() );
    segmentation.SetQ1( imQ1->GetBufferPointer() );
    segmentation.SetQ2( imQ2->GetBufferPointer() );
    
    segmentation.SetPX1( imPX1->GetBufferPointer() );
    segmentation.SetPX2( imPX2->GetBufferPointer() );
    segmentation.SetPY1( imPY1->GetBufferPointer() );
    segmentation.SetPY2( imPY2->GetBufferPointer() );
    segmentation.SetPZ1( imPZ1->GetBufferPointer() );
    segmentation.SetPZ2( imPZ2->GetBufferPointer() );
    
    segmentation.SetDisplayFrequency( 10 );
    segmentation.SetNumberOfIterations( 1000 );
    segmentation.SetTimeStep( 0.2 );
    segmentation.SetGapThreshold( 5e-3 );
    segmentation.DoIt();
    
    ImageData<short> femur;
    ImageData<short> tibia;
    femur.SetData( imFemur->GetBufferPointer() );
    femur.SetSize( sz[0], sz[1], sz[2] );
    tibia.SetData( imTibia->GetBufferPointer() );
    tibia.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData<float> u1;
    ImageData<float> u2;
    u1.SetData( imU1->GetBufferPointer() );
    u1.SetSize( sz[0], sz[1], sz[2] );
    u2.SetData( imU2->GetBufferPointer() );
    u2.SetSize( sz[0], sz[1], sz[2] );
    
    for (unsigned int k = 0; k < sz[2]; k++) {
        for (unsigned int j = 0; j < sz[1]; j++) {
            for (unsigned int i = 0; i < sz[0]; i++) {
                femur(i,j,k) = ( u1(i,j,k) > 1-u2(i,j,k) && u1(i,j,k) > u2(i,j,k)-u1(i,j,k) ) ? 1 : 0;
                tibia(i,j,k) = ( 1-u2(i,j,k) > u1(i,j,k) && 1-u2(i,j,k) > u2(i,j,k)-u1(i,j,k) ) ? 1 : 0;
            }
        }
    }
    
    WriteImage< ShortImageType >::DoIt( outputFemurSegmentation.c_str(), imFemur, (bool)compression );
    WriteImage< ShortImageType >::DoIt( outputTibiaSegmentation.c_str(), imTibia, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType_femur;
    ImageIOBase::IOComponentType componentType_femur;
    
    ImageIOBase::IOPixelType pixelType_background;
    ImageIOBase::IOComponentType componentType_background;
    
    ImageIOBase::IOPixelType pixelType_tibia;
    ImageIOBase::IOComponentType componentType_tibia;
    
    try
    {
        GetImageType (inputFemurCost, pixelType_femur, componentType_femur);
        GetImageType (inputBackgroundCost, pixelType_background, componentType_background);
        GetImageType (inputTibiaCost, pixelType_tibia, componentType_tibia);
        
        if ((componentType_femur != componentType_background) || (componentType_femur != componentType_tibia))
        {
            cerr << "Input shape images must have the same image type!" << endl;
            return EXIT_FAILURE;
        }
        
        switch (componentType_femur)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}