#include <itkImage.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtImageData.h>
#include <Common/ksrtNew3DImage.h>

#include <iostream>
#include <fstream>

#include <itkPluginUtilities.h>

#include "ksrtComputeClassificationProbabilitiesCLP.h"

using namespace std;
using namespace itk;

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef typename itk::Image< T, 3 >             ImageType;
	
	typedef short 									ShortPixelType;
	typedef Image< ShortPixelType, 3 >              ShortImageType;
	typedef ShortImageType::Pointer					ShortImagePointerType;

	typedef float									FloatPixelType;
	typedef Image< FloatPixelType, 3 >              FloatImageType;
	typedef FloatImageType::Pointer 				FloatImagePointerType;
	typedef FloatImageType::RegionType				RegionType;
	typedef FloatImageType::SizeType				SizeType;
	typedef FloatImageType::IndexType				IndexType;
	typedef FloatImageType::PointType				PointType;
    typedef FloatImageType::DirectionType           DirectionType;

	FloatImagePointerType candidateImage1 = Cast< ImageType, FloatImageType>::DoIt( ReadImage< ImageType >::DoIt( inputMaskA.c_str() ) );
	FloatImagePointerType candidateImage2 = Cast< ImageType, FloatImageType>::DoIt( ReadImage< ImageType >::DoIt( inputMaskB.c_str() ) );
	SizeType sz = candidateImage1->GetRequestedRegion().GetSize();

    IndexType start;
	start.Fill( 0 );
    
	FloatImagePointerType pbImage1 = New3DImage< FloatImageType >::DoIt(sz, candidateImage1->GetSpacing(), candidateImage1->GetDirection(), start, candidateImage1->GetOrigin(), 0.0);
	FloatImagePointerType pbImage2 = New3DImage< FloatImageType >::DoIt(sz, candidateImage1->GetSpacing(), candidateImage1->GetDirection(), start, candidateImage1->GetOrigin(), 0.0);
	FloatImagePointerType pbImage3 = New3DImage< FloatImageType >::DoIt(sz, candidateImage1->GetSpacing(), candidateImage1->GetDirection(), start, candidateImage1->GetOrigin(), 0.0);

	ImageData< FloatPixelType > candidateData1( sz[0], sz[1], sz[2] );
	ImageData< FloatPixelType > candidateData2( sz[0], sz[1], sz[2] );
	
	candidateData1.SetData( candidateImage1->GetBufferPointer() );
	candidateData2.SetData( candidateImage2->GetBufferPointer() );

	ImageData< FloatPixelType > pbData1( sz[0], sz[1], sz[2] );
	ImageData< FloatPixelType > pbData2( sz[0], sz[1], sz[2] );
	ImageData< FloatPixelType > pbData3( sz[0], sz[1], sz[2] );

	pbData1.SetData( pbImage1->GetBufferPointer() );
	pbData2.SetData( pbImage2->GetBufferPointer() );
	pbData3.SetData( pbImage3->GetBufferPointer() );

	unsigned int nnidx[ 100 ] = {0};
	ifstream is;
	is.open( inputNNIndex.c_str() );
	if ( ! is.is_open() )
	{
		cerr << "Cannot open file " << inputNNIndex.c_str() << endl;
		exit( 1 );
	}
	
	ifstream is2;
	is2.open( inputGroups.c_str() );
	if ( ! is2.is_open() )
	{
		cerr << "Cannot open file " << inputGroups.c_str() << endl;
		exit( 1 );
	}

	unsigned int numOfLines = 0;
	char tmp [100];
	while( ! is2.eof() )
	{
		numOfLines ++;
		is2.getline(tmp, 100);
	}
	is2.close();
	unsigned int * group = new unsigned int [ numOfLines ];
	
	// store the group of each sample into an array "group"
	unsigned int tmpIdx = 0;
	unsigned int numOfSampleOnes = 0;
	unsigned int numOfSampleTwos = 0;
	unsigned int numOfSampleThrees = 0;
	
	ifstream is3;
	is3.open( inputGroups.c_str() );
	if ( ! is3.is_open() )
	{
		cerr << "Cannot open file " << inputGroups.c_str() << endl;
		exit( 1 );
	}

	while( ! is3.eof() )
	{
		is3 >> group[tmpIdx];

		if( group[tmpIdx] == 1)
			numOfSampleOnes++;
		else if ( group[tmpIdx] == 2 )
			numOfSampleTwos++;
		else if ( group[tmpIdx] == 3 )
			numOfSampleThrees++;
		
		tmpIdx++; 
	}
	is3.close();

	cout << numOfSampleOnes << endl;
	cout << numOfSampleTwos << endl;
	cout << numOfSampleThrees << endl;

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				if (candidateData1(i,j,k) > threshold || candidateData2(i,j,k) > threshold)
				{
					unsigned int numOfOnes = 0;
					unsigned int numOfTwos = 0;
					unsigned int numOfThrees = 0;
					for (unsigned int ii = 0; ii < K; ii++)
					{
						is >> nnidx[ii];
						numOfOnes = (group[nnidx[ii]] == 1) ? numOfOnes + 1 : numOfOnes;
						numOfTwos = (group[nnidx[ii]] == 2) ? numOfTwos + 1 : numOfTwos;
						numOfThrees = (group[nnidx[ii]] == 3) ? numOfThrees + 1 : numOfThrees;
					}

					pbData1(i,j,k) = (float)numOfOnes / K;
					pbData2(i,j,k) = (float)numOfTwos / K;
					pbData3(i,j,k) = (float)numOfThrees / K;

					
					float tmpPb1 = pbData1(i,j,k) / numOfSampleOnes;
					float tmpPb2 = pbData2(i,j,k) / numOfSampleTwos;
					float tmpPb3 = pbData3(i,j,k) / numOfSampleThrees;

					pbData1(i,j,k) = tmpPb1 / (tmpPb1 + tmpPb2 + tmpPb3);
					pbData2(i,j,k) = tmpPb2 / (tmpPb1 + tmpPb2 + tmpPb3);
					pbData3(i,j,k) = tmpPb3 / (tmpPb1 + tmpPb2 + tmpPb3);
				}
				else
				{
					pbData1(i,j,k) = 0;
					pbData2(i,j,k) = 0;
					pbData3(i,j,k) = 0;
				}
			}
		}
	}
	
	is.close();
	delete [] group;

	WriteImage< FloatImageType >::DoIt( outputProbA.c_str(), pbImage1, (bool)compression );
	WriteImage< FloatImageType >::DoIt( outputProbB.c_str(), pbImage2, (bool)compression );

	return 0;

} 	

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelTypeA;
    ImageIOBase::IOComponentType componentTypeA;
    
    ImageIOBase::IOPixelType pixelTypeB;
    ImageIOBase::IOComponentType componentTypeB;
    
    try
    {
        GetImageType (inputMaskA, pixelTypeA, componentTypeA);
        GetImageType (inputMaskB, pixelTypeB, componentTypeB);
        
        if (componentTypeA != componentTypeB)
        {
            std::cerr << "Masks must have the same type!" << std::endl;
            return EXIT_FAILURE;
        }
        switch (componentTypeA)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
