#include <itkImage.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtImageData.h>
#include <Common/ksrtNew3DImage.h>

#include <iostream>
#include <fstream>
#include <string>

#include <itkPluginUtilities.h>

#include "ksrtComputeClassificationProbabilities_SVMCLP.h"

using namespace std;
using namespace itk;

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef typename itk::Image< T, 3 >             ImageType;
	
	typedef short 									ShortPixelType;
	typedef Image< ShortPixelType, 3 >              ShortImageType;
	typedef ShortImageType::Pointer					ShortImagePointerType;

	typedef float									FloatPixelType;
	typedef Image< FloatPixelType, 3 >              FloatImageType;
	typedef FloatImageType::Pointer 				FloatImagePointerType;
	typedef FloatImageType::RegionType				RegionType;
	typedef FloatImageType::SizeType				SizeType;
	typedef FloatImageType::IndexType				IndexType;
	typedef FloatImageType::PointType				PointType;
    typedef FloatImageType::DirectionType           DirectionType;

	FloatImagePointerType candidateImage1 = Cast< ImageType, FloatImageType>::DoIt( ReadImage< ImageType >::DoIt( inputMaskA.c_str() ) );
	FloatImagePointerType candidateImage2 = Cast< ImageType, FloatImageType>::DoIt( ReadImage< ImageType >::DoIt( inputMaskB.c_str() ) );
	SizeType sz = candidateImage1->GetRequestedRegion().GetSize();

    IndexType start;
	start.Fill( 0 );
    
	FloatImagePointerType pbImage1 = New3DImage< FloatImageType >::DoIt(sz, candidateImage1->GetSpacing(), candidateImage1->GetDirection(), start, candidateImage1->GetOrigin(), 0.0);
	FloatImagePointerType pbImage2 = New3DImage< FloatImageType >::DoIt(sz, candidateImage1->GetSpacing(), candidateImage1->GetDirection(), start, candidateImage1->GetOrigin(), 0.0);
	FloatImagePointerType pbImage3 = New3DImage< FloatImageType >::DoIt(sz, candidateImage1->GetSpacing(), candidateImage1->GetDirection(), start, candidateImage1->GetOrigin(), 0.0);

	ImageData< FloatPixelType > candidateData1( sz[0], sz[1], sz[2] );
	ImageData< FloatPixelType > candidateData2( sz[0], sz[1], sz[2] );
	
	candidateData1.SetData( candidateImage1->GetBufferPointer() );
	candidateData2.SetData( candidateImage2->GetBufferPointer() );

	ImageData< FloatPixelType > pbData1( sz[0], sz[1], sz[2] );
	ImageData< FloatPixelType > pbData2( sz[0], sz[1], sz[2] );
	ImageData< FloatPixelType > pbData3( sz[0], sz[1], sz[2] );

	pbData1.SetData( pbImage1->GetBufferPointer() );
	pbData2.SetData( pbImage2->GetBufferPointer() );
	pbData3.SetData( pbImage3->GetBufferPointer() );

	ifstream is;
	is.open( inputPredictedGroups.c_str() );
	if ( ! is.is_open() )
	{
		cerr << "Cannot open file " << inputPredictedGroups.c_str() << endl;
		exit( 1 );
	}
	
    string tmp;
    getline(is, tmp);
	

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				if (candidateData1(i,j,k) > threshold || candidateData2(i,j,k) > threshold)
				{
                    int group;
                    float p1, p2, p3;
                    is >> group;
                    is >> p3;
                    is >> p2;
                    is >> p1;
					pbData1(i,j,k) = p1;
					pbData2(i,j,k) = p2;
					pbData3(i,j,k) = p3;
				}
				else
				{
					pbData1(i,j,k) = 0;
					pbData2(i,j,k) = 0;
					pbData3(i,j,k) = 0;
				}
			}
		}
	}
	
	is.close();

	WriteImage< FloatImageType >::DoIt( outputProbA.c_str(), pbImage1, (bool)compression );
	WriteImage< FloatImageType >::DoIt( outputProbB.c_str(), pbImage2, (bool)compression );

	return 0;

} 	

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelTypeA;
    ImageIOBase::IOComponentType componentTypeA;
    
    ImageIOBase::IOPixelType pixelTypeB;
    ImageIOBase::IOComponentType componentTypeB;
    
    try
    {
        GetImageType (inputMaskA, pixelTypeA, componentTypeA);
        GetImageType (inputMaskB, pixelTypeB, componentTypeB);
        
        if (componentTypeA != componentTypeB)
        {
            std::cerr << "Masks must have the same type!" << std::endl;
            return EXIT_FAILURE;
        }
        switch (componentTypeA)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
