#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>

#include <itkPluginUtilities.h>

#include "ksrtFlipCLP.h"

using namespace std;
using namespace itk;

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef    T       PixelType;
    
    typedef typename itk::Image< PixelType,  3 >     ImageType;    
    typedef typename ImageType::Pointer             ImagePointerType;
    
    typedef typename ImageType::SizeType        SizeType;
    typedef typename ImageType::SpacingType     SpacingType;
    typedef typename ImageType::DirectionType   DirectionType;   
    typedef typename ImageType::PointType       PointType;
    typedef typename ImageType::IndexType       IndexType;
    
    ImagePointerType imIn = ReadImage< ImageType >::DoIt( inputVolume.c_str() );

    SizeType sz = imIn->GetRequestedRegion().GetSize();
    SpacingType sp = imIn->GetSpacing();
    DirectionType dir = imIn->GetDirection();
    PointType origin = imIn->GetOrigin();
    IndexType start;
    start.Fill(0);
    
    ImagePointerType imOut = New3DImage< ImageType >::DoIt( sz, sp, dir, start, origin, 0 );
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idxOut;
                idxOut[0] = i;
                idxOut[1] = j;
                idxOut[2] = k;
                
                IndexType idxIn;
                idxIn[0] = (flip[0] == 0) ? idxOut[0] : sz[0]-1-idxOut[0];
                idxIn[1] = (flip[1] == 0) ? idxOut[1] : sz[1]-1-idxOut[1];
                idxIn[2] = (flip[2] == 0) ? idxOut[2] : sz[2]-1-idxOut[2];
                imOut->SetPixel( idxOut, imIn->GetPixel(idxIn) );
            }
        }
    }
    
    WriteImage< ImageType >::DoIt( outputVolume.c_str(), imOut, (bool)compression );
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}