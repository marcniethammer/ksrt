#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkBSplineDeformableTransform.h>
#include <itkResampleImageFilter.h>
#include <itkTransformFileReader.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>


#include <itkPluginUtilities.h>
#include "ksrtApplyBSplineTransformCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

typedef BSplineDeformableTransform< double, 3, 3 >      BSplineTransformType;
typedef BSplineTransformType::Pointer                   BSplineTransformPointerType;

typedef ResampleImageFilter< FloatImageType, 
                             FloatImageType, 
                             double >                   ResamplerType;
typedef ResamplerType::Pointer                          ResamplerPointerType;
typedef LinearInterpolateImageFunction< FloatImageType, 
                                        double >        InterpolatorType;
typedef InterpolatorType::Pointer						InterpolatorPointerType;
typedef InterpolatorType::ContinuousIndexType           ContinuousIndexType;

using namespace std;
using namespace itk;


FloatImagePointerType applyBSplineTransform( FloatImagePointerType imMoving, BSplineTransformPointerType transform, SizeType sz, SpacingType sp, DirectionType dir, PointType origin);

template<class TReference, class TMoving> int DoIt( int argc, char * argv[], TReference, TMoving )
{
    
    PARSE_ARGS;
    
    typedef Image< TReference, 3 >              ReferenceImageType;
    typedef Image< TMoving, 3 >                 MovingImageType;
    
    FloatImagePointerType imReference = Cast< ReferenceImageType, FloatImageType >::DoIt( ReadImage< ReferenceImageType >::DoIt( inputReferenceImage.c_str() ) );
    FloatImagePointerType imMoving = Cast< MovingImageType, FloatImageType >::DoIt( ReadImage< MovingImageType >::DoIt( inputMovingImage.c_str() ) ); 
    
    
	SizeType sz = imReference->GetRequestedRegion().GetSize();
	SpacingType sp = imReference->GetSpacing();
	DirectionType dir = imReference->GetDirection();
	PointType origin = imReference->GetOrigin();
    
    TransformFileReader::Pointer transformReader = TransformFileReader::New();
    transformReader->SetFileName( inputBSplineTransform.c_str() );
    transformReader->Update();
    
    
	BSplineTransformPointerType bsplineTransform = BSplineTransformType::New();
    bsplineTransform = dynamic_cast<BSplineTransformType * > ( transformReader->GetTransformList()->front().GetPointer());
    
    // second transform
    transformReader->GetTransformList()->pop_front();
    if ( transformReader->GetTransformList()->size() == 0 )
    {
        cerr << "Error, the second transform does not exist." << endl;
        return EXIT_FAILURE;
    }
    if (!dynamic_cast<BSplineTransformType::BulkTransformType* > ( transformReader->GetTransformList()->front().GetPointer()))
    {
        cerr << "Error, the second transform is not Bulk Transform." << endl;
        return EXIT_FAILURE;
    }
    BSplineTransformType::BulkTransformType* bulkTfm =
    dynamic_cast<BSplineTransformType::BulkTransformType*> (transformReader->GetTransformList()->front().GetPointer());
    bsplineTransform->SetBulkTransform(bulkTfm);
    
    FloatImagePointerType imMoved = applyBSplineTransform(imMoving, bsplineTransform, sz, sp, dir, origin);
    WriteImage< FloatImageType >::DoIt( outputMovedImage.c_str(), imMoved, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    ImageIOBase::IOPixelType pixelType_reference;
    ImageIOBase::IOComponentType componentType_reference;
    
    ImageIOBase::IOPixelType pixelType_moving;
    ImageIOBase::IOComponentType componentType_moving;
    
    
    try
    {
        GetImageType (inputReferenceImage, pixelType_reference, componentType_reference);
        GetImageType (inputMovingImage, pixelType_moving, componentType_moving);
        
        switch (componentType_reference) {
            case ImageIOBase::UCHAR:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::CHAR:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::USHORT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::SHORT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::UINT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::INT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::ULONG:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::LONG:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::FLOAT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::DOUBLE:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            default:
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

FloatImagePointerType applyBSplineTransform( FloatImagePointerType imMoving, BSplineTransformPointerType transform, SizeType sz, SpacingType sp, DirectionType dir, PointType origin)
{
	ResamplerPointerType resampler = ResamplerType::New();
	resampler->SetTransform( transform );
	resampler->SetInput( imMoving );
	resampler->SetSize( sz );
	resampler->SetOutputOrigin( origin );
	resampler->SetOutputSpacing( sp );
	resampler->SetOutputDirection( dir );
	resampler->SetDefaultPixelValue( 0 );
	resampler->Update();
	return resampler->GetOutput();
    
}

