#include <iostream>
#include <itkImage.h>
#include <itkGradientImageFilter.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtResample.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtDiffusion3D.h>

#include <itkPluginUtilities.h>

#include "ksrtComputeNormalDirectionCLP.h"

using namespace std;
using namespace itk;

template< class T > 
int DoIt( int argc, char * argv[], T )
{
    PARSE_ARGS;
    
    const unsigned int Dim = 3;
    
    typedef Image< T, Dim >                     InputImageType;
    
    typedef short								ShortPixelType;
    typedef Image< ShortPixelType, Dim >		ShortImageType;
    typedef ShortImageType::Pointer				ShortImagePointerType;

    typedef float								FloatPixelType;
    typedef Image< FloatPixelType, Dim >		FloatImageType;
    typedef FloatImageType::Pointer				FloatImagePointerType;
    
    typedef FloatImageType::RegionType          RegionType;
    typedef FloatImageType::SizeType			SizeType;
    typedef FloatImageType::SpacingType			SpacingType;
    typedef FloatImageType::DirectionType		DirectionType;
    typedef FloatImageType::IndexType			IndexType;
    typedef FloatImageType::PointType			PointType;

    
    
	ShortImagePointerType imFemurBoneSeg = Cast< InputImageType, ShortImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputFemur.c_str() ) );
	ShortImagePointerType imTibiaBoneSeg = Cast< InputImageType, ShortImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputTibia.c_str() ) );
    
    SizeType boneSz = imFemurBoneSeg->GetRequestedRegion().GetSize();
    
	SizeType cartSz;
	cartSz[0] = (unsigned int)(upsampling[0] * boneSz[0]);
	cartSz[1] = (unsigned int)(upsampling[1] * boneSz[1]);
	cartSz[2] = (unsigned int)(upsampling[2] * boneSz[2]);

    SpacingType boneSp = imFemurBoneSeg->GetSpacing();
    DirectionType boneDir = imFemurBoneSeg->GetDirection();
    PointType boneOrigin = imFemurBoneSeg->GetOrigin();
    IndexType start;
    start.Fill( 0 );

    ImageData< ShortPixelType > dataFemurBoneSeg( boneSz[0], boneSz[1], boneSz[2] );
    dataFemurBoneSeg.SetData( imFemurBoneSeg->GetBufferPointer() );
    ImageData< ShortPixelType > dataTibiaBoneSeg( boneSz[0], boneSz[1], boneSz[2] );
    dataTibiaBoneSeg.SetData( imTibiaBoneSeg->GetBufferPointer() );

	FloatImagePointerType imBoneSeg = New3DImage< FloatImageType >::DoIt( boneSz, boneSp, boneDir, start, boneOrigin, 0);
    ImageData< FloatPixelType > dataBoneSeg( boneSz[0], boneSz[1], boneSz[2] );
	dataBoneSeg.SetData( imBoneSeg->GetBufferPointer() ); 
    
    for (unsigned int k = 0; k < boneSz[2]; k++)
    {
		for (unsigned int j = 0; j < boneSz[1]; j++)
		{
	    	for (unsigned int i = 0; i < boneSz[0]; i++)
	    	{
				dataBoneSeg(i,j,k) = 1;
				dataBoneSeg(i,j,k) = ( dataFemurBoneSeg(i,j,k) > 0 ) ? 0 : dataBoneSeg(i,j,k);
				dataBoneSeg(i,j,k) = ( dataTibiaBoneSeg(i,j,k) > 0 ) ? 2 : dataBoneSeg(i,j,k);
	   		}
		}
    } 
     
	ShortImagePointerType imFixed = New3DImage< ShortImageType >::DoIt( boneSz, boneSp, boneDir, start, boneOrigin, 0 );
    ImageData< ShortPixelType > dataFixed;
	dataFixed.SetData( imFixed->GetBufferPointer() );
    dataFixed.SetSize( boneSz[0], boneSz[1], boneSz[2] );
    for (unsigned int k = 0; k < boneSz[2]; k++)
    {
        for (unsigned int j = 0; j < boneSz[1]; j++)
        {
            for (unsigned int i = 0; i < boneSz[0]; i++) 
            {
                dataFixed(i,j,k) = (dataBoneSeg(i,j,k) == 0 || dataBoneSeg(i,j,k) == 2) ? 1 : 0;
            }
        }
    }
    
	Diffusion3D diffusion;
    diffusion.SetSize( boneSz[0], boneSz[1], boneSz[2] );
	diffusion.SetSpacing( boneSp[0], boneSp[1], boneSp[2] );
	diffusion.SetTimeStep( 0.02 );
    diffusion.SetN( 200 );
    diffusion.SetFixedRegion( imFixed->GetBufferPointer() );

	diffusion.SetImageData( imBoneSeg->GetBufferPointer() );
	diffusion.DoIt();

    FloatImagePointerType imGradX = New3DImage< FloatImageType >::DoIt(boneSz, boneSp, boneDir, start, boneOrigin, 0.0);
    FloatImagePointerType imGradY = New3DImage< FloatImageType >::DoIt(boneSz, boneSp, boneDir, start, boneOrigin, 0.0);
    FloatImagePointerType imGradZ = New3DImage< FloatImageType >::DoIt(boneSz, boneSp, boneDir, start, boneOrigin, 0.0);
    
    ImageData< FloatPixelType > dataGradX( boneSz[0], boneSz[1], boneSz[2] );
    ImageData< FloatPixelType > dataGradY( boneSz[0], boneSz[1], boneSz[2] );
    ImageData< FloatPixelType > dataGradZ( boneSz[0], boneSz[1], boneSz[2] );

    dataGradX.SetData( imGradX->GetBufferPointer() );
    dataGradY.SetData( imGradY->GetBufferPointer() );
    dataGradZ.SetData( imGradZ->GetBufferPointer() ); 
    
    //central difference to compute gradient in x-direction for inner region
    for (unsigned int k = 0; k < boneSz[2]; k++)
    {
		for (unsigned int j = 0; j < boneSz[1]; j++)
		{
		    for (unsigned int i = 1; i < boneSz[0]-1; i++)
		    {
				dataGradX(i,j,k) = (float)dataBoneSeg(i+1,j,k) - (float)dataBoneSeg(i-1,j,k);
				dataGradX(i,j,k) /= 2;
		    }
		}
    }
    // deal with boundry
    for (unsigned int k = 0; k < boneSz[2]; k++)
    {
		for (unsigned int j = 0; j < boneSz[1]; j++)
		{
		    dataGradX(0,j,k) = (float)dataBoneSeg(1,j,k) - (float)dataBoneSeg(0,j,k);
		    dataGradX(boneSz[0]-1,j,k) = (float)dataBoneSeg(boneSz[0]-1,j,k) - (float)dataBoneSeg(boneSz[0]-2,j,k);
		}
    }
    
    //central difference to compute gradient in y-direction for inner region
    for (unsigned int k = 0; k < boneSz[2]; k++)
    {
		for (unsigned int j = 1; j < boneSz[1]-1; j++)
		{
		    for (unsigned int i = 0; i < boneSz[0]; i++)
		    {
				dataGradY(i,j,k) = (float)dataBoneSeg(i,j+1,k) - (float)dataBoneSeg(i,j,k);
				dataGradY(i,j,k) /= 2;
		    }
		}
    }
    // deal with boundry
    for (unsigned int k = 0; k < boneSz[2]; k++)
    {
		for (unsigned int i = 0; i < boneSz[0]; i++)
		{
		    dataGradY(i,0,k) = (float)dataBoneSeg(i,1,k) - (float)dataBoneSeg(i,0,k);
		    dataGradY(i,boneSz[1]-1,k) = (float)dataBoneSeg(i,boneSz[1]-1,k) - (float)dataBoneSeg(i,boneSz[1]-2,k);
		}
    }

    //central difference to compute gradient in z-direction for inner region
    for (unsigned int k = 1; k < boneSz[2]-1; k++)
    {
		for (unsigned int j = 0; j < boneSz[1]; j++)
		{
		    for (unsigned int i = 0; i < boneSz[0]; i++)
		    {
				dataGradZ(i,j,k) = (float)dataBoneSeg(i,j,k+1) - (float)dataBoneSeg(i,j,k-1);
				dataGradZ(i,j,k) /= 2;
		    }
		}
    }
    // deal with boundry
    for (unsigned int j = 0; j < boneSz[1]; j++)
    {
		for (unsigned int i = 0; i < boneSz[0]; i++)
		{
		    dataGradZ(i,j,0) = (float)dataBoneSeg(i,j,1) - (float)dataBoneSeg(i,j,0);
		    dataGradZ(i,j,boneSz[2]-1) = (float)dataBoneSeg(i,j,boneSz[2]-1) - (float)dataBoneSeg(i,j,boneSz[2]-2);
		}
    }
	
	for (unsigned int k = 0; k < boneSz[2]; k++)
	{
		for (unsigned int j = 0; j < boneSz[1]; j++)
		{
			for (unsigned int i = 0; i < boneSz[0]; i++)
			{
				dataGradX(i,j,k) /= boneSp[0];
				dataGradY(i,j,k) /= boneSp[1];
				dataGradZ(i,j,k) /= boneSp[2];
			}
		}
	}

    Resample< FloatImageType > resample;
    resample.SetInput( imGradX );
    resample.SetNewSize( cartSz );
    FloatImagePointerType imGradXN = resample.GetOutput();
    ImageData< FloatPixelType > dataGradXN;
    dataGradXN.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    dataGradXN.SetData( imGradXN->GetBufferPointer() );
    
    resample.SetInput( imGradY );
    resample.SetNewSize( cartSz );
    FloatImagePointerType imGradYN = resample.GetOutput();
    ImageData< FloatPixelType > dataGradYN;
    dataGradYN.SetData( imGradYN->GetBufferPointer() );
    dataGradYN.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    
    resample.SetInput( imGradZ );
    resample.SetNewSize( cartSz );
    FloatImagePointerType imGradZN = resample.GetOutput();
    ImageData< FloatPixelType > dataGradZN;
    dataGradZN.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    dataGradZN.SetData( imGradZN->GetBufferPointer() );

    for (unsigned int k = 0; k < cartSz[2]; k++)
    {
        for (unsigned int j = 0; j < cartSz[1]; j++)
        {
            for (unsigned int i = 0; i < cartSz[0]; i++)
            {
                float grad = sqrt(dataGradXN(i,j,k)*dataGradXN(i,j,k) + dataGradYN(i,j,k)*dataGradYN(i,j,k) + dataGradZN(i,j,k)*dataGradZN(i,j,k));
				dataGradXN(i,j,k) = (grad > 0) ? dataGradXN(i,j,k)/grad : 0;
				dataGradYN(i,j,k) = (grad > 0) ? dataGradYN(i,j,k)/grad : 0;
				dataGradZN(i,j,k) = (grad > 0) ? dataGradZN(i,j,k)/grad : 0;
            }
        }
    }

	WriteImage< FloatImageType >::DoIt( outputNormX.c_str(), imGradXN, (bool)compression );
	WriteImage< FloatImageType >::DoIt( outputNormY.c_str(), imGradYN, (bool)compression );
	WriteImage< FloatImageType >::DoIt( outputNormZ.c_str(), imGradZN, (bool)compression );

    return 0;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType femurPixelType;
    ImageIOBase::IOComponentType femurComponentType;
    
    ImageIOBase::IOPixelType tibiaPixelType;
    ImageIOBase::IOComponentType tibiaComponentType;
    
    try
    {
        GetImageType (inputFemur, femurPixelType, femurComponentType);
        GetImageType (inputTibia, tibiaPixelType, tibiaComponentType);
        
        if (femurComponentType != tibiaComponentType)
        {
            std::cerr << "Input femur and tibia should have the same image type!" << std::endl;
            return EXIT_FAILURE;
        }
        switch (femurComponentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
