#include <iostream>
#include <string>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>

#include <itkResampleImageFilter.h>
#include <itkIdentityTransform.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkNearestNeighborInterpolateImageFunction.h>
#include <itkBSplineInterpolateImageFunction.h>

#include <itkPluginUtilities.h>

#include "ksrtResampleCLP.h"

using namespace std;
using namespace itk;

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef    T       InputPixelType;
    typedef    T       OutputPixelType;
    
    typedef typename itk::Image< InputPixelType,  3 >   InputImageType;
    typedef typename itk::Image< OutputPixelType, 3 >   OutputImageType;
    
    typedef typename InputImageType::Pointer            InputImagePointerType;
    typedef typename OutputImageType::Pointer           OutputImagePointerType;
    
    typedef typename InputImageType::SizeType           SizeType;
    typedef typename InputImageType::SpacingType        SpacingType;
    typedef typename InputImageType::DirectionType      DirectionType;   
    typedef typename InputImageType::PointType          PointType;
    typedef typename InputImageType::IndexType          IndexType;
    
    typedef typename itk::ResampleImageFilter< InputImageType, InputImageType >                 ResampleFilterType;
    typedef typename ResampleFilterType::Pointer                                                ResampleFilterPointerType;
    typedef typename itk::IdentityTransform< double, 3 >                                        TransformType;
    typedef typename TransformType::Pointer                                                     TransformPointerType;
    typedef typename itk::LinearInterpolateImageFunction< InputImageType, double >              LinearInterpolatorType;
    typedef typename LinearInterpolatorType::Pointer                                            LinearInterpolatorPointerType;
    typedef typename itk::NearestNeighborInterpolateImageFunction< InputImageType, double >     NearestNeighborInterpolatorType;
    typedef typename NearestNeighborInterpolatorType::Pointer                                   NearestNeighborInterpolatorPointerType;
    typedef typename itk::BSplineInterpolateImageFunction< InputImageType, double >             BSplineInterpolatorType;
    typedef typename BSplineInterpolatorType::Pointer                                           BSplineInterpolatorPointerType;
    
    InputImagePointerType imIn = ReadImage< InputImageType >::DoIt( inputVolume.c_str() );
    
    LinearInterpolatorPointerType linearInterpolator = LinearInterpolatorType::New();
    NearestNeighborInterpolatorPointerType nearestNeighborInterpolator = NearestNeighborInterpolatorType::New();
    BSplineInterpolatorPointerType bsplineInterpolator = BSplineInterpolatorType::New();
    
    TransformPointerType transform = TransformType::New();
    transform->SetIdentity();
    
    ResampleFilterPointerType resampler = ResampleFilterType::New();
    
    resampler->SetInput( imIn );
    resampler->SetTransform( transform );
    
    if (interpolationType == "linear")
    {
        resampler->SetInterpolator( linearInterpolator );
    }
    else if (interpolationType == "nearestNeighbor")
    {
        resampler->SetInterpolator( nearestNeighborInterpolator );
    }
    else if (interpolationType == "bspline" )
    {
        resampler->SetInterpolator( bsplineInterpolator );
    }
    else
    {
        resampler->SetInterpolator( linearInterpolator );
    }
    
    
    SizeType sz = imIn->GetRequestedRegion().GetSize();
    SpacingType sp = imIn->GetSpacing();
    
    SizeType newSz;
    SpacingType newSp;
    if (rule == "newSize")
    {
        newSz[0] = (unsigned int)value[0];
        newSz[1] = (unsigned int)value[1];
        newSz[2] = (unsigned int)value[2];
        
        newSp[0] = (float)sz[0] * sp[0] / (float)newSz[0];
        newSp[1] = (float)sz[1] * sp[1] / (float)newSz[1];
        newSp[2] = (float)sz[2] * sp[2] / (float)newSz[2];
    }
    else if (rule == "newSpacing" )
    {
        newSp[0] = value[0];
        newSp[1] = value[1];
        newSp[2] = value[2];
        
        newSz[0] = (unsigned int)(sz[0] * sp[0] / newSp[0]);
        newSz[1] = (unsigned int)(sz[1] * sp[1] / newSp[1]);
        newSz[2] = (unsigned int)(sz[2] * sp[2] / newSp[2]);
    }
    else if (rule == "downsamplingFactor" )
    {
        newSz[0] = (unsigned int)(sz[0] / value[0]);
        newSz[1] = (unsigned int)(sz[1] / value[1]);
        newSz[2] = (unsigned int)(sz[2] / value[2]);
        
        newSp[0] = sp[0] * value[0];
        newSp[1] = sp[1] * value[1];
        newSp[2] = sp[2] * value[2];
    }
    
    DirectionType dir = imIn->GetDirection();
    PointType origin = imIn->GetOrigin();
    IndexType start;
    start.Fill(0);
    
    resampler->SetOutputOrigin ( origin );
    resampler->SetOutputSpacing ( newSp );
    resampler->SetOutputDirection ( dir);
    resampler->SetSize ( newSz );
    resampler->Update ();
    
    WriteImage< OutputImageType >::DoIt( outputVolume.c_str(), resampler->GetOutput(), (bool)compression );
    return EXIT_SUCCESS;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}