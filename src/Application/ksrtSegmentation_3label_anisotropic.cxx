#include <iostream>
#include <itkImage.h>
#include <itkGradientImageFilter.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtResample.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtDiffusion3D.h>

#include <Segmentation/ksrtAnisotropicThreeLabelSegmentation.h>

#include <itkPluginUtilities.h>
#include "ksrtSegmentation_3label_anisotropicCLP.h"

using namespace std;
using namespace itk;


int DoIt( int argc, char * argv[] )
{
    
    PARSE_ARGS;

    const unsigned int Dim = 3;
    typedef short								ShortPixelType;
    typedef Image< ShortPixelType, Dim >		ShortImageType;
    typedef ShortImageType::Pointer				ShortImagePointerType;

    typedef float								FloatPixelType;
    typedef Image< FloatPixelType, Dim >		FloatImageType;
    typedef FloatImageType::Pointer				FloatImagePointerType;
    
    typedef FloatImageType::RegionType          RegionType;
    typedef FloatImageType::SizeType			SizeType;
    typedef FloatImageType::SpacingType			SpacingType;
    typedef FloatImageType::DirectionType		DirectionType;
    typedef FloatImageType::IndexType			IndexType;
    typedef FloatImageType::PointType			PointType;

    FloatImagePointerType imFemurCost = ReadImage< FloatImageType >::DoIt( inputFemurCost.c_str() );
    FloatImagePointerType imBackgroundCost = ReadImage< FloatImageType >::DoIt( inputBackgroundCost.c_str() );
    FloatImagePointerType imTibiaCost = ReadImage< FloatImageType >::DoIt( inputTibiaCost.c_str() );
    
    SizeType cartSz = imFemurCost->GetRequestedRegion().GetSize();
    SpacingType cartSp = imFemurCost->GetSpacing();
    DirectionType cartDir = imFemurCost->GetDirection();
    PointType cartOrigin = imFemurCost->GetOrigin();
    IndexType start;
    start.Fill( 0 );
     
	FloatImagePointerType imGradXN = ReadImage< FloatImageType >::DoIt( inputNormX.c_str() );
	FloatImagePointerType imGradYN = ReadImage< FloatImageType >::DoIt( inputNormY.c_str() );
	FloatImagePointerType imGradZN = ReadImage< FloatImageType >::DoIt( inputNormZ.c_str() );

	ImageData< FloatPixelType > dataGradXN;
	dataGradXN.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    dataGradXN.SetData( imGradXN->GetBufferPointer() );

	ImageData< FloatPixelType > dataGradYN;
	dataGradYN.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    dataGradYN.SetData( imGradYN->GetBufferPointer() );

	ImageData< FloatPixelType > dataGradZN;
	dataGradZN.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    dataGradZN.SetData( imGradZN->GetBufferPointer() );
    
    FloatImagePointerType imG = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, g);
    FloatImagePointerType imD11 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD12 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD13 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD21 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD22 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD23 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD31 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD32 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    FloatImagePointerType imD33 = New3DImage< FloatImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 1);
    
    ImageData< FloatPixelType > G( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D11( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D12( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D13( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D21( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D22( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D23( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D31( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D32( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > D33( cartSz[0], cartSz[1], cartSz[2] );
    
    G.SetData( imG->GetBufferPointer() );
    D11.SetData( imD11->GetBufferPointer() );
    D12.SetData( imD12->GetBufferPointer() );
    D13.SetData( imD13->GetBufferPointer() );
    D21.SetData( imD21->GetBufferPointer() );
    D22.SetData( imD22->GetBufferPointer() );
    D23.SetData( imD23->GetBufferPointer() );
    D31.SetData( imD31->GetBufferPointer() );
    D32.SetData( imD32->GetBufferPointer() );
    D33.SetData( imD33->GetBufferPointer() );
    
    D11 *= alpha;
    D12 *= alpha;
    D13 *= alpha;
    D21 *= alpha;
    D22 *= alpha;
    D23 *= alpha;
    D31 *= alpha;
    D32 *= alpha;
    D33 *= alpha;
    D11 -= 1;
    D12 -= 1;
    D13 -= 1;
    D21 -= 1;
    D22 -= 1;
    D23 -= 1;
    D31 -= 1;
    D32 -= 1;
    D33 -= 1;
    D11 *= dataGradXN;
    D12 *= dataGradXN;
    D13 *= dataGradXN;
    D21 *= dataGradYN;
    D22 *= dataGradYN;
    D23 *= dataGradYN;
    D31 *= dataGradZN;
    D32 *= dataGradZN;
    D33 *= dataGradZN;
    D11 *= dataGradXN;
    D12 *= dataGradYN;
    D13 *= dataGradZN;
    D21 *= dataGradXN;
    D22 *= dataGradYN;
    D23 *= dataGradZN;
    D31 *= dataGradXN;
    D32 *= dataGradYN;
    D33 *= dataGradZN;
    D11 += 1;
    D22 += 1;
    D33 += 1;
    D11 *= G;
    D12 *= G;
    D13 *= G;
    D21 *= G;
    D22 *= G;
    D23 *= G;
    D31 *= G;
    D32 *= G;
    D33 *= G;

    AnisotropicThreeLabelSegmentation seg;
    seg.SetSize( cartSz[0], cartSz[1], cartSz[2] );
    seg.SetSpacing( cartSp[0], cartSp[1], cartSp[2] );
    
    seg.SetR0( imFemurCost->GetBufferPointer() );
    seg.SetR1( imBackgroundCost->GetBufferPointer() );
    seg.SetR2( imTibiaCost->GetBufferPointer() );
    
    seg.SetD11( imD11->GetBufferPointer() );
    seg.SetD12( imD12->GetBufferPointer() );
    seg.SetD13( imD13->GetBufferPointer() );
    seg.SetD21( imD21->GetBufferPointer() );
    seg.SetD22( imD22->GetBufferPointer() );
    seg.SetD23( imD23->GetBufferPointer() );
    seg.SetD31( imD31->GetBufferPointer() );
    seg.SetD32( imD32->GetBufferPointer() );
    seg.SetD33( imD33->GetBufferPointer() );

    FloatImagePointerType imU1  = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0.33 );
    FloatImagePointerType imU2  = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0.67 );
    
    FloatImagePointerType imPX1 = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imPY1 = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imPZ1 = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imPX2 = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imPY2 = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imPZ2 = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    
    FloatImagePointerType imQ0  = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imQ1  = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    FloatImagePointerType imQ2  = New3DImage< FloatImageType >::DoIt( cartSz, cartSp, cartDir, start, cartOrigin, 0 );
    
    seg.SetU1 ( imU1 ->GetBufferPointer() );
    seg.SetU2 ( imU2 ->GetBufferPointer() );
    seg.SetPX1( imPX1->GetBufferPointer() );
    seg.SetPX2( imPX2->GetBufferPointer() );
    seg.SetPY1( imPY1->GetBufferPointer() );
    seg.SetPY2( imPY2->GetBufferPointer() );
    seg.SetPZ1( imPZ1->GetBufferPointer() );
    seg.SetPZ2( imPZ2->GetBufferPointer() );
    seg.SetQ0 ( imQ0 ->GetBufferPointer() );
    seg.SetQ1 ( imQ1 ->GetBufferPointer() );
    seg.SetQ2 ( imQ2 ->GetBufferPointer() );
    
    seg.SetGapThreshold( 5e-3 );
    seg.SetNumberOfIterations( 1000 );
    seg.SetTimeStep( 0.1 );
    seg.SetDisplayFrequency( 10 );

    seg.DoIt();

	ShortImagePointerType imFemurSeg = New3DImage< ShortImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 0);
    ShortImagePointerType imTibiaSeg= New3DImage< ShortImageType >::DoIt(cartSz, cartSp, cartDir, start, cartOrigin, 0);
    
    ImageData< ShortPixelType > dataFemurSeg( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< ShortPixelType > dataTibiaSeg( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > dataU1 ( cartSz[0], cartSz[1], cartSz[2] );
    ImageData< FloatPixelType > dataU2 ( cartSz[0], cartSz[1], cartSz[2] );
    
	dataFemurSeg.SetData( imFemurSeg->GetBufferPointer() );
	dataTibiaSeg.SetData( imTibiaSeg->GetBufferPointer() );
	dataU1.SetData( imU1->GetBufferPointer() );
	dataU2.SetData( imU2->GetBufferPointer() );

    for (unsigned int k = 0; k < cartSz[2]; k++)
    {
        for (unsigned int j = 0; j < cartSz[1]; j++)
        {
            for (unsigned int i = 0; i < cartSz[0]; i++)
            {
                float diff0 = dataU1(i,j,k);
                float diff1 = dataU2(i,j,k) - dataU1(i,j,k);
                float diff2 = 1 - dataU2(i,j,k);
                
                dataFemurSeg(i,j,k) = (short)(diff0 > diff1 && diff0 > diff2);
                dataTibiaSeg(i,j,k) = (short)(diff2 > diff0 && diff2 > diff1);
            }
        }
    }

	WriteImage< ShortImageType >::DoIt( outputFemurSegmentation.c_str(), imFemurSeg, (bool)compression );
	WriteImage< ShortImageType >::DoIt( outputTibiaSegmentation.c_str(), imTibiaSeg, (bool)compression );

    return 0;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;

    ImageIOBase::IOPixelType pixelType_femur;
    ImageIOBase::IOComponentType componentType_femur;
    
    ImageIOBase::IOPixelType pixelType_background;
    ImageIOBase::IOComponentType componentType_background;
    
    ImageIOBase::IOPixelType pixelType_tibia;
    ImageIOBase::IOComponentType componentType_tibia;
    
    ImageIOBase::IOPixelType pixelType_NX;
    ImageIOBase::IOComponentType componentType_NX;
    
    ImageIOBase::IOPixelType pixelType_NY;
    ImageIOBase::IOComponentType componentType_NY;
    
    ImageIOBase::IOPixelType pixelType_NZ;
    ImageIOBase::IOComponentType componentType_NZ;
    
    try {
        GetImageType (inputFemurCost, pixelType_femur, componentType_femur);
        GetImageType (inputBackgroundCost, pixelType_background, componentType_background);
        GetImageType (inputTibiaCost, pixelType_tibia, componentType_tibia);
        
        GetImageType (inputNormX, pixelType_NX, componentType_NX);
        GetImageType (inputNormY, pixelType_NY, componentType_NY);
        GetImageType (inputNormZ, pixelType_NZ, componentType_NZ);
        
        if (componentType_femur != ImageIOBase::FLOAT || componentType_background != ImageIOBase::FLOAT ||
            componentType_tibia != ImageIOBase::FLOAT || componentType_NX != ImageIOBase::FLOAT    ||
            componentType_NY != ImageIOBase::FLOAT    || componentType_NZ != ImageIOBase::FLOAT )
        {
            std::cerr << "Input images must have float type!" << std::endl;
            return EXIT_FAILURE;
        }
        else
        {
            return DoIt( argc, argv );
        }
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
