#include <itkImage.h>

#include <Common/ksrtCast.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtGaussianConvolution.h>

#include <Common/ksrtImageData.h>

#include <fstream>
#include <iostream>


#include <itkPluginUtilities.h>
#include "ksrtExtractTestingFeatures_SVMCLP.h"

using namespace std;
using namespace itk;

template<class T, class TMask> 
int DoIt( int argc, char * argv[], T, TMask )
{
    PARSE_ARGS;


    const unsigned int Dim = 3;
    
    typedef Image< T, Dim >                         InputImageType;
    typedef Image< TMask, Dim >                     InputMaskType;
    
    typedef short                                   ShortPixelType;
    typedef Image< ShortPixelType, Dim >            ShortImageType;
    typedef ShortImageType::Pointer                 ShortImagePointerType;

    typedef float                                   FloatPixelType;
    typedef Image< FloatPixelType, Dim >            FloatImageType;
    typedef FloatImageType::Pointer                 FloatImagePointerType;

    typedef FloatImageType::IndexType               IndexType;
    typedef FloatImageType::SizeType                SizeType;

	FloatImagePointerType im = Cast< InputImageType, FloatImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputImage.c_str() ) ); 

	FloatImagePointerType imMask1 =Cast< InputMaskType, FloatImageType >::DoIt( ReadImage< InputMaskType >::DoIt( inputMaskA.c_str() ) ); 
 
	FloatImagePointerType imMask2 =Cast< InputMaskType, FloatImageType >::DoIt( ReadImage< InputMaskType >::DoIt( inputMaskB.c_str() ) ); 
	
	SizeType sz = im->GetRequestedRegion().GetSize();

	ImageData< FloatPixelType > dataMask1( sz[0], sz[1], sz[2] );
    dataMask1.SetData( imMask1->GetBufferPointer() );
	
	ImageData< FloatPixelType > dataMask2( sz[0], sz[1], sz[2] );
    dataMask2.SetData( imMask2->GetBufferPointer() );

	float sigma1 = scales[0];
    float sigma2 = scales[1];
    float sigma3 = scales[2];

	FloatImagePointerType imS1 = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 0, sigma1, sigma1, sigma1 );
	FloatImagePointerType imS2 = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 0, sigma2, sigma2, sigma2 );
    FloatImagePointerType imS3 = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 0, sigma3, sigma3, sigma3 );

    FloatImagePointerType imS1x = GaussianConvolution< FloatImageType >::DoIt( im, 1, 0, 0, sigma1, sigma1, sigma1 );
    FloatImagePointerType imS1y = GaussianConvolution< FloatImageType >::DoIt( im, 0, 1, 0, sigma1, sigma1, sigma1 );
    FloatImagePointerType imS1z = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 1, sigma1, sigma1, sigma1 );

    FloatImagePointerType imS2x = GaussianConvolution< FloatImageType >::DoIt( im, 1, 0, 0, sigma2, sigma2, sigma2 ); 
    FloatImagePointerType imS2y = GaussianConvolution< FloatImageType >::DoIt( im, 0, 1, 0, sigma2, sigma2, sigma2 );
    FloatImagePointerType imS2z = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 1, sigma2, sigma2, sigma2 );

    FloatImagePointerType imS3x = GaussianConvolution< FloatImageType >::DoIt( im, 1, 0, 0, sigma3, sigma3, sigma3 );
    FloatImagePointerType imS3y = GaussianConvolution< FloatImageType >::DoIt( im, 0, 1, 0, sigma3, sigma3, sigma3 );
    FloatImagePointerType imS3z = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 1, sigma3, sigma3, sigma3 );

    FloatImagePointerType imS1xx = GaussianConvolution< FloatImageType >::DoIt( im, 2, 0, 0, sigma1, sigma1, sigma1 );
    FloatImagePointerType imS1yy = GaussianConvolution< FloatImageType >::DoIt( im, 0, 2, 0, sigma1, sigma1, sigma1 );
    FloatImagePointerType imS1zz = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 2, sigma1, sigma1, sigma1 );

    FloatImagePointerType imS1xy = GaussianConvolution< FloatImageType >::DoIt( im, 1, 1, 0, sigma1, sigma1, sigma1 );
    FloatImagePointerType imS1xz = GaussianConvolution< FloatImageType >::DoIt( im, 1, 0, 1, sigma1, sigma1, sigma1 );
    FloatImagePointerType imS1yz = GaussianConvolution< FloatImageType >::DoIt( im, 0, 1, 1, sigma1, sigma1, sigma1 );

    FloatImagePointerType imS2xx = GaussianConvolution< FloatImageType >::DoIt( im, 2, 0, 0, sigma2, sigma2, sigma2 );
    FloatImagePointerType imS2yy = GaussianConvolution< FloatImageType >::DoIt( im, 0, 2, 0, sigma2, sigma2, sigma2 );
    FloatImagePointerType imS2zz = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 2, sigma2, sigma2, sigma2 );

    FloatImagePointerType imS2xy = GaussianConvolution< FloatImageType >::DoIt( im, 1, 1, 0, sigma2, sigma2, sigma2 );
    FloatImagePointerType imS2xz = GaussianConvolution< FloatImageType >::DoIt( im, 1, 0, 1, sigma2, sigma2, sigma2 );
    FloatImagePointerType imS2yz = GaussianConvolution< FloatImageType >::DoIt( im, 0, 1, 1, sigma2, sigma2, sigma2 );

    FloatImagePointerType imS3xx = GaussianConvolution< FloatImageType >::DoIt( im, 2, 0, 0, sigma3, sigma3, sigma3 );
    FloatImagePointerType imS3yy = GaussianConvolution< FloatImageType >::DoIt( im, 0, 2, 0, sigma3, sigma3, sigma3 );
    FloatImagePointerType imS3zz = GaussianConvolution< FloatImageType >::DoIt( im, 0, 0, 2, sigma3, sigma3, sigma3 );

    FloatImagePointerType imS3xy = GaussianConvolution< FloatImageType >::DoIt( im, 1, 1, 0, sigma3, sigma3, sigma3 );
    FloatImagePointerType imS3xz = GaussianConvolution< FloatImageType >::DoIt( im, 1, 0, 1, sigma3, sigma3, sigma3 );
    FloatImagePointerType imS3yz = GaussianConvolution< FloatImageType >::DoIt( im, 0, 1, 1, sigma3, sigma3, sigma3 );

    ImageData< FloatPixelType > data( sz[0], sz[1], sz[2] );
    data.SetData( im->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1( sz[0], sz[1], sz[2] );
    dataS1.SetData( imS1->GetBufferPointer() );
        
    ImageData< FloatPixelType > dataS2( sz[0], sz[1], sz[2] );
    dataS2.SetData( imS2->GetBufferPointer() );

    ImageData< FloatPixelType > dataS3( sz[0], sz[1], sz[2] );
    dataS3.SetData( imS3->GetBufferPointer() );

    ImageData< FloatPixelType > dataS1x( sz[0], sz[1], sz[2] );
    dataS1x.SetData( imS1x->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1y( sz[0], sz[1], sz[2] );
    dataS1y.SetData( imS1y->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1z( sz[0], sz[1], sz[2] );
    dataS1z.SetData( imS1z->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2x( sz[0], sz[1], sz[2] );
    dataS2x.SetData( imS2x->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2y( sz[0], sz[1], sz[2] );
    dataS2y.SetData( imS2y->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2z( sz[0], sz[1], sz[2] );
    dataS2z.SetData( imS2z->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3x( sz[0], sz[1], sz[2] );
    dataS3x.SetData( imS3x->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3y( sz[0], sz[1], sz[2] );
    dataS3y.SetData( imS3y->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3z( sz[0], sz[1], sz[2] );
    dataS3z.SetData( imS3z->GetBufferPointer() );
   
    ImageData< FloatPixelType > dataS1xx( sz[0], sz[1], sz[2] );
    dataS1xx.SetData( imS1xx->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1yy( sz[0], sz[1], sz[2] );
    dataS1yy.SetData( imS1yy->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1zz( sz[0], sz[1], sz[2] );
    dataS1zz.SetData( imS1zz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1xy( sz[0], sz[1], sz[2] );
    dataS1xy.SetData( imS1xy->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1xz( sz[0], sz[1], sz[2] );
    dataS1xz.SetData( imS1xz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS1yz( sz[0], sz[1], sz[2] );
    dataS1yz.SetData( imS1yz->GetBufferPointer() );
   
    ImageData< FloatPixelType > dataS2xx( sz[0], sz[1], sz[2] );
    dataS2xx.SetData( imS2xx->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2yy( sz[0], sz[1], sz[2] );
    dataS2yy.SetData( imS2yy->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2zz( sz[0], sz[1], sz[2] );
    dataS2zz.SetData( imS2zz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2xy( sz[0], sz[1], sz[2] );
    dataS2xy.SetData( imS2xy->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2xz( sz[0], sz[1], sz[2] );
    dataS2xz.SetData( imS2xz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS2yz( sz[0], sz[1], sz[2] );
    dataS2yz.SetData( imS2yz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3xx( sz[0], sz[1], sz[2] );
    dataS3xx.SetData( imS3xx->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3yy( sz[0], sz[1], sz[2] );
    dataS3yy.SetData( imS3yy->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3zz( sz[0], sz[1], sz[2] );
    dataS3zz.SetData( imS3zz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3xy( sz[0], sz[1], sz[2] );
    dataS3xy.SetData( imS3xy->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3xz( sz[0], sz[1], sz[2] );
    dataS3xz.SetData( imS3xz->GetBufferPointer() );
    
    ImageData< FloatPixelType > dataS3yz( sz[0], sz[1], sz[2] );
    dataS3yz.SetData( imS3yz->GetBufferPointer() );

	unsigned int numOfCandidates = 0;
    unsigned int numOfFeatures = 15;
	
	float maskThreshold = atof( argv[4] );
    
	for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                if ( dataMask1(i,j,k) > threshold || dataMask2(i,j,k) > threshold )
                {
                    numOfCandidates ++;
                }
            }
        }
    }

    ofstream outFeature( outputFeatures.c_str() );
    
    if ( ! outFeature.is_open() )
    {
        cout << "Open " << argv[8] << " failed. " << endl;
        exit( 1 );
    }

	float ** features = new float *[ numOfFeatures ];
    for (unsigned int i = 0; i < numOfFeatures; i++)
        features[i] = new float [numOfCandidates];

    unsigned int idx = 0;
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                if ( dataMask1(i,j,k) > maskThreshold || dataMask2(i,j,k) > maskThreshold )
                {
                    features[0][idx] = dataS1(i,j,k);
                    features[1][idx] = dataS2(i,j,k);
                    features[2][idx] = dataS3(i,j,k);
                    features[3][idx] = dataS1x(i,j,k);
                    features[4][idx] = dataS2x(i,j,k);
                    features[5][idx] = dataS3x(i,j,k);
                    features[6][idx] = dataS1y(i,j,k);
                    features[7][idx] = dataS2y(i,j,k);
                    features[8][idx] = dataS3y(i,j,k);
                    features[9][idx] = dataS1z(i,j,k);
                    features[10][idx] = dataS2z(i,j,k);
                    features[11][idx] = dataS3z(i,j,k);
                    features[12][idx] = dataS1yy(i,j,k);
                    features[13][idx] = dataS2yy(i,j,k);
                    features[14][idx] = dataS3yy(i,j,k);
                    idx ++;
                }
            }
        }
        
    }

    for (unsigned int i = 0; i < numOfFeatures; i++)
    {
        // compute the average of a feature
        float avg = 0;
        for (unsigned int j = 0; j < numOfCandidates; j++)
        {
            avg += features[i][j];
        }
        avg /= numOfCandidates;
            
        // compute the standard deviation of a feature
        float sumOfSquares = 0;
        for (unsigned int j = 0; j < numOfCandidates; j++)
        {
            sumOfSquares += pow(features[i][j] - avg, 2);
        }
        float stdev = sqrt( sumOfSquares/numOfCandidates ); 

        for (unsigned int j = 0; j < numOfCandidates; j++)
        {
            features[i][j] -= avg;
            features[i][j] /= stdev;
        }
    }

    for (unsigned int j = 0; j < numOfCandidates; j++)
    {
        outFeature << 0 << " ";
        for (unsigned int i = 0; i < numOfFeatures-1; i++)
        {
            outFeature << i+1 << ":" << features[i][j] << " ";
        }
        outFeature << numOfFeatures << ":" << features[numOfFeatures-1][j] << "\n";
    }
    
	outFeature.close();
    delete features;
    
    return 0;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    ImageIOBase::IOPixelType pixelType_maskA;
    ImageIOBase::IOComponentType componentType_maskA;
    
    ImageIOBase::IOPixelType pixelType_maskB;
    ImageIOBase::IOComponentType componentType_maskB;
    
    
    try
    {
        GetImageType (inputImage, pixelType, componentType);
        GetImageType (inputMaskA, pixelType_maskA, componentType_maskA);
        GetImageType (inputMaskB, pixelType_maskB, componentType_maskB);
        
        if (componentType_maskA != componentType_maskB)
        {
            std::cerr << "Two masks must have the same type!" << std::endl;
            return EXIT_FAILURE;
        }
        switch (componentType) {
            case ImageIOBase::UCHAR:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::CHAR:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::USHORT:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::SHORT:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::UINT:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::INT:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::ULONG:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::LONG:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::FLOAT:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::DOUBLE:
                switch (componentType_maskA) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            default:
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
