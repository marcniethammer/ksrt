#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkRawImageIO.h>
#include <itkImageRegionIteratorWithIndex.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>


#include <itkPluginUtilities.h>
#include "ksrtComputeBoneProbabilityUsing1DIntensityHistogramCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

using namespace std;
using namespace itk;

template<class T1> int DoIt( int argc, char * argv[], T1 )
{
    
    PARSE_ARGS;
    
    typedef Image< T1, 3 >                  T1ImageType;
    
    FloatImagePointerType imT1 = Cast< T1ImageType, FloatImageType >::DoIt( ReadImage< T1ImageType >::DoIt( inputT1.c_str() ) );
    
    SizeType sz = imT1->GetRequestedRegion().GetSize();
    SpacingType sp = imT1->GetSpacing();
    DirectionType dir = imT1->GetDirection();
    PointType origin = imT1->GetOrigin();
    IndexType start;
    start.Fill(0);

    float maxT1 = 0;
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType tmp_idx;
                tmp_idx[0] = i;
                tmp_idx[1] = j;
                tmp_idx[2] = k;
                
                if ( imT1->GetPixel( tmp_idx ) > maxT1 )
                    maxT1 = imT1->GetPixel( tmp_idx );
            }
        }
    }
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType tmp_idx;
                tmp_idx[0] = i;
                tmp_idx[1] = j;
                tmp_idx[2] = k;
                    
                imT1->SetPixel( tmp_idx, imT1->GetPixel(tmp_idx)/maxT1*99 );
            }
        }
    }
    
    FILE * file = fopen( inputHistogram.c_str(), "r");
    float * hist = new float[100];
    fread(hist, 100, sizeof(float), file);
    fclose(file);
//    int dim_hist[1] = {100};
//    float spacing_hist[1] = {1.0};
//    int origin_hist[1] = {0};
//    
//    typedef Image< float, 1 >                 HistogramType;
//    ImageFileReader< HistogramType >::Pointer reader = ImageFileReader< HistogramType >::New();
//    RawImageIO< float, 1 >::Pointer rawIO = RawImageIO< float, 1 >::New();
//    
//    rawIO->SetFileName( inputHistogram.c_str() );
//    
//    for ( unsigned int i = 0; i < 1; i++ )	
//    {		
//        rawIO->SetDimensions( i, dim_hist[i] );		
//        rawIO->SetSpacing( i, spacing_hist[i] );
//		rawIO->SetOrigin( i, origin_hist[i] );	
//    }
//    rawIO->SetByteOrderToLittleEndian();
//	rawIO->SetPixelType( itk::ImageIOBase::SCALAR );	
//    rawIO->SetNumberOfComponents(1);	
//    reader->SetImageIO( rawIO );
//	reader->SetFileName( inputHistogram.c_str() );	
//    try{		
//        reader->Update();	
//    }catch( ExceptionObject & e ){
//        cerr << e.GetDescription() << endl;
//    }	
//    HistogramType::Pointer pBoneHist = reader->GetOutput();
    
    FloatImagePointerType imPb = New3DImage< FloatImageType >::DoIt( sz, sp, dir, start, origin, 0.0);
    
    ImageRegionIteratorWithIndex< FloatImageType > iter( imPb, imPb->GetRequestedRegion() );
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                float valueT1 = imT1->GetPixel( idx );
                int hist_idx = (int)floor(valueT1 + 0.5);
                imPb->SetPixel( idx, hist[ hist_idx ] );
            }
        }
    }
    
    WriteImage< FloatImageType >::DoIt( outputBoneProbability.c_str(), imPb, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    ImageIOBase::IOPixelType pixelType_T1;
    ImageIOBase::IOComponentType componentType_T1;
        
    try
    {
        GetImageType (inputT1, pixelType_T1, componentType_T1);
         
        switch (componentType_T1) {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0) );
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0) );
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0) );
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0) );
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0) );
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0) );
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0) );
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0) );
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0) );
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0) );
                break;
            default:
                break;
        }
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
