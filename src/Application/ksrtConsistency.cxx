#include <iostream>
#include <fstream>

#include <itkImage.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>


#include <itkPluginUtilities.h>
#include "ksrtConsistencyCLP.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace itk;


int DoIt( int argc, char * argv[] )
{
    
    PARSE_ARGS;

    const unsigned int Dim = 3;
    typedef short								ShortPixelType;
    typedef Image< ShortPixelType, Dim >		ShortImageType;
    typedef ShortImageType::Pointer				ShortImagePointerType;

    typedef float								FloatPixelType;
    typedef Image< FloatPixelType, Dim >		FloatImageType;
    typedef FloatImageType::Pointer				FloatImagePointerType;
    
    typedef FloatImageType::RegionType          RegionType;
    typedef FloatImageType::SizeType			SizeType;
    typedef FloatImageType::SpacingType			SpacingType;
    typedef FloatImageType::DirectionType		DirectionType;
    typedef FloatImageType::IndexType			IndexType;
    typedef FloatImageType::PointType			PointType;

    ifstream inList;
    inList.open( inputList.c_str() );
    
    if ( ! inList.is_open())
    {
        cout << "Can't open file " << inputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }
    
    unsigned int numOfVisits = 0;
    string tmp;
    while (getline(inList,tmp)) {
        numOfVisits++;
    }
    inList.close();
    
    if (numOfVisits == 1)
    {
        ofstream outText( output.c_str(), fstream::out | fstream::app );
        if ( ! outText.is_open() )
        {
            cerr << "Open " << output.c_str() << " failed!" << endl;
            exit(1);
        }
        outText << inputList.c_str() << "\t" << 0 << "\t" << 0 << "\n";
        return 0;
    }
        
    inList.open( inputList.c_str() );
    
    if ( ! inList.is_open())
    {
        cout << "Can't open file " << inputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }
    
    float * times = new float[ numOfVisits ];
    
    string inputFemSegPrev;
    string inputTibSegPrev;
    string inputTime;
    
    inList >> inputFemSegPrev;
    inList >> inputTibSegPrev;
    inList >> inputTime;
    
    FloatImagePointerType imFemSegPrev = ReadImage< FloatImageType >::DoIt( inputFemSegPrev.c_str() );
    FloatImagePointerType imTibSegPrev = ReadImage< FloatImageType >::DoIt( inputTibSegPrev.c_str() );
    
    SizeType sz = imFemSegPrev->GetRequestedRegion().GetSize();
    SpacingType sp =  imFemSegPrev->GetSpacing();
    DirectionType dir = imFemSegPrev->GetDirection();
    PointType origin = imFemSegPrev->GetOrigin();
    IndexType start;
    start.Fill(0);
 
    unsigned int t = 0;
    times[t] = atof(inputTime.c_str());
    
    string inputFemSeg;
    string inputTibSeg;
    
    FloatImagePointerType imFemSeg = ReadImage< FloatImageType >::DoIt( inputFemSegPrev.c_str() );
    FloatImagePointerType imTibSeg = ReadImage< FloatImageType >::DoIt( inputTibSegPrev.c_str() );
    
    double femTC = 0;
    double tibTC = 0;
    t = 1;
    while (t < numOfVisits)
    {
        inList >> inputFemSeg;
        inList >> inputTibSeg;
        inList >> inputTime;
        
        imFemSeg = ReadImage< FloatImageType >::DoIt( inputFemSeg.c_str() );
        imTibSeg = ReadImage< FloatImageType >::DoIt( inputTibSeg.c_str() );
        
        times[t] = atof(inputTime.c_str());
        
        double femCount = 0;
        for (unsigned int k = 0; k < sz[2]; k++)
        {
            for (unsigned int j = 0; j < sz[1]; j++)
            {
                for (unsigned int i = 0; i < sz[0]; i++)
                {
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    int binSeg = (int)(imFemSeg->GetPixel(idx)>0.5);
					int binSegPrev = (int)(imFemSegPrev->GetPixel(idx)>0.5);
					femCount += abs(binSeg-binSegPrev);
                }
            }
        }
        femTC += femCount / (times[t]-times[t-1]);
        
        double tibCount = 0;
        for (unsigned int k = 0; k < sz[2]; k++)
        {
            for (unsigned int j = 0; j < sz[1]; j++)
            {
                for (unsigned int i = 0; i < sz[0]; i++)
                {
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                	int binSeg = (int)(imTibSeg->GetPixel(idx)>0.5);
					int binSegPrev = (int)(imTibSegPrev->GetPixel(idx)>0.5);
					tibCount += abs(binSeg-binSegPrev);

				}
            }
        }
        tibTC += tibCount / (times[t]-times[t-1]);
        
        t++;
        imFemSegPrev = imFemSeg;
        imTibSegPrev = imTibSeg;
        
    }
    
    ofstream outText( output.c_str(), fstream::out | fstream::app );
    if ( ! outText.is_open() )
    {
        cerr << "Open " << output.c_str() << " failed!" << endl;
        exit(1);
    }
    outText << inputList.c_str() << "\t" << femTC << "\t" << tibTC << "\n";
    
    inList.close();
    
    delete [] times;
    
    return 0;
    
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    
    return DoIt( argc, argv );
}
