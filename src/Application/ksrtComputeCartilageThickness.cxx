#include <itkImage.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtImageData.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtLaplaceThickness.h>

#include <itkImageRegionIterator.h>
#include <itkPoint.h>
#include <itkBinaryBallStructuringElement.h>
#include <itkBinaryErodeImageFilter.h>
#include <itkBinaryDilateImageFilter.h>
#include <itkBinaryThresholdImageFilter.h> 
#include <itkSubtractImageFilter.h>
#include <itkResampleImageFilter.h>
#include <itkNearestNeighborInterpolateImageFunction.h>
#include <itkGrayscaleFillholeImageFilter.h>

#include <ANN/ANN.h>

#include <itkPluginUtilities.h>

#include "ksrtComputeCartilageThicknessCLP.h"

using namespace std;
using namespace itk;

int DoIt( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    typedef Image< short, 3 >                           ShortImageType;
    typedef ShortImageType::Pointer                     ShortImagePointerType;
    
    typedef Image< float, 3 >                           FloatImageType;
    typedef FloatImageType::Pointer                     FloatImagePointerType;
    
    typedef ImageRegionIterator< ShortImageType >       IteratorType;
    typedef BinaryBallStructuringElement< short, 3 >    StructuringType;
    typedef BinaryErodeImageFilter< ShortImageType, 
                                    ShortImageType, 
                                    StructuringType >   ErodeFilterType;
	typedef BinaryDilateImageFilter< ShortImageType,
									 ShortImageType,
									 StructuringType >	DilateFilterType;
    typedef SubtractImageFilter< ShortImageType >       SubFilterType;
    
    typedef ResampleImageFilter< ShortImageType, 
                                 ShortImageType >       ResampleFilterType;
    typedef NearestNeighborInterpolateImageFunction<
                                    ShortImageType >    InterpolatorType;
    typedef GrayscaleFillholeImageFilter< 
                                    ShortImageType, 
                                    ShortImageType >    FillHoleFilterType;
    
    typedef ShortImageType::SpacingType                 SpacingType;
    typedef ShortImageType::SizeType                    SizeType;
	typedef ShortImageType::DirectionType				DirectionType;
	typedef ShortImageType::IndexType					IndexType;
	typedef ShortImageType::PointType					PointType;

    StructuringType structuringBall;
    structuringBall.SetRadius( 1 );
    structuringBall.CreateStructuringElement();

    ShortImagePointerType imCart = ReadImage< ShortImageType >::DoIt( inputVolume.c_str() );
    
    FillHoleFilterType::Pointer fillCartFilter = FillHoleFilterType::New();
    fillCartFilter->SetInput( imCart );
	fillCartFilter->Update();
	imCart = fillCartFilter->GetOutput();
    SpacingType sp = imCart->GetSpacing();
    SizeType sz = imCart->GetLargestPossibleRegion().GetSize();
	DirectionType dir = imCart->GetDirection();
	PointType origin = imCart->GetOrigin();

	DilateFilterType::Pointer dilatecartFilter = DilateFilterType::New();
	dilatecartFilter->SetInput( imCart );
	dilatecartFilter->SetKernel( structuringBall );
	dilatecartFilter->SetDilateValue( 1 );
	dilatecartFilter->SetBackgroundValue( 0 );
	dilatecartFilter->Update();

	ShortImagePointerType imCartDilate = dilatecartFilter->GetOutput();
	
	SubFilterType::Pointer subcartFilter = SubFilterType::New();
    subcartFilter->SetInput2( imCart );
    subcartFilter->SetInput1( imCartDilate );
    subcartFilter->UpdateLargestPossibleRegion();
    ShortImagePointerType imCartOut = subcartFilter->GetOutput();

	ImageData< short > cart;
	cart.SetData( imCart->GetBufferPointer() );
	cart.SetSize( sz[0], sz[1], sz[2] );

	ImageData< short > cartOut;
	cartOut.SetData( imCartOut->GetBufferPointer() );
	cartOut.SetSize( sz[0], sz[1], sz[2] );

	IndexType start;
	start.Fill( 0 );
	ShortImagePointerType imCartUpper = New3DImage< ShortImageType >::DoIt( sz, sp, dir, start, origin, 0 );
	ShortImagePointerType imCartLower = New3DImage< ShortImageType >::DoIt( sz, sp, dir, start, origin, 0 );

	ImageData< short > cartUpper;
	cartUpper.SetData( imCartUpper->GetBufferPointer() );
	cartUpper.SetSize( sz[0], sz[1], sz[2] );

	ImageData< short > cartLower;
	cartLower.SetData( imCartLower->GetBufferPointer() );
	cartLower.SetSize( sz[0], sz[1], sz[2] );

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 1; j < sz[1]-1; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				cartUpper(i,j,k) = cart(i,j+1,k) - cart(i,j,k);
				cartLower(i,j,k) = cart(i,j,k) - cart(i,j+1,k);
			}
		}
	}
	
	unsigned int numCartUpper = 0;
	unsigned int numCartLower = 0;

	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 1; j < sz[1]-1; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				numCartUpper = (cartUpper(i,j,k) == 1) ? numCartUpper + 1 : numCartUpper;
				numCartLower = (cartLower(i,j,k) == 1) ? numCartLower + 1 : numCartLower;
			}
		}
	}

	ANNpointArray cartUpperPoints = annAllocPts( numCartUpper, 3 );
	ANNpointArray cartLowerPoints = annAllocPts( numCartLower, 3 );

	IteratorType itCartUpper( imCartUpper, imCartUpper->GetLargestPossibleRegion() );
	IteratorType itCartLower( imCartLower, imCartLower->GetLargestPossibleRegion() );

	numCartUpper = 0;
	for (itCartUpper.GoToBegin(); !itCartUpper.IsAtEnd(); ++itCartUpper)
	{
		if (itCartUpper.Get() == 1)
		{
			PointType point;
			imCartUpper->TransformIndexToPhysicalPoint( itCartUpper.GetIndex(), point );
			for (unsigned int d = 0; d < 3; d++)
			{
				cartUpperPoints[numCartUpper][d] = point[d];
			}
			numCartUpper++;
		}
	}
	ANNkd_tree * cartUpperTree = new ANNkd_tree( cartUpperPoints, numCartUpper, 3 );

	numCartLower = 0;
	for (itCartLower.GoToBegin(); !itCartLower.IsAtEnd(); ++itCartLower)
	{
		if (itCartLower.Get() == 1)
		{
			PointType point;
			
			imCartLower->TransformIndexToPhysicalPoint( itCartLower.GetIndex(), point );
			for (unsigned int d = 0; d < 3; d++)
			{
				cartLowerPoints[numCartLower][d] = point[d];
			}
			numCartLower++;
		}
	}
	ANNkd_tree * cartLowerTree = new ANNkd_tree( cartLowerPoints, numCartLower, 3 );

	ANNidxArray nnIdxUpper = new ANNidx[1];
	ANNdistArray distsUpper = new ANNdist[1];

	ANNidxArray nnIdxLower = new ANNidx[1];
	ANNdistArray distsLower = new ANNdist[1];

	IteratorType itCartOut( imCartOut, imCartOut->GetLargestPossibleRegion() );
	for (itCartOut.GoToBegin(); !itCartOut.IsAtEnd(); ++itCartOut)
	{
		if (itCartOut.Get() == 1)
		{
			PointType point;
			ANNpoint annpoint = annAllocPt( 3 );
			imCartOut->TransformIndexToPhysicalPoint( itCartOut.GetIndex(), point );
			for (unsigned int d = 0; d < 3; d++)
			{
				annpoint[d] = point[d];
			}
			cartUpperTree->annkSearch( annpoint,1, nnIdxUpper, distsUpper );
			cartLowerTree->annkSearch( annpoint,1, nnIdxLower, distsLower );
			if (distsLower[0] > distsUpper[0])
			{
				itCartOut.Set( 2 );
			}
			annDeallocPt( annpoint );
		}
	}

	annDeallocPts( cartUpperPoints );
	annDeallocPts( cartLowerPoints );
	delete cartUpperTree;
	delete cartLowerTree;
	delete [] nnIdxUpper;
	delete [] nnIdxLower;
	delete [] distsUpper;
	delete [] distsLower;
    
	FloatImagePointerType imCart_float = Cast< ShortImageType, FloatImageType >::DoIt( imCart );
	FloatImagePointerType imCartOut_float = Cast< ShortImageType, FloatImageType >::DoIt( imCartOut );
    LaplaceDistance thickness;
	thickness.SetData( imCartOut_float->GetBufferPointer() );
	thickness.SetSolution( imCart_float->GetBufferPointer() );
	thickness.SetSize( sz[0], sz[1], sz[2] );
	thickness.SetSpacing( sp[0], sp[1], sp[2] );
	thickness.SetTimeStep( 0.2 );
	thickness.DoIt();

	WriteImage< FloatImageType >::DoIt( outputVolume.c_str(), imCart_float, (bool)compression );
	return 0;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    return DoIt( argc, argv );

}