#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkImageFileReader.h>
#include <itkTransformFileWriter.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>

#include <Registration/ksrtAffineRegistration.h>
#include <Registration/ksrtJointAffineRegistration.h>

#include <itkPluginUtilities.h>
#include "ksrtAlignBoneShapeModelCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

typedef AffineTransform< double, 3 >                    AffineTransformType;
typedef AffineTransformType::Pointer                    AffineTransformPointerType;
typedef Array< double >									ParametersType;

typedef ResampleImageFilter< FloatImageType, 
                             FloatImageType, 
                             double >                   ResamplerType;
typedef ResamplerType::Pointer                          ResamplerPointerType;
typedef LinearInterpolateImageFunction< 
                        FloatImageType, 
                        double >                        InterpolatorType;
typedef InterpolatorType::Pointer						InterpolatorPointerType;
typedef InterpolatorType::ContinuousIndexType           ContinuousIndexType;

FloatImagePointerType applyAffineTransform( FloatImagePointerType imMoving, AffineTransformPointerType transform, SizeType sz, SpacingType sp, DirectionType dir, PointType origin);

using namespace std;
using namespace itk;

template< class TBone, class TShape >
int DoIt( int argc, char * argv[], TBone, TShape )
{
    
    PARSE_ARGS;
    
    typedef Image< TBone, 3 >           TBoneImage;
    typedef Image< TShape, 3 >          TShapeImage;
    
    FloatImagePointerType pBone = Cast< TBoneImage, FloatImageType >::DoIt( ReadImage< TBoneImage >::DoIt( inputBoneProbability.c_str() ) );
    FloatImagePointerType shapeBone = Cast< TShapeImage, FloatImageType >::DoIt( ReadImage< TShapeImage >::DoIt( inputBoneShape.c_str() ) );
    FloatImagePointerType shapeFemur = Cast< TShapeImage, FloatImageType >::DoIt( ReadImage< TShapeImage >::DoIt( inputFemurShape.c_str() ) );
    FloatImagePointerType shapeTibia = Cast< TShapeImage, FloatImageType >::DoIt( ReadImage< TShapeImage >::DoIt( inputTibiaShape.c_str() ) );
    
    AffineRegistration affineRegistration;
    affineRegistration.SetFixedImage( pBone );
    affineRegistration.SetMovingImage( shapeBone );
    affineRegistration.SetRelaxationFactor( 0.5 );
    affineRegistration.SetNumberOfSamples( 50000 );
    affineRegistration.SetNumberOfIterations( 500 );
    affineRegistration.SetMaxStepLength(0.2);
    affineRegistration.SetTranslationScale( 1.0/700 );
    
    affineRegistration.DoIt();
    
    JointAffineRegistration jointAffineRegistration;
    jointAffineRegistration.SetMovingImageA( shapeFemur );
    jointAffineRegistration.SetMovingImageB( shapeTibia );
//    AffineTransformPointerType initialTransformFemur = AffineTransformType::New();
//    initialTransformFemur->SetIdentity();
//    AffineTransformPointerType initialTransformTibia = AffineTransformType::New();
//    initialTransformTibia->SetIdentity();
    
    AffineTransformPointerType initialTransformFemur = affineRegistration.GetFinalTransform();
    AffineTransformPointerType initialTransformTibia = affineRegistration.GetFinalTransform();
    
    ParametersType xFemur = initialTransformFemur->GetParameters();
    ParametersType xTibia = initialTransformTibia->GetParameters();
    
    xFemur.SetElement(11, xFemur.GetElement(11)-15);
    xTibia.SetElement(11, xTibia.GetElement(11)+15);
    
    initialTransformFemur->SetParameters(xFemur);
    initialTransformTibia->SetParameters(xTibia);
    jointAffineRegistration.SetInitialTransformA( initialTransformFemur );
    jointAffineRegistration.SetInitialTransformB( initialTransformTibia );

    jointAffineRegistration.SetFixedImage( pBone );
    jointAffineRegistration.SetTranslationScale( 1.0/700 );
    jointAffineRegistration.SetNumberOfIterations( 50 );
    jointAffineRegistration.DoIt();
    
    WriteImage< FloatImageType >::DoIt( outputMovedFemurShape.c_str(), jointAffineRegistration.GetMovedImageA(), (bool)compression );
    WriteImage< FloatImageType >::DoIt( outputMovedTibiaShape.c_str(), jointAffineRegistration.GetMovedImageB(), (bool)compression );
    
    TransformFileWriter::Pointer transformWriterA = TransformFileWriter::New();
    transformWriterA->SetFileName( outputFemurTransform.c_str() );
    transformWriterA->SetInput( jointAffineRegistration.GetFinalTransformA() );
    try{
        transformWriterA->Update();
    }catch( ExceptionObject & e)
    {
        cerr << e.GetDescription() << endl;
    }

    TransformFileWriter::Pointer transformWriterB = TransformFileWriter::New();
    transformWriterB->SetFileName( outputTibiaTransform.c_str() );
    transformWriterB->SetInput( jointAffineRegistration.GetFinalTransformB() );
    try{
        transformWriterB->Update();
    }catch( ExceptionObject & e)
    {
        cerr << e.GetDescription() << endl;
    }

    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;

    ImageIOBase::IOPixelType pixelType_pBone;
    ImageIOBase::IOComponentType componentType_pBone;
    
    ImageIOBase::IOPixelType pixelType_shapeBone;
    ImageIOBase::IOComponentType componentType_shapeBone;
    
    ImageIOBase::IOPixelType pixelType_shapeFemur;
    ImageIOBase::IOComponentType componentType_shapeFemur;
    
    ImageIOBase::IOPixelType pixelType_shapeTibia;
    ImageIOBase::IOComponentType componentType_shapeTibia;
    
    try
    {
        GetImageType (inputBoneProbability, pixelType_pBone, componentType_pBone);
        GetImageType (inputFemurShape, pixelType_shapeBone, componentType_shapeBone);
        GetImageType (inputFemurShape, pixelType_shapeFemur, componentType_shapeFemur);
        GetImageType (inputTibiaShape, pixelType_shapeTibia, componentType_shapeTibia);
        
        if ((componentType_shapeBone != componentType_shapeFemur) || (componentType_shapeBone != componentType_shapeTibia))
        {
            cerr << "Input shape images must have the same image type!" << endl;
            return EXIT_FAILURE;
        }
        else
        {
            switch (componentType_pBone) {
                case ImageIOBase::UCHAR:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::CHAR:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::USHORT:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::SHORT:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::UINT:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::INT:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::ULONG:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::LONG:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::FLOAT:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::DOUBLE:
                    switch (componentType_shapeBone) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                default:
                    break;
            }
        }

    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
    

FloatImagePointerType applyAffineTransform( FloatImagePointerType imMoving, AffineTransformPointerType transform, SizeType sz, SpacingType sp, DirectionType dir, PointType origin)
{
	ResamplerPointerType resampler = ResamplerType::New();
	resampler->SetTransform( transform );
	resampler->SetInput( imMoving );
	
	resampler->SetSize( sz );
	resampler->SetOutputOrigin( origin );
	resampler->SetOutputSpacing( sp );
	resampler->SetOutputDirection( dir );
	resampler->SetDefaultPixelValue( 0 );
	resampler->Update();
	return resampler->GetOutput();
    
}