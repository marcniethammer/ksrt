#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkResampleImageFilter.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtResample.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>


#include <itkPluginUtilities.h>
#include "ksrtApplyKneeDeformationCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;


typedef ResampleImageFilter< FloatImageType, 
FloatImageType, 
double >                   ResamplerType;
typedef ResamplerType::Pointer                          ResamplerPointerType;
typedef LinearInterpolateImageFunction< 
FloatImageType, 
double >                        InterpolatorType;
typedef InterpolatorType::Pointer						InterpolatorPointerType;
typedef InterpolatorType::ContinuousIndexType           ContinuousIndexType;



template<class TReference, class TMoving> 
int DoIt( int argc, char * argv[], TReference, TMoving )
{
    PARSE_ARGS;
    typedef Image<TReference, 3>        ReferenceImageType;
    typedef Image<TMoving, 3>           MovingImageType;

    
    std::cout <<"input displacement X: " << inputDisplacementX.c_str() << std::endl;
    std::cout <<"input displacement Y: " << inputDisplacementY.c_str() << std::endl;
    std::cout <<"input displacement Z: " <<  inputDisplacementZ.c_str() << std::endl;
    std::cout <<"input moving image: " << inputMovingImage.c_str() << std::endl;
    std::cout <<"input reference image: " << inputReferenceImage.c_str() << std::endl;
    std::cout <<"output moved image: " << outputMovedImage.c_str() << std::endl;
    
    FloatImagePointerType imDX = ReadImage<FloatImageType>::DoIt( inputDisplacementX.c_str() );
    FloatImagePointerType imDY = ReadImage<FloatImageType>::DoIt( inputDisplacementY.c_str() );
    FloatImagePointerType imDZ = ReadImage<FloatImageType>::DoIt( inputDisplacementZ.c_str() );
    
    FloatImagePointerType imMoving = Cast< MovingImageType, FloatImageType>::DoIt( ReadImage< MovingImageType >::DoIt( inputMovingImage.c_str() ) );
    
    FloatImagePointerType imReference = Cast< ReferenceImageType, FloatImageType>::DoIt( ReadImage< ReferenceImageType >::DoIt( inputReferenceImage.c_str() ) );
    
    SizeType szReference = imReference->GetRequestedRegion().GetSize();
    SizeType szMoving = imMoving->GetRequestedRegion().GetSize();
    
    SpacingType spMoving = imMoving->GetSpacing();
    SpacingType spReference = imReference->GetSpacing();
    
    DirectionType dirMoving = imMoving->GetDirection();
    DirectionType dirReference = imReference->GetDirection();
    
    PointType originMoving = imMoving->GetOrigin();
    PointType originReference = imReference->GetOrigin();
    
    IndexType start;
    start.Fill(0);
    
    Resample< FloatImageType > resampleDX;
    resampleDX.SetInput( imDX );
    resampleDX.SetNewSize( szReference );
    imDX = resampleDX.GetOutput();
    
    Resample< FloatImageType > resampleDY;
    resampleDY.SetInput( imDY );
    resampleDY.SetNewSize( szReference );
    imDY = resampleDY.GetOutput();
    
    Resample< FloatImageType > resampleDZ;
    resampleDZ.SetInput( imDZ );
    resampleDZ.SetNewSize( szReference );
    imDZ = resampleDZ.GetOutput();
    
    ImageData< FloatPixelType > dx;	
    dx.SetSize( szReference[0], szReference[1], szReference[2] );
    dx.SetData( imDX->GetBufferPointer() );
    
    ImageData< FloatPixelType > dy;
    dy.SetSize( szReference[0], szReference[1], szReference[2] );
    dy.SetData( imDY->GetBufferPointer() );
    
    ImageData< FloatPixelType > dz;
    dz.SetSize( szReference[0], szReference[1], szReference[2] );
    dz.SetData( imDZ->GetBufferPointer() );
   
    FloatImagePointerType imXMoving = New3DImage< FloatImageType >::DoIt(szMoving, spMoving, dirMoving, start, originMoving, 0);
    FloatImagePointerType imYMoving = New3DImage< FloatImageType >::DoIt(szMoving, spMoving, dirMoving, start, originMoving, 0);
    FloatImagePointerType imZMoving = New3DImage< FloatImageType >::DoIt(szMoving, spMoving, dirMoving, start, originMoving, 0);

    FloatImagePointerType imXReference = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);
    FloatImagePointerType imYReference = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);
    FloatImagePointerType imZReference = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);
    
    ImageData< FloatPixelType > xReference;
    xReference.SetSize( szReference[0], szReference[1], szReference[2] );
    xReference.SetData( imXReference->GetBufferPointer() );
    
    ImageData< FloatPixelType > yReference;
    yReference.SetSize( szReference[0], szReference[1], szReference[2] );
    yReference.SetData( imYReference->GetBufferPointer() );
    
    ImageData< FloatPixelType > zReference;
    zReference.SetSize( szReference[0], szReference[1], szReference[2] );
    zReference.SetData( imZReference->GetBufferPointer() );
    
    for (unsigned int k = 0; k < szReference[2]; k++)
    {
        for (unsigned int j = 0; j < szReference[1]; j++)
        {
            for (unsigned int i = 0; i < szReference[0]; i++)
            {
                xReference(i,j,k) = i;
                yReference(i,j,k) = j;
                zReference(i,j,k) = k;
            }
        }
    }
    
    FloatImagePointerType imNX = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);
    FloatImagePointerType imNY = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);
    FloatImagePointerType imNZ = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);
    
    ImageData< FloatPixelType > nx;
    nx.SetSize( szReference[0], szReference[1], szReference[2] );
    nx.SetData( imNX->GetBufferPointer() );
    
    ImageData< FloatPixelType > ny;
    ny.SetSize( szReference[0], szReference[1], szReference[2] );
    ny.SetData( imNY->GetBufferPointer() );
    
    ImageData< FloatPixelType > nz;
    nz.SetSize( szReference[0], szReference[1], szReference[2] );
    nz.SetData( imNZ->GetBufferPointer() );
    
    for (unsigned int k = 0; k < szReference[2]; k++)
    {
        for (unsigned int j = 0; j < szReference[1]; j++)
        {
            for (unsigned int i = 0; i < szReference[0]; i++)
            {
                nx(i,j,k) = xReference(i,j,k) + inputDownsamplingFactor[0] * dx(i,j,k);
                ny(i,j,k) = yReference(i,j,k) + inputDownsamplingFactor[1] * dy(i,j,k);
                nz(i,j,k) = zReference(i,j,k) + inputDownsamplingFactor[2] * dz(i,j,k);
                
                nx(i,j,k) = ( nx(i,j,k) >= 1 ) ? nx(i,j,k) : 1;
				ny(i,j,k) = ( ny(i,j,k) >= 1 ) ? ny(i,j,k) : 1;
				nz(i,j,k) = ( nz(i,j,k) >= 1 ) ? nz(i,j,k) : 1;
                
				nx(i,j,k) = ( nx(i,j,k) <= szMoving[0]-2 ) ? nx(i,j,k) : szMoving[0]-2;
				ny(i,j,k) = ( ny(i,j,k) <= szMoving[1]-2 ) ? ny(i,j,k) : szMoving[1]-2;
				nz(i,j,k) = ( nz(i,j,k) <= szMoving[2]-2 ) ? nz(i,j,k) : szMoving[2]-2;
            }
        }
    }
    
    InterpolatorPointerType interpolator  = InterpolatorType::New();
    
    FloatImagePointerType imMoved = New3DImage< FloatImageType >::DoIt(szReference, spReference, dirReference, start, originReference, 0);

    interpolator->SetInputImage( imMoving );
    for (unsigned int k = 0; k < szReference[2]; k++)
    {
        for (unsigned int j = 0; j < szReference[1]; j++)
        {
            for (unsigned int i = 0; i < szReference[0]; i++)
            {
                IndexType index;
                index[0] = i;
                index[1] = j;
                index[2] = k;
                
                ContinuousIndexType index2;
                index2[0] = nx(i,j,k);
                index2[1] = ny(i,j,k);
                index2[2] = nz(i,j,k);
                imMoved->SetPixel(index, interpolator->EvaluateAtContinuousIndex(index2));
            }
        }
    }
	WriteImage< FloatImageType >::DoIt( outputMovedImage.c_str(), imMoved, (bool)compression );
    return 0;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType_reference;
    ImageIOBase::IOComponentType componentType_reference;
    
    ImageIOBase::IOPixelType pixelType_moving;
    ImageIOBase::IOComponentType componentType_moving;

    
    try
    {
        GetImageType (inputReferenceImage, pixelType_reference, componentType_reference);
        GetImageType (inputMovingImage, pixelType_moving, componentType_moving);
        
        switch (componentType_reference) {
            case ImageIOBase::UCHAR:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::CHAR:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::USHORT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::SHORT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::UINT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::INT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::ULONG:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::LONG:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::FLOAT:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::DOUBLE:
                switch (componentType_moving) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            default:
                break;
        }
    
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
