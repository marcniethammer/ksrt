#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <itkPluginUtilities.h>

#include "ksrtInsertJointRegionCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

void getWholeVolume( FloatImagePointerType sub, FloatImagePointerType whole, IndexType startIdx);

template<class T_input, class T_bone> int DoIt( int argc, char * argv[], T_input, T_bone )
{
    PARSE_ARGS;
    
    typedef Image<T_input, 3>          InputImageType;
    typedef Image<T_bone, 3>           BoneImageType;
    
	FloatImagePointerType im = Cast<InputImageType, FloatImageType>::DoIt(ReadImage< InputImageType >::DoIt(inputVolume.c_str()));
    FloatImagePointerType femur = Cast<BoneImageType, FloatImageType>::DoIt(ReadImage< BoneImageType >::DoIt(inputFemur.c_str()));
    FloatImagePointerType tibia = Cast<BoneImageType, FloatImageType>::DoIt(ReadImage< BoneImageType >::DoIt(inputTibia.c_str()));
	SizeType sz = femur->GetRequestedRegion().GetSize();
	DirectionType dir = femur->GetDirection();
	SpacingType sp = femur->GetSpacing();
    PointType origin = femur->GetOrigin();
    
    SizeType sz_joint = im->GetRequestedRegion().GetSize();

    
	unsigned int * sum_0_femur = new unsigned int [sz[0]];
	for (unsigned int i = 0; i < sz[0]; i++)
		sum_0_femur[i] = 0;
    
	unsigned int * sum_1_femur = new unsigned int [sz[1]];
	for (unsigned int j = 0; j < sz[1]; j++)
		sum_1_femur[j] = 0;
	
	unsigned int * sum_2_femur = new unsigned int [sz[2]];
	for (unsigned int k = 0; k < sz[2]; k++)
		sum_2_femur[k] = 0;
    
	for (unsigned int i = 0; i < sz[0]; i++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int k = 0; k < sz[2]; k++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				FloatPixelType p = femur->GetPixel( idx );
				sum_0_femur[i] = (p > 0) ? sum_0_femur[i]+1 : sum_0_femur[i];
				sum_1_femur[j] = (p > 0) ? sum_1_femur[j]+1 : sum_1_femur[j];
				sum_2_femur[k] = (p > 0) ? sum_2_femur[k]+1 : sum_2_femur[k];
			}
		}
	}
	unsigned int min0_femur = 0;
	unsigned int min1_femur = 0;
	unsigned int min2_femur = 0;
	unsigned int max0_femur = sz[0]-1;
	unsigned int max1_femur = sz[1]-1;                                                        
	unsigned int max2_femur = sz[2]-1;
    
	while(sum_0_femur[min0_femur] == 0)
	{
		min0_femur++;
	}
	while(sum_1_femur[min1_femur] == 0)
	{
		min1_femur++;
	}
	while(sum_2_femur[min2_femur] == 0)
	{
		min2_femur++;
	}
	while(sum_0_femur[max0_femur] == 0)
	{
		max0_femur--;
	}
	while(sum_1_femur[max1_femur] == 0)
	{
		max1_femur--;
	}
	while(sum_2_femur[max2_femur] == 0)
	{
		max2_femur--;
	}
    
	unsigned int * sum_0_tibia = new unsigned int [sz[0]];
	for (unsigned int i = 0; i < sz[0]; i++)
		sum_0_tibia[i] = 0;
	unsigned int * sum_1_tibia = new unsigned int [sz[1]];
	for (unsigned int j = 0; j < sz[1]; j++)
		sum_1_tibia[j] = 0;
	unsigned int * sum_2_tibia = new unsigned int [sz[2]];
	for (unsigned int k = 0; k < sz[2]; k++)
		sum_2_tibia[k] = 0;
    
	for (unsigned int i = 0; i < sz[0]; i++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int k = 0; k < sz[2]; k++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				FloatPixelType p = tibia->GetPixel( idx );
				sum_0_tibia[i] = (p > 0) ? sum_0_tibia[i]+1 : sum_0_tibia[i];
				sum_1_tibia[j] = (p > 0) ? sum_1_tibia[j]+1 : sum_1_tibia[j];
				sum_2_tibia[k] = (p > 0) ? sum_2_tibia[k]+1 : sum_2_tibia[k];
			}
		}
	}
	unsigned int min0_tibia = 0;
	unsigned int min1_tibia = 0;
	unsigned int min2_tibia = 50;
	unsigned int max0_tibia = sz[0]-1;
	unsigned int max1_tibia = sz[1]-150;                                                        
	unsigned int max2_tibia = sz[2]-1;
    
	while(sum_0_tibia[min0_tibia] == 0)
	{
		min0_tibia++;
	}
	while(sum_1_tibia[min1_tibia] == 0)
	{
		min1_tibia++;
	}
	while(sum_2_tibia[min2_tibia] == 0)
	{
		min2_tibia++;
	}
	while(sum_0_tibia[max0_tibia] == 0)
	{
		max0_tibia--;
	}
	while(sum_1_tibia[max1_tibia] == 0)
	{
		max1_tibia--;
	}
	while(sum_2_tibia[max2_tibia] == 0)
	{
		max2_tibia--;
	}
    
	unsigned int min0 = std::min(min0_femur, min0_tibia);
	unsigned int max0 = std::max(max0_femur, max0_tibia);
    
	unsigned int min1 = std::min(min1_femur, max1_tibia)-20;
	unsigned int max1 = std::max(min1_femur, max1_tibia)+20;
    
	unsigned int min2 = std::min(min2_femur, min2_tibia);
	unsigned int max2 = std::max(max2_femur, max2_tibia);
    
	unsigned int center0 = (int)(min0+max0)/2;
	unsigned int center1 = (int)(min1+max1)/2;
	unsigned int center2 = (int)(min2+max2)/2;
    
	unsigned int box0_min = center0 - sz_joint[0]/2 + 1;
	unsigned int box1_min = center1 - sz_joint[1]/2 + 1;
	unsigned int box2_min = center2 - sz_joint[2]/2 + 1;
    
    IndexType start;
    start.Fill(0);
    
    FloatImagePointerType whole = New3DImage< FloatImageType >::DoIt( sz, sp, dir, start, origin, 0);
    
    IndexType startIdx;
	startIdx[0] = box0_min;
	startIdx[1] = box1_min;
	startIdx[2] = box2_min;
    
    getWholeVolume( im, whole, startIdx );
    
    WriteImage< InputImageType >::DoIt( outputVolume.c_str(), Cast< FloatImageType, InputImageType>::DoIt( whole ), (bool)compression );
    
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType_input;
    ImageIOBase::IOComponentType componentType_input;
    
    ImageIOBase::IOPixelType pixelType_femur;
    ImageIOBase::IOComponentType componentType_femur;
    
    ImageIOBase::IOPixelType pixelType_tibia;
    ImageIOBase::IOComponentType componentType_tibia;
    
    
    try
    {
        GetImageType (inputVolume, pixelType_input, componentType_input);
        GetImageType (inputFemur, pixelType_femur, componentType_femur);
        GetImageType (inputTibia, pixelType_tibia, componentType_tibia);
        
        if (componentType_femur != componentType_tibia)
        {
            std::cerr << "Input femur and tibia should have the same image type!" << std::endl;
            return EXIT_FAILURE;
        }
        else 
        {
            switch (componentType_input) {
                case ImageIOBase::UCHAR:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::CHAR:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::USHORT:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::SHORT:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::UINT:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::INT:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::ULONG:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::LONG:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::FLOAT:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::DOUBLE:
                    switch (componentType_femur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                default:
                    break;
            }
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void getWholeVolume( FloatImagePointerType sub, FloatImagePointerType whole, IndexType startIdx)
{
	SizeType sze = sub->GetRequestedRegion().GetSize();
    float sum = 0;
    
	for (unsigned int i = 0; i < sze[0]; i++)
	{
		for (unsigned int j = 0; j < sze[1]; j++)
		{
			for (unsigned int k = 0; k < sze[2]; k++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
                
				IndexType idx2;
				idx2[0] = i + startIdx[0];
				idx2[1] = j + startIdx[1];
				idx2[2] = k + startIdx[2];
                sum += sub->GetPixel(idx);
                
                whole->SetPixel( idx2, sub->GetPixel( idx ) );
			}
		}
	}
    sum = sum + 1;
}