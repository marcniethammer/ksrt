#include <iostream>
#include <itkImage.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <itkPluginUtilities.h>
#include "ksrtPatchBasedLabelFusionCLP.h"

#include <iostream>
#include <fstream>

//#include <ANN/ANN.h>

#include <algorithm>
#include <vector>
#include <cmath>

using namespace std;
using namespace itk;


template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef typename itk::Image< T, 3 >         InputImageType;    
    typedef typename InputImageType::Pointer    InputImagePointerType;

    typedef short								ShortPixelType;
    typedef Image< ShortPixelType, 3 >          ShortImageType;
    typedef ShortImageType::Pointer				ShortImagePointerType;

    typedef float								FloatPixelType;
    typedef Image< FloatPixelType, 3 >          FloatImageType;
    typedef FloatImageType::Pointer				FloatImagePointerType;
    
    typedef FloatImageType::RegionType          RegionType;
    typedef FloatImageType::SizeType			SizeType;
    typedef FloatImageType::SpacingType			SpacingType;
    typedef FloatImageType::DirectionType		DirectionType;
    typedef FloatImageType::IndexType			IndexType;
    typedef FloatImageType::PointType			PointType;

    FloatImagePointerType queryIm = Cast< InputImageType, FloatImageType>::DoIt( ReadImage< InputImageType >::DoIt( inputVolume.c_str() ) );
    SizeType sz = queryIm->GetRequestedRegion().GetSize();
    SpacingType sp =  queryIm->GetSpacing();
    DirectionType dir = queryIm->GetDirection();
    PointType origin = queryIm->GetOrigin();
    IndexType start;
    start.Fill(0);
    
    ImageData< float > query;
    query.SetData( queryIm->GetBufferPointer() );
    query.SetSize( sz[0],sz[1],sz[2] );
    
    FloatImagePointerType pbFemIm = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType pbTibIm = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    ifstream inList;
    inList.open( inputList.c_str() );
    
    if ( ! inList.is_open())
    {
        cout << "Can't open file " << inputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }
    
    int numOfAtlases = 0;
    string tmp;
    while (getline(inList,tmp)) {
        numOfAtlases++;
    }
    inList.close();
    
    inList.open( inputList.c_str() );
    
    if ( ! inList.is_open())
    {
        cout << "Can't open file " << inputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }
    
    string inputMRFem;
    string inputLabelFem;
    string inputMRTib;
    string inputLabelTib;
    
    inList >> inputMRFem;
    inList >> inputLabelFem;
    inList >> inputMRTib;
    inList >> inputLabelTib;
    
    FloatImagePointerType imMRFem = ReadImage< FloatImageType >::DoIt( inputMRFem.c_str() );
    FloatImagePointerType imLabelFem = ReadImage< FloatImageType >::DoIt( inputLabelFem.c_str() );
    FloatImagePointerType imMRTib = ReadImage< FloatImageType >::DoIt( inputMRTib.c_str() );
    FloatImagePointerType imLabelTib = ReadImage< FloatImageType >::DoIt( inputLabelTib.c_str() );
    
    FloatImagePointerType imRoiFem = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0.0);
    FloatImagePointerType imRoiTib = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0.0);
    
    ImageData< float > roi_fem;
    ImageData< float > roi_tib;
    roi_fem.SetData(imRoiFem->GetBufferPointer());
    roi_fem.SetSize(sz[0],sz[1],sz[2]);
    roi_tib.SetData(imRoiTib->GetBufferPointer());
    roi_tib.SetSize(sz[0],sz[1],sz[2]);
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                roi_fem(i,j,k) = 0;
                roi_tib(i,j,k) = 0;
                
            }
        }
    }
    
    float * mr_fem_arr = new float [sz[0]*sz[1]*sz[2]*numOfAtlases];
    float * mr_tib_arr = new float [sz[0]*sz[1]*sz[2]*numOfAtlases];
    float * label_fem_arr = new float [sz[0]*sz[1]*sz[2]*numOfAtlases];
    float * label_tib_arr = new float [sz[0]*sz[1]*sz[2]*numOfAtlases];
    
    ImageData< float > mr_fem;
    ImageData< float > mr_tib;
    ImageData< float > label_fem;
    ImageData< float > label_tib;
    
    mr_fem.SetData(mr_fem_arr);
    mr_tib.SetData(mr_tib_arr);
    label_fem.SetData(label_fem_arr);
    label_tib.SetData(label_tib_arr);
    
    mr_fem.SetSize(sz[0],sz[1],sz[2],numOfAtlases);
    mr_tib.SetSize(sz[0],sz[1],sz[2],numOfAtlases);
    label_fem.SetSize(sz[0],sz[1],sz[2],numOfAtlases);
    label_tib.SetSize(sz[0],sz[1],sz[2],numOfAtlases);
    
    int a = 0;
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                
                mr_fem(i,j,k,a) = imMRFem->GetPixel( idx );
                mr_tib(i,j,k,a) = imMRTib->GetPixel( idx );
                label_fem(i,j,k,a) = imLabelFem->GetPixel( idx );
                label_tib(i,j,k,a) = imLabelTib->GetPixel( idx );

                roi_fem(i,j,k) += label_fem(i,j,k,a);
                roi_tib(i,j,k) += label_tib(i,j,k,a);

            }
        }
    }

    a = 1;
    while (a < numOfAtlases)
    {
        inList >> inputMRFem;
        inList >> inputLabelFem;
        inList >> inputMRTib;
        inList >> inputLabelTib;
        
        imMRFem = ReadImage< FloatImageType >::DoIt( inputMRFem.c_str() );
        imLabelFem = ReadImage< FloatImageType >::DoIt( inputLabelFem.c_str() );
        imMRTib = ReadImage< FloatImageType >::DoIt( inputMRTib.c_str() );
        imLabelTib = ReadImage< FloatImageType >::DoIt( inputLabelTib.c_str() );

        for (unsigned int k = 0; k < sz[2]; k++)
        {
            for (unsigned int j = 0; j < sz[1]; j++)
            {
                for (unsigned int i = 0; i < sz[0]; i++)
                {
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                
                    mr_fem(i,j,k,a) = imMRFem->GetPixel( idx );
                    mr_tib(i,j,k,a) = imMRTib->GetPixel( idx );
                    label_fem(i,j,k,a) = imLabelFem->GetPixel( idx );
                    label_tib(i,j,k,a) = imLabelTib->GetPixel( idx );
                    
                    roi_fem(i,j,k) += label_fem(i,j,k,a);
                    roi_tib(i,j,k) += label_tib(i,j,k,a);
                    
                }
            }
        }
        a++;
    }
    
    inList.close();
    
    int maxPts = numOfAtlases * (2*ns+1) * (2*ns+1) * (2*ns+1);
    
    int dim = (2*ps+1) * (2*ps+1) * (2*ps+1);
    
//    double eps = 0;

    float * queryPt = new float [dim];
    for (int iter = 0; iter != dim; iter++)
    {
        queryPt[iter] = 0;
    }
    float * dataPts = new float [maxPts*dim];
    for (int iter = 0; iter != maxPts*dim; iter++)
    {
        dataPts[iter] = 0;
    }
    float * labels = new float [maxPts];
    for (int iter = 0; iter != maxPts; iter++)
    {
        labels[iter] = 0;
    }
    
    
    /* femoral cartilage */
    for (int k = ps; k < (int)sz[2]-ps; k++)
    {
        for (int j = ps; j < (int)sz[1]-ps; j++)
        {
            for (int i = ps; i < (int)sz[0]-ps; i++)
            {
                if (roi_fem(i,j,k) > 0)
                {
//                    cout << i << "\t" << j << "\t" << k << endl;
                    
//                    ANNpoint queryPt = annAllocPt( dim );
//                    ANNpointArray dataPts = annAllocPts( maxPts, dim );
//                    ANNidxArray nnIdx = new ANNidx [nn];
//                    ANNdistArray dists = new ANNdist [nn];
                    
                    for (int kk = 0; kk < 2*ps+1; kk++)
                    {
                        for (int jj = 0; jj < 2*ps+1; jj++)
                        {
                            for (int ii = 0; ii < 2*ps+1; ii++)
                            {
                                queryPt[ii+jj*(2*ps+1)+kk*(2*ps+1)*(2*ps+1)] = (float)query(i-ps+ii,j-ps+jj,k-ps+kk);
                            }
                        }
                    }
                   
                    int nPts = 0;
                    for (int aa = 0; aa < numOfAtlases; aa++)
                    {
                        for (int ck = (std::max(ps,k-ns)); ck <= (std::min((int)sz[2]-ps-1,k+ns)); ck++)
                        {
                            for (int cj = (std::max(ps,j-ns)); cj <= (std::min((int)sz[1]-ps-1,j+ns)); cj++)
                            {
                                for (int ci = (std::max(ps,i-ns)); ci <= (std::min((int)sz[0]-ps-1,i+ns)); ci++)
                                {
                                    labels[nPts] = label_fem(ci,cj,ck,aa);
                                    for (int kk = 0; kk < 2*ps+1; kk++)
                                    {
                                        for (int jj = 0; jj < 2*ps+1; jj++)
                                        {
                                            for (int ii = 0; ii < 2*ps+1; ii++)
                                            {
                                                dataPts[nPts*dim + ii+jj*(2*ps+1)+kk*(2*ps+1)*(2*ps+1)] = mr_fem(ci-ps+ii,cj-ps+jj,ck-ps+kk,aa);
                                                
                                            }
                                        }
                                    }
                                    nPts++;
                                }
                            }
                        }
                    }
                    
                    vector<float> dist;
                    
                    for (int pi = 0; pi < nPts; pi++)
                    {
                        float tmp_dist = 0;
                        for (int di = 0; di < dim; di++)
                        {
                            tmp_dist += (queryPt[di] - dataPts[pi*dim+di]) * (queryPt[di] - dataPts[pi*dim+di]);
                        }
                        dist.push_back(tmp_dist);
                    }
                    
                    float min_dist = dist[0];
                    for (int pi = 0; pi < nPts; pi++)
                        min_dist = (min_dist > dist[pi]) ? dist[pi] : min_dist;
                    
                    float h = min_dist + 0.1;
                    
                    vector<float> weight(nPts, 0);
                    float weight_sum = 0;
                    for (int pi = 0; pi < nPts; pi++)
                    {
                        weight[pi] = exp(-dist[pi]/h);
                        weight_sum += weight[pi];
                    }
                    
                    float pb = 0;
                    for (int pi = 0; pi < nPts; pi++)
                    {
                        weight[pi] = weight[pi]/weight_sum;
                        pb += weight[pi] * labels[pi];
                    }
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    pbFemIm->SetPixel(idx, pb);

                    
                    
//                    ANNkd_tree* kdTree = new ANNkd_tree(dataPts, nPts, dim);
//                    kdTree->annkSearch(queryPt, nn, nnIdx, dists, eps);
//                    delete kdTree;
//                    
//                    float * tmpWeight = new float [std::min(nn,nPts)];
//                    float weightSum = 0;
//                    for (int tmpIdx = 0; tmpIdx < std::min(nn,nPts); tmpIdx++) 
//                    {
//                        tmpWeight[tmpIdx] = 1.0/(dists[nnIdx[tmpIdx]]/10000.0+1.0);
//                        weightSum += tmpWeight[tmpIdx];
//                    }
//                    float tmpPb = 0.0;
//                    
//                    for (int tmpIdx = 0; tmpIdx < std::min(nn,nPts); tmpIdx++)
//                    {
//                        tmpPb += tmpWeight[tmpIdx]/weightSum*labels[nnIdx[tmpIdx]];
//                    }
//                    cout << "\tdist = " << dists[nnIdx[0]] << "\t";
//                    cout << "weightSum = " << weightSum << "\t";
//                    cout << "tmpPb = " << tmpPb << endl;
//                    IndexType idx;
//                    idx[0] = i;
//                    idx[1] = j;
//                    idx[2] = k;
//                    
//                    pbFemIm->SetPixel(idx, (float)tmpPb);
//                    
//                    delete [] dists;
//                    delete [] nnIdx;
//                    
//                    annDeallocPts( dataPts );
//                    annDeallocPt( queryPt );
                }
            }
        }
    }
    
    
    
    WriteImage< FloatImageType >::DoIt(outputFemCartFusion.c_str(), pbFemIm, (bool)compression);
    
    /* tibial cartilage */
    
    for (int k = ps; k < (int)sz[2]-ps; k++)
    {
        for (int j = ps; j < (int)sz[1]-ps; j++)
        {
            for (int i = ps; i < (int)sz[0]-ps; i++)
            {
                if (roi_tib(i,j,k) > 0)
                {
                    for (int kk = 0; kk < 2*ps+1; kk++)
                    {
                        for (int jj = 0; jj < 2*ps+1; jj++)
                        {
                            for (int ii = 0; ii < 2*ps+1; ii++)
                            {
                                queryPt[ii+jj*(2*ps+1)+kk*(2*ps+1)*(2*ps+1)] = (float)query(i-ps+ii,j-ps+jj,k-ps+kk);
                            }
                        }
                    }
                    
                    int nPts = 0;
                    for (int aa = 0; aa < numOfAtlases; aa++)
                    {
                        for (int ck = (std::max(ps,k-ns)); ck <= (std::min((int)sz[2]-ps-1,k+ns)); ck++)
                        {
                            for (int cj = (std::max(ps,j-ns)); cj <= (std::min((int)sz[1]-ps-1,j+ns)); cj++)
                            {
                                for (int ci = (std::max(ps,i-ns)); ci <= (std::min((int)sz[0]-ps-1,i+ns)); ci++)
                                {
                                    labels[nPts] = label_tib(ci,cj,ck,aa);
                                    for (int kk = 0; kk < 2*ps+1; kk++)
                                    {
                                        for (int jj = 0; jj < 2*ps+1; jj++)
                                        {
                                            for (int ii = 0; ii < 2*ps+1; ii++)
                                            {
                                                dataPts[nPts*dim + ii+jj*(2*ps+1)+kk*(2*ps+1)*(2*ps+1)] = mr_tib(ci-ps+ii,cj-ps+jj,ck-ps+kk,aa);
                                                
                                            }
                                        }
                                    }
                                    nPts++;
                                }
                            }
                        }
                    }
                    
                    vector<float> dist;
                    
                    for (int pi = 0; pi < nPts; pi++)
                    {
                        float tmp_dist = 0;
                        for (int di = 0; di < dim; di++)
                        {
                            tmp_dist += (queryPt[di] - dataPts[pi*dim+di]) * (queryPt[di] - dataPts[pi*dim+di]);
                        }
                        dist.push_back(tmp_dist);
                    }
                    
                    float min_dist = dist[0];
                    for (int pi = 0; pi < nPts; pi++)
                        min_dist = (min_dist > dist[pi]) ? dist[pi] : min_dist;
                    
                    float h = min_dist + 0.1;
                    
                    vector<float> weight(nPts, 0);
                    float weight_sum = 0;
                    for (int pi = 0; pi < nPts; pi++)
                    {
                        weight[pi] = exp(-dist[pi]/h);
                        weight_sum += weight[pi];
                    }
                    
                    float pb = 0;
                    for (int pi = 0; pi < nPts; pi++)
                    {
                        weight[pi] = weight[pi]/weight_sum;
                        pb += weight[pi] * labels[pi];
                    }
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    pbTibIm->SetPixel(idx, pb);

                }
            }
        }
    }
    
    WriteImage< FloatImageType >::DoIt(outputTibCartFusion.c_str(), pbTibIm, (bool)compression);

    delete [] queryPt;
    delete [] dataPts;
    delete [] labels;
    
    delete [] mr_fem_arr;
    delete [] mr_tib_arr;
    delete [] label_fem_arr;
    delete [] label_tib_arr;
    
    
    return 0;
    
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
