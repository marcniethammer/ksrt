#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>

#include <itkPluginUtilities.h>

#include "ksrtMergeSegmentationsCLP.h"

using namespace std;
using namespace itk;

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef T               InputPixelType;
    typedef unsigned char   OutputPixelType;
    
    typedef typename itk::Image< InputPixelType,  3 >     InputImageType;
    typedef typename itk::Image< OutputPixelType, 3 >     OutputImageType;
    
    typedef typename InputImageType::Pointer         InputImagePointerType;
    typedef typename OutputImageType::Pointer        OutputImagePointerType;
    
    typedef typename InputImageType::SizeType        SizeType;
    typedef typename InputImageType::SpacingType     SpacingType;
    typedef typename InputImageType::DirectionType   DirectionType;   
    typedef typename InputImageType::PointType       PointType;
    typedef typename InputImageType::IndexType       IndexType;
    
    InputImagePointerType fb = ReadImage< InputImageType >::DoIt( inputFB.c_str() );
    InputImagePointerType fc = ReadImage< InputImageType >::DoIt( inputFC.c_str() );
    InputImagePointerType tb = ReadImage< InputImageType >::DoIt( inputTB.c_str() );
    InputImagePointerType tc = ReadImage< InputImageType >::DoIt( inputTC.c_str() );

    SizeType sz = fb->GetRequestedRegion().GetSize();
    SpacingType sp = fb->GetSpacing();
    DirectionType dir = fb->GetDirection();
    PointType origin = fb->GetOrigin();
    IndexType start;
    start.Fill(0);
    
    OutputImagePointerType out = New3DImage< OutputImageType >::DoIt( sz, sp, dir, start, origin, 0 );
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                out->SetPixel( idx, fb->GetPixel(idx) > 0 ? 1 : 0 );
            }
        }
    }
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                out->SetPixel( idx, tb->GetPixel(idx) > 0 ? 3 : out->GetPixel(idx) );
            }
        }
    }
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                out->SetPixel( idx, fc->GetPixel(idx) > 0 ? 2 : out->GetPixel(idx) );
            }
        }
    }
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                out->SetPixel( idx, tc->GetPixel(idx) > 0 ? 4 : out->GetPixel(idx) );
            }
        }
    }
    WriteImage< OutputImageType >::DoIt( output.c_str(), out, (bool)compression );
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputFB, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}