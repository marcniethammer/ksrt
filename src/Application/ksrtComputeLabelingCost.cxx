#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkImageFileReader.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <itkPluginUtilities.h>
#include "ksrtComputeLabelingCostCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;


using namespace std;
using namespace itk;

template< class T1, class T2 >
int DoIt( int argc, char * argv[], T1, T2 )
{
    
    PARSE_ARGS;
    
    typedef Image< T1, 3 >              T1Image;
    typedef Image< T2, 3 >              T2Image;
    
    FloatImagePointerType imLikelihoodFemur = Cast< T1Image, FloatImageType >::DoIt( ReadImage< T1Image >::DoIt( inputFemurLikelihood.c_str() ) );
    FloatImagePointerType imLikelihoodTibia = Cast< T1Image, FloatImageType >::DoIt( ReadImage< T1Image >::DoIt( inputTibiaLikelihood.c_str() ) );
    FloatImagePointerType imPriorFemur = Cast< T2Image, FloatImageType >::DoIt( ReadImage< T2Image >::DoIt( inputFemurPrior.c_str() ) );
    FloatImagePointerType imPriorTibia = Cast< T2Image, FloatImageType >::DoIt( ReadImage< T2Image >::DoIt( inputTibiaPrior.c_str() ) );
    
    SpacingType sp = imLikelihoodFemur->GetSpacing();
    SizeType sz = imLikelihoodFemur->GetRequestedRegion().GetSize();
    DirectionType dir = imLikelihoodFemur->GetDirection();
    
    IndexType start;
    start.Fill(0);
    
    PointType origin = imLikelihoodFemur->GetOrigin();
    
    FloatImagePointerType imR0 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imR1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imR2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
        
    ImageData< float > r0;
    r0.SetData( imR0->GetBufferPointer() );
    r0.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > r1;
    r1.SetData( imR1->GetBufferPointer() );
    r1.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > r2;
    r2.SetData( imR2->GetBufferPointer() );
    r2.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > likelihoodFemur;
    likelihoodFemur.SetData( imLikelihoodFemur->GetBufferPointer() );
    likelihoodFemur.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > likelihoodTibia;
    likelihoodTibia.SetData( imLikelihoodTibia->GetBufferPointer() );
    likelihoodTibia.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > priorFemur;
    priorFemur.SetData( imPriorFemur->GetBufferPointer() );
    priorFemur.SetSize( sz[0], sz[1], sz[2] );
    
    ImageData< float > priorTibia;
    priorTibia.SetData( imPriorTibia->GetBufferPointer() );
    priorTibia.SetSize( sz[0], sz[1], sz[2] );

    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                float minP = 0.01;
                float maxP = 0.98;
                
                float femurLike = likelihoodFemur(i,j,k);
                float tibiaLike = likelihoodTibia(i,j,k);
                float femurPrior = priorFemur(i,j,k);
                float tibiaPrior = priorTibia(i,j,k);
                
                femurLike = std::min(maxP, std::max(minP, femurLike));
                tibiaLike = std::min(maxP, std::max(minP, tibiaLike));
                femurPrior = std::min(maxP, std::max(minP, femurPrior));
                tibiaPrior = std::min(maxP, std::max(minP, tibiaPrior));
                
                float bkgrdPrior = 1 - femurPrior - tibiaPrior;			
                bkgrdPrior = std::min(maxP, std::max(minP, bkgrdPrior));
                
                float bkgrdLike = 1- femurLike - tibiaLike;
                bkgrdLike = std::min(maxP, std::max(minP, bkgrdLike));
                
                float femurTemp = femurLike * femurPrior;
                float tibiaTemp = tibiaLike * tibiaPrior;
                float bkgrdTemp = bkgrdLike * bkgrdPrior; 
                
                float femurPosterior = femurTemp / (femurTemp + tibiaTemp + bkgrdTemp);
                float tibiaPosterior = tibiaTemp / (femurTemp + tibiaTemp + bkgrdTemp);
                
                femurPosterior = std::min(maxP, std::max(minP, femurPosterior));
                tibiaPosterior = std::min(maxP, std::max(minP, tibiaPosterior));
                float bkgrdPosterior = 1 - femurPosterior - tibiaPosterior;
                bkgrdPosterior = std::min(maxP, std::max(minP, bkgrdPosterior));
                
                r0(i,j,k) = -log(femurPosterior); 
                r1(i,j,k) = -log(bkgrdPosterior);
                r2(i,j,k) = -log(tibiaPosterior);
            }
        }
    }

    WriteImage< FloatImageType >::DoIt( outputFemurCost.c_str(), imR0, (bool)compression );
    WriteImage< FloatImageType >::DoIt( outputBkgrdCost.c_str(), imR1, (bool)compression );
    WriteImage< FloatImageType >::DoIt( outputTibiaCost.c_str(), imR2, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType_likelihoodFemur;
    ImageIOBase::IOComponentType componentType_likelihoodFemur;
    
    ImageIOBase::IOPixelType pixelType_likelihoodTibia;
    ImageIOBase::IOComponentType componentType_likelihoodTibia;
    
    ImageIOBase::IOPixelType pixelType_priorFemur;
    ImageIOBase::IOComponentType componentType_priorFemur;
    
    ImageIOBase::IOPixelType pixelType_priorTibia;
    ImageIOBase::IOComponentType componentType_priorTibia;
    
    try
    {
        GetImageType (inputFemurLikelihood, pixelType_likelihoodFemur, componentType_likelihoodFemur);
        GetImageType (inputTibiaLikelihood, pixelType_likelihoodTibia, componentType_likelihoodTibia);
        
        GetImageType (inputFemurPrior, pixelType_priorFemur, componentType_priorFemur);
        GetImageType (inputTibiaPrior, pixelType_priorTibia, componentType_priorTibia);
        
        if ((componentType_likelihoodFemur != componentType_likelihoodTibia) || (componentType_priorFemur != componentType_priorTibia))
        {
            cerr << "Input images must have the same image type!" << endl;
            return EXIT_FAILURE;
        }
        
        switch (componentType_likelihoodFemur) {
            case ImageIOBase::UCHAR:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::CHAR:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::USHORT:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::SHORT:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::UINT:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::INT:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::ULONG:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::LONG:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::FLOAT:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::DOUBLE:
                switch (componentType_priorFemur) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            default:
                break;
        }
        
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}