#include <itkImage.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>

#include <itkSignedDanielssonDistanceMapImageFilter.h>

#include <itkPluginUtilities.h>

#include "ksrtComputeSignedDistanceMapCLP.h"

using namespace std;
using namespace itk;

int DoIt( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    typedef Image< short, 3 >                           ShortImageType;
    typedef ShortImageType::Pointer                     ShortImagePointerType;
    
    typedef Image< float, 3 >                           FloatImageType;
    typedef FloatImageType::Pointer                     FloatImagePointerType;
    

    typedef ShortImageType::SpacingType                 SpacingType;
    typedef ShortImageType::SizeType                    SizeType;
	typedef ShortImageType::DirectionType				DirectionType;
	typedef ShortImageType::IndexType					IndexType;
	typedef ShortImageType::PointType					PointType;

    typedef SignedDanielssonDistanceMapImageFilter< 
                                    ShortImageType, 
                                    FloatImageType >    FilterType;
    

    ShortImagePointerType input = ReadImage<ShortImageType>::DoIt(inputVolume.c_str());
    
    FilterType::Pointer filter = FilterType::New();
    filter->SetInput(input);
    filter->UseImageSpacingOff();
    FloatImagePointerType output = filter->GetDistanceMap();
    filter->Update();
    
	WriteImage< FloatImageType >::DoIt( outputVolume.c_str(), output, (bool)compression );
	return 0;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    return DoIt( argc, argv );

}