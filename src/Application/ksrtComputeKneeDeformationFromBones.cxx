#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkResampleImageFilter.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtResample.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <Registration/ksrtAffineRegistration.h>
#include <Registration/ksrtElasticCoordinatesDiffusion3D.h>

#include <itkPluginUtilities.h>
#include "ksrtComputeKneeDeformationFromBonesCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;

typedef AffineTransform< double, 3 >                    AffineTransformType;
typedef AffineTransformType::Pointer                    AffineTransformPointerType;

typedef ResampleImageFilter< FloatImageType, 
FloatImageType, 
double >                   ResamplerType;
typedef ResamplerType::Pointer                          ResamplerPointerType;
typedef LinearInterpolateImageFunction< 
FloatImageType, 
double >                        InterpolatorType;
typedef InterpolatorType::Pointer						InterpolatorPointerType;
typedef InterpolatorType::ContinuousIndexType           ContinuousIndexType;

FloatImagePointerType cutFemur( FloatImagePointerType femur, double len );
FloatImagePointerType cutTibia( FloatImagePointerType tibia, double len );
FloatImagePointerType applyAffineTransform( FloatImagePointerType imMoving, AffineTransformPointerType transform, SizeType sz, SpacingType sp, DirectionType dir, PointType origin);


template<class TFixed, class TMoving> 
int DoIt( int argc, char * argv[], TFixed, TMoving )
{
    PARSE_ARGS;
    
    typedef Image<TFixed, 3>            FixedImageType;
    typedef Image<TMoving, 3>           MovingImageType;
    
	FloatImagePointerType imFixedFemur = Cast<FixedImageType, FloatImageType>::DoIt(ReadImage< FixedImageType >::DoIt(inputFixedFemur.c_str()));
	FloatImagePointerType imFixedTibia = Cast<FixedImageType, FloatImageType>::DoIt(ReadImage< FixedImageType >::DoIt(inputFixedTibia.c_str()));
    FloatImagePointerType imMovingFemur = Cast<MovingImageType, FloatImageType>::DoIt(ReadImage< MovingImageType >::DoIt(inputMovingFemur.c_str()));
    FloatImagePointerType imMovingTibia = Cast<MovingImageType, FloatImageType>::DoIt(ReadImage< MovingImageType >::DoIt(inputMovingTibia.c_str()));
    
	SizeType szFixed = imFixedFemur->GetRequestedRegion().GetSize();
    SizeType szMoving = imMovingFemur->GetRequestedRegion().GetSize();
	
	SpacingType spFixed = imFixedFemur->GetSpacing();
    SpacingType spMoving = imMovingFemur->GetSpacing();
	
    DirectionType dirFixed = imFixedFemur->GetDirection();
	DirectionType dirMoving = imMovingFemur->GetDirection();
	
    PointType originFixed = imFixedFemur->GetOrigin();
    PointType originMoving = imMovingFemur->GetOrigin();
    
    IndexType start;
    start.Fill(0);
    
    FloatImagePointerType imMovingFemur_cut = cutFemur( imMovingFemur, 55.0 );
    FloatImagePointerType imMovingTibia_cut = cutTibia( imMovingTibia, 55.0 );
    FloatImagePointerType imFixedFemur_cut = cutFemur( imFixedFemur, 55.0 );
    FloatImagePointerType imFixedTibia_cut = cutTibia( imFixedTibia, 55.0 );
    
	AffineRegistration affineFemur;
	affineFemur.SetMovingImage( imMovingFemur_cut );
	affineFemur.SetFixedImage( imFixedFemur_cut );
	affineFemur.SetTranslationScale( 1.0/500 );
	affineFemur.SetRelaxationFactor( 0.5 );
    
	affineFemur.DoIt();
	FloatImagePointerType imMovedFemur = applyAffineTransform(imMovingFemur, affineFemur.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    
    AffineRegistration affineTibia;
	affineTibia.SetMovingImage( imMovingTibia_cut );
	affineTibia.SetFixedImage( imFixedTibia_cut );
	affineTibia.SetTranslationScale( 1.0/500 );
	affineTibia.SetRelaxationFactor( 0.5 );
	affineTibia.DoIt();
	FloatImagePointerType imMovedTibia = applyAffineTransform(imMovingTibia, affineTibia.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    
	ImageData< FloatPixelType > movedFemur;
    movedFemur.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    movedFemur.SetData( imMovedFemur->GetBufferPointer() );
    
    ImageData< FloatPixelType > movedTibia;
    movedTibia.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    movedTibia.SetData( imMovedTibia->GetBufferPointer() );
    
    FloatImagePointerType imXMoving = New3DImage< FloatImageType >::DoIt(szMoving, spMoving, dirMoving, start, originMoving, 0);
    FloatImagePointerType imYMoving = New3DImage< FloatImageType >::DoIt(szMoving, spMoving, dirMoving, start, originMoving, 0);
    FloatImagePointerType imZMoving = New3DImage< FloatImageType >::DoIt(szMoving, spMoving, dirMoving, start, originMoving, 0);
    
    ImageData< FloatPixelType > xMoving;
    xMoving.SetSize( szMoving[0], szMoving[1], szMoving[2] );
    xMoving.SetData( imXMoving->GetBufferPointer() );
    
    ImageData< FloatPixelType > yMoving;
    yMoving.SetSize( szMoving[0], szMoving[1], szMoving[2] );
    yMoving.SetData( imYMoving->GetBufferPointer() );
    
    ImageData< FloatPixelType > zMoving;
    zMoving.SetSize( szMoving[0], szMoving[1], szMoving[2] );
    zMoving.SetData( imZMoving->GetBufferPointer() );
    
    for (unsigned int k = 0; k < szMoving[2]; k++)
    {
        for (unsigned int j = 0; j < szMoving[1]; j++)
        {
            for (unsigned int i = 0; i < szMoving[0]; i++)
            {
                xMoving(i,j,k) = i;
                yMoving(i,j,k) = j;
                zMoving(i,j,k) = k;
            }
        }
    }
    
    FloatImagePointerType imXFixed = New3DImage< FloatImageType >::DoIt(szFixed, spFixed, dirFixed, start, originFixed, 0);
    FloatImagePointerType imYFixed = New3DImage< FloatImageType >::DoIt(szFixed, spFixed, dirFixed, start, originFixed, 0);
    FloatImagePointerType imZFixed = New3DImage< FloatImageType >::DoIt(szFixed, spFixed, dirFixed, start, originFixed, 0);
    
    ImageData< FloatPixelType > xFixed;
    xFixed.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    xFixed.SetData( imXFixed->GetBufferPointer() );
    
    ImageData< FloatPixelType > yFixed;
    yFixed.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    yFixed.SetData( imYFixed->GetBufferPointer() );
    
    ImageData< FloatPixelType > zFixed;
    zFixed.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    zFixed.SetData( imZFixed->GetBufferPointer() );
    
    for (unsigned int k = 0; k < szFixed[2]; k++)
    {
        for (unsigned int j = 0; j < szFixed[1]; j++)
        {
            for (unsigned int i = 0; i < szFixed[0]; i++)
            {
                xFixed(i,j,k) = i;
                yFixed(i,j,k) = j;
                zFixed(i,j,k) = k;
            }
        }
    }
    
    FloatImagePointerType imXMovedFemur = applyAffineTransform(imXMoving, affineFemur.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    FloatImagePointerType imYMovedFemur = applyAffineTransform(imYMoving, affineFemur.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    FloatImagePointerType imZMovedFemur = applyAffineTransform(imZMoving, affineFemur.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    
    FloatImagePointerType imXMovedTibia = applyAffineTransform(imXMoving, affineTibia.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    FloatImagePointerType imYMovedTibia = applyAffineTransform(imYMoving, affineTibia.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    FloatImagePointerType imZMovedTibia = applyAffineTransform(imZMoving, affineTibia.GetFinalTransform(), szFixed, spFixed, dirFixed, originFixed);
    
    ImageData< FloatPixelType > xMovedFemur;
    xMovedFemur.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    xMovedFemur.SetData( imXMovedFemur->GetBufferPointer() );
    
    ImageData< FloatPixelType > yMovedFemur;
    yMovedFemur.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    yMovedFemur.SetData( imYMovedFemur->GetBufferPointer() );
    
    ImageData< FloatPixelType > zMovedFemur;
    zMovedFemur.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    zMovedFemur.SetData( imZMovedFemur->GetBufferPointer() );
    
    ImageData< FloatPixelType > xMovedTibia;
    xMovedTibia.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    xMovedTibia.SetData( imXMovedTibia->GetBufferPointer() );
    
    ImageData< FloatPixelType > yMovedTibia;
    yMovedTibia.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    yMovedTibia.SetData( imYMovedTibia->GetBufferPointer() );
    
    ImageData< FloatPixelType > zMovedTibia;
    zMovedTibia.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    zMovedTibia.SetData( imZMovedTibia->GetBufferPointer() );
    
    FloatImagePointerType imDX = New3DImage< FloatImageType >::DoIt(szFixed, spFixed, dirFixed, start, originFixed, 0);
    FloatImagePointerType imDY = New3DImage< FloatImageType >::DoIt(szFixed, spFixed, dirFixed, start, originFixed, 0);
    FloatImagePointerType imDZ = New3DImage< FloatImageType >::DoIt(szFixed, spFixed, dirFixed, start, originFixed, 0);
    
    ShortImagePointerType imMask = New3DImage< ShortImageType >::DoIt( szFixed, spFixed, dirFixed, start, originFixed, 0 );
    
    ImageData< FloatPixelType > dx;	
    dx.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    dx.SetData( imDX->GetBufferPointer() );
    
    ImageData< FloatPixelType > dy;
    dy.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    dy.SetData( imDY->GetBufferPointer() );
    
    ImageData< FloatPixelType > dz;
    dz.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    dz.SetData( imDZ->GetBufferPointer() );
    
    ImageData< ShortPixelType > mask;
    mask.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    mask.SetData( imMask->GetBufferPointer() );
    
    for (unsigned int k = 0; k < szFixed[2]; k++)
    {
        for (unsigned int j = 0; j < szFixed[1]; j++)
        {
            for (unsigned int i = 0; i < szFixed[0]; i++)
            {
                bool mask_femur_x = ( xMovedFemur(i,j,k) >= 0 ) && ( xMovedFemur(i,j,k) < szFixed[0] ) && ( movedFemur(i,j,k) > 0.5 );
                bool mask_femur_y = ( yMovedFemur(i,j,k) >= 0 ) && ( yMovedFemur(i,j,k) < szFixed[1] ) && ( movedFemur(i,j,k) > 0.5 );
                bool mask_femur_z = ( zMovedFemur(i,j,k) >= 0 ) && ( zMovedFemur(i,j,k) < szFixed[2] ) && ( movedFemur(i,j,k) > 0.5 );
                bool mask_femur = mask_femur_x * mask_femur_y * mask_femur_z;
                
                bool mask_tibia_x = ( xMovedTibia(i,j,k) >= 0 ) && ( xMovedTibia(i,j,k) < szFixed[0] ) && ( movedTibia(i,j,k) > 0.5 );
                bool mask_tibia_y = ( yMovedTibia(i,j,k) >= 0 ) && ( yMovedTibia(i,j,k) < szFixed[1] ) && ( movedTibia(i,j,k) > 0.5 );
                bool mask_tibia_z = ( zMovedTibia(i,j,k) >= 0 ) && ( zMovedTibia(i,j,k) < szFixed[2] ) && ( movedTibia(i,j,k) > 0.5 );
                bool mask_tibia = mask_tibia_x * mask_tibia_y * mask_tibia_z;
                
                mask(i,j,k) = (short)(mask_femur || mask_tibia );
                
                dx(i,j,k) = ( ( mask_femur ) ? xMovedFemur(i,j,k) - xFixed(i,j,k) : 0 ) + ( ( mask_tibia ) ? xMovedTibia(i,j,k) - xFixed(i,j,k) : 0 );
                dy(i,j,k) = ( ( mask_femur ) ? yMovedFemur(i,j,k) - yFixed(i,j,k) : 0 ) + ( ( mask_tibia ) ? yMovedTibia(i,j,k) - yFixed(i,j,k) : 0 );
                dz(i,j,k) = ( ( mask_femur ) ? zMovedFemur(i,j,k) - zFixed(i,j,k) : 0 ) + ( ( mask_tibia ) ? zMovedTibia(i,j,k) - zFixed(i,j,k) : 0 );
            }
        }
    }

	
    ElasticCoordinatesDiffusion3D diffuse;
    diffuse.SetSize( szFixed[0], szFixed[1], szFixed[2] );
    diffuse.SetSpacing( spFixed[0], spFixed[1], spFixed[2] );
    diffuse.SetU( dx.GetData() );
    diffuse.SetV( dy.GetData() );
    diffuse.SetW( dz.GetData() );	
    
    diffuse.SetMask( mask.GetData() );
    diffuse.SetLambda( 0.667 );
    diffuse.SetMu( 1.0 );
    diffuse.SetNumberOfIterations( numOfIterations );
    diffuse.SetTimeStep( timestep );
    diffuse.SetDisplayFrequency( 10 );
    diffuse.DoIt();
    
    WriteImage<FloatImageType>::DoIt(outputDisplacementX.c_str(), imDX, (bool)compression);
    WriteImage<FloatImageType>::DoIt(outputDisplacementY.c_str(), imDY, (bool)compression);
    WriteImage<FloatImageType>::DoIt(outputDisplacementZ.c_str(), imDZ, (bool)compression);

    return 0;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType_movingFemur;
    ImageIOBase::IOComponentType componentType_movingFemur;
    
    ImageIOBase::IOPixelType pixelType_movingTibia;
    ImageIOBase::IOComponentType componentType_movingTibia;
    
    ImageIOBase::IOPixelType pixelType_fixedFemur;
    ImageIOBase::IOComponentType componentType_fixedFemur;
    
    ImageIOBase::IOPixelType pixelType_fixedTibia;
    ImageIOBase::IOComponentType componentType_fixedTibia;
    
    try
    {
        GetImageType (inputFixedFemur, pixelType_fixedFemur, componentType_fixedFemur);
        GetImageType (inputFixedTibia, pixelType_fixedTibia, componentType_fixedTibia);
        GetImageType (inputMovingFemur, pixelType_movingFemur, componentType_movingFemur);
        GetImageType (inputMovingTibia, pixelType_movingTibia, componentType_movingTibia);
        
        if (componentType_movingFemur != componentType_movingTibia)
        {
            std::cerr << "Moving femur and tibia should have the same image type!" << std::endl;
            return EXIT_FAILURE;
        }
        else if (componentType_fixedFemur != componentType_fixedTibia)
        {
            std::cerr << "Femoral and tibial cartilage should have the same image type!" << std::endl;
            return EXIT_FAILURE;
        }
        else 
        {
            switch (componentType_fixedFemur) {
                case ImageIOBase::UCHAR:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::CHAR:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::USHORT:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::SHORT:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::UINT:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::INT:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::ULONG:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::LONG:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::FLOAT:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                case ImageIOBase::DOUBLE:
                    switch (componentType_movingFemur) {
                        case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                        case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                        case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                        case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                        case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                        case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                        case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                        case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                        case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                        case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                        default:break;
                    }
                    break;
                default:
                    break;
            }
        }
    
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

FloatImagePointerType cutFemur( FloatImagePointerType femur, double len )
{
    SizeType sz = femur->GetRequestedRegion().GetSize();
    
	DirectionType dir = femur->GetDirection();
	SpacingType sp = femur->GetSpacing();
    IndexType start;
    start.Fill(0);
    
    
	FloatImagePointerType femur_cut = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, femur->GetOrigin(), 0);
    
	double * sum_femur = new double [sz[1]]; 
    
	for (unsigned int i = 0; i < sz[1]; i++)
	{
		sum_femur[i] = 0;
	}
	
	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				sum_femur[j] += femur->GetPixel( idx );
			}
		}
	}
    
	
	unsigned int min_femur =  sz[1]/4;
    
	while(sum_femur[min_femur] <= 1)
	{
		min_femur++;
	}
    
	unsigned int jmax = std::min((unsigned int)sz[1], (unsigned int)floor(min_femur + len/sp[1]));
    
	for (unsigned int i = 0; i < sz[0]; i++)
	{
		for (unsigned int j = min_femur; j < jmax; j++)
		{
			for (unsigned int k = 0; k < sz[2]; k++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				femur_cut->SetPixel(idx, femur->GetPixel(idx));
			}
		}
	}
    return femur_cut;
}


FloatImagePointerType cutTibia( FloatImagePointerType tibia, double len )
{
    SizeType sz = tibia->GetRequestedRegion().GetSize();
    
	DirectionType dir = tibia->GetDirection();
	SpacingType sp = tibia->GetSpacing();
    IndexType start;
    start.Fill(0);
    
    
	FloatImagePointerType tibia_cut = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, tibia->GetOrigin(), 0);
    
    
    double * sum_tibia = new double [sz[1]];
	
	for (unsigned int i = 0; i < sz[1]; i++)
	{
		sum_tibia[i] = 0;
	}
	
	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = 0; j < sz[1]; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				sum_tibia[j] += tibia->GetPixel( idx );
			}
		}
	}
	
	unsigned int max_tibia = 3*sz[1]/4;
    
	while(sum_tibia[max_tibia] <= 1)
	{
		max_tibia--;
	}
    
	
	unsigned int jmin = std::max((int)0, (int)ceil(max_tibia - len/sp[1]));
    
    
	for (unsigned int k = 0; k < sz[2]; k++)
	{
		for (unsigned int j = jmin; j < max_tibia; j++)
		{
			for (unsigned int i = 0; i < sz[0]; i++)
			{
				IndexType idx;
				idx[0] = i;
				idx[1] = j;
				idx[2] = k;
				tibia_cut->SetPixel(idx, tibia->GetPixel(idx));
			}
		}
	}
    return tibia_cut;
    
}



FloatImagePointerType applyAffineTransform( FloatImagePointerType imMoving, AffineTransformPointerType transform, SizeType sz, SpacingType sp, DirectionType dir, PointType origin)
{
	ResamplerPointerType resampler = ResamplerType::New();
	resampler->SetTransform( transform );
	resampler->SetInput( imMoving );
	
	resampler->SetSize( sz );
	resampler->SetOutputOrigin( origin );
	resampler->SetOutputSpacing( sp );
	resampler->SetOutputDirection( dir );
	resampler->SetDefaultPixelValue( 0 );
	resampler->Update();
	return resampler->GetOutput();
    
}
