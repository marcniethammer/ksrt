#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>

#include <itkImage.h>
#include <itkImageFileReader.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtDiffusion3D.h>

#include <itkPluginUtilities.h>
#include "ksrtComputeLocalFusionWeightCLP.h"

using namespace std;
using namespace itk;

typedef float											FloatPixelType;
typedef Image< FloatPixelType, 3 >						FloatImageType;
typedef FloatImageType::Pointer							FloatImagePointerType;

typedef short											ShortPixelType;
typedef Image< ShortPixelType, 3 >						ShortImageType;
typedef ShortImageType::Pointer							ShortImagePointerType;

typedef FloatImageType::SizeType						SizeType;
typedef FloatImageType::SpacingType						SpacingType;
typedef FloatImageType::DirectionType					DirectionType;
typedef FloatImageType::IndexType						IndexType;
typedef FloatImageType::PointType						PointType;


using namespace std;
using namespace itk;

template< class T1, class T2 >
int DoIt( int argc, char * argv[], T1, T2 )
{
    
    PARSE_ARGS;
    
    typedef Image< T1, 3 >              T1Image;
    typedef Image< T2, 3 >              T2Image;
    
    FloatImagePointerType imLabel = Cast< T1Image, FloatImageType >::DoIt( ReadImage< T1Image >::DoIt( inputLabel.c_str() ) );
    FloatImagePointerType imOriginal = Cast< T2Image, FloatImageType >::DoIt( ReadImage< T2Image >::DoIt( inputOriginal.c_str() ) );
    
    SpacingType sp = imOriginal->GetSpacing();
    SizeType sz = imOriginal->GetRequestedRegion().GetSize();
    DirectionType dir = imOriginal->GetDirection();
    
    IndexType start;
    start.Fill(0);
    
    PointType origin = imOriginal->GetOrigin();
    
    FloatImagePointerType imWeight = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                float label = imLabel->GetPixel( idx );
                float original = imOriginal->GetPixel( idx );
                float weight = 1.0f/(0.01*fabs(label-original)+0.001);
                imWeight->SetPixel(idx, weight);
            }
        }
    }

    Diffusion3D diffusion;
    diffusion.SetImageData( imWeight->GetBufferPointer() );
    diffusion.SetSize(sz[0], sz[1], sz[2]);
    diffusion.SetSpacing(sp[0], sp[1], sp[2]);
    diffusion.SetDisplayFrequency(10);
    diffusion.SetTimeStep( 0.02 );
    diffusion.SetN(11);
    diffusion.DoIt();
    
    WriteImage< FloatImageType >::DoIt( outputWeight.c_str(), imWeight, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType_label;
    ImageIOBase::IOComponentType componentType_label;
    
    ImageIOBase::IOPixelType pixelType_original;
    ImageIOBase::IOComponentType componentType_original;
    
    try
    {
        GetImageType (inputLabel, pixelType_label, componentType_label);
        GetImageType (inputOriginal, pixelType_original, componentType_original);
        
        switch (componentType_label) {
            case ImageIOBase::UCHAR:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::CHAR:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<char>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<char>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<char>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<char>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<char>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<char>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::USHORT:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::SHORT:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<short>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<short>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<short>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<short>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<short>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<short>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::UINT:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::INT:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<int>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<int>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<int>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<int>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<int>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<int>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::ULONG:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<unsigned long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::LONG:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<long>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<long>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<long>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<long>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<long>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<long>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::FLOAT:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<float>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<float>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<float>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<float>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<float>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<float>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            case ImageIOBase::DOUBLE:
                switch (componentType_original) {
                    case ImageIOBase::UCHAR:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned char>(0));      break;
                    case ImageIOBase::CHAR:     return DoIt(argc, argv, static_cast<double>(0), static_cast<char>(0));               break;
                    case ImageIOBase::USHORT:   return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned short>(0));     break;
                    case ImageIOBase::SHORT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<short>(0));              break;
                    case ImageIOBase::UINT:     return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned int>(0));       break;
                    case ImageIOBase::INT:      return DoIt(argc, argv, static_cast<double>(0), static_cast<int>(0));                break;
                    case ImageIOBase::ULONG:    return DoIt(argc, argv, static_cast<double>(0), static_cast<unsigned long>(0));      break;
                    case ImageIOBase::LONG:     return DoIt(argc, argv, static_cast<double>(0), static_cast<long>(0));               break;
                    case ImageIOBase::FLOAT:    return DoIt(argc, argv, static_cast<double>(0), static_cast<float>(0));              break;
                    case ImageIOBase::DOUBLE:   return DoIt(argc, argv, static_cast<double>(0), static_cast<double>(0));             break;
                    default:break;
                }
                break;
            default:
                break;
        }
        
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}