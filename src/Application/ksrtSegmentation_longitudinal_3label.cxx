#include <iostream>
#include <itkImage.h>

#include <Common/ksrtImageData.h>
#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>
#include <Common/ksrtNew3DImage.h>

#include <Segmentation/ksrtLongitudinalThreeLabelSegmentation.h>

#include <itkPluginUtilities.h>
#include "ksrtSegmentation_longitudinal_3labelCLP.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace itk;


int DoIt( int argc, char * argv[] )
{
    
    PARSE_ARGS;

    const unsigned int Dim = 3;
    typedef short								ShortPixelType;
    typedef Image< ShortPixelType, Dim >		ShortImageType;
    typedef ShortImageType::Pointer				ShortImagePointerType;

    typedef float								FloatPixelType;
    typedef Image< FloatPixelType, Dim >		FloatImageType;
    typedef FloatImageType::Pointer				FloatImagePointerType;
    
    typedef FloatImageType::RegionType          RegionType;
    typedef FloatImageType::SizeType			SizeType;
    typedef FloatImageType::SpacingType			SpacingType;
    typedef FloatImageType::DirectionType		DirectionType;
    typedef FloatImageType::IndexType			IndexType;
    typedef FloatImageType::PointType			PointType;

    ifstream inList;
    inList.open( inputList.c_str() );
    
    if ( ! inList.is_open())
    {
        cout << "Can't open file " << inputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }
    
    unsigned int numOfVisits = 0;
    string tmp;
    while (getline(inList,tmp)) {
        numOfVisits++;
    }
    inList.close();
    
    inList.open( inputList.c_str() );
    
    if ( ! inList.is_open())
    {
        cout << "Can't open file " << inputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }
    
    float * times = new float[ numOfVisits ];
    
    string inputFemCost;
    string inputBgCost;
    string inputTibCost;
    string inputTime;
    
    inList >> inputFemCost;
    inList >> inputBgCost;
    inList >> inputTibCost;
    inList >> inputTime;
    
    FloatImagePointerType imFemCost = ReadImage< FloatImageType >::DoIt( inputFemCost.c_str() );
    FloatImagePointerType imTibCost = ReadImage< FloatImageType >::DoIt( inputTibCost.c_str() );
    FloatImagePointerType imBgCost = ReadImage< FloatImageType >::DoIt( inputBgCost.c_str() );
    
    SizeType sz = imFemCost->GetRequestedRegion().GetSize();
    SpacingType sp =  imFemCost->GetSpacing();
    DirectionType dir = imFemCost->GetDirection();
    PointType origin = imFemCost->GetOrigin();
    IndexType start;
    start.Fill(0);
 
    float * r0 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * r1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * r2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    
    float * u1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * u2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    
    float * px1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * px2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * py1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * py2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * pz1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * pz2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    
    float * q0 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * q1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * q2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    
    float * s1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * s2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    
    float * g1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * g2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * h1 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    float * h2 = new float [sz[0]*sz[1]*sz[2]*numOfVisits];
    
    for (unsigned int i = 0; i < sz[0]*sz[1]*sz[2]*numOfVisits; i++)
    {
        r0[i] = 0;
        r1[i] = 0;
        r2[i] = 0;
        
        u1[i] = 0.33;
        u2[i] = 0.67;
        
        px1[i] = 0;
        px2[i] = 0;
        py1[i] = 0;
        py2[i] = 0;
        pz1[i] = 0;
        pz2[i] = 0;
        
        q0[i] = 0;
        q1[i] = 0;
        q2[i] = 0;
        
        s1[i] = 0;
        s2[i] = 0;
        
        g1[i] = g;
        g2[i] = g;
        h1[i] = h;
        h2[i] = h;
    }
    
    unsigned int t = 0;
    for (unsigned int k = 0; k < sz[2]; k++)
    {
        for (unsigned int j = 0; j < sz[1]; j++)
        {
            for (unsigned int i = 0; i < sz[0]; i++)
            {
                IndexType idx;
                idx[0] = i;
                idx[1] = j;
                idx[2] = k;
                r0[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] = imFemCost->GetPixel( idx );
                r1[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] = imBgCost->GetPixel( idx );
                r2[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] = imTibCost->GetPixel( idx );
                
            }
        }
    }
    times[t] = atof(inputTime.c_str());
    
    t = 1;
    while (t < numOfVisits)
    {
        inList >> inputFemCost;
        inList >> inputBgCost;
        inList >> inputTibCost;
        inList >> inputTime;
        
        imFemCost = ReadImage< FloatImageType >::DoIt( inputFemCost.c_str() );
        imTibCost = ReadImage< FloatImageType >::DoIt( inputTibCost.c_str() );
        imBgCost = ReadImage< FloatImageType >::DoIt( inputBgCost.c_str() );
        
        for (unsigned int k = 0; k < sz[2]; k++)
        {
            for (unsigned int j = 0; j < sz[1]; j++)
            {
                for (unsigned int i = 0; i < sz[0]; i++)
                {
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    r0[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] = imFemCost->GetPixel( idx );
                    r1[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] = imBgCost->GetPixel( idx );
                    r2[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] = imTibCost->GetPixel( idx );
                    
                }
            }
        }
        times[t] = atof(inputTime.c_str());
        t++;
    }
    
//    double sum = 0;
//    for (unsigned int i = 0; i < sz[0]*sz[1]*sz[2]*numOfVisits; i++)
//    {
//        sum += r0[i];
//        sum += r1[i];
//        sum += r2[i];
//    }
    
    inList.close();
    
    LongitudinalThreeLabelSegmentation seg;
    seg.SetSize(sz[0], sz[1], sz[2], numOfVisits);
    seg.SetSpatialSpacing(sp[0], sp[1], sp[2]);
    
    seg.SetU1(u1);
    seg.SetU2(u2);
    
    seg.SetR0(r0);
    seg.SetR1(r1);
    seg.SetR2(r2);
    
    seg.SetPX1(px1);
    seg.SetPX2(px2);
    seg.SetPY1(py1);
    seg.SetPY2(py2);
    seg.SetPZ1(pz1);
    seg.SetPZ2(pz2);
    
    seg.SetQ0(q0);
    seg.SetQ1(q1);
    seg.SetQ2(q2);
    
    seg.SetS1(s1);
    seg.SetS2(s2);
    
    seg.SetG1(g1);
    seg.SetG2(g2);
    seg.SetH1(h1);
    seg.SetH2(h2);
    
    seg.SetTime(times);
    
    seg.SetNumberOfIterations(1000);
    seg.SetTimeStep(0.2);
    seg.SetGapThreshold(5e-3);
    seg.SetDisplayFrequency(10);
    seg.DoIt();
    
    ShortImagePointerType imSegFem = New3DImage< ShortImageType >::DoIt(sz, sp, dir, start, origin, 0);
    ShortImagePointerType imSegTib = New3DImage< ShortImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imU1 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);
    FloatImagePointerType imU2 = New3DImage< FloatImageType >::DoIt(sz, sp, dir, start, origin, 0);

    ifstream outList;
    outList.open( outputList.c_str() );
    
    if ( ! outList.is_open())
    {
        cout << "Can't open file " << outputList.c_str() << "!" << endl;
        return EXIT_FAILURE;
    }

    string outputFemSeg;
    string outputTibSeg;
    string tmpT;

    t = 0;
    
    while (t < numOfVisits)
    {
        outList >> outputFemSeg;
        outList >> outputTibSeg;
		outList >> tmpT;
        
        cout << outputFemSeg.c_str() << endl;
        cout << outputTibSeg.c_str() << endl;
        
        for (unsigned int k = 0; k < sz[2]; k++)
        {
            for (unsigned int j = 0; j < sz[1]; j++)
            {
                for (unsigned int i = 0; i < sz[0]; i++)
                {
                    float diff_0 = u1[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]];
                    float diff_1 = u2[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]] - u1[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]];
                    float diff_2 = 1 - u2[i+j*sz[0]+k*sz[0]*sz[1]+t*sz[0]*sz[1]*sz[2]];
                    
                    IndexType idx;
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    
                    imSegFem->SetPixel(idx, (short)((diff_0 > diff_1) && (diff_0 > diff_2)));
                    imSegTib->SetPixel(idx, (short)((diff_2 > diff_0) && (diff_2 > diff_1)));
                }
            }
        }
        
        WriteImage< ShortImageType >::DoIt( outputFemSeg.c_str(), imSegFem, (bool)compression );
        WriteImage< ShortImageType >::DoIt( outputTibSeg.c_str(), imSegTib, (bool)compression );

        t++;           
    }
  
    outList.close();
    delete [] r0;
    delete [] r1;
    delete [] r2;
    
    delete [] u1;
    delete [] u2;
    
    delete [] px1;
    delete [] px2;
    delete [] py1;
    delete [] py2;
    delete [] pz1;
    delete [] pz2;
    
    delete [] q0;
    delete [] q1;
    delete [] q2;
    
    delete [] s1;
    delete [] s2;
    
    delete [] g1;
    delete [] g2;
    delete [] h1;
    delete [] h2;
    
    return 0;
    
}

int main(int argc, char * argv[])
{
    PARSE_ARGS;
    
    return DoIt( argc, argv );
}
