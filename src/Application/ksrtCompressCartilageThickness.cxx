#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtCast.h>


#include <itkPluginUtilities.h>

#include "ksrtCompressCartilageThicknessCLP.h"

using namespace std;
using namespace itk;

template<class T> 
int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;

    typedef Image< T, 3 >                       InputImageType;
    
    typedef Image< float, 3 >                   FloatImageType;
    typedef FloatImageType::Pointer             FloatImagePointerType;
    
    typedef Image< float, 2 >                   OutputImageType;
    typedef OutputImageType::Pointer            OutputImagePointerType;
    
    FloatImagePointerType thickness = Cast< InputImageType, FloatImageType >::DoIt( ReadImage< InputImageType >::DoIt( inputVolume.c_str() ) );
    
    OutputImagePointerType output = OutputImageType::New();
    
    OutputImageType::IndexType start;
    start.Fill(0);
    
    FloatImageType::SizeType size = thickness->GetRequestedRegion().GetSize();
    OutputImageType::SizeType sz;
    sz[0] = size[0];
    sz[1] = size[2];

    FloatImageType::SpacingType spacing= thickness->GetSpacing();
    OutputImageType::SpacingType sp;
    sp[0] = spacing[0];
    sp[1] = spacing[2];
    
    FloatImageType::DirectionType direction = thickness->GetDirection();
    OutputImageType::DirectionType dir;
    dir[0][0] = direction[0][0];
    dir[0][1] = direction[0][2];
    dir[1][0] = direction[1][0];
    dir[1][1] = direction[1][2];
    
    OutputImageType::RegionType region;
    region.SetSize( sz );
    region.SetIndex( start );
    
    OutputImageType::PointType origin;
    origin[0] = 0.0;
    origin[0] = 0.0;
    
    output->SetRegions( region );
    output->SetSpacing( sp );
    output->SetOrigin( origin );
    output->SetDirection( dir );
    output->Allocate();
    
    float threshold = 0.1;
    for (unsigned int j = 0; j < sz[1]; j++)
    {
        for (unsigned int i = 0; i < sz[0]; i++)
        {
            OutputImageType::IndexType idx;
            idx[0] = i;
            idx[1] = j;
            vector<float> myvector;
            
            for (unsigned int k = 0; k < size[1]; k++)
            {
                FloatImageType::IndexType index;
                index[0] = i;
                index[1] = k;
                index[2] = j;
                float tmp = thickness->GetPixel( index );
                if (tmp > threshold) 
                {
                    myvector.push_back( tmp );
                }
            }
            if (myvector.size()==0)
            {
                output->SetPixel( idx, 0 );
            }
            else if (myvector.size()%2 == 0)
            {
                sort(myvector.begin(), myvector.end());
                unsigned int half = myvector.size()/2;
                output->SetPixel( idx, 0.5*(myvector[half-1]+myvector[half]) );
            }
            else
            {
                sort(myvector.begin(), myvector.end());
                unsigned int half = (myvector.size()-1)/2;
                output->SetPixel( idx, myvector[half] );
            }
            myvector.clear();
//            double sum = 0;
//            int count = 0;
//            for (unsigned int k = 0; k < size[1]; k++)
//            {
//                FloatImageType::IndexType index;
//                index[0] = i;
//                index[1] = k;
//                index[2] = j;
//                float tmp = thickness->GetPixel( index );
//                sum += ( tmp > threshold ) ? tmp : 0;
//                count += ( tmp > threshold ) ? 1 : 0;
//            }
//            float avg = (count > 0) ? sum/count : 0;
//            output->SetPixel( idx, avg );
        }
    }
    WriteImage< OutputImageType >::DoIt( outputVolume.c_str(), output, (bool)compression );
    
    return EXIT_SUCCESS;
}

int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}