#include <iostream>
#include <itkImage.h>

#include <Common/ksrtReadImage.h>
#include <Common/ksrtWriteImage.h>
#include <Common/ksrtNew3DImage.h>
#include <Common/ksrtCast.h>

#include <itkPluginUtilities.h>

#include "ksrtScaleIntensitiesCLP.h"

using namespace std;
using namespace itk;


typedef itk::Image< float, 3 >              FloatImageType;
typedef FloatImageType::Pointer             FloatImagePointerType;
typedef itk::Image< short, 3 >              ShortImageType;
typedef ShortImageType::Pointer             ShortImagePointerType;
typedef FloatImageType::SizeType            SizeType;
typedef FloatImageType::SpacingType         SpacingType;
typedef FloatImageType::DirectionType       DirectionType;   
typedef FloatImageType::PointType           PointType;
typedef FloatImageType::IndexType           IndexType;

void filterOutliers( FloatImagePointerType im, float newMaxValue );

template<class T> int DoIt( int argc, char * argv[], T )
{
    
    PARSE_ARGS;
    
    typedef itk::Image< T, 3 >              ImageType;   
    
    FloatImagePointerType im = Cast< ImageType, FloatImageType >::DoIt( ReadImage< ImageType >::DoIt( inputVolume.c_str() ) );
    
    filterOutliers( im, newMaxValue);
    
    WriteImage< ShortImageType >::DoIt( outputVolume.c_str(), Cast< FloatImageType, ShortImageType >::DoIt( im ), (bool)compression );
    return EXIT_SUCCESS;
}


int main( int argc, char * argv[] )
{
    
    PARSE_ARGS;
    
    ImageIOBase::IOPixelType pixelType;
    ImageIOBase::IOComponentType componentType;
    
    try
    {
        GetImageType (inputVolume, pixelType, componentType);
        
        switch (componentType)
        {
            case ImageIOBase::UCHAR:
                return DoIt( argc, argv, static_cast<unsigned char>(0));
                break;
            case ImageIOBase::CHAR:
                return DoIt( argc, argv, static_cast<char>(0));
                break;
            case ImageIOBase::USHORT:
                return DoIt( argc, argv, static_cast<unsigned short>(0));
                break;
            case ImageIOBase::SHORT:
                return DoIt( argc, argv, static_cast<short>(0));
                break;
            case ImageIOBase::UINT:
                return DoIt( argc, argv, static_cast<unsigned int>(0));
                break;
            case ImageIOBase::INT:
                return DoIt( argc, argv, static_cast<int>(0));
                break;
            case ImageIOBase::ULONG:
                return DoIt( argc, argv, static_cast<unsigned long>(0));
                break;
            case ImageIOBase::LONG:
                return DoIt( argc, argv, static_cast<long>(0));
                break;
            case ImageIOBase::FLOAT:
                return DoIt( argc, argv, static_cast<float>(0));
                break;
            case ImageIOBase::DOUBLE:
                return DoIt( argc, argv, static_cast<double>(0));
                break;
            case ImageIOBase::UNKNOWNCOMPONENTTYPE:
            default:
                cout << "unknown component type" << endl;
                break;
        }
        
    }
    catch( itk::ExceptionObject &excep)
    {
        std::cerr << argv[0] << ": exception caught !" << std::endl;
        std::cerr << excep << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void filterOutliers( FloatImagePointerType im, float newMaxValue )
{
	double thresh = 1e-3;
	itk::ImageRegionIterator< FloatImageType > iter( im, im->GetRequestedRegion() );
	float maxValue = 0;
	float minValue = 1000;
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		
		if( iter.Get() < 0 )
			iter.Set( 0 );
		if( iter.Get() > maxValue )
			maxValue = iter.Get();		
		if( iter.Get() < minValue )
			minValue = iter.Get();
	} 
	float cutValue = maxValue;
	double ratio = 0.0;
	unsigned int numOutliers = 0;
	SizeType size = im->GetRequestedRegion().GetSize();
	unsigned int numTotal = size[0]*size[1]*size[2];
    unsigned int numOfIter = 0;
    unsigned int maxNumOfIter = 100;
	do
	{
        numOfIter ++;
		numOutliers = 0;
		cutValue = (cutValue + minValue)/20*19;
		for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
			numOutliers = (iter.Get() > cutValue) ? numOutliers+1 : numOutliers;
		ratio = (double)numOutliers / (double)numTotal;
	}
	while( (ratio < thresh) && (numOfIter < maxNumOfIter) );
	for( iter.GoToBegin(); !iter.IsAtEnd(); ++iter )
	{
		float tmp_value = (iter.Get() > cutValue) ? newMaxValue : iter.Get()/cutValue*newMaxValue;
		iter.Set( tmp_value );
	}
}
