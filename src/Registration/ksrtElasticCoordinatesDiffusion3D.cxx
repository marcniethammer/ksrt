#ifndef _ksrtElasticCoordinatesDiffusion3D_cxx
#define _ksrtElasticCoordinatesDiffusion3D_cxx

#include "ksrtElasticCoordinatesDiffusion3D.h"

void ElasticCoordinatesDiffusion3D::SetU( float * u )
{
    m_u.SetData( u );
}

void ElasticCoordinatesDiffusion3D::SetV( float * v )
{
    m_v.SetData( v );
}

void ElasticCoordinatesDiffusion3D::SetW( float * w )
{
    m_w.SetData( w );
}

void ElasticCoordinatesDiffusion3D::SetMask( short * mask )
{
    m_mask.SetData( mask );
}

void ElasticCoordinatesDiffusion3D::SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 )
{
    m_size[0] = sz0;
    m_size[1] = sz1;
    m_size[2] = sz2;
    
    m_u.SetSize( m_size[0], m_size[1], m_size[2] );
    m_v.SetSize( m_size[0], m_size[1], m_size[2] );
    m_w.SetSize( m_size[0], m_size[1], m_size[2] );
    m_mask.SetSize( m_size[0], m_size[1], m_size[2] );
}

void ElasticCoordinatesDiffusion3D::SetSpacing( float sp0, float sp1, float sp2 )
{
    m_spacing[0] = sp0;
    m_spacing[1] = sp1;
    m_spacing[2] = sp2;
}

void ElasticCoordinatesDiffusion3D::SetTimeStep( float tau )
{
    m_tau = tau;
}

void ElasticCoordinatesDiffusion3D::SetDisplayFrequency( unsigned int dispFreq )
{
    m_dispFreq = dispFreq;
}

void ElasticCoordinatesDiffusion3D::SetMu( float mu )
{
    m_mu = mu;
}

void ElasticCoordinatesDiffusion3D::SetLambda( float lambda )
{
    m_lambda = lambda;
}

void ElasticCoordinatesDiffusion3D::SetNumberOfIterations( unsigned int N )
{
    m_N = N;
}


void ElasticCoordinatesDiffusion3D::DoIt()
{
    for (unsigned int n = 0; n < m_N; n++)
    {
        if (n%m_dispFreq == 0)
			std::cout << "Iteration " << n << std::endl;

        for (unsigned int k = 0; k < m_size[2]; k++)
        {
            for (unsigned int j = 0; j < m_size[1]; j++)
            {
                for (unsigned int i = 0; i < m_size[0]; i++)
                {
                    float u_xp = (i < m_size[0]-1) ? m_u(i+1,j,k) : m_u(m_size[0]-1,j,k);
                    float u_yp = (j < m_size[1]-1) ? m_u(i,j+1,k) : m_u(i,m_size[1]-1,k);
                    float u_zp = (k < m_size[2]-1) ? m_u(i,j,k+1) : m_u(i,j,m_size[2]-1);
                    
                    float u_xm = (i > 0) ? m_u(i-1,j,k) : m_u(0,j,k);
                    float u_ym = (j > 0) ? m_u(i,j-1,k) : m_u(i,0,k);
                    float u_zm = (k > 0) ? m_u(i,j,k-1) : m_u(i,j,0);
                    
                    float v_xp = (i < m_size[0]-1) ? m_v(i+1,j,k) : m_v(m_size[0]-1,j,k);
                    float v_yp = (j < m_size[1]-1) ? m_v(i,j+1,k) : m_v(i,m_size[1]-1,k);
                    float v_zp = (k < m_size[2]-1) ? m_v(i,j,k+1) : m_v(i,j,m_size[2]-1);
                    
                    float v_xm = (i > 0) ? m_v(i-1,j,k) : m_v(0,j,k);
                    float v_ym = (j > 0) ? m_v(i,j-1,k) : m_v(i,0,k);
                    float v_zm = (k > 0) ? m_v(i,j,k-1) : m_v(i,j,0);
                    
                    float w_xp = (i < m_size[0]-1) ? m_w(i+1,j,k) : m_w(m_size[0]-1,j,k);
                    float w_yp = (j < m_size[1]-1) ? m_w(i,j+1,k) : m_w(i,m_size[1]-1,k);
                    float w_zp = (k < m_size[2]-1) ? m_w(i,j,k+1) : m_w(i,j,m_size[2]-1);
                    
                    float w_xm = (i > 0) ? m_w(i-1,j,k) : m_w(0,j,k);
                    float w_ym = (j > 0) ? m_w(i,j-1,k) : m_w(i,0,k);
                    float w_zm = (k > 0) ? m_w(i,j,k-1) : m_w(i,j,0);
                    
                    
                    float lapU = u_xp + u_yp + u_zp + u_xm + u_ym + u_zm - 6 * m_u(i,j,k);
                    lapU /= ( m_spacing[0] * m_spacing[0] );
                    
                    float lapV = v_xp + v_yp + v_zp + v_xm + v_ym + v_zm - 6 * m_v(i,j,k);
                    lapV /= ( m_spacing[1] * m_spacing[1] );
                    
                    float lapW = w_xp + w_yp + w_zp + w_xm + w_ym + w_zm - 6 * m_w(i,j,k);
                    lapW /= ( m_spacing[2] * m_spacing[2] );
                    
                    float uxx = u_xp + u_xm - 2 * m_u(i,j,k);
                    uxx /= ( m_spacing[0] * m_spacing[0] );
                    float vyy = v_yp + v_ym - 2 * m_v(i,j,k);
                    vyy /= ( m_spacing[1] * m_spacing[1] );
                    float wzz = w_zp + w_zm - 2 * m_w(i,j,k);
                    wzz /= ( m_spacing[2] * m_spacing[2] );
                    
                    
                    float u_xpyp = m_u(i < m_size[0]-1 ? i+1 : m_size[0]-1, j < m_size[1]-1 ? j+1 : m_size[1]-1, k);
                    float u_xmym = m_u(i > 0 ? i-1 : 0, j > 0 ? j-1 : 0, k);
                    float u_xpym = m_u(i < m_size[0]-1 ? i+1 : m_size[0]-1, j > 0 ? j-1 : 0, k);
                    float u_xmyp = m_u(i > 0 ? i-1 : 0, j < m_size[1]-1 ? j+1 : m_size[1]-1, k);
                    
                    float u_xpzp = m_u(i < m_size[0]-1 ? i+1 : m_size[0]-1, j, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    float u_xmzm = m_u(i > 0 ? i-1 : 0, j, k > 0 ? k-1 : 0);
                    float u_xpzm = m_u(i < m_size[0]-1 ? i+1 : m_size[0]-1, j, k > 0 ? k-1 : 0);
                    float u_xmzp = m_u(i > 0 ? i-1 : 0, j, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    
                    float v_xpyp = m_v(i < m_size[0]-1 ? i+1 : m_size[0]-1, j < m_size[1]-1 ? j+1 : m_size[1]-1, k);
                    float v_xmym = m_v(i > 0 ? i-1 : 0, j > 0 ? j-1 : 0, k);
                    float v_xpym = m_v(i < m_size[0]-1 ? i+1 : m_size[0]-1, j > 0 ? j-1 : 0, k);
                    float v_xmyp = m_v(i > 0 ? i-1 : 0, j < m_size[1]-1 ? j+1 : m_size[1]-1, k);
                    
                    float v_ypzp = m_v(i, j < m_size[1]-1 ? j+1 : m_size[1]-1, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    float v_ymzm = m_v(i, j > 0 ? j-1 : 0, k > 0 ? k-1 : 0);
                    float v_ypzm = m_v(i, j < m_size[1]-1 ? j+1 : m_size[1]-1, k > 0 ? k-1 : 0);
                    float v_ymzp = m_v(i, j > 0 ? j-1 : 0, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    
                    float w_xpzp = m_w(i < m_size[0]-1 ? i+1 : m_size[0]-1, j, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    float w_xmzm = m_w(i > 0 ? i-1 : 0, j, k > 0 ? k-1 : 0);
                    float w_xpzm = m_w(i < m_size[0]-1 ? i+1 : m_size[0]-1, j, k > 0 ? k-1 : 0);
                    float w_xmzp = m_w(i > 0 ? i-1 : 0, j, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    
                    float w_ypzp = m_w(i, j < m_size[1]-1 ? j+1 : m_size[1]-1, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    float w_ymzm = m_w(i, j > 0 ? j-1 : 0, k > 0 ? k-1 : 0);
                    float w_ypzm = m_w(i, j < m_size[1]-1 ? j+1 : m_size[1]-1, k > 0 ? k-1 : 0);
                    float w_ymzp = m_w(i, j > 0 ? j-1 : 0, k < m_size[2]-1 ? k+1 : m_size[2]-1);
                    
                    float uxy = u_xpyp + u_xmym - u_xpym - u_xmyp;
                    uxy /= ( 4 * m_spacing[0] * m_spacing[1] );
                    float uxz = u_xpzp + u_xmzm - u_xpzm - u_xmzp;
                    uxz /= ( 4 * m_spacing[0] * m_spacing[2] );
                    float vxy = v_xpyp + v_xmym - v_xpym - v_xmyp;
                    vxy /= ( 4 * m_spacing[0] * m_spacing[1] );
                    float vyz = v_ypzp + v_ymzm - v_ypzm - v_ymzp;
                    vyz /= ( 4 * m_spacing[1] * m_spacing[2] );	
                    float wxz = w_xpzp + w_xmzm - w_xpzm - w_xmzp;
                    wxz /= ( 4 * m_spacing[0] * m_spacing[2] );
                    float wyz = w_ypzp + w_ymzm - w_ypzm - w_ymzp;
                    wyz /= ( 4 * m_spacing[1] * m_spacing[2] );

                    m_u(i,j,k) += ( 1.0 - (float)m_mask(i,j,k) ) * m_tau * ( m_mu * lapU + ( m_lambda + m_mu ) * ( uxx + vxy + wxz ) );
                    m_v(i,j,k) += ( 1.0 - (float)m_mask(i,j,k) ) * m_tau * ( m_mu * lapV + ( m_lambda + m_mu ) * ( uxy + vyy + wyz ) );
                    m_w(i,j,k) += ( 1.0 - (float)m_mask(i,j,k) ) * m_tau * ( m_mu * lapW + ( m_lambda + m_mu ) * ( uxz + vyz + wzz ) );
                }
            }
        }

    }// end for n
}




#endif//_ksrtElasticCoordinatesDiffusion3D_cxx
