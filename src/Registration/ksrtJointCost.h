#ifndef _ksrtJointCost_h
#define _ksrtJointCost_h

#include <cmath>
#include <iostream>

#include <itkPoint.h>
#include <itkImageBase.h>
#include <itkTransform.h>
#include <itkSpatialObject.h>
#include <itkCovariantVector.h>

#include <itkInterpolateImageFunction.h>
#include <itkSingleValuedCostFunction.h>

#include <itkImageToImageMetric.h>
#include <itkGradientImageFilter.h>
#include <itkResampleImageFilter.h>
#include <itkGradientRecursiveGaussianImageFilter.h>
#include <itkCenteredTransformInitializer.h>

template< class TFixedImage, class TMovingImage >
class JointCost : public itk::ImageToImageMetric< TFixedImage, TMovingImage >
{
public:

	typedef itk::ImageToImageMetric< TFixedImage, TMovingImage >        SuperClass;
    
    typedef typename SuperClass::FixedImageType                         FixedImageType;
    typedef typename SuperClass::MovingImageType                        MovingImageType;
    typedef typename SuperClass::FixedImageConstPointer                 FixedImageConstPointerType;
    typedef typename SuperClass::MovingImageConstPointer                MovingImageConstPointerType;
    typedef typename SuperClass::OutputPointType                        OutputPointType;
	typedef typename SuperClass::InputPointType                         InputPointType;
    typedef typename SuperClass::MeasureType                            MeasureType;
    typedef typename SuperClass::DerivativeType                         DerivativeType;
    typedef typename SuperClass::ParametersType                         ParametersType;
    typedef typename SuperClass::TransformJacobianType                  TransformJacobianType;    
    typedef typename SuperClass::CoordinateRepresentationType           CoordinateRepresentationType;
	typedef itk::AffineTransform< double, 3 >                           TransformType;
    typedef typename TransformType::Pointer                             TransformPointerType;
	typedef itk::LinearInterpolateImageFunction< TMovingImage, double > InterpolatorType;
    typedef typename InterpolatorType::Pointer                          InterpolatorPointerType;
    
    typedef typename TFixedImage::Pointer                               FixedImagePointerType;
	typedef typename TMovingImage::Pointer                              MovingImagePointerType;
    
    typedef double                                                      RealType;
	typedef itk::CovariantVector< double,3 >                            GradientPixelType;
	typedef itk::Image< GradientPixelType,3 >                           GradientImageType;
	typedef typename GradientImageType::Pointer                         GradientImagePointerType;
    
	typedef typename itk::CenteredTransformInitializer< TransformType, 
                                          TFixedImage, 
                                          TMovingImage >                TransformInitializerType;
	typedef typename TransformInitializerType::Pointer                  TransformInitializerPointerType;
	typedef itk::ImageRegionConstIteratorWithIndex< FixedImageType >    FixedIteratorType;
	typedef itk::ImageRegionConstIteratorWithIndex< GradientImageType > GradientIteratorType;
	typedef itk::GradientImageFilter< TMovingImage, double, double >    GradientImageFilterType;
    typedef typename GradientImageFilterType::Pointer                   GradientImageFilterPointerType;
    
	typedef typename OutputPointType::CoordRepType                      CoordRepType;
	typedef itk::ContinuousIndex< CoordRepType, 3 >                     MovingImageContinuousIndexType;
	
	typedef typename FixedImageType::IndexType                          FixedImageIndexType;
    
    JointCost();
    unsigned int GetNumberOfParameters() const;
    void SetMovingImageA( MovingImagePointerType imMovingA );
    void SetMovingImageB( MovingImagePointerType imMovingB );
    void SetFixedImage( FixedImagePointerType imFixed );
    void SetInitialTransformA( TransformPointerType transformA );
    void SetInitialTransformB( TransformPointerType transformB );
    void SetTransformParameters( const ParametersType & parameters ) const;
    void GetDerivative( const ParametersType & parameters, DerivativeType & derivative ) const;
    MeasureType GetValue( const ParametersType & parameters ) const;
    void GetValueAndDerivative( const ParametersType & parameters, MeasureType& Value, DerivativeType& Derivative ) const;
    
private:
    
    unsigned int m_ParametersDimension;
    unsigned int m_ImageDimension;
    
    FixedImageConstPointerType m_imFixed;
	MovingImageConstPointerType m_imMovingA;
	MovingImageConstPointerType m_imMovingB;
    
	InterpolatorPointerType m_interpolatorA;
	InterpolatorPointerType m_interpolatorB;
    
	TransformPointerType m_transformA;
	TransformPointerType m_transformB;
	
};

#include "ksrtJointCost.cxx"

#endif //_ksrtJointCost_h