#ifndef _ksrtBSplineRegistration_cxx
#define _ksrtBSplineRegistration_cxx

#include "ksrtBSplineRegistration.h"

BSplineRegistration::BSplineRegistration()
{
    m_imMoving = NULL;
    m_imFixed = NULL;
    m_imMoved = NULL;
    m_transform = TTransform::New();
	m_transform->SetIdentity();
    
	m_gridSize = 5;
	m_iterations = 20;
	m_histogramBins = 100;
	m_spatialSamples = 50000;
    m_optimalValue = 1e+8;
}

void BSplineRegistration::SetMovingImage( TImagePointer imMoving )
{
    m_imMoving = imMoving;
}
void BSplineRegistration::SetMovingImage2( TImagePointer imMoving2 )
{
    m_imMoving2 = imMoving2;
}
void BSplineRegistration::SetFixedImage( TImagePointer imFixed )
{
    m_imFixed = imFixed;
}

void BSplineRegistration::SetGridSize( unsigned int gridSize )
{
    m_gridSize = gridSize;
}

void BSplineRegistration::SetNumberOfIterations( unsigned int iterations )
{
    m_iterations = iterations;
}

void BSplineRegistration::SetNumberOfHistogramBins( unsigned int histogramBins )
{
    m_histogramBins = histogramBins;
}

void BSplineRegistration::SetNumberOfSpatialSamples( unsigned int spatialSamples )
{
    m_spatialSamples = spatialSamples;
}

BSplineRegistration::TImagePointer BSplineRegistration::GetMovedImage()
{
    return m_imMoved;
}

BSplineRegistration::TTransformPointer BSplineRegistration::GetTransform()
{
	return m_transform;
}

double BSplineRegistration::GetOptimalValue()
{
    return m_optimalValue;
}


void BSplineRegistration::DoIt()
{
    TMetric::Pointer       metric       = TMetric::New();
    TOptimizer::Pointer    optimizer    = TOptimizer::New();
    TInterpolator::Pointer interpolator = TInterpolator::New();
    TRegistration::Pointer registration = TRegistration::New();
    
    TRegion bsplineRegion;
    TSize gridSizeOnImage;
    TSize gridBorderSize;
    TSize totalGridSize;
    
    gridSizeOnImage.Fill( m_gridSize );
    gridBorderSize.Fill( 3 );
    totalGridSize = gridSizeOnImage + gridBorderSize;
    
    bsplineRegion.SetSize( totalGridSize );

    TSpacing spacing = m_imFixed->GetSpacing();
    TOrigin origin = m_imFixed->GetOrigin();
    TDirection direction = m_imFixed->GetDirection();
    
    TRegion fixedRegion = m_imFixed->GetLargestPossibleRegion();
    TSize fixedImageSize = fixedRegion.GetSize();
    
    for (unsigned int r = 0; r < 3; r++)
    {
        double spacingMultiplier = floor( (double)(fixedImageSize[r]-1)/(double)(gridSizeOnImage[r]-1) );
        if (spacingMultiplier < 1)
        {
            std::cout << "Image size along dimension " << r << " is smaller than requested grid size " << m_gridSize << std::endl;
            spacingMultiplier = 1;
        }
        spacing[r] *= spacingMultiplier;
    }
    
    origin -= direction * spacing;
    
    m_transform->SetGridSpacing( spacing );
    m_transform->SetGridOrigin( origin );
    m_transform->SetGridRegion( bsplineRegion );
    m_transform->SetGridDirection( direction );
    
    const unsigned int numberOfParameters = m_transform->GetNumberOfParameters();
    
    TParameters parameters( numberOfParameters );
    
    TTransform::InputPointType centerFixed;
    TSize sizeFixed = m_imFixed->GetLargestPossibleRegion().GetSize();
    TContinuousIndex indexFixed;
    
    for (unsigned int j = 0; j < 3; j++)
    {
        indexFixed[j] = (sizeFixed[j]-1) / 2.0;
    }
    
    m_imFixed->TransformContinuousIndexToPhysicalPoint( indexFixed, centerFixed );
    TTransform::InputPointType centerMoving;
    TSize sizeMoving = m_imMoving->GetLargestPossibleRegion().GetSize();
    TContinuousIndex indexMoving;
    for (unsigned int j = 0; j < 3; j++)
    {
        indexMoving[j] = (sizeMoving[j]-1)/2.0;
    }
    m_imMoving->TransformContinuousIndexToPhysicalPoint( indexMoving, centerMoving );
    
    TAffineTransform::Pointer centeringTransform;
    centeringTransform = TAffineTransform::New();
    centeringTransform->SetIdentity();
    centeringTransform->SetCenter( centerFixed );
    centeringTransform->Translate( centerMoving-centerFixed );
    std::cout << "Centering transform: ";
    centeringTransform->Print( std::cout );
    
    m_transform->SetBulkTransform( centeringTransform );
    
    TOptimizer::BoundSelectionType boundSelect( m_transform->GetNumberOfParameters() );
    TOptimizer::BoundValueType upperBound( m_transform->GetNumberOfParameters() );
    TOptimizer::BoundValueType lowerBound( m_transform->GetNumberOfParameters() );
    
    boundSelect.Fill( 0 );
    upperBound.Fill( 0.0 );
    lowerBound.Fill( 0.0 );
    
    optimizer->SetBoundSelection( boundSelect );
    optimizer->SetUpperBound( upperBound );
    optimizer->SetLowerBound( lowerBound );
    
    optimizer->SetCostFunctionConvergenceFactor( 1e+1 );
    optimizer->SetProjectedGradientTolerance   ( 1e-7 );
    optimizer->SetMaximumNumberOfIterations    ( m_iterations );
    optimizer->SetMaximumNumberOfEvaluations   ( 500 );
    optimizer->SetMaximumNumberOfCorrections   ( 12 );
    
//    metric->ReinitializeSeed( 76926294 );
//    metric->SetNumberOfHistogramBins( m_histogramBins );
    metric->SetNumberOfSpatialSamples( m_spatialSamples );
    
    std::cout << std::endl << "Starting Registration" << std::endl;
    
    registration->SetFixedImage  ( m_imFixed );
    registration->SetMovingImage ( m_imMoving );
    registration->SetMetric      ( metric );
    registration->SetOptimizer   ( optimizer );
    registration->SetInterpolator( interpolator );
    registration->SetTransform   ( m_transform );
    registration->SetInitialTransformParameters( m_transform->GetParameters() );
    
    try{
        registration->Update();
    }catch( itk::ExceptionObject & err )
    {
        std::cerr << "ExceptionObject caught!" << std::endl;
        std::cerr << err << std::endl;
        exit( 1 );
    }
    
    m_optimalValue = optimizer->GetValue();
    
    TOptimizer::ParametersType finalParameters = registration->GetLastTransformParameters();
	m_transform->SetParameters( finalParameters );	
	m_transform->SetParametersByValue( finalParameters );
	m_transform->Print( std::cout );

	TResampleFilter::Pointer resample = TResampleFilter::New();
    resample->SetTransform( m_transform );
    resample->SetInput( m_imMoving );
    resample->SetDefaultPixelValue( 0 );
    resample->SetOutputParametersFromImage( m_imFixed );
    
    resample->Update();
    
    m_imMoved = resample->GetOutput();

}











#endif //_ksrtBSplineRegistration_cxx