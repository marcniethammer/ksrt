#ifndef _ksrtBSplineRegistration_h
#define _ksrtBSplineRegistration_h

#include <itkImage.h>
#include <itkResampleImageFilter.h>

#include <itkAffineTransform.h>
#include <itkBSplineDeformableTransform.h>
#include <itkImageRegistrationMethod.h>
#include <itkMattesMutualInformationImageToImageMetric.h>
#include <itkMeanSquaresImageToImageMetric.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkLBFGSBOptimizer.h>

class BSplineRegistration
{
public:
    
    typedef float                                                   TPixel;
    typedef itk::Image< TPixel, 3 >									TImage;
    typedef TImage::Pointer                                         TImagePointer;

    typedef itk::ResampleImageFilter< TImage, TImage >              TResampleFilter;
    
    typedef double                                                  TCoordinateRep;
    typedef itk::ContinuousIndex< TCoordinateRep, 3 >               TContinuousIndex;
    typedef itk::BSplineDeformableTransform< TCoordinateRep, 3, 3 > TTransform;
    typedef itk::AffineTransform< TCoordinateRep >                  TAffineTransform;
    
    typedef itk::LBFGSBOptimizer                                    TOptimizer;
//    typedef itk::MattesMutualInformationImageToImageMetric< 
//                                                    TImage, 
//                                                    TImage >        TMetric;
    typedef itk::MeanSquaresImageToImageMetric< TImage, TImage >    TMetric;
    typedef itk::LinearInterpolateImageFunction< TImage >           TInterpolator;
    typedef itk::ImageRegistrationMethod< TImage, TImage >          TRegistration;
    typedef TTransform::Pointer                                     TTransformPointer;
    typedef TTransform::RegionType                                  TRegion;
    typedef TRegion::SizeType                                       TSize;
    typedef TTransform::SpacingType                                 TSpacing;
    typedef TTransform::OriginType                                  TOrigin;
    typedef TTransform::DirectionType                               TDirection;
    typedef TTransform::ParametersType                              TParameters;
   
    BSplineRegistration();
    void SetMovingImage( TImagePointer imMoving );
	void SetMovingImage2( TImagePointer imMoving2 );
    void SetFixedImage(TImagePointer imFixed );
    void SetGridSize( unsigned int gridSize );
    void SetNumberOfIterations( unsigned int N );
    void SetNumberOfHistogramBins( unsigned int histogramBins );
    void SetNumberOfSpatialSamples( unsigned int spatialSamples );
    void DoIt();
	void ApplyTransform();
    
    TImagePointer GetMovedImage();
	TImagePointer ApplyTransform( TImagePointer imMoving );
	TTransformPointer GetTransform();
    double GetOptimalValue();
    
private:
    
    TImagePointer            m_imFixed;
    TImagePointer            m_imMoving;
	TImagePointer			 m_imMoving2;
    TImagePointer            m_imMoved;
    
    TTransformPointer        m_transform;
    
    unsigned int             m_gridSize;
    unsigned int             m_iterations;
    unsigned int             m_histogramBins;
    unsigned int             m_spatialSamples;
   
    double                   m_optimalValue;
};

#include "ksrtBSplineRegistration.cxx"

#endif //_ksrtBSplineRegistration_h
