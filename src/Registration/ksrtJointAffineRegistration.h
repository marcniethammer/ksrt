#ifndef _ksrtJointAffineRegistration_h
#define _ksrtJointAffineRegistration_h

#include <itkImage.h>
#include <itkResampleImageFilter.h>
#include <itkAffineTransform.h>

#include <itkImageRegistrationMethod.h>
#include <itkCenteredTransformInitializer.h>
#include <itkMeanSquaresImageToImageMetric.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkRegularStepGradientDescentOptimizer.h>

#include <Registration/ksrtJointCost.h>
//#include "C:\Users\shan\Documents\source\SKI10\JointCost.h"

class JointAffineRegistration
{
public:
    typedef float													TPixel;
	typedef itk::Image< TPixel, 3 >									TImage;
    typedef TImage::Pointer											TImagePointer;

    typedef TImage::SizeType										TSize;
    typedef TImage::IndexType										TIndex;
    typedef TImage::SpacingType										TSpacing;
    
	typedef itk::ImageRegistrationMethod< TImage, TImage >			TRegistration;
	typedef TRegistration::Pointer									TRegistrationPointer;
    
	typedef itk::RegularStepGradientDescentOptimizer				TOptimizer;
	typedef itk::Array< double >									TParameters;
	typedef itk::MeanSquaresImageToImageMetric< TImage, TImage >	TMetric;
	typedef itk::LinearInterpolateImageFunction< TImage, double >	TInterpolator;
	typedef TInterpolator::Pointer									TInterpolatorPointer;
	typedef itk::AffineTransform< double, 3 >						TTransform;
	typedef TTransform::Pointer										TTransformPointer;
    
	typedef itk::ResampleImageFilter< TImage, TImage, double >      TResampleFilter;
	typedef TResampleFilter::Pointer								TResampleFilterPointer;
    
    JointAffineRegistration();
	void SetInitialTransformA( TTransformPointer initialTransformA );
	void SetInitialTransformB( TTransformPointer initialTransformB );
    void SetMovingImageA( TImagePointer imMovingA );
    void SetMovingImageB( TImagePointer imMovingB );
    void SetFixedImage( TImagePointer imFixed );
    void SetTranslationScale( float translationScale );
    void SetRotationScale( float rotationScale );
    void SetNumberOfIterations( unsigned int numOfIterations );
    void SetMaxStepLength( float maxStepLength );
    void SetMinStepLength( float minStepLength );
    void SetRelaxationFactor( float relaxationFactor );
    
    void DoIt();
    
    TTransformPointer GetFinalTransformA();
    TTransformPointer GetFinalTransformB();
    
    TImagePointer GetMovedImageA();
    TImagePointer GetMovedImageB();
    
private:
    
    TImagePointer m_imMovingA;
    TImagePointer m_imMovingB;
    
    TImagePointer m_imFixed;
    
    TImagePointer m_imMovedA;
    TImagePointer m_imMovedB;
    
    TTransformPointer m_transformA;
    TTransformPointer m_transformB;
    
    double m_translationScale;
    double m_rotationScale;
    
    unsigned int m_numOfIterations;
    
    float m_maxStepLength;
    float m_minStepLength;
    float m_gradientMagnitudeTolerance;
    float m_relaxationFactor;

    TImagePointer DoResampling( TImagePointer imMoving, TImagePointer imFixed, TTransformPointer transform );
    
};

#include "ksrtJointAffineRegistration.cxx"

#endif //_ksrtJointAffineRegistration_h
