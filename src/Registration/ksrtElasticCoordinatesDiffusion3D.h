#ifndef _ksrtElasticCoordinatesDiffusion3D_h
#define _ksrtElasticCoordinatesDiffusion3D_h

#include <iostream>

#include <Common/ksrtImageData.h>

class ElasticCoordinatesDiffusion3D
{
public:
    /**
     * Set x-coordinate.
     */
    void SetU( float * u );
    void SetV( float * v );
    void SetW( float * w );
    void SetMask( short * mask );
    
    void SetSpacing( float sp0, float sp1, float sp2 );
    void SetSize( unsigned int sz0, unsigned int sz1, unsigned int sz2 );
    
    void SetMu( float mu );
    void SetLambda( float lambda );
    void SetTimeStep( float tau );
    void SetNumberOfIterations( unsigned int N );
	void SetDisplayFrequency( unsigned int dispFreq );
    void DoIt();
    
private:
    ImageData< float >          m_u;
    ImageData< float >          m_v;
    ImageData< float >          m_w;
    ImageData< short >          m_mask;
    
    /**
     * Number of iterations.
     */
    unsigned int m_N;
    
	/**
	 * Display frequency.
	 */
	unsigned int m_dispFreq;

    /**
     * Time step.
     */
    float m_tau;
    
    /**
     * Lame's first parameter.
     */
    float m_mu;
    
    /**
     * Lame's second parameter.
     */
    float m_lambda;
    
    /**
     * Image sie.
     */
    unsigned int m_size[3];
    
    /**
     * Image spacing.
     */
    float m_spacing[3];    
};

#include "ksrtElasticCoordinatesDiffusion3D.cxx"

#endif //_ksrtElasticCoordinatesDiffusion3D_h
