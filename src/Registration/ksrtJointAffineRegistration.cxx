#ifndef _ksrtJointAffineRegistration_cxx
#define _ksrtJointAffineRegistration_cxx

#include "ksrtJointAffineRegistration.h"

JointAffineRegistration::JointAffineRegistration()
{
    m_imFixed = NULL;
    m_imMovedA = NULL;
    m_imMovedB = NULL;
    m_imMovingA = NULL;
    m_imMovingB = NULL;
    
    m_transformA = TTransform::New();
    m_transformB = TTransform::New();

	m_transformA->SetIdentity();
	m_transformB->SetIdentity();
    
    m_rotationScale = 1.0;
    m_translationScale = 1.0/500;
    
    m_numOfIterations = 200;
    m_minStepLength = 0.0001;
    m_maxStepLength = 1.0;
    m_gradientMagnitudeTolerance = 0.00001;    
    m_relaxationFactor = 0;
}

void JointAffineRegistration::SetFixedImage( TImagePointer imFixed )
{
    m_imFixed = imFixed;
}

void JointAffineRegistration::SetMovingImageA( TImagePointer imMovingA )
{
    m_imMovingA = imMovingA;
}

void JointAffineRegistration::SetMovingImageB( TImagePointer imMovingB )
{
    m_imMovingB = imMovingB;
}

void JointAffineRegistration::SetInitialTransformA( TTransformPointer transformA )
{
    m_transformA->SetParameters( transformA->GetParameters() );
    m_transformA->SetCenter( transformA->GetCenter() );
}

void JointAffineRegistration::SetInitialTransformB( TTransformPointer transformB )
{
    m_transformB->SetParameters(transformB->GetParameters());
    m_transformB->SetCenter( transformB->GetCenter() );
}

void JointAffineRegistration::SetTranslationScale( float translationScale )
{
    m_translationScale = translationScale;
}

void JointAffineRegistration::SetRotationScale( float rotationScale )
{
    m_rotationScale = rotationScale;
}

void JointAffineRegistration::SetNumberOfIterations( unsigned int numOfIterations )
{
    m_numOfIterations = numOfIterations;
}

void JointAffineRegistration::SetMaxStepLength( float maxStepLength )
{
    m_maxStepLength = maxStepLength;
}

void JointAffineRegistration::SetMinStepLength( float minStepLength )
{
    m_minStepLength = minStepLength;
}

void JointAffineRegistration::SetRelaxationFactor( float relaxationFactor )
{
    m_relaxationFactor = relaxationFactor;
}

JointAffineRegistration::TImagePointer JointAffineRegistration::GetMovedImageA()
{
    return m_imMovedA;
}

JointAffineRegistration::TImagePointer JointAffineRegistration::GetMovedImageB()
{
    return m_imMovedB;
}

JointAffineRegistration::TTransformPointer JointAffineRegistration::GetFinalTransformA() 
{
    return m_transformA;
}

JointAffineRegistration::TTransformPointer JointAffineRegistration::GetFinalTransformB() 
{
    return m_transformB;
}

void JointAffineRegistration::DoIt()
{
    TInterpolatorPointer interpolator = TInterpolator::New();
	TRegistrationPointer regA = TRegistration::New();
	TRegistrationPointer regB = TRegistration::New();
    
	typedef TOptimizer::ParametersType		TOptimizerParameters;
	typedef TOptimizer::ScalesType			TOptimizerScales;
	typedef TOptimizer::MeasureType			TOptimizerMeasure;
	typedef TOptimizer::CostFunctionType	TOptimizerCostFucntion;
    
	TOptimizer::Pointer optimizer = TOptimizer::New();
	
	TOptimizerParameters x ( 2 * m_transformA->GetNumberOfParameters() );
	TOptimizerParameters xA( m_transformA->GetNumberOfParameters() );
	TOptimizerParameters xB( m_transformB->GetNumberOfParameters() );
    
	xA = m_transformA->GetParameters();
	xB = m_transformB->GetParameters();
    
	for (unsigned int i = 0; i < xA.GetSize(); i++)
		x.SetElement( i, xA.GetElement(i) );
	for (unsigned int i = 0; i < xB.GetSize(); i++)
		x.SetElement( i+xA.GetSize(), xB.GetElement(i) );
    
	optimizer->SetInitialPosition( x );
    

	TOptimizerScales optimizerScales( xA.GetSize() + xB.GetSize() );
    
	optimizerScales[ 0] = 1.0;
	optimizerScales[ 1] = m_rotationScale;
	optimizerScales[ 2] = m_rotationScale;
	optimizerScales[ 3] = m_rotationScale;
	optimizerScales[ 4] = 1.0;
	optimizerScales[ 5] = m_rotationScale;
	optimizerScales[ 6] = m_rotationScale;
	optimizerScales[ 7] = m_rotationScale;
	optimizerScales[ 8] = 1.0;
	optimizerScales[ 9] = m_translationScale;
	optimizerScales[10] = m_translationScale;
	optimizerScales[11] = m_translationScale;
    
    
	optimizerScales[ 0+xA.GetSize()] = 1.0;
	optimizerScales[ 1+xA.GetSize()] = m_rotationScale;
	optimizerScales[ 2+xA.GetSize()] = m_rotationScale;
	optimizerScales[ 3+xA.GetSize()] = m_rotationScale;
	optimizerScales[ 4+xA.GetSize()] = 1.0;
	optimizerScales[ 5+xA.GetSize()] = m_rotationScale;
	optimizerScales[ 6+xA.GetSize()] = m_rotationScale;
	optimizerScales[ 7+xA.GetSize()] = m_rotationScale;
	optimizerScales[ 8+xA.GetSize()] = 1.0;
	optimizerScales[ 9+xA.GetSize()] = m_translationScale;
	optimizerScales[10+xA.GetSize()] = m_translationScale;
	optimizerScales[11+xA.GetSize()] = m_translationScale;
	
	JointCost< TImage, TImage > * cost = new JointCost< TImage, TImage >;
	cost->SetMovingImageA( m_imMovingA );
	cost->SetMovingImageB( m_imMovingB );
	cost->SetFixedImage( m_imFixed );
	cost->SetInitialTransformA( m_transformA );
	cost->SetInitialTransformB( m_transformB );

	optimizer->SetScales( optimizerScales );
	optimizer->SetCostFunction( cost );
	optimizer->SetNumberOfIterations( m_numOfIterations );
	optimizer->SetGradientMagnitudeTolerance( m_gradientMagnitudeTolerance );
	optimizer->SetMaximumStepLength( m_maxStepLength );
	optimizer->SetMinimumStepLength( m_minStepLength );
	optimizer->SetRelaxationFactor( m_relaxationFactor );
	optimizer->MinimizeOn();
    
	optimizer->StartOptimization();
    
	x = optimizer->GetCurrentPosition();
    
	for (unsigned int i = 0; i < xA.GetSize(); i++)
		xA.SetElement( i, x.GetElement(i) );
	for (unsigned int i = 0; i < xB.GetSize(); i++)
		xB.SetElement( i, x.GetElement(i+xA.GetSize()) );
    
    m_transformA->SetParameters( xA );
	m_transformB->SetParameters( xB );
	
	m_imMovedA = DoResampling( m_imMovingA, m_imFixed, m_transformA );
	m_imMovedB = DoResampling( m_imMovingB, m_imFixed, m_transformB );
    
    
	TOptimizerParameters finalParameters = optimizer->GetCurrentPosition();
	const float finalRotationCenterXA = m_transformA->GetCenter()[0];
	const float finalRotationCenterYA = m_transformA->GetCenter()[1];
	const float finalRotationCenterZA = m_transformA->GetCenter()[2];
	const float finalTranslationXA = finalParameters[9];
	const float finalTranslationYA = finalParameters[10];
	const float finalTranslationZA = finalParameters[11];
    
	const float finalRotationCenterXB = m_transformB->GetCenter()[0];
	const float finalRotationCenterYB = m_transformB->GetCenter()[1];
	const float finalRotationCenterZB = m_transformB->GetCenter()[2];
	const float finalTranslationXB = finalParameters[21];
	const float finalTranslationYB = finalParameters[22];
	const float finalTranslationZB = finalParameters[23];
    
	const unsigned int numOfIterations = optimizer->GetCurrentIteration();
	const float bestValue = optimizer->GetValue();
    
	std::cout << " Center XA      = " << finalRotationCenterXA  << std::endl;
	std::cout << " Center YA      = " << finalRotationCenterYA  << std::endl;
	std::cout << " Center ZA      = " << finalRotationCenterZA  << std::endl;
	std::cout << " Translation XA = " << finalTranslationXA		<< std::endl;
	std::cout << " Translation YA = " << finalTranslationYA		<< std::endl;
	std::cout << " Translation ZA = " << finalTranslationZA		<< std::endl;
    
	std::cout << " Center XB      = " << finalRotationCenterXB  << std::endl;
	std::cout << " Center YB      = " << finalRotationCenterYB  << std::endl;
	std::cout << " Center ZB      = " << finalRotationCenterZB  << std::endl;
	std::cout << " Translation XB = " << finalTranslationXB		<< std::endl;
	std::cout << " Translation YB = " << finalTranslationYB		<< std::endl;
	std::cout << " Translation ZB = " << finalTranslationZB		<< std::endl;
    
	std::cout << " Iterations    = " << numOfIterations			<< std::endl;
	std::cout << " Metric value  = " << bestValue				<< std::endl;

}

JointAffineRegistration::TImagePointer JointAffineRegistration::DoResampling( TImagePointer imMoving, TImagePointer imFixed, TTransformPointer transform )
{
    TResampleFilterPointer resampler = TResampleFilter::New();
    resampler->SetTransform( transform );
    resampler->SetInput( imMoving );
    
    resampler->SetSize( imFixed->GetLargestPossibleRegion().GetSize() );
    resampler->SetOutputOrigin( imFixed->GetOrigin() );
    resampler->SetOutputSpacing( imFixed->GetSpacing() );
    resampler->SetOutputDirection( imFixed->GetDirection() );
    resampler->SetDefaultPixelValue( 0 );
    resampler->Update();
    return resampler->GetOutput();
}
#endif//_ksrtJointAffineRegistration_cxx
