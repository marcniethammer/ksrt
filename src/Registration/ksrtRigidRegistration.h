#ifndef _ksrtRigidRegistration_h
#define _ksrtRigidRegistration_h

#include <itkImage.h>

#include <itkImageRegistrationMethod.h>
#include <itkMeanSquaresImageToImageMetric.h>
#include <itkMattesMutualInformationImageToImageMetric.h>
#include <itkLinearInterpolateImageFunction.h>

#include <itkVersorRigid3DTransform.h>
#include <itkCenteredTransformInitializer.h>

#include <itkVersorRigid3DTransformOptimizer.h>

#include <itkResampleImageFilter.h>
#include <itkMultiResolutionPyramidImageFilter.h>
#include <itkMultiResolutionImageRegistrationMethod.h>
#include <itkRecursiveMultiResolutionPyramidImageFilter.h>

class RigidRegistration
{
public:
    
    typedef float                                                           TPixel;
	typedef itk::Image< TPixel, 3 >											TImage;
    typedef TImage::Pointer                                                 TImagePointer;
    typedef TImage::SizeType                                                TSize;
    typedef TImage::SpacingType                                             TSpacing;
    typedef TImage::IndexType                                               TIndex;

    typedef itk::MultiResolutionImageRegistrationMethod< TImage, TImage >   TRegistration;
    typedef TRegistration::Pointer                                          TRegistrationPointer;
	typedef itk::VersorRigid3DTransformOptimizer                            TOptimizer;
    typedef TOptimizer::Pointer                                             TOptimizerPointer;
    typedef TOptimizer::ScalesType                                          TOptimizerScales;
    
	typedef itk::Array< double >                                            TParameters;
	typedef itk::MeanSquaresImageToImageMetric< TImage, TImage >            TSSDMetric;
	typedef itk::MattesMutualInformationImageToImageMetric<TImage, TImage>	TMutualInfoMetric; 
    typedef TSSDMetric::Pointer                                             TSSDMetricPointer;
    typedef TMutualInfoMetric::Pointer                                      TMutualInfoMetricPointer;
    
	typedef itk::LinearInterpolateImageFunction< TImage, double >           TInterpolator;
    typedef TInterpolator::Pointer                                          TInterpolatorPointer;
    
	typedef itk::VersorRigid3DTransform< double >                           TTransform;
    typedef TTransform::Pointer                                             TTransformPointer;
    
	typedef itk::ResampleImageFilter< TImage, TImage, double >              TResampleFilter;
	typedef TResampleFilter::Pointer                                        TResampleFilterPointer;
	typedef itk::CenteredTransformInitializer< TTransform, TImage, TImage > TTransformInitializer;
	typedef TTransformInitializer::Pointer                                  TTransformInitializerPointer;
    
	typedef itk::MultiResolutionPyramidImageFilter<TImage, TImage>          TFixedImagePyramid;
	typedef TFixedImagePyramid::Pointer                                     TFixedImagePyramidPointer;
	typedef itk::MultiResolutionPyramidImageFilter<TImage, TImage>          TMovingImagePyramid;
	typedef TMovingImagePyramid::Pointer                                    TMovingImagePyramidPointer;
    
    
public:

    RigidRegistration();

    void SetMovingImage( TImagePointer movingImg );
    
    void SetFixedImage( TImagePointer fixedImg );

    void SetTranslationScale( float translationScale );
    
    void SetNumberOfLevels( unsigned int numOfLevels );
    
    void SetMaxStepLength( float maxStepLength );
    
    void SetMinStepLength( float minStepLength );
    
    void SetGradientMagnitudeTolerance( float gradientMagnitudeTolerance );
    
    void SetRelaxationFactor( float relaxationFactor );
    
    void SetNumberOfIterations( unsigned int numOfIterations );
    
    void SetNumberOfHistogramBins( unsigned int numOfBins );
    
    void UseMutualInformation();
    
	void UseSSD();
    
    void DoIt();
    
    TImagePointer GetMovedImage();
    
    TTransformPointer GetFinalTransform();
    
    double GetOptimalValue();

private:
    
    TImagePointer m_movingImg;
	TImagePointer m_fixedImg;    
	TImagePointer m_movedImg;
	TTransformPointer m_transform;

	float m_translationScale;
    double m_optimalValue;
    unsigned int m_numOfLevels;
    
    float m_relaxationFactor;
    float m_minStepLength;
    float m_maxStepLength;
    float m_gradientMagnitudeTolerance;
    
    unsigned int m_numOfIterations;
    unsigned int m_numOfBins;
    
    bool m_useMutualInfo;
	bool m_useSSD;

    TImagePointer DoResampling();
    
};

#include "ksrtRigidRegistration.cxx"

#endif //_ksrtRigidRegistration_h
