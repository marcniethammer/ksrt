#ifndef _ksrtSimilarityRegistration_h
#define _ksrtSimilarityRegistration_h

#include <itkImage.h>
#include <itkSimilarity3DTransform.h>
#include <itkResampleImageFilter.h>
#include <itkImageRegistrationMethod.h>

#include <itkCenteredTransformInitializer.h>
#include <itkMeanSquaresImageToImageMetric.h>
#include <itkMattesMutualInformationImageToImageMetric.h>
#include <itkLinearInterpolateImageFunction.h>
#include <itkRegularStepGradientDescentOptimizer.h>

#include <itkMultiResolutionPyramidImageFilter.h>
#include <itkMultiResolutionImageRegistrationMethod.h>
#include <itkRecursiveMultiResolutionPyramidImageFilter.h>

class SimilarityRegistration
{
public:
    typedef float                                                           TPixel;
	typedef itk::Image< TPixel, 3 >                                         TImage;
    typedef TImage::Pointer                                                 TImagePointer;
    typedef TImage::SizeType                                                TSize;
    typedef TImage::SpacingType                                             TSpacing;
    typedef TImage::IndexType                                               TIndex;
    
	typedef itk::MultiResolutionImageRegistrationMethod< TImage, TImage >   TRegistration;
    typedef TRegistration::Pointer                                          TRegistrationPointer;
	typedef itk::RegularStepGradientDescentOptimizer                        TOptimizer;
    typedef TOptimizer::Pointer                                             TOptimizerPointer;
    typedef TOptimizer::ScalesType                                          TOptimizerScales;
    
	typedef itk::Array< double >                                            TParameters;
	typedef itk::MeanSquaresImageToImageMetric< TImage, TImage >            TSSDMetric;
	typedef itk::MattesMutualInformationImageToImageMetric<TImage, TImage>	TMutualInfoMetric; 
    typedef TSSDMetric::Pointer                                             TSSDMetricPointer;
    typedef TMutualInfoMetric::Pointer                                      TMutualInfoMetricPointer;
	
    typedef itk::LinearInterpolateImageFunction< TImage, double >           TInterpolator;
    typedef TInterpolator::Pointer                                          TInterpolatorPointer;
    
	typedef itk::Similarity3DTransform< double >                            TTransform;
    typedef TTransform::Pointer                                             TTransformPointer;
    
	typedef itk::ResampleImageFilter< TImage, TImage, double >              TResampleFilter;
	typedef TResampleFilter::Pointer                                        TResampleFilterPointer;
	typedef itk::CenteredTransformInitializer< TTransform, TImage, TImage > TTransformInitializer;
	typedef TTransformInitializer::Pointer                                  TTransformInitializerPointer;
    
	typedef itk::MultiResolutionPyramidImageFilter<TImage, TImage>          TFixedImagePyramid;
	typedef TFixedImagePyramid::Pointer                                     TFixedImagePyramidPointer;
	typedef itk::MultiResolutionPyramidImageFilter<TImage, TImage>          TMovingImagePyramid;
	typedef TMovingImagePyramid::Pointer                                    TMovingImagePyramidPointer;
    
public:
    
    /**
     * Constructor. Set default parameters.
     */
    SimilarityRegistration();
    
    /**
     * Set moving image.
     * @param movingImg the moving image.
     */
    void SetMovingImage( TImagePointer movingImg );
    
    /**
     * Set fixed image.
     * @param fixedImg the fixed image.
     */
    void SetFixedImage( TImagePointer fixedImg );
    
    /**
     * Set rotation scale. 
     * @param rotationScale the rotation scale. Default value = 1.
     */
    void SetRotationScale( float rotationScale );
    
    /**
     * Set translation scale.
     * @param translationScale the translation scale. Default value = 1.0/100.
     */
    void SetTranslationScale( float translationScale );
    
    /**
     * Set relaxation factor.
     * @param relaxationFactor the relaxation factor. Default value = 0.2.
     */
    void SetRelaxationFactor( float relaxationFactor );
    
    /**
     * Set number of image pyramid levels.
     * @param numberOfLevels the number of image pyramid levels. Default value = 2.
     */
    void SetNumberOfLevels( unsigned int numOfLevels );
    
    /**
     * Set the maximum step length for gradient descent.
     * @param maxStepLength the maximum step length. Default value = 1.0.
     */
    void SetMaxStepLength( float maxStepLength );
    
    /**
     * Set the minimum step length for gradient descent.
     * @param minStepLength the minimum step length. Default value = 0.0001.
     */
    void SetMinStepLength( float minStepLength );
    
    /**
     * Set the gradient magnitude tolerance.
     * @param gradientMagnitudeTolerance the gradient magnitude tolerance to terminate gradient descent. Default value = 0.00001
     */
    void SetGradientMagnitudeTolerance( float gradientMagnitudeTolerance );
    
    /**
     * Set the maximum number of iterations for gradient descent.
     * @param numOfIterations the maximum number of gradient descent iterations. Default value = 500.
     */
    void SetNumberOfIterations( unsigned int numOfIterations );
    
    /**
     * Set the number of samples used to compute the image matching metric. 
     * @param numOfSamples the number of samples. Default value = 50000.
     */
    void SetNumberOfSamples( unsigned int numOfSamples );
    
    /**
     * Set the number of samples used to compute the image matching metric. 
     * @param numOfSamples the number of samples. Default value = 30.
     */
    void SetNumberOfHistogramBins( unsigned int numOfBins );
    
	void UseMutualInformation();
    
	void UseSSD();
    
    /**
     * Do the affine registration.
     */
    void DoIt();
    
    double GetOptimalValue();

  /**
   * return the name of the class as a const char*
   */
  const char* GetNameOfClass() const { return "ksrtSimilarityRegistration"; };
    
    /**
     * Get the moved image.
     */
    TImagePointer GetMovedImage();
    
    /**
     * Get the final transform.
     */
    TTransformPointer GetFinalTransform();
    
    
    
private:
    
	TImagePointer m_movingImg;
	TImagePointer m_fixedImg;    
	TImagePointer m_movedImg;
	TTransformPointer m_transform;
    
    float m_rotationScale;
	float m_translationScale;
	float m_relaxationFactor;
	unsigned int m_numOfLevels;
    
    float m_minStepLength;
    float m_maxStepLength;
    float m_gradientMagnitudeTolerance;
    unsigned int m_numOfIterations;
    
    unsigned int m_numOfSamples;
    unsigned int m_numOfBins;
    
	bool m_useMutualInfo;
	bool m_useSSD;
    
    double m_optimalValue;
    TImagePointer DoResampling();

    
};


#include "ksrtSimilarityRegistration.cxx"

#endif //_ksrtSimilarityRegistration_h
