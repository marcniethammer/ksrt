#ifndef _ksrtRigidRegistration_cxx
#define _ksrtRigidRegistration_cxx

#include "ksrtRigidRegistration.h"

RigidRegistration::RigidRegistration()
{
    m_movingImg = NULL;
    m_fixedImg = NULL;
    m_movedImg = NULL;
    
    m_transform = TTransform::New();
	m_transform->SetIdentity();
    
    m_translationScale = 1.0/500;
    m_relaxationFactor = 0.0;
    m_numOfLevels = 2;
    
    m_maxStepLength = 0.2;
    m_minStepLength = 0.0001;
    m_gradientMagnitudeTolerance = 0.00001;
    
    m_numOfIterations = 500;
    m_numOfBins = 30;
    
    m_useSSD = true;
	m_useMutualInfo = false;
    m_optimalValue = 1e+5;
}

void RigidRegistration::SetMovingImage( TImagePointer movingImg )
{
    m_movingImg = movingImg;
}

void RigidRegistration::SetFixedImage( TImagePointer fixedImg )
{
    m_fixedImg = fixedImg;
}

void RigidRegistration::SetNumberOfLevels( unsigned int numOfLevels )
{
    m_numOfLevels = numOfLevels;
}

void RigidRegistration::SetRelaxationFactor( float relaxationFactor )
{
    m_relaxationFactor = relaxationFactor;
}

void RigidRegistration::SetTranslationScale( float translationScale )
{
    m_translationScale = translationScale;
}

void RigidRegistration::SetMaxStepLength( float maxStepLength )
{
    m_maxStepLength = maxStepLength;
}

void RigidRegistration::SetMinStepLength( float minStepLength )
{
    m_minStepLength = minStepLength;
}

void RigidRegistration::SetGradientMagnitudeTolerance( float gradientMagnitudeTolerance )
{
    m_gradientMagnitudeTolerance = gradientMagnitudeTolerance;
}

void RigidRegistration::SetNumberOfIterations( unsigned int numOfIterations )
{
    m_numOfIterations = numOfIterations;
}

void RigidRegistration::SetNumberOfHistogramBins( unsigned int numOfBins )
{
    m_numOfBins = numOfBins;
}

void RigidRegistration::UseMutualInformation()
{
	m_useMutualInfo = true;
	m_useSSD = false;
}

void RigidRegistration::UseSSD()
{
	m_useMutualInfo = false;
	m_useSSD = true;
}

RigidRegistration::TImagePointer RigidRegistration::GetMovedImage()
{
    return m_movedImg;
}

RigidRegistration::TTransformPointer RigidRegistration::GetFinalTransform()
{
    return m_transform;
}

double RigidRegistration::GetOptimalValue()
{
    return m_optimalValue;
}

RigidRegistration::TImagePointer RigidRegistration::DoResampling()
{
    TResampleFilterPointer resampler = TResampleFilter::New();
    resampler->SetTransform( m_transform );
    resampler->SetInput( m_movingImg );
    
    resampler->SetSize( m_fixedImg->GetLargestPossibleRegion().GetSize() );
    resampler->SetOutputOrigin( m_fixedImg->GetOrigin() );
    resampler->SetOutputSpacing( m_fixedImg->GetSpacing() );
    resampler->SetOutputDirection( m_fixedImg->GetDirection() );
    resampler->SetDefaultPixelValue( 0 );
    resampler->Update();
    return resampler->GetOutput();
}


void RigidRegistration::DoIt()
{
	TOptimizerPointer      optimizer     = TOptimizer::New();
	TInterpolatorPointer   interpolator  = TInterpolator::New();
	TRegistrationPointer   registration  = TRegistration::New();
    
    if( m_useMutualInfo )
	{	
		TMutualInfoMetricPointer metric = TMutualInfoMetric::New();
		metric->SetNumberOfHistogramBins( m_numOfBins );
		registration->SetMetric( metric );
	}
	if( m_useSSD )
	{
		TSSDMetricPointer metric = TSSDMetric::New();
		registration->SetMetric( metric );
    }

    TFixedImagePyramidPointer fixedImagePyramid = TFixedImagePyramid::New();
	TMovingImagePyramidPointer movingImagePyramid = TMovingImagePyramid::New();
    
	
	registration->SetOptimizer( optimizer );
	registration->SetTransform( m_transform );
	registration->SetInterpolator( interpolator );
	
    
	registration->SetFixedImagePyramid( fixedImagePyramid );
	registration->SetMovingImagePyramid( movingImagePyramid );
    
	registration->SetFixedImage( m_fixedImg );
	registration->SetMovingImage( m_movingImg );
	registration->SetFixedImageRegion( m_fixedImg->GetBufferedRegion() );
    
    TTransformInitializerPointer initializer = TTransformInitializer::New();
	initializer->SetTransform( m_transform );
	initializer->SetFixedImage( m_fixedImg );
	initializer->SetMovingImage( m_movingImg );
	initializer->MomentsOn();
	initializer->InitializeTransform();
    
    typedef TTransform::VersorType  TVersor;
    typedef TVersor::VectorType     TVector;
    
    TVersor     rotation;
    TVector     axis;
    
    axis[0] = 0.0;
    axis[1] = 0.0;
    axis[2] = 1.0;
    
    const double angle = 0;
    
    rotation.Set( axis, angle );
    
    m_transform->SetRotation( rotation );
    
    registration->SetInitialTransformParameters( m_transform->GetParameters() );

    TOptimizerScales optimizerScales( m_transform->GetNumberOfParameters() );
    const double translationScale = 1.0 / 1000.0;
    
    optimizerScales[0] = 1.0;
    optimizerScales[1] = 1.0;
    optimizerScales[2] = 1.0;
    optimizerScales[3] = translationScale;
    optimizerScales[4] = translationScale;
    optimizerScales[5] = translationScale;
    
    optimizer->SetScales( optimizerScales );
    
    optimizer->SetMaximumStepLength( m_maxStepLength ); 
	optimizer->SetMinimumStepLength( m_minStepLength ); 
	optimizer->SetNumberOfIterations( m_numOfIterations );
    
    try 
    { 
		registration->StartRegistration(); 
    } 
	catch( itk::ExceptionObject & err ) 
    { 
		std::cerr << "Exception caught when doing affine registration!" << std::endl; 
		std::cerr << err << std::endl; 
    }
    m_optimalValue = optimizer->GetValue();
    
	TOptimizer::ParametersType finalParameters = optimizer->GetCurrentPosition();
    
    const double versorX              = finalParameters[0];
    const double versorY              = finalParameters[1];
    const double versorZ              = finalParameters[2];
    const double finalTranslationX    = finalParameters[3];
    const double finalTranslationY    = finalParameters[4];
    const double finalTranslationZ    = finalParameters[5];
    
    const unsigned int numberOfIterations = optimizer->GetCurrentIteration();

    std::cout << std::endl << std::endl;
    std::cout << "Result = " << std::endl;
    std::cout << " versor X      = " << versorX  << std::endl;
    std::cout << " versor Y      = " << versorY  << std::endl;
    std::cout << " versor Z      = " << versorZ  << std::endl;
    std::cout << " Translation X = " << finalTranslationX  << std::endl;
    std::cout << " Translation Y = " << finalTranslationY  << std::endl;
    std::cout << " Translation Z = " << finalTranslationZ  << std::endl;
    std::cout << " Iterations    = " << numberOfIterations << std::endl;
    std::cout << " Metric value  = " << m_optimalValue     << std::endl;
    
    m_transform->SetParameters( finalParameters );
    
    TTransform::MatrixType matrix = m_transform->GetMatrix();
    TTransform::OffsetType offset = m_transform->GetOffset();
    
    std::cout << "Matrix = " << std::endl << matrix << std::endl;
    std::cout << "Offset = " << std::endl << offset << std::endl;
    
    m_movedImg = DoResampling();
}
#endif //_ksrtRigidRegistration_cxx
