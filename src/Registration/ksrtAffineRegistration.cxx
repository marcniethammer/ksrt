#ifndef _ksrtAffineRegistration_cxx
#define _ksrtAffineRegistration_cxx

#include "ksrtAffineRegistration.h"

AffineRegistration::AffineRegistration()
{
    m_movingImg = NULL;
    m_fixedImg = NULL;
    m_movedImg = NULL;
    
    m_transform = TTransform::New();
	m_transform->SetIdentity();
    
    m_rotationScale = 1.0;
    m_translationScale = 1.0/100;
    m_relaxationFactor = 0.0;
    m_numOfLevels = 2;
    
    m_maxStepLength = 1.0;
    m_minStepLength = 0.0001;
    m_gradientMagnitudeTolerance = 0.00001;
    m_numOfIterations = 500;
    
    m_numOfSamples = 50000;
	m_numOfBins = 30;
	m_useSSD = true;
	m_useMutualInfo = false;
    m_optimalValue = 1e+5;
}


void AffineRegistration::SetMovingImage( TImagePointer movingImg )
{
    m_movingImg = movingImg;
}

void AffineRegistration::SetFixedImage( TImagePointer fixedImg )
{
    m_fixedImg = fixedImg;
}

void AffineRegistration::SetNumberOfLevels( unsigned int numOfLevels )
{
    m_numOfLevels = numOfLevels;
}

void AffineRegistration::SetRelaxationFactor( float relaxationFactor )
{
    m_relaxationFactor = relaxationFactor;
}

void AffineRegistration::SetRotationScale( float rotationScale )
{
    m_rotationScale = rotationScale;
}

void AffineRegistration::SetTranslationScale( float translationScale )
{
    m_translationScale = translationScale;
}

void AffineRegistration::SetMaxStepLength( float maxStepLength )
{
    m_maxStepLength = maxStepLength;
}

void AffineRegistration::SetMinStepLength( float minStepLength )
{
    m_minStepLength = minStepLength;
}

void AffineRegistration::SetGradientMagnitudeTolerance( float gradientMagnitudeTolerance )
{
    m_gradientMagnitudeTolerance = gradientMagnitudeTolerance;
}

void AffineRegistration::SetNumberOfIterations( unsigned int numOfIterations )
{
    m_numOfIterations = numOfIterations;
}

void AffineRegistration::SetNumberOfSamples( unsigned int numOfSamples )
{
    m_numOfSamples = numOfSamples;
}

void AffineRegistration::SetNumberOfHistogramBins( unsigned int numOfBins )
{
    m_numOfBins = numOfBins;
}

void AffineRegistration::UseMutualInformation()
{
	m_useMutualInfo = true;
	m_useSSD = false;
}

void AffineRegistration::UseSSD()
{
	m_useMutualInfo = false;
	m_useSSD = true;
}

AffineRegistration::TImagePointer AffineRegistration::GetMovedImage()
{
    return m_movedImg;
}

AffineRegistration::TTransformPointer AffineRegistration::GetFinalTransform()
{
    return m_transform;
}

double AffineRegistration::GetOptimalValue()
{
    return m_optimalValue;
}
AffineRegistration::TImagePointer AffineRegistration::DoResampling()
{
    TResampleFilterPointer resampler = TResampleFilter::New();
    resampler->SetTransform( m_transform );
    resampler->SetInput( m_movingImg );
    
    resampler->SetSize( m_fixedImg->GetLargestPossibleRegion().GetSize() );
    resampler->SetOutputOrigin( m_fixedImg->GetOrigin() );
    resampler->SetOutputSpacing( m_fixedImg->GetSpacing() );
    resampler->SetOutputDirection( m_fixedImg->GetDirection() );
    resampler->SetDefaultPixelValue( 0 );
    resampler->Update();
    return resampler->GetOutput();
}

void AffineRegistration::DoIt()
{
	TOptimizerPointer      optimizer     = TOptimizer::New();
	TInterpolatorPointer   interpolator  = TInterpolator::New();
	TRegistrationPointer   registration  = TRegistration::New();

	if( m_useMutualInfo )
	{	
		TMutualInfoMetricPointer metric = TMutualInfoMetric::New();
		metric->SetNumberOfHistogramBins( m_numOfBins );
		registration->SetMetric( metric );
		#ifdef ITK_USE_REVIEW 
			#ifdef ITK_USE_OPTIMIZED_REGISTRATION_METHODS
				metric->SetNumberOfSpatialSamples( m_numOfSamples );
			#else
				itkWarningMacro(<< "ITK not compiled with ITK_USE_OPTIMIZED_REGISTRATION_METHODS. Performance will suffer.");
			#endif
		#else
			itkWarningMacro(<< "ITK not compiled with ITK_USE_REVIEW. Performance will suffer.");
		#endif
	}
	if( m_useSSD )
	{
		TSSDMetricPointer metric = TSSDMetric::New();
		registration->SetMetric( metric );
		#ifdef ITK_USE_REVIEW 
			#ifdef ITK_USE_OPTIMIZED_REGISTRATION_METHODS
				metric->SetNumberOfSpatialSamples( m_numOfSamples );
			#else
				itkWarningMacro(<< "ITK not compiled with ITK_USE_OPTIMIZED_REGISTRATION_METHODS. Performance will suffer.");
			#endif
		#else
			itkWarningMacro(<< "ITK not compiled with ITK_USE_REVIEW. Performance will suffer.");
		#endif
	}

	TFixedImagePyramidPointer fixedImagePyramid = TFixedImagePyramid::New();
	TMovingImagePyramidPointer movingImagePyramid = TMovingImagePyramid::New();
    
	
	registration->SetOptimizer( optimizer );
	registration->SetTransform( m_transform );
	registration->SetInterpolator( interpolator );
	

	registration->SetFixedImagePyramid( fixedImagePyramid );
	registration->SetMovingImagePyramid( movingImagePyramid );
    
	registration->SetFixedImage( m_fixedImg );
	registration->SetMovingImage( m_movingImg );
	registration->SetFixedImageRegion( m_fixedImg->GetBufferedRegion() );
    
	TTransformInitializerPointer initializer = TTransformInitializer::New();
	initializer->SetTransform( m_transform );
	initializer->SetFixedImage( m_fixedImg );
	initializer->SetMovingImage( m_movingImg );
	initializer->MomentsOn();
	initializer->InitializeTransform();
    
	registration->SetInitialTransformParameters( m_transform->GetParameters() );
    
	TOptimizerScales optimizerScales( m_transform->GetNumberOfParameters() );
    
	optimizerScales[0] = 1.0;
	optimizerScales[1] = m_rotationScale;
	optimizerScales[2] = m_rotationScale;
	optimizerScales[3] = m_rotationScale;
	optimizerScales[4] = 1.0;
	optimizerScales[5] = m_rotationScale;
	optimizerScales[6] = m_rotationScale;
	optimizerScales[7] = m_rotationScale;
	optimizerScales[8] = 1.0;
	optimizerScales[9] = m_translationScale;
	optimizerScales[10] = m_translationScale;
	optimizerScales[11] = m_translationScale;
    
	optimizer->SetScales( optimizerScales );
    
	optimizer->SetMaximumStepLength( m_maxStepLength ); 
	optimizer->SetMinimumStepLength( m_minStepLength ); 
	optimizer->SetGradientMagnitudeTolerance( m_gradientMagnitudeTolerance); 
	optimizer->SetNumberOfIterations( m_numOfIterations );
	optimizer->SetRelaxationFactor( m_relaxationFactor );
	optimizer->MinimizeOn();
	registration->SetNumberOfLevels( m_numOfLevels );
	try 
    { 
		registration->StartRegistration(); 
    } 
	catch( itk::ExceptionObject & err ) 
    { 
		std::cerr << "Exception caught when doing affine registration!" << std::endl; 
		std::cerr << err << std::endl; 
    }
    m_optimalValue = optimizer->GetValue();
    
	TOptimizer::ParametersType finalParameters = optimizer->GetCurrentPosition();
	const float finalRotationCenterX = m_transform->GetCenter()[0];
	const float finalRotationCenterY = m_transform->GetCenter()[1];
	const float finalRotationCenterZ = m_transform->GetCenter()[2];
    
	const float finalTranslationX = finalParameters[9];
	const float finalTranslationY = finalParameters[10];
	const float finalTranslationZ = finalParameters[11];
    
	std::cout << " Result :  " << std::endl;
    
	std::cout << " Center X      = " << finalRotationCenterX  << std::endl;
	std::cout << " Center Y      = " << finalRotationCenterY  << std::endl;
	std::cout << " Center Z      = " << finalRotationCenterZ  << std::endl;
	std::cout << " Translation X = " << finalTranslationX     << std::endl;
	std::cout << " Translation Y = " << finalTranslationY     << std::endl;
	std::cout << " Translation Z = " << finalTranslationZ     << std::endl;
	std::cout << " Iterations    = " << optimizer->GetCurrentIteration() << std::endl;
	std::cout << " Metric value  = " << optimizer->GetValue()	        << std::endl;
    
	m_movedImg = DoResampling();
    
}

#endif //_ksrtAffineRegistration_cxx