#ifndef _ksrtJointCost_cxx
#define _ksrtJointCost_cxx

#include "ksrtJointCost.h"

template< class TFixedImage, class TMovingImage >
JointCost< TFixedImage, TMovingImage >::JointCost()
{
    m_imFixed = NULL;
    m_imMovingA= NULL;
    m_imMovingB = NULL;
    m_interpolatorA = InterpolatorType::New();
    m_interpolatorB = InterpolatorType::New();
    m_transformA = TransformType::New();
    m_transformB = TransformType::New();
    m_transformA->SetIdentity();
	m_transformB->SetIdentity();

    m_ImageDimension = 3;
    m_ParametersDimension = 24;
}

template< class TFixedImage, class TMovingImage >
unsigned int JointCost< TFixedImage, TMovingImage >::GetNumberOfParameters() const
{
    return m_ParametersDimension;
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::SetMovingImageA( MovingImagePointerType imMovingA )
{
    m_imMovingA = imMovingA;
    m_interpolatorA->SetInputImage( m_imMovingA );
    
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::SetMovingImageB( MovingImagePointerType imMovingB )
{
    m_imMovingB = imMovingB;
    m_interpolatorB->SetInputImage( m_imMovingB );
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::SetFixedImage( FixedImagePointerType imFixed )
{
    m_imFixed = imFixed;
    m_ImageDimension = m_imFixed->GetImageDimension();
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::SetInitialTransformA( TransformPointerType initialTransformA )
{
    m_transformA->SetParameters( initialTransformA->GetParameters() );
    m_transformA->SetCenter( initialTransformA->GetCenter() );
    m_ParametersDimension = 2 * m_transformA->GetNumberOfParameters();
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::SetInitialTransformB( TransformPointerType initialTransformB )
{
    m_transformB->SetParameters( initialTransformB->GetParameters() );
    m_transformB->SetCenter( initialTransformB->GetCenter() );
    m_ParametersDimension = 2 * m_transformB->GetNumberOfParameters();
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::SetTransformParameters( const ParametersType & parameters ) const
{
    ParametersType xA( GetNumberOfParameters()/2 );
    ParametersType xB( GetNumberOfParameters()/2 );
    
    for (unsigned int i = 0; i < GetNumberOfParameters()/2; i++)
        xA.SetElement( i, parameters.GetElement( i ) );
    for (unsigned int i = GetNumberOfParameters()/2; i < GetNumberOfParameters(); i++)
        xB.SetElement( i-GetNumberOfParameters()/2, parameters.GetElement(i) );
    m_transformA->SetParameters( xA );
    m_transformB->SetParameters( xB );
    
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::GetDerivative( const ParametersType & parameters, DerivativeType & derivative ) const
{
    FixedImageConstPointerType imFixed = m_imFixed;
    FixedIteratorType ti( imFixed, imFixed->GetLargestPossibleRegion() );
    FixedImageIndexType index;
    
    this->m_NumberOfPixelsCounted = 0;
    
    SetTransformParameters( parameters );
    
    derivative = DerivativeType( m_ParametersDimension );
	derivative.Fill( itk::NumericTraits< ITK_TYPENAME DerivativeType::ValueType >::Zero );
    
    
    GradientImageFilterPointerType gradientFilterA = GradientImageFilterType::New();
    gradientFilterA->SetUseImageSpacingOn();
    gradientFilterA->SetInput( m_imMovingA );
    gradientFilterA->Update();
    GradientImagePointerType imGradientMovingA = gradientFilterA->GetOutput();
    
    GradientImageFilterPointerType gradientFilterB = GradientImageFilterType::New();
    gradientFilterB->SetUseImageSpacingOn();
    gradientFilterB->SetInput( m_imMovingB );
    gradientFilterB->Update();
    GradientImagePointerType imGradientMovingB = gradientFilterB->GetOutput();
    
    ti.GoToBegin();
    
    while(!ti.IsAtEnd())
    {
        index = ti.GetIndex();
        InputPointType inputPoint;
        imFixed->TransformIndexToPhysicalPoint( index, inputPoint );
        
        OutputPointType transformedPointA = m_transformA->TransformPoint( inputPoint );
        OutputPointType transformedPointB = m_transformB->TransformPoint( inputPoint );
        
        if( m_interpolatorA->IsInsideBuffer( transformedPointA ) && m_interpolatorB->IsInsideBuffer( transformedPointB ) )
        {
            const RealType movingValueA = m_interpolatorA->Evaluate( transformedPointA );
            const RealType movingValueB = m_interpolatorB->Evaluate( transformedPointB );
            const RealType fixedValue   = ti.Get();
            
            this->m_NumberOfPixelsCounted++;
            
            const RealType diff = (movingValueA > movingValueB) ? movingValueA - fixedValue : movingValueB - fixedValue;
            
            const TransformJacobianType & jacobianA = m_transformA->GetJacobian( inputPoint ); 
            const TransformJacobianType & jacobianB = m_transformB->GetJacobian( inputPoint );
            
            
            MovingImageContinuousIndexType tempIndexA, tempIndexB;
            m_imMovingA->TransformPhysicalPointToContinuousIndex( transformedPointA, tempIndexA );
            m_imMovingB->TransformPhysicalPointToContinuousIndex( transformedPointB, tempIndexB );
            
            typename MovingImageType::IndexType mappedIndexA, mappedIndexB; 
            mappedIndexA.CopyWithRound( tempIndexA );
            mappedIndexB.CopyWithRound( tempIndexB );
            
            
            const GradientPixelType gradientA = imGradientMovingA->GetPixel( mappedIndexA );
            const GradientPixelType gradientB = imGradientMovingB->GetPixel( mappedIndexB );
            
            for(unsigned int par = 0; par < m_ParametersDimension/2; par++)
            {
				RealType sum = itk::NumericTraits< RealType >::Zero;
                for(unsigned int dim = 0; dim < m_ImageDimension; dim++)
                {
                    sum += 2.0 * diff * jacobianA( dim, par ) * gradientA[dim];
                    sum += movingValueB * jacobianA( dim, par ) * gradientA[dim];
                }
                derivative[par] += sum;
            }
            for(unsigned int par = m_ParametersDimension/2; par < m_ParametersDimension; par++)
            {
				RealType sum = itk::NumericTraits< RealType >::Zero;
                for(unsigned int dim = 0; dim < m_ImageDimension; dim++)
                {
                    sum += 2.0 * diff * jacobianB( dim, par-m_ParametersDimension/2 ) * gradientB[dim];
					sum += movingValueA * jacobianB( dim, par-m_ParametersDimension/2 ) * gradientB[dim];
                }
                derivative[par] += sum;
            }
        }
        ++ti;
    }
    
    if( !this->m_NumberOfPixelsCounted )
    {
		std::cerr << "All the points mapped to outside of the moving image" << std::endl;
    }
    else
    {
        for(unsigned int i = 0; i < m_ParametersDimension; i++)
		{
			derivative[i] /= this->m_NumberOfPixelsCounted;    
		}
    }
}


template< class TFixedImage, class TMovingImage >
typename JointCost< TFixedImage, TMovingImage >::MeasureType 
JointCost< TFixedImage, TMovingImage >::GetValue( const ParametersType & parameters ) const
{
    FixedImageConstPointerType imFixed = m_imFixed;
	typedef itk::ImageRegionConstIteratorWithIndex< FixedImageType > FixedIteratorType;
    FixedIteratorType ti( imFixed, imFixed->GetLargestPossibleRegion() );
    typename FixedImageType::IndexType index;
	MeasureType measure = itk::NumericTraits< MeasureType >::Zero;
    this->m_NumberOfPixelsCounted = 0;
    this->SetTransformParameters( parameters );
    while(!ti.IsAtEnd())
    {
        index = ti.GetIndex();
        
        InputPointType inputPoint;
        imFixed->TransformIndexToPhysicalPoint( index, inputPoint );
        OutputPointType transformedPointA = m_transformA->TransformPoint( inputPoint );
        OutputPointType transformedPointB = m_transformB->TransformPoint( inputPoint );
        if( m_interpolatorA->IsInsideBuffer( transformedPointA ) && m_interpolatorB->IsInsideBuffer( transformedPointB ) )
        {
            const RealType movingValueA = m_interpolatorA->Evaluate( transformedPointA );
            const RealType movingValueB = m_interpolatorB->Evaluate( transformedPointB );
            const RealType fixedValue = ti.Get();
            
            this->m_NumberOfPixelsCounted++;
            const RealType diff = movingValueA + movingValueB - fixedValue; 
            measure += diff * diff;
            measure += movingValueA * movingValueB;
        }
        ++ ti;
    }
    
    if( !this->m_NumberOfPixelsCounted )
    {
		std::cerr << "All the points mapped to outside of the moving image" << std::endl;
    }
    else
    {
        measure /= this->m_NumberOfPixelsCounted;
    }
    return measure;
}

template< class TFixedImage, class TMovingImage >
void JointCost< TFixedImage, TMovingImage >::GetValueAndDerivative( const ParametersType & parameters, MeasureType & Value, DerivativeType & Derivative ) const
{
    Value = GetValue( parameters );
    GetDerivative( parameters, Derivative );
}

#endif //_ksrtJointCost_cxx
