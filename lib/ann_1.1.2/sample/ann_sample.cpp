#include <cstdlib>
#include <cstdio>
#include <time.h>
#include <iostream>
#include <fstream>

#include "ANN/ANN.h"
using namespace std;

const int dim = 15;
const int k = 30;
const double eps = 0.0;

bool readPt(ifstream &in, ANNpoint p)			
{
	for (int i = 0; i < dim; i++) 
	{
		if(!(in >> p[i])) 
			return false;
	}
	return true;
}
void writeRt(ostream &out, ANNidxArray id)
{
	for (int i = 0; i < k; i++)
	{
		out << id[i] << " ";
	}
	out << "\n";
}
int main(int argc, char  ** argv)
{

	
	char * training_path = argv[1];
	char * test_path = argv[2];
	char * result_path = argv[3];

	time_t start, end;
	start = time(NULL);
	

	const unsigned int maxPts = 6000000;
	cout << training_path <<endl;
	cout << test_path <<endl;
	cout << result_path << endl;
	int nPts = 0;
	int nQrys = 0;

	ANNpointArray dataPts;
	ANNpointArray queryPts;
	ANNidxArray nnIdx;
	ANNdistArray dists;	
	ANNkd_tree* kdTree;

	queryPts = annAllocPts(maxPts, dim);
	dataPts = annAllocPts(maxPts, dim);
	nnIdx = new ANNidx[k];
	dists = new ANNdist[k];

	
	ifstream queryIn( test_path );
	ofstream resultOut( result_path );
	ifstream dataIn( training_path );

	while( nPts < maxPts && readPt( dataIn, dataPts[nPts] ) )
	{
		nPts++;
	}

	kdTree = new ANNkd_tree( dataPts, nPts, dim );

	while( nQrys < maxPts && readPt( queryIn, queryPts[nQrys] ) )
	{
		kdTree->annkSearch( queryPts[nQrys], k, nnIdx, dists, eps );
		writeRt( resultOut, nnIdx );
		nQrys++;
	}
	annDeallocPts( dataPts );
	annDeallocPts( queryPts );
	end = time(NULL);
	std::cout<<difftime(end, start)<<" seconds."<<std::endl;

	return 0;
}

