\section{Tutorial: Pfizer Longitudinal Study Dataset}
\label{sec:PLS_tutorial}

We will illustrate how to segment cartilage using the Longitudinal Multi-Atlas Approach based on the Pfizer Longitudinal Dataset. The software can be run in a similar way on the SKI10 and the OASIS datasets. This tutorial provides step-by-step instructions to run the software for analyzing longitudinal changes in cartilage thickness on subjects.  In this example, we will use the Multi-Atlas based cartilage segmentation with longitudinal constraints.

\subsection{PLS Dataset} \label{sec:Dataset}
\input{Dataset}

\subsection{Running the Software Pipeline} \label{sec:Run}
In this section we describe the expected output after running the provided scripts.
Recall that prerequisite tasks described in~\ref{sec:prereq} need to be done prior to running the scripts.

\subsubsection{Preprocessing}
As previously discussed, the first step is to process the data appropriately for analysis.
By executing \verb+preprocess.sh+ we perform the preprocessing on the entire dataset as described in~\ref{sec:Preprocessing}.
Type the following command to run preprocessing scripts on the dataset.
\begin{lstlisting}[frame=single]
qsub submit_Preprocess
\end{lstlisting}

We store all of the intermediate results of the preprocessing.
Below are the list of files that are saved as the intermediate results of the preprocessing.
\begin{itemize}
	\item \verb+cor_t1_correct+: Bias field caused by magnetic non-homogeneity is corrected from the original image \verb+cor_t1+.
	\item \verb+cor_t1_correct_scale+: Intensity values of \verb+cor_t1_correct+ are rescaled so that values are between 0 and 100
	\item \verb+cor_t1_correct_scale_smooth+: The result of the previous step, i.e., \verb+cor_t1_correct_scale+, are smoothed while preserving edges in the image.
	\item \verb+cor_t1_correct_scale_smooth_small+: The smoothed image, i.e., \verb+cor_t1_correct_scale_smooth+, is downsampled to be used during registration.
\end{itemize}

Figure~\ref{fig:after_preprocessing} shows the result of the preprocessing on an image taken at baseline for subject \verb+10011003+.

\begin{figure}[H]
	\centering
	\begin{tabular}{cc}
	\includegraphics[width=.45\textwidth]{images/cor_t1_correct.png} &
	\includegraphics[width=.45\textwidth]{images/cor_t1_correct_scale.png} \\
	(a) & (b) \\
	\includegraphics[width=.45\textwidth]{images/cor_t1_correct_scale_smooth.png} &
	\includegraphics[width=.45\textwidth]{images/cor_t1_correct_scale_smooth_small.png}\\
	(c) & (d) \\
	\end{tabular}
        \caption{Result of preprocessing of a subject baseline image: (a) bias-field corrected image, (b) intensity-scaled image, (c) smoothed image, (d) downsampled image.}
        \label{fig:after_preprocessing}
\end{figure}

\subsubsection{Training Classifiers} \label{sec:TrainSVM}
The next step is to train SVM classifiers to be used during cartilage segmentation to provide likelihood terms for the labeling cost in~(\ref{eq:CartLabelCost}).
We train SVM classifiers to perform probabilistic classification of the femoral cartilage, the background, and the tibial cartilage.
The classifier outputs probabilities of each voxels in the image belonging to the femoral cartilage, the background, and the tibial cartilage.
The classifier is trained using features described in~\ref{subsec:classfn} extracted from the atlas images.

Type the following command in the terminal to train the SVM classifier.
\begin{lstlisting}[frame=single]
qsub submit_TrainSVM
\end{lstlisting}
The script first creates a folder named \verb+SVM+ at the dataset directory to save trained classifiers and extracted features.
The path to this folder looks something like the following.
\begin{lstlisting}[frame=single,label=SVMOutDir]
$DataDIR/SVM
\end{lstlisting}
Once the directory is created (or already exists) the following files are saved in the folder after the execution of the script.
\begin{itemize}
	\item \verb+cor_t1_correct_scale_30000+:
	Intensity values of the atlas images are rescaled to range from 0 to 30000 prior to feature extraction.
	Recall that after the preprocessing intensity values of the image range from 0 to 100.
	\item \verb+trainLabel.nhdr+:
	Label image extracted to be used in the training
	\item \verb+train_1+:
	Features extracted for the femoral cartilage.
	\item \verb+train_2+:
	Features extracted for the background.
	\item \verb+train_3+:
	Features extracted for the tibial cartilage.
	\item \verb+train_1_small+:
	subset of \verb+train_1+ to be used during training
	\item \verb+train_2_small+:
	subset of \verb+train_2+ to be used during training
	\item \verb+train_3_small+:
	subset of \verb+train_3+ to be used during training	
	\item \verb+train_small+: 
	Concatenation of \verb+train_1_small+, \verb+train_2_small+, and \verb+train_3_small+
	\item \verb+train_balance.model+:
	trained SVM classifier.
\end{itemize}
%Intensity values of the atlas images are rescaled to range from 0 to 30000 prior to feature extraction;
%these rescaled images are saved as \verb+cor_t1_correct_scale_30000+.
%Features described in the previous section are extracted on these images.
%Along with the features training labels are also extracted using manual segmentation for the femoral cartilage and the tibial cartilage (Recall that these segmentation masks are available because training cases are atlas patients).
%Finally classifiers are trained on a reduced set of the features; the trained classifiers are saved as \verb+train_balance.model+ in the \verb+SVM+ folder.

\subsubsection{Automatic Cartilage Segmentation}
%Please run \verb+seg.sh+
Once the classifiers are trained we are now ready to process the query images. Please type the following command in the terminal.
\begin{lstlisting}[frame=single]
qsub submit_CartSeg_indp
\end{lstlisting}

In this section we provide what to expect as an output for each stage in the script along with visualization of intermediate results.

\subsubsection*{Bone Registration}
We register all of atlases to a given query image via computing a series of spatial transformations. 

During execution, files are saved to an output directory of the form
\begin{lstlisting}[frame=single,label=BoneRegOutDir]
$DataDIR/$SITEID_Q/$ID_Q/$VISIT_Q/moved_atlas
\end{lstlisting}
where \verb+$SITEID_Q+ is one of the \verb+site+ directories described in~\ref{sec:Dataset}, \verb+$ID_Q+ is one of the subject id directories under the \verb+SITEID_Q+, and \verb+$VISIT_Q+ is one of the measurement time points of that subject denoted by \verb+$ID_Q+. 
If the output directory does not exist, the script creates the output directory first. 
Once the output directory is created (or already exists), you should find the following files in the output directory.
\begin{itemize}
	\item affine transform file: \verb+affine_$ID_A.tfm+
	\item B-Spline transform file: \verb+bspline_$ID_A.tfm+
	\item transformed structural image: \verb+moved_$ID_A.nhdr+ and \verb+moved_$ID_A.raw.gz+
	\item transformed femur bone segmentation: \verb+moved_femur_$ID_A+ and \verb+moved_femur_$ID_A.raw.gz+
	\item transformed tibia bone segmentation: \verb+moved_tibia_$ID_A+ and \verb+moved_tibia_$ID_A.raw.gz+
\end{itemize}
\verb+$ID_A+ denotes different IDs of atlas subjects. 

\subsubsection*{Bone Label Fusion}
After the registration, we fuse all of the moved segmentations of the atlases to form the prior probability of a voxel belonging to the femur bone. We simply take an arithmetic mean of all of the transformed segmentation masks to form the prior probability map for the femur bone $p(Femur)$ and the tibia bone $p(Tibia)$ respectively. Two images named \verb+pFemur+ and \verb+pTibia+ are saved after the label fusion. Figure~\ref{fig:prior_tibia_femur} illustrates what these prior probability maps look like.

\begin{figure}[H]
  \centering
  \begin{tabular}{cc}
    \includegraphics[width=.45\textwidth]{images/pFemur.png} &
    \includegraphics[width=.45\textwidth]{images/pTibia.png} \\
    (a) & (b)
  \end{tabular}
  \caption{Spatial prior probability map for the femur bone (left, a) and the tibia bone (right, b).}
  \label{fig:prior_tibia_femur}
\end{figure}

\subsubsection*{Bone Segmentation}
We compute bone likelihoods for each voxel, which are saved as \verb+pBone+. Given bone likelihoods and priors we compute the labeling cost map for the femur bone, the background, and the tibia bone;
these are saved as \verb+cost_femur+, \verb+cost_bkgrd+, and \verb+cost_tibia+ respectively.
These labeling cost maps are used within a three-label segmentation method to segment the femur bone and the tibia bone. Figure~\ref{fig:costs_femur_tibia_background} shows visualizations of these costs.

\begin{figure}[H]
  \centering
  \begin{tabular}{c}
    \includegraphics[width=.8\textwidth]{images/cost_femur.png} \\
    \includegraphics[width=.8\textwidth]{images/cost_background.png} \\
    \includegraphics[width=.8\textwidth]{images/cost_tibia.png}
  \end{tabular}
  \caption{Labeling costs for the femur bone (top), the background (middle), and the tibia (bottom).}
  \label{fig:costs_femur_tibia_background}
\end{figure}

There is a regularization parameter to choose in segmentation method (see~\cite{shan2014} for details); this optimization; we set this parameter to $0.5$. Figure~\ref{fig:final_bone_segmentations} shows the final bone segmentations.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{images/KneeBoneSeg.png}
  \caption{Final bone segmentations.}
  \label{fig:final_bone_segmentations}
\end{figure}

%\begin{figure}[H]
%\centering
%	\includegraphics[width=.99\textwidth]{images/seg_femur.png}
%	\caption{Final Segmentation of the femur bone}
%\end{figure}
%
%\begin{figure}[H]
%\centering
%	\includegraphics[width=.99\textwidth]{images/seg_tibia.png}
%	\caption{Final Segmentation of the femur bone}
%\end{figure}

\subsubsection*{Extract Joint Region}
We automatically extract a smaller region (from this point on, we refer this region as \textit{joint region}) from the original images to make the further processing more efficient. These regions are automatically extracted using the bone segmentation masks from the structural image, the femur bone segmentation mask, and the tibia bone segmentation mask. 

In addition, we create another structural image which is a rescaled version of the structural image of the joint region.  The intensity values are rescaled so that they range from 0 to 30000. (Recall that after the preprocessing, intensity values range from 0 to 100.) This rescaled image is used for extracting features to be used in classification.

During execution of the script, we store all of the smaller images extracted around the joint region to an output directory. The output directory has the form
\begin{lstlisting}[frame=single,label=jointOutDir]
$DataDIR/$SITEID_Q/$ID_Q/$VISIT_Q/joint/
\end{lstlisting}
where \verb+$SITEID_Q+ is one of the \verb+site+ directories described in~\ref{sec:Dataset}, \verb+$ID_Q+ is one of the subject id directories under the \verb+SITEID_Q+, and \verb+$VISIT_Q+ is one of the measurement time points of that subject denoted by \verb+$ID_Q+. If the output directory does not exist, the script first creates the directory.

The following files are saved under the output directory after the script has finished extracting the joint region:
\begin{itemize}
	\item Structural image of this region: \verb+cor_t1_correct_scale+
	\item The rescaled structural image: \verb+cor_t1_correct_scale_30000+
	\item The femur bone segmentation mask of the region: \verb+seg_femur+
	\item The tibia bone segmentation mask of the region: \verb+seg_tibia+
\end{itemize}
Figure~\ref{fig:extracted} shows the results for a sample query image.

\begin{figure}[H]
	\centering
        \begin{tabular}{c}
	  \includegraphics[width=0.8\textwidth]{images/test.png} \\
	  \includegraphics[width=0.8\textwidth]{images/seg_femur_joint.png} \\
          \includegraphics[width=0.8\textwidth]{images/seg_tibia_joint.png}
        \end{tabular}
	\caption{Structural image extracted around the joint region (top), segmentation mask for the femur bone extracted around the joint region (middle), and segmentation mask for the tibia bone extracted around the joint region (bottom).}
        \label{fig:extracted}
\end{figure}

\subsubsection*{Cartilage Registration}
Just as for the bone segmentation, we first register all of the atlas subjects to the query subject as a first step of cartilage segmentation. The script first creates a folder to save the results generated during the cartilage registration of the form
\begin{lstlisting}[frame=single,label=CartRegOutDir]
$DataDIR/$SITEID_Q/$ID_Q/$VISIT_Q/moved_cart_atlas
\end{lstlisting}
where \verb+$SITEID_Q+ is one of the \verb+site+ directories described in~\ref{sec:Dataset}, \verb+$ID_Q+ is one of the subject id directories under the \verb+SITEID_Q+, and \verb+$VISIT_Q+ is one of the measurement time points of that subject denoted by \verb+$ID_Q+. If the output directory does not exist, the script creates the output directory first. Once the output directory is created (or already exists), the following files are saved in the output directory:
\begin{itemize}
	\item $T^{affine}_{i,FC}$: \verb+affine_femur_$ID_A.tfm+
	\item $\tilde{I}^{FC}_i$: \verb+moved_mri_femur_$ID_A+
	\item $\tilde{S}^{FC}_i$: \verb+moved_fem_$ID_A+
	\item $T^{affine}_{i,TC}$: \verb+affine_tibia_$ID_A.tfm+
	\item $\tilde{I}^{TC}_i$: \verb+moved_mri_tibia_$ID_A+
	\item $\tilde{S}^{TC}_i$: \verb+moved_tib_$ID_A+
\end{itemize}
\verb+$ID_A+ denotes different IDs of atlas subjects. 

\subsubsection*{Cartilage Tissue Classification}
\label{sec:CartClassn}
We perform probabilistic classification to output probabilities of a voxel belonging to three classes: the femoral cartilage, the background, and the tibial cartilage. Recall that these probabilities will be used to compute the likelihood terms for the labeling cost in~(\ref{eq:CartLabelCost}).

The script first creates a folder of the following form to save results of the SVM classification:
\begin{lstlisting}[frame=single]
$DataDIR/$SITEID_Q/$ID_Q/$VISIT_Q/joint_segementation
\end{lstlisting}
where \verb+$SITEID_Q+ is one of the \verb+site+ directories described in~\ref{sec:Dataset}, \verb+$ID_Q+ is one of the subject id directories under the \verb+SITEID_Q+, and \verb+$VISIT_Q+ is one of the measurement time points of that subject denoted by \verb+$ID_Q+. Then features are extracted from \verb+cor_t1_correct_scale_30000+ and are saved in a file named \verb+f_$ID_$VISIT+ in the \verb+SVM+ folder (\verb+$ID+ and \verb+$VISIT+ denote subject id and the time point of the current query image respectively).

We use the trained classifier~\ref{sec:TrainSVM} to perform probabilistic classification; the output of the classification is saved in a file named \verb+p_$ID_$VISIT+ under the \verb+SVM+ folder. The classification output is then transformed to likelihood probability maps for the femoral cartilage and the tibial cartilage respectively; these likelihood maps are saved under \verb+joint_segmentation+.

The following files are generated as a result of classification:
\begin{itemize}
\item The likelihood map for the femoral cartilage: \verb+pFem_svm+
\item The likelihood map for the tibial cartilage: \verb+pTib_svm+
\end{itemize}

Figure~\ref{fig:pFemTib} shows the likelihood probability maps of the femoral cartilage and the tibial cartilage for one of queries.

\begin{figure}[H]
  \centering
  \begin{tabular}{c}
    \includegraphics[width=0.8\textwidth]{images/pFem.png} \\
    \includegraphics[width=0.8\textwidth]{images/pTib.png}
  \end{tabular}
  \caption{The classification map of the femoral cartilage (top) and of the tibial cartilage (bottom).}
  \label{fig:pFemTib}
\end{figure}

\subsubsection*{Cartilage Label Fusion} \label{sec:CartFusion}
Once all of the atlases are registered to the query, we now fuse the registered labels of the atlases to form spatial priors for the femoral cartilage and the tibial cartilage. We use non-local patch-based label fusion techniques to fuse all of deformed label images.

We first upscale both deformed structural image and deformed segmentation masks by a factor of 3 to make them approximately isotropic. Then we compute~\ref{eq:PatchFusion} for each voxel locations with patch size being set to 2, neighborhood size set to 2, and nearest neighbor set to 5. The resulting fused labels for the femoral cartilage and the tibial cartilages are downsampled to the original size.

After the label fusion is finished, the results are saved as images under \verb+joint_segmentation+ created in the previous step. This results in:
\begin{itemize}
\item Non-local patch based label fusion of the femoral cartilage: \verb+fem_fusion_patch+
\item Non-local patch based label fusion of the tibial cartilage: \verb+tib_fusion_patch+
\end{itemize}
Figure~\ref{fig:cartilage_label_fusion} shows the label fusion results for the femoral and the tibial cartilage for an example query image.

\begin{figure}[H]
  \centering
  \begin{tabular}{c}
    \includegraphics[width=0.8\textwidth]{images/fem_fusion.png}\\
    \includegraphics[width=0.8\textwidth]{images/tib_fusion.png}
  \end{tabular}
  \caption{The label fusion result for the femoral cartilage (top) and the tibial cartilage (bottom).}
  \label{fig:cartilage_label_fusion}
\end{figure}

\subsubsection*{Compute Normal Directions} \label{sec:NormalDir}
The script also computes the normal directions for each dimension. These normal directions are needed because we use anisotropic regularization during segmentation. Normal directions are saved as \verb+nx+, \verb+ny+, and \verb+nz+ under the folder where the current query image is.

\subsubsection*{Cartilage Segmentation}
Once we have computed prior probabilities from label fusion and likelihood probabilities provided by SVM, we can now compute the labeling cost for each of the classes in terms of posterior probabilities.
There are two parameters $g$ and $a$ that control two regularization terms used in the optimization process. In our example, we set $g$ to be 1.4 and $a$ to be 0.2. We use the normal directions computed previously because we are segmenting the images using anisotropic regularization. For more details on these parameters, please refer~\cite{shan2014}.

The script saves the following files under the \verb+joint_segmentation+ folder:
\begin{itemize}
	\item Labeling cost of background: \verb+bkg_cost+
	\item Labeling cost of the femoral cartilage: \verb+fem_cost+
	\item Labeling cost of the tibial cartilage: \verb+tib_cost+
	\item Segmentation mask of the femoral cartilage: \verb+seg_fem+
	\item Segmentation mask of the tibial cartilage: \verb+seg_tib+
\end{itemize}
Figure~\ref{fig:labeling_costs} shows the labeling costs for background, and the femoral and tibial cartilage respectively. Figure~\ref{fig:segmentation_masks} shows the resulting segmentations for femur and tibia.

\begin{figure}[H]
  \centering
  \begin{tabular}{c}
    \includegraphics[width=0.8\textwidth]{images/joint_cost_fem.png}\\
    \includegraphics[width=0.8\textwidth]{images/joint_cost_background.png}\\
    \includegraphics[width=0.8\textwidth]{images/joint_cost_tib.png}
  \end{tabular}
  \caption{Labeling cost for the femur (top), background (middle), and tibia (bottom).}
  \label{fig:labeling_costs}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{tabular}{c}
    \includegraphics[width=0.8\textwidth]{images/seg_fem.png}\\
    \includegraphics[width=0.8\textwidth]{images/seg_tib.png}
  \end{tabular}
  \caption{Final segmentation mask of the femoral cartilage (top) and the tibial cartilage (bottom).}
  \label{fig:segmentation_masks}
\end{figure}

\subsubsection*{Temporal Alignment}
If we know the dataset is longitudinal, then we can encourage temporal consistencies among resulting cartilage segmentation masks of different time points.

We need the following two text files for the longitudinal segmentation:
\begin{itemize}
	\item \verb+in_aniso.txt+:
	A text file listing input files needed for longitudinal segmentation. Each line corresponds to the femoral cartilage labeling cost, background labeling cost, the tibial cartilage labeling cost and normal directions of different time points of a subject.
	\item \verb+out_aniso.txt+:
	A text file listing output files of longitudinal segmentation. Each line corresponds to the longitudinal segmentation of the femoral cartilage and the tibial cartilage of different time points of a subject.
\end{itemize}
These text files are automatically generated by running the following bash script; please type the following command in the terminal.
\begin{lstlisting}[frame=single]
sh ksrt/script/CartSeg_Long_Preprocess.sh
\end{lstlisting}
Notice that this script is run as a standard \textbf{bash} script.
Please run this script only once.

After running \verb+CartSeg_Long_Preprocess.sh+, please type the following command in terminal to rigidly align segmentation masks of different time points.
\begin{lstlisting}[frame=single]
qsub submit_temporal_align
\end{lstlisting}

The script first creates intermediate output folders for each subjects to store intermediate results of the temporal alignment. It first creates the following folder:
\begin{lstlisting}[frame=single]
$DataDIR/$SITE/$ID/joint4D
\end{lstlisting}
(Recall that \verb+$ID+ and \verb+$SITE+ denote subject id and the site id of the current query patient).

Then the script creates another folder under the \verb+joint4D+ folder just created to save intermediate results of registrations.
\begin{lstlisting}[frame=single]
$DataDIR/$SITE/$ID/joint4D/transform
\end{lstlisting}

As noted in the previous section (i.e. section~\ref{sec:LongCartSeg}) we use a series of rigid registrations to temporally align segmentation results of different time points to the baseline.
The following files are the results of the first registration which is based on bone segmentation masks.
These files are saved in the \verb+transform+ folder.
Different time points of a subject are denoted by \verb+$VISIT+.
\begin{itemize}
\item \verb+$VISIT_femur.tfm+: Rigid transformation computed based on femur bone segmentation masks
\item \verb+$VISIT_femurBased.nhdr+: Rigidly transformed original structural image according to the above transformation (i.e., \verb+$VISIT_femur.tfm+)
\item \verb+$VISIT_femur_femurBased.nhdr+: Rigidly transformed femur bone segmentation mask according to the above transformation (i.e., \verb+$VISIT_femur.tfm+)
\item \verb+$VISIT_seg_fem_femurBased.nhdr+: Rigidly transformed femoral cartilage segmentation mask according to the above transformation (i.e., \verb+$VISIT_femur.tfm+)
\item \verb+$VISIT_fem_fusion_patch_femurBased.nhdr+: Rigidly transformed femoral cartilage spatial prior map according to the above transformation (i.e., \verb+$VISIT_femur.tfm+)
\item \verb+$VISIT_pFem_femurBased.nhdr+: Rigidly transformed femoral cartilage likelihood map according to the above transformation (i.e., \verb+$VISIT_femur.tfm+)

\item \verb+$VISIT_tibia.tfm+: Rigid transformation computed based on tibia bone segmentation masks
\item \verb+$VISIT_tibiaBased.nhdr+: Rigidly transformed original structural image according to the above transformation (i.e., \verb+$VISIT_tibia.tfm+)
\item \verb+$VISIT_tibia_tibiaBased.nhdr+: Rigidly transformed tibia bone segmentation mask according to the above transformation (i.e., \verb+$VISIT_tibia.tfm+)
\item \verb+$VISIT_seg_tib_tibiaBased.nhdr+: Rigidly transformed tibial cartilage segmentation mask according to the above transformation (i.e., \verb+$VISIT_tibia.tfm+)
\item \verb+$VISIT_tib_fusion_patch_tibiaBased.nhdr+: Rigidly transformed tibial cartilage spatial prior map according to the above transformation (i.e., \verb+$VISIT_tibia.tfm+)
\item \verb+$VISIT_pTib_tibiaBased.nhdr+: Rigidly transformed tibial cartilage likelihood map according to the above transformation (i.e., \verb+$VISIT_tibia.tfm+)
\end{itemize}

The following files are the results of the second registration which is based on cartilage segmentation masks. The rigid transformation is computed among deformed cartilage segmentation masks (i.e., \verb+$VISIT_seg_fem_femurBased.nhdr+ and \verb+$VISIT_seg_tib_tibiaBased.nhdr+).
Then this transformation is applied to deformed images, deformed segmentation masks, deformed fusion results, and deformed likelihood maps.
These files are also saved in \verb+transform+ folder.
\begin{itemize}
\item \verb+$VISIT_fem.tfm+: Rigid transformation computed based on the femoral cartilage segmentation masks
\item \verb+$VISIT_femBased.nhdr+: Rigidly transformed original structural image according to the above transformation (i.e., \verb+$VISIT_fem.tfm+)
\item \verb+$VISIT_femur_femBased.nhdr+: Rigidly transformed femur bone segmentation mask according to the above transformation (i.e., \verb+$VISIT_fem.tfm+)
\item \verb+$VISIT_seg_fem_femBased.nhdr+: Rigidly transformed femoral cartilage segmentation mask according to the above transformation (i.e., \verb+$VISIT_fem.tfm+)
\item \verb+$VISIT_fem_fusion_patch_femBased.nhdr+: Rigidly transformed femoral cartilage spatial prior map according to the above transformation (i.e., \verb+$VISIT_fem.tfm+)
\item \verb+$VISIT_pFem_femBased.nhdr+: Rigidly transformed femoral cartilage likelihood map according to the above transformation (i.e., \verb+$VISIT_fem.tfm+)

\item \verb+$VISIT_tib.tfm+: Rigid transformation computed based on the tibial cartilage segmentation masks
\item \verb+$VISIT_tibBased.nhdr+: Rigidly transformed original structural image according to the above transformation (i.e., \verb+$VISIT_tib.tfm+)
\item \verb+$VISIT_tibia_tibBased.nhdr+: Rigidly transformed tibia bone segmentation mask according to the above transformation (i.e., \verb+$VISIT_tib.tfm+)
\item \verb+$VISIT_seg_tib_tibBased.nhdr+: Rigidly transformed tibial cartilage segmentation mask according to the above transformation (i.e., \verb+$VISIT_tib.tfm+)
\item \verb+$VISIT_tib_fusion_patch_tibBased.nhdr+: Rigidly transformed tibial cartilage spatial prior map according to the above transformation (i.e., \verb+$VISIT_tib.tfm+)
\item \verb+$VISIT_pTib_tibBased.nhdr+: Rigidly transformed tibial cartilage likelihood map according to the above transformation (i.e., \verb+$VISIT_tib.tfm+)

\end{itemize}

In addition to the \verb+transform+ folder, the script creates another folder under the \verb+joint4D+ folder to save results of the longitudinal segmentation.
The folder name is of the following form:
\begin{lstlisting}[frame=single]
$DataDIR/$SITE/$ID/joint4D/segmentation
\end{lstlisting}

The script computes labeling cost for the femoral cartilage, background, and the tibial cartilage based on rigidly deformed cartilage segmentation masks, fusion results, and the likelihood maps.
Normal directions are also computed because we are using anisotropic regularization for the longitudinal segmentation.
The following files are saved under the \verb+segmentation+ folder.
\begin{itemize}
\item \verb+$VISIT_fem_cost.nhdr+: Labeling cost of the femoral cartilage
\item \verb+$VISIT_bg_cost.nhdr+: Labeling cost of the background
\item \verb+$VISIT_tib_cost.nhdr+: Labeling cost of the tibial cartilage
\item \verb+$VISIT_nx.nhdr+: Normal direction along the first dimension
\item \verb+$VISIT_ny.nhdr+: Normal direction along the second dimension
\item \verb+$VISIT_nz.nhdr+: Normal direction along the third dimension
\end{itemize}

\subsection{Longitudinal Segmentation}
Please type the following command for longitudinal three label based segmentation with anisotropic regularization.
\begin{lstlisting}[frame=single]
qsub submit_CartSeg_long
\end{lstlisting}
The following files are saved under \verb+segmentation+ folder.
\begin{itemize}
\item \verb+$VISIT_seg_fem_long.nhdr+: Longitudinal segmentation masks for the femoral cartilage
\item \verb+$VISIT_seg_tib_long.nhdr+: Longitudinal segmentation masks for the tibial cartilage
\end{itemize}


\subsection{Cartilage Thickness Computation}

Please type the following command to compute tissue thickness based on the longitudinal segmentation masks.
\begin{lstlisting}[frame=single]
qsub submit_Thickness_long
\end{lstlisting}
The script will create a folder to save intermediate results of the following form:
\begin{lstlisting}[frame=single]
$DataDIR/thick
\end{lstlisting}
Then 3D tissue thickness is computed on the results of the longitudinal segmentation.
The following files are saved under the \verb+thick+ folder.
\begin{itemize}
\item \verb+fem_$ID_$VISIT.nhdr+: 3D thickness computed on longitudinal segmentation masks for the femoral cartilage
\item \verb+tib_$ID_$VISIT.nhdr+: 3D thickness computed on longitudinal segmentation masks for the tibial cartilage
\end{itemize}

Then we deform each of these 3D thickness maps to the baseline of a subject $10021014$ prior to creating 2D thickness maps.
This is achieved via affine transformation followed by B-Spline transformation. The script creates another folder to save the results of these spatial transformations of the following form:
\begin{lstlisting}[frame=single]
$DataDIR/thick/transform
\end{lstlisting}

After the \verb+transform+ folder is created, the following files are saved.
\begin{itemize}
\item \verb+affine_$ID_$VISIT_femur.tfm+
\item \verb+bspline_$ID_$VISIT_femur.tfm+
\item \verb+affine_$ID_$VISIT_tibia.tfm+
\item \verb+bspline_$ID_$VISIT_tibia.tfm+
\end{itemize}

After applying the affine followed by the B-Sspline transform, the resulting deformed 3D thickness is saved under the folder of the following form:
\begin{lstlisting}[frame=single]
$DataDIR/thick/moved
\end{lstlisting}

\begin{itemize}
\item \verb+fem_$ID_$VISIT.nhdr+: Deformed 3D thickness computed on longitudinal segmentation masks for the femoral cartilage
\item \verb+tib_$ID_$VISIT.nhdr+: Deformed 3D thickness computed on longitudinal segmentation masks for the tibial cartilage
\end{itemize}

Finally 2D thickness maps are generated via projection of these deformed 3D thickness.
The script creates a folder to save these 2D projected thickness maps of the following form:
\begin{lstlisting}[frame=single]
$DataDIR/thick/moved_2d
\end{lstlisting}

The following files are saved under \verb+moved_2d+ folder as a result of the computation:
\begin{itemize}
\item \verb+fem_$ID_$VISIT.nhdr+: Projected 2D thickness computed on longitudinal segmentation masks for the femoral cartilage
\item \verb+tib_$ID_$VISIT.nhdr+: Projected 2D thickness computed on longitudinal segmentation masks for the tibial cartilage
\end{itemize}

\subsection{Statistical Analysis on the Thickness}
Once the 2D thickness maps are generated, we use MATLAB scripts to perform localized analysis of changes on cartilage thickness. The Statistical Toolbox is required to run the analysis. 

To associate the images with KLG grades we need to provide a textfile named \verb+list_KLG.txt+.
This file is formatted in the following way:
\begin{verbatim}
... TODO ...
\end{verbatim}

We can then run the analysis based on the following scripts:

\begin{enumerate}
	\item First run the following script in MATLAB
\begin{verbatim}
>> create_mask
\end{verbatim}
This script will compute a binary mask for a region where the majority of pixels in the projected 2D thickness map lie. The binary mask is computed for the femoral cartilage and the tibial cartilage separately. These binary masks are saved as \verb+fem_mask.mat+ (binary mask for the femoral cartilage) and \verb+tib_mask.mat+ (binary mask for the tibial cartilage). This script also reads in previously computed 2D thickness maps and saves as \verb+fem.mat+ and \verb+tib.mat+.
These \verb+.mat+ files are saved under the \verb+mat+ folder which is created if the folder does not exist.
\item Then run the following scripts to fit initial linear mixed-effects model to model difference between OA subjects and Normal Control subjects.
\begin{verbatim}
>> fitlme_fem
>> fitlme_tib
\end{verbatim}
These two scripts will fit the models respectively based on KLG score specified in \verb+list_KLG.txt+ and 2D thickness values. Just as the previous script, these scripts save the results in \verb+.mat+ files. These scripts save the computed models as \verb+lme_cell_fem.mat+ and \verb+lme_cell_tib.mat+ respectively.
\item Run the following scripts to cluster OA subjects.
\begin{verbatim}
>>cluster_fem
>>cluster_tib
\end{verbatim}
We cache fixed effects, random effects, standard deviations, and p-values in \verb+.mat+ files. 
We use a standard K-means cluster method to cluster OA subjects.
The result of cluster indices for OA subjects are saved as \verb+index_fem.mat+ and \verb+index_tib.mat+. 
\item Then finally run the following scripts to fit models to each clusters.
There are total of 8 scripts to run.

\begin{verbatim}
>>fitlme_cluster1_fem
>>fitlme_cluster1_tib
>>fitlme_cluster2_fem
>>fitlme_cluster2_tib
>>fitlme_cluster3_fem
>>fitlme_cluster3_tib
>>fitlme_cluster4_fem
>>fitlme_cluster4_tib
\end{verbatim}

Each of the scripts computes a linear mixed-effects model respectively and caches coefficients of the model, p-values, and error sum of squares in \verb+.mat+ file. After running \verb+fitlme_cluster1_fem.m+, the following files are saved under the \verb+mat+ folder.
\begin{itemize}
\item \verb+lme_cell_femC1.mat+: Computed Linear Mixed-Effects Model between Normal Control subjects and OA subects in cluster 1
\item \verb+C1_fem_f0.mat+: Fixed effect 0
\item \verb+C1_fem_f1.mat+: Fixed effect 1
\item \verb+C1_fem_f2.mat+: Fixed effect 2
\item \verb+C1_fem_f3.mat+: Fixed effect 3
\item \verb+C1_fem_se0.mat+: Error sum of squares for fixed effect 0
\item \verb+C1_fem_se1.mat+: Error sum of squares for fixed effect 1
\item \verb+C1_fem_se2.mat+: Error sum of squares for fixed effect 2
\item \verb+C1_fem_se3.mat+: Error sum of squares for fixed effect 3
\item \verb+C1_fem_pf0_raw.mat+: Raw p-values of fixed effect 0 not being 0 
\item \verb+C1_fem_pf1_raw.mat+: Raw p-values of fixed effect 1 not being 0 
\item \verb+C1_fem_pf2_raw.mat+: Raw p-values of fixed effect 2 not being 0 
\item \verb+C1_fem_pf3_raw.mat+: Raw p-values of fixed effect 3 not being 0
\item \verb+C1_fem_pf0_raw.mat+: False Discovery Rate adjusted p-values of fixed effect 0 not being 0 for cluster 1
\item \verb+C1_fem_pf1_raw.mat+: False Discovery Rate adjusted p-values of fixed effect 1 not being 0 for cluster 1
\item \verb+C1_fem_pf2_raw.mat+: False Discovery Rate adjusted p-values of fixed effect 1 not being 0 for cluster 1
\item \verb+C1_fem_pf3_raw.mat+: False Discovery Rate adjusted p-values of fixed effect 1 not being 0 for cluster 1
\end{itemize}
\end{enumerate}
The same set of files is saved for each cluster and for femoral cartilage and tibial cartilage respectively.
