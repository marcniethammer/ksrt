\section{KSRT Software}
\label{sec:software}

This section describes the software available implementing the algorithms described in Section~\ref{sec:analysis_approaches}. Specifically, Section~\ref{sec:software_overview} gives an overview of the software pipeline. Section~\ref{sec:image_data_format} discusses the image format used. Section~\ref{sec:building_the_software} discusses how to build the software. Section~\ref{sec:supported_platforms} discusses the supported platforms.

\subsection{Overview of the Software Pipeline}
\label{sec:software_overview}
% Provide workflow of the pipeline
This software package contains several components for automatic segmentation of cartilage. In what follows, we will give an overview of all the stages of the analysis pipeline.
The source code distribution contains the following directories:
\begin{itemize}
\item \textit{src}:
This directory contains \textit{C++} implementation of the algorithm.
\item \textit{lib}:
This directory contains various applications used.
\item \textit{scripts}:
This directory contains scripts to run the segmentation on a compute cluster.
\item \textit{matlab}:
This directory contains scripts written in \textit{MATLAB} to perform statistical analysis on cartilage thickness.
\end{itemize}

\subsubsection{Overview of Scripts}
In the source code distribution we provide scripts to submit jobs on clusters using the \verb+qsub+ command to automatically segment cartilage from query images.

Under the \verb+scripts+ directory, you will find the following job submission scripts:
\begin{itemize}
\item \verb+submit_Preprocess+:
This script performs preprocessing on the dataset as described in~\ref{sec:Preprocessing}.
\item \verb+submit_TrainSVM+:
This script trains SVM classifiers using the \textbf{libSVM} implementation.
Three SVM classifiers for the femoral cartilage, the background, and the tibial cartilage are trained.
The trained classifiers are saved to be used during the cartilage segmentation.
\item \verb+submit_CartSeg_indp+:
This script is the main script that automatically segments femoral and tibial cartilage from query images as described in~\ref{sec:cartilage_segmentation}.
In order to run this script successfully, some prerequisite tasks need to be performed prior to executing the script.
Please refer to Section~\ref{sec:prereq} for the details on these tasks.
\item \verb+submit_temporal_align+:
This script enforces temporal consistencies across segmentation results of different time points of a single subject.
In order to run this script successfully the dataset needs to be longitudinal and also needs to be organized in a specific way as described later in this document.
\item \verb+submit_CartSeg_long+:
This script refines segmentation results via establishing spatial correspondence across different time points.
\end{itemize}

\subsubsection{Prerequisites Tasks} 
\label{sec:prereq}
To run the pipeline using the provided scripts the following path variables need to be defined.
If you are using \verb+bash+, then follow the instructions below to define path variables in your \verb+.bashrc+ so that scripts running the pipeline can access the appropriate paths. In addition, you need to create two text files to denote query images and atlas images in the dataset so that the aforementioned scripts can access and process data appropriately.
\begin{itemize}
	\item \textbf{Path Variables}
	\begin{itemize}
		\item \textbf{ksrt Application Directory}:
		You have to define ksrt Application directory as \verb+ksrtAppDIR+ in your system setting file. 
	If you are using bash, then put the following in your \verb+.bashrc+. 
		\begin{verbatim}
			AppDIR=/home/username/ksrt/build/Application
			export AppDIR
		\end{verbatim}
		\item \textbf{ksrt Library Directory}:
		If you choose to use the provided scripts to run the pipeline, then put the following in your \verb+.bashrc+. 
			\begin{verbatim}
				LibDIR=/home/username/ksrt/lib
				export LibDIR
			\end{verbatim}
		\item \textbf{Dataset Directory}:
		You have to define \verb+DataDIR+ to be the path to your dataset. Please put the following in your \verb+.bashrc+.
			\begin{verbatim}
				DataDIR=/home/username/your_path_to_data
				export DataDIR
			\end{verbatim}
		\item \textbf{Slicer Directory}:
		If you choose to use the provided scripts to run the pipeline on a compute cluster, some of applications that are part of Slicer3 are needed.
		Please put the following lines in your \verb+.bashrc+.
		\begin{verbatim}
		SlicerDIR=/home/username/your_path_to_Slicer
		export SlicerDIR
		\end{verbatim}
	\end{itemize}
	\item \textbf{Text files}:
	Below are the text files needed. Place these two text files where the dataset is.
	That is the directory pointed by \verb+$DataDIR+ defined in \verb+.bashrc+.
	\begin{itemize}
		\item \verb+list.txt+:
		This text file contains a list of all query images to be segmented.
		This text file needs to list relative paths to the query images from \verb+$DataDIR+.
		For example, \verb+list.txt+ used later in~\ref{sec:PLS_tutorial} looks like the following.
		\begin{verbatim}	
site1001 10011003 0
site1001 10011003 3
site1001 10011003 6
site1001 10011003 12
site1001 10011003 24
site1001 10011004 0
.....
		\end{verbatim}
		With each row corresponding to a single query image. There are three columns per row.
		The first column denotes which \verb+site+ directory the query image resides in.
		The second column denotes the subject id of the query image.
		Lastly, the third column denotes the time point of the query image (in month).		
		\item \verb+list_atlas.txt+:
		This text file contains a list of all atlas images.
		The format is exactly the same as \verb+list.txt+.
	\end{itemize}
\end{itemize}

\subsection{Image Data Format}
\label{sec:image_data_format}

We use the Nearly Raw Raster Data (NRRD) format throughout the analysis. See \url{teem.sourceforge.net/nrrd} for a detailed description of this format. This data format is supported by ITK (\url{www.itk.org}). AS our analysis software uses ITK to read and write images other ITK supported file-formats could in principle also easily be supported in future releases of the software. For now please convert all your data to NRRD format before running this software. This conversion could for example be done using \textbf{Slicer} or using a simple ITK script.

\subsection{Building the Software}
\label{sec:building_the_software}

\textbf{KSRT} depends on a number of different open-source packages which are freely available online.
\textbf{CMake} is used to configure the build and is available from ~\url{http://www.cmake.org}. 
Earlier versions of CMake may not be supported so we recommend downloading CMake at least higher than version 2.6. 

%which is in \textit{lib} directory when you unzip the archive file.

\subsubsection{Dependencies} \label{sec:Dep} % switch the first paragraph in this section up and the first paragraph in the previous section
To build the software, you will need the following packages. 
If you have already built \textbf{Slicer-3},
then you only need to build the \textbf{Approximate Nearest Neighbor} library and the \textbf{ImageMath} package.

\begin{itemize}
	\item \textbf{InsightToolkit 3.20 (ITK3)} is required for all of applications in \textbf{KSRT} package. Both \textbf{ITK3} and \textbf{ITK4} are available to download at \url{http://www.itk.org}. 
	%\item \textbf{InsightToolkit 4.3.1 (ITK4)} is required for few applications under \verb+lib+ provided as a part of the software distribution.
	%Both \textbf{ITK3} and \textbf{ITK4} are available to download at \url{http://www.itk.org} and both of them are needed to successfully run the pipeline.
	\item \textbf{Slicer-3} is also required to build \textbf{KSRT }successfully. 
	It is available to download from~\url{http://www.slicer.org/slicerWiki/index.php/Slicer3:Downloads}.
	\item \textbf{ann 1.1.2} is required to build \textbf{KSRT}. 
	This is an open-source library for approximate nearest neighbor search and is available to download at \url{http://www.cs.umd.edu/~mount/ANN/}.
	This is provided as a part of the software distribution.
\end{itemize}
\subsubsection{Utility Applications}
In addition, the software uses several applications. 
These applications are not required to build \textbf{KSRT},
but these are required to run the pipeline using the provided scripts successfully.
These applications are included as part of the source code distribution.
Below are the list of the applications used along with short descriptions. 
\begin{itemize}
	\item \textbf{ImageMath} is an open-source C++ library for mathematical operations on images. 
	It is available to download at \url{http://www.nitrc.org/projects/niral_utilities/}.
	\item \textbf{ExtractLargestConnectedComponentFromBinaryImage} is used to segment the largest connected component from a binary image. This is used to segment out bones and cartilages after the labeling cost of each object is computed. This application can be found under~\verb+/path_to_ksrt_source/lib/ExtractLargestConnectedComponentFromBinaryImage/+. A detailed description of the application is available at~\url{http://www.itk.org/Wiki/ITK/Examples/ImageSegmentation/ExtractLargestConnectedComponentFromBinaryImage}. \\
	%\textbf{ITK4} is required to build this application.
	%\fixme{\mn{Is this really necessary? Do we need to install two versions of ITK just for this filter? Can it either be provided for ITK3 or can we switch the whole project to ITK4?}}
	\item \textbf{libsvm-3.17} is a C++ implementation of Support Vector Machine libraries. This is needed if you wish to use SVM to compute likelihoods which in turn are used to compute labeling costs. It is available to download at~\url{http://www.csie.ntu.edu.tw/~cjlin/libsvm/}.
\end{itemize}

\subsubsection{Build Instruction}

%\fixme{
%\mn{It is unclear in this section where the KSRT source needs to go, how you get the source and where all the other libraries need to go. I doubt that this is easy to follow for anybody. Can this not be streamlined somehow?}
%}

\textbf{CMake} is used to configure the build and generate Makefiles or Visual Studio Solution files. 
After successfully configuring, the software can be built using the native compiler.
For information on using \textbf{CMake}, please refer to~\url{http://www.cmake.org}.

The following section provides step-by-step instruction on how to build dependent packages.
% ITK3
\begin{itemize}
\item \textbf{Slicer3}
Please refer for build instructions on building \textbf{Slicer3}~\url{http://www.slicer.org/slicerWiki/index.php/Slicer3:Build_Instructions}.
When finished building \textbf{Slicer3}, \textbf{ITK3} will also be downloaded and installed.
%\item \textbf{ITK4}
%\begin{enumerate}
%\item Please download \textbf{InsightToolkit 4.3.1} using \textbf{Git} via typing the following commands in the terminal. 
%\begin{lstlisting}
%git clone git://itk.org/ITK.git
%\end{lstlisting}
%This command will download the most recent version of \textbf{ITK} from official \textbf{Git} repository.
%\item Create a separate build directory that looks like the following in the current directory. Then change into the created directory.
%\begin{lstlisting}
%mkdir ITK4-build
%cd ITK4-build
%\end{lstlisting}

%\item Type the following command to configure \textbf{ITK4} using \textbf{CMake}.
%\begin{lstlisting}
%ccmake ../ITK4
%\end{lstlisting}
%\item Once configured and generated via \textbf{CMake}, type the following command to build \textbf{ITK4}.
%\begin{lstlisting}
%make
%\end{lstlisting}
%
%\end{enumerate}

\end{itemize}

Once all of dependent applications are built, then please follow the steps below to build \textbf{KSRT}.

\begin{enumerate}
	\item Clone the git source code distribution at~\url{git@bitbucket.org:marcniethammer/ksrt.git} by executing
          \begin{lstlisting}
            git clone git@bitbucket.org:marcniethammer/ksrt.git
          \end{lstlisting}
	\item Create a separate directory for your build files and executables. For example,
	if you cloned the source code distribution in~\verb+/home/username/ksrt+, 
	then you may create a separate directory named\\~\verb+/home/username/ksrt-build+. 
	\item Change into the build directory created in the previous step.
	Then run\verb+ccmake ../ksrt+ in the terminal to configure \textbf{KSRT}.
	\item Below are the list of path variables needed to configure \textbf{KSRT} successfully.

\begin{itemize}
\item \textbf{ANN\_LIB} is a path to actual library file which is located at \verb+ksrt/lib/ann_1.1.2/lib/libANN.a+ directory of the source code distribution.
	\item \textbf{ANN\_PATH}  is path to include directory of \textbf{ann\_1.1.2} package.\\ 
	This directory is at \verb+ksrt/lib/ann_1.1.2/include+
	%\item \textbf{ITK4\_DIR} is a path to build directory of \textbf{ITK4}.
	\item \textbf{Slicer3\_DIR} is a path build directory of \textbf{Slicer3}.
	\item \textbf{GenerateCLP\_DIR} is a path to a package that comes with \textbf{Slicer3}.
	This package should be located at under the build directory of \textbf{Slicer3} that looks like the following.
	\begin{verbatim}
your_path_to_slicer3/Slicer3-build/Libs/SlicerExecutionModel/GenerateCLP
	\end{verbatim}
	\item \textbf{ITK3\_DIR} is also installed along with \textbf{Slicer3}.
	This should be set to something like the following.
	\begin{verbatim}
your_path_to_slicer3/Slicer3-lib/Insight-build
	\end{verbatim}

\end{itemize}		
	
	Once these path variables are set, then let \textbf{CMake} configure again and again until \textbf{CMake} can configure the build without any errors.
	\item Once Configured, you can generate the build, i.e., Makefile for \textbf{KSRT}.
	\item Use \verb+make+ command to build \textbf{KSRT}.
\end{enumerate}

\subsection{Supported Platform}
\label{sec:supported_platforms}

We have built and tested the software package under Unix-based system (e.g. Linux and MacOSx). We recommend running the system on a cpmpute cluster as some parts of the pipeline are computationally demanding, but can be computed in parallel on a cluster.
