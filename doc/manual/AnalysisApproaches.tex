\section{Analysis Approaches}
\label{sec:analysis_approaches}

This section describes briefly the analysis approaches used in KSRT. Specifically, Section~\ref{sec:cartilage_segmentation} describes how cartilage segmentation is performed, Section~\ref{sec:cartilage_thickness_computation} discusses the cartilage thickness computations, and Section~\ref{sec:statistical_thickness_analysis} describes the statistical analysis approach.

\subsection{Cartilage Segmentation}
\label{sec:cartilage_segmentation}

\begin{figure}[!h]
\centering
	\includegraphics[width=0.99\textwidth]{images/MA_CartSeg.png}
	\caption{Flowchart of overall cartilage segmentation pipeline}
	\label{fig:PipelineFlowChart}
\end{figure}
KSRT uses a multi-atlas-based segmentation approach~\cite{shan2014} with non-local patch-based label fusion.
Figure~\ref{fig:PipelineFlowChart} shows a flow-chart of the overall analysis pipeline, which consists of the following steps. First, both the femur bone and the tibia bone are automatically segmented using a multi-atlas strategy.  These bone segmentations are used for an initial rough image alignment and to automatically extract a smaller region around the knee joint within which a refined registration and the segmentation of cartilage is performed. Focusing on this smaller region significantly reduces the computational cost of the approach. The cartilage segmentation itself is based on a spatial prior obtained from multi-atlas registration of labeled joint regions and a data-likelihood terms obtained through local feature-based probabilistic classification. Both the spatial prior and the data-likelihoods are used within a three-label convex segmentation formulation to obtain the segmentation labels for femoral and tibial cartilage as well as background. The three-label segmentation makes use of an anisotropic spatial regularization term to simultaneously suppress noise while respecting the thin cartilage structures.

In the context of atlas-based segmentation, an atlas~\cite{aljabar2009multi} is defined as the pair of an original structural image and the corresponding segmentation. The segmentation in the atlas pair is assumed to be obtained for example by manual segmentation by a domain expert.

Suppose we have N atlases, then in each atlas $\mathcal{A}_i$ $(i=1,2,...,N)$, we have
\begin{itemize}
	\item A Structural Image, $I_i$
	\item A Manual Segmentation of the Femur bone, $S^{FB}_{i}$
	\item A Manual Segmentation of the Tibia bone, $S^{TB}_{i}$
	\item A Manual Segmentation of the Femoral cartilage, $S^{FC}_{i}$
	\item A Manual Segmentation of the Tibial cartilage, $S^{TC}_{i}$
\end{itemize}

The remainder of this section provides brief descriptions for each stage of the segmentation pipeline.
Section~\ref{sec:Preprocessing} gives an overview of the preprocessing required for datasets prior to cartilage segmentation and statistical analysis. Section~\ref{sec:MABoneSeg} explains the multi-atlas-based automatic segmentation of the femur and the tibia. Section~\ref{sec:MACartSeg} explains how femoral and tibial cartilage are segmented using a multi-atlas segmentation strategy guided by the segmentations of femor and tibia. Finally, Section~\ref{sec:LongCartSeg} shows how cartilage segmentations can be refined for longitudinal data (as available for example from the Pfizer Longitudinal Study or the data of the Osteoarthritis Initative) by enforcing temporal regularity.

\subsubsection{Preprocessing} \label{sec:Preprocessing}
Before we can use the data for analysis some preprocessing needs to be performed. We first remove artifacts in images caused by the nonhomogeneity in magnetic fields. Then we rescale image intensities so that values range from 0 to 100. We perform edge preserving smoothing to further remove noise in the images. Finally we downsample the images by a factor of 4 for the first 2 dimensions to make image registration more efficient while not losing too much information. Note that this only affects the registration, segmentation is still performed at the full image resolution.

\subsubsection{Multi-Atlas-Based Bone Segmentation} \label{sec:MABoneSeg}

After the data is pre-processed appropriately for the analysis, we automatically segment knee bones from the images using a multi-atlas strategy.
The labeling cost $c$ of each voxel location $x$ in the query image for each label $l$ in ${FB,BG,TB}$ ("$FB$", "$BG$", and "$TB$" denotes the femur bone, the background, and the tibia bone respectively) are defined by log-likelihoods :
\begin{equation} \label{eq:BoneLabelCost0}
c(\mathbf{x},l)=-\log{\left( P(l|I(\mathbf{x}))\right)}
=-\log{\left( \frac{p(I(\mathbf{x})|l) \cdot P(l)}{p(I(\mathbf{x})) }
\right)} \propto -\log\left( p( I(\mathbf{x})|l ) \cdot P(l) \right)
\end{equation}
We first segment femur bone and tibia bone from the query image by performing the following steps:
\begin{enumerate}
	\item[1)]
	\textbf{Registration}: We compute spatial transformation to register each of the atlases to the query image.
	We first compute affine transformation based on structural images of each atlases and the query such that
	\begin{equation} \label{eq:BoneAffineT}
		T^{affine}_i: I_i \rightarrow I_q.
	\end{equation}
	Then we apply $T^{affine}_i$ computed from~(\ref{eq:BoneAffineT}) to $I_i$, $S^{FB}_i$, and $S^{TB}_i$ $(i = 1,2,...,N)$.
	\begin{equation}
		\begin{split}
T^{affine}_i &\circ I_i\\
T^{affine}_i &\circ S^{FB}_i\\
T^{affine}_i &\circ S^{TB}_i
		\end{split}
	\end{equation}
	This is followed by a B-Spline transformation such that
	\begin{equation} \label{eq:BoneBsplineT}
		T^{bspline}_i: ( T^{affine}_i \circ I_i ) \rightarrow I_q
	\end{equation}
	Just as for the affine transformation, we apply the transformation computed in~(\ref{eq:BoneBsplineT}) to $T^{affine}_i \circ I_i$, $T^{affine}_i \circ S^{FB}_i$, and $T^{affine}_i \circ S^{TB}_i$
	\begin{equation}
		\begin{aligned}
T^{bspline}_i &\circ T^{affine}_i \circ I_i\\
T^{bspline}_i &\circ T^{affine}_i \circ S^{FB}_i\\
T^{bspline}_i &\circ T^{affine}_i \circ S^{TB}_i
		\end{aligned}
	\end{equation}	
	
	\item[2)] 
	\textbf{Label Fusion}: We propagate the registered segmentation masks, i.e., $T^{bspline}_i \circ T^{affine}_i \circ S^{FB}_i$ and $T^{bspline}_i \circ T^{affine}_i \circ S^{TB}_i$ to form the spatial prior of the femur bone and the tibia bone, i.e., $p(FB)$ and $p(TB)$, for the query image $I_q$:
	\begin{equation} \label{eq:BoneLabelFusion}
		\begin{split}
			p(FB) = \frac{1}{N} \sum\limits_{i=1}^N \left( T^{bspline}_i \circ T^{affine}_i \circ S^{FB}_i \right)\\
			p(TB) = \frac{1}{N} \sum\limits_{i=1}^N \left( T^{bspline}_i \circ T^{affine}_i \circ S^{TB}_i \right)
		\end{split}
	\end{equation}
	where $FB$ and $TB$ respectively denote the femur bone and the tibia bone.
	
	Recall from~\ref{sec:Preprocessing} that we have downsampled the image to make registration efficient while not losing too much information.
	After summation and division in~(\ref{eq:BoneLabelFusion}), we upsample $p(FB)$ and $p(TB)$ back to the original dimension.
	\item[3)] 
	\textbf{Segmentation}: Finally, we segment the femur bone and the tibia bone from the query by minimizing the labeling cost of each voxel location for the femur bone and the tibia bone.
	Recall from~(\ref{eq:BoneLabelCost0}) that this cost is expressed in terms of posterior probability: 
	\begin{equation}\label{eq:BoneLabelCost}
		\begin{split}
			c(\mathbf{x},FB) = -\log\left(p\left(FB|I\left(\mathbf{x}\right)\right)\right) \propto -\log\left( p( I(\mathbf{x})|FB ) \cdot p(FB) \right)\\
			c(\mathbf{x},TB) = -\log\left(p\left(TB|I\left(\mathbf{x}\right)\right)\right) \propto -\log\left( p( I(\mathbf{x})|TB ) \cdot p(TB) \right)	
		\end{split}
	\end{equation}
	where $\mathbf{x}$ denotes each voxel location in the image, and $I(\mathbf{x})$ denotes intensity value at $\mathbf{x}$.
	
	The likelihood of the femur bone and the tibia bone, i.e., $p(I(\mathbf{x})|FB)$ and $p(I(\mathbf{x})|TB)$, are computed as the following.
	\begin{equation} \label{eq:BoneLikelihood}
		p(I(\mathbf{x})|FB)=p(I(\mathbf{x})|TB)=\exp(-\beta  I(\mathbf{x}))
	\end{equation}
	where $\beta$ is set to 0.02 and the intensity values $I(x)$ range from 0 to 100 as a result of the preprocessing described in~\ref{sec:Preprocessing}
	
	We use the three-label segmentation described in~\cite{shan2014} to segment the femur bone and the tibia bone by minimizing labeling costs~(\ref{eq:BoneLabelCost}).
	%\item[4)] %\mn{Segmenting} the bones by minimizing the labeling costs from the previous step.
\end{enumerate}

%This approach is more robust \mn{than the average-shape-atlas approach and therefore the preferred method for cartilage segmentation}, because we are using all of the available information. However, \mn{it} is computationally demanding. 
\subsubsection{Multi-Atlas-Based Cartilage Segmentation} \label{sec:MACartSeg}
After segmenting the knee bones from the query (denoted by $S^{FB}$ and $S^{TB}$), we can use these segmentation masks to guide cartilage segmentation.
The labeling cost for the femoral cartilage and the tibial cartilage is expressed in terms of posterior probabilities:
with priors computed from multi-atlas-based registration followed by non-local patch-based label fusion and likelihoods computed from probabilistic classification (we use a Support Vector Machine (SVM)).
The labeling cost $c$ of each voxel $x$ for each label $l$ in ${FC,BG,TC}$ ("$FC$", "$BG$", "$TC$" respectively denote the femoral cartilage, the background, and the tibial cartilage) is,
\begin{equation} \label{eq:CartLabelCost}
	c(x,l) = -\log{ \left( P \left( l | \mathbf{f} \left( \mathbf{x} \right) \right) \right) } 
		   = -\log{ \left( \frac{p\left(\mathbf{f}\left(\mathbf{x}\right)|l\right) \cdot p \left(l\right)}
		   {p\left(\mathbf{f}\left(\mathbf{x}\right)\right)}\right)}
		   \propto -\log{ 
		   \left( p
			   \left(\mathbf{f}
				   \left(\mathbf{x}
				   \right)|l
			   \right) \cdot p 
				   \left(l
				   \right)
			\right)
			}
\end{equation}
We perform the following steps to automatically segment both cartilages from the query.
\begin{enumerate}
	\item[1)] \textbf{Joint Region Extraction}\label{subsec:joint}:
	We automatically extract a smaller region around the joint.
	This region is computed based on the bone segmentation from the previous stage of the pipeline.
	This process makes the further processing more efficient while not affecting the cartilage segmentation performance.
	\item[2)] \textbf{Probabilistic Classification}:\label{subsec:classfn}
	We compute likelihood terms $p(\mathbf{f}(\mathbf{x})|l)$ for the labeling cost $c$ via probabilistic classification.
	For features, we used:
	\begin{itemize}
		\item intensities on three scales
		\item first-order derivatives in three directions on three scales
		\item second-order derivatives in the axial directions on three scales
	\end{itemize}
	where three scales are obtained by convolving with Gaussian kernels of $\sigma$ = 0.3 $mm$, 0.6 $mm$, and 1.0 $mm$.
	All features are normalized to be centered at 0 and have unit standard deviation.
	
	We use an SVM for classification.
	\item[3)] \textbf{Registration}: We use multi-atlas-based registration to propagate segmentation masks for the femoral cartilage and the tibial cartilage.
	We register each atlas $\mathcal{A}_i$ to the query via computing affine transformations:
	\begin{equation}\label{eq:CartAffineT}
		\begin{split}		
			T^{affine}_{i,FC}:S^{FB}_i \rightarrow S^{FB}\\
			T^{affine}_{i,TC}:S^{TB}_i \rightarrow S^{TB}
		\end{split} 
	\end{equation}
	
	We apply $T^{affine}_{i,FC}$ to the femoral cartilage segmentation $S^{FC}_i$ and the structural image $I_i$,
	and $T^{affine}_{i,TC}$to the tibial cartilage segmentation $S^{TC}_i$ and the structural image $I_i$.
	\begin{equation} \label{eq:CartReg}
		\begin{split}	
			\tilde{S}^{FC}_i&=T^{affine}_{i,FC} \circ S^{FC}_i\\
			\tilde{I}^{FC}_i&=T^{affine}_{i,FC} \circ I_i\\
			\tilde{S}^{TC}_i&=T^{affine}_{i,TC} \circ S^{TC}_i\\
			\tilde{I}^{TC}_i&=T^{affine}_{i,TC} \circ I_i
		\end{split}
	\end{equation}
	\item[4)] \textbf{Label Fusion}: 
	After the registration, we form priors for the femoral cartilage (i.e., $p(FC)$) and the tibial cartilage (i.e., $p(TC)$) using the warped label images, i.e., $\tilde{S}^{FC}_i$ and $\tilde{S}^{TC}_i$.
	Note that the non-local patch based strategy in label fusion is robust to local errors introduced by registration.
	
	Let $p^{FC}(x)$ denote $p(FC)$ at the voxel location $x$ and $p^{TC}(x)$ denote $p(TC)$ at the voxel location $x$.
	$p^{FC}(x)$ is computed as:
	\begin{equation} \label{eq:PatchFusion}
		p^{FC}(x)=
		\frac
		{\sum\limits_{i=1}^N\sum\nolimits_{y \in \mathcal{N}(x) } w^{FC}(x,y)\tilde{S}^{FC}_i(y)}
		{\sum\limits_{i=1}^N\sum\nolimits_{y \in \mathcal{N}(x) } w^{FC}(x,y)},\\
	\end{equation}
	\begin{equation} \label{eq:PatchWeight}
		w^{FC}(x,y) = \exp\left(
		\frac
		{\sum\nolimits_{x' \in \mathcal{P}(x) y' \in \mathcal{P}(y) } \left( I(x')-\tilde{I}^{FC}_i(y')\right)^2 }
		{h^{FC}(x)}
		\right),
	\end{equation}
	where $x'$ is a voxel in the patch $\mathcal{P}(x)$ centered at $x$ (similarly $y'$ a voxel in the patch $\mathcal{P}(y)$ centered at $y$) and $h^{FC}(x)$ is defined by
	\begin{equation} \label{eq:PatchH}
		h^{FC}(x)=
		\min_{\substack{
				1 \leq i \leq N \\
				y \in \mathcal{N}(x) 
				}
			}
		\sum\limits_{
			\substack{
			x' \in \mathcal{P}(x)\\
			y' \in \mathcal{P}(y)
			}
		}\left( I(x')-\tilde{I}^{FC}_i(y')\right)^2 + \epsilon
	\end{equation}
	We compute $p^{TC}(x)$ exactly the same way by substituting $FC$ with $TC$ in equations:~(\ref{eq:PatchFusion}),~(\ref{eq:PatchWeight}), and~(\ref{eq:PatchH}).
	
	\item[5)] \textbf{Segmentation}: 
	Finally with spatial priors computed from~(\ref{eq:PatchFusion}) and the likelihood computed via classification,
	we use the three-label segmentation with anisotropic regularization to segment cartilage.
	In order to make use of anisotropic regularization, we compute the normal directions.
\end{enumerate}

\subsubsection{Longitudinal Segmentation} \label{sec:LongCartSeg}

The segmentation result computed so far is assumed to be temporally independent. I.e., each timepoint is segmented independently.
However, if we know the dataset is longitudinal, then we can further enforce temporal consistencies across image data at different time points.
Our longitudinal segmentation also follows a similar framework: registration followed by three-label segmentation.
\begin{enumerate}
	\item 
	\textbf{Registration}: To align longitudinal images of a given subject, we register all of the time points to the first time-point (baseline image).
	We first compute a rigid transformation between the $t-$th time point and the first time point based on the femur bone segmentation.
	\begin{equation} \label{eq:rigidTFemur}
			R^{FB}_t: S^{FB}_t \rightarrow S^{FB}_0
	\end{equation}
	where subscript $t$ denotes $t-$th time point.
	Then we apply the transformations~(\ref{eq:rigidTFemur}) to: the segmentation mask of the femoral cartilage, the local likelihood map of the femoral cartilage, spatial prior map of the femoral cartilage, and the structural image around the joint from \textbf{Joint Region Extraction} stage.
	\begin{equation} \label{eq:movedIndpCart}
		\begin{split}
			R^{FB}_t &\circ S^{FC}_t\\
			R^{FB}_t &\circ p_t\left( \mathbf{f}( \mathbf{x} )|FC \right)\\
			R^{FB}_t &\circ p_t(FC)\\
			R^{FB}_t &\circ I^{joint}_t									
		\end{split}
	\end{equation}	
	Then we compute another rigid transformation based on the transformed femoral cartilage segmentation mask.
	\begin{equation}\label{eq:rigidTFCart}
		R^{FC}_t: \left( R^{FB}_t \circ S^{FC}_t \right) \rightarrow S^{FC}_0
	\end{equation}
	We apply $R^{FC}_t$ to all of the transformed: the segmentation mask of the femoral cartilage, the local likelihood map of the femoral cartilage, the spatial prior map of the femoral cartilage, and the structural image around the joint.
	\begin{equation}\label{eq:movedIndpCart2}		
		\begin{split}
			&\tilde{S}^{FC}_t = R^{FC}_t \circ R^{FB}_t \circ S^{FC}_t\\
			&\tilde{p}_t\left( \mathbf{f}( \mathbf{x} )|FC \right) =R^{FC}_t \circ R^{FB}_t \circ p_t\left( \mathbf{f}( \mathbf{x} )|FC \right)\\
			&\tilde{p}_t(FC) = R^{FC}_t \circ R^{FB}_t \circ p_t(FC)\\
			&\tilde{I}^{joint}_t = R^{FC}_t \circ R^{FB}_t \circ I^{joint}_t								
		\end{split}		
	\end{equation}
	
	We repeat the above procedure to align the tibial segmentation mask, the tibial cartilage likelihood map, the spatial prior for the tibial cartilage, and the structural image around the joint based on the tibia bone segmentation and the tibial cartilage segmentation.
	\item 
	\textbf{Segmentation}:
	Once the different time point are aligned to the baseline, we compute the labeling cost $c$ for $t-$th time point,
	\begin{equation} \label{eq:LongCartLabelCost}
		c(\mathbf{x},l,t) = -\log{ \left( p_t(l|\mathbf{f}(\mathbf{x} ) ) \right) } =
		-\log{ \left( 
		\frac
		{\tilde{p}_t\left(\mathbf{f}\left(\mathbf{x}\right)|l\right) \cdot \tilde{p}_t \left(l\right)}
		{\tilde{p}_t\left(\mathbf{f}\left(\mathbf{x}\right)\right)}
		\right)
		}
		\propto
		-\log{ \left(
		\tilde{p}_t\left(\mathbf{f}\left(\mathbf{x}\right)|l\right) \cdot \tilde{p}_t \left(l\right)
		\right) }
	\end{equation}
	Then we compute the normal directions because we make use of anisotropic regularization for the longitudinal segmentation.
	We longitudinally segment cartilages by minimizing the labeling cost~(\ref{eq:LongCartLabelCost}) with each terms computed from~(\ref{eq:movedIndpCart2}).
\end{enumerate}
\subsection{Cartilage Thickness Computation}
\label{sec:cartilage_thickness_computation}

Once both femoral and tibial cartilages are segmented we use the method described in~\cite{yezzi2003eulerian} to compute thickness of those cartilages. Please refer~\cite{yezzi2003eulerian} for more details on computing tissue thickness. 

\subsection{Statistical Thickness Analysis}
\label{sec:statistical_thickness_analysis}

Once cartilage thicknesses are computed we establish spatial correspondence of the thicknesses by mapping them to a common atlas space.
We use an affine followed by B-Spline transform which are computed based on bone segmentations for each cartilage thickness volume.
Then these transformed thicknesses are projected down to an axial slice by taking the median thickness value along the axial direction (where thicknesses are approximately constant). Then these thickness maps are comparable across time-points and subjects. Note that all segmentations and thicknesses are computed in native image space. Figure~\ref{fig:ThickComp} illustrates how the thickness maps are computed.

\begin{figure} \label{fig:ThickComp}
	\centering
	\includegraphics[width=0.99\textwidth]{images/ThicknessMap.png}
	\caption{Thickness Computation pipeline.
	Extract the joint region and automatically segment cartilages.
	Compute cartilage thickness.
	Register all of the 3D thickness maps to a common atlas space.
	Project 3D thickness maps onto a 2D plane along axial direction.}
\end{figure}

Once the 2D thickness maps are created we first fit a linear mixture model to model differences between normal control subjects and OA subjects.
Then we cluster OA subjects such that subjects in the same cluster have similar patterns in thinning of the cartilages.
Then we fit another linear mixture models to each OA cluster for a refined statistical analysis. See~\cite{shan2014} for more details.
