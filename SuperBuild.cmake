include( ExternalProject )

set( base "${CMAKE_BINARY_DIR}" )
set_property( DIRECTORY PROPERTY EP_BASE ${base} )

set( shared ON )
set( testing ON )
set( build_type "Debug" )
if( CMAKE_BUILD_TYPE )
  set( build_type "${CMAKE_BUILD_TYPE}" )
endif()

set( KSRT_DEPENDS "" )

set( gen "${CMAKE_GENERATOR}" )

set ( COMMON_PROJECT_COMPILER_FLAGS
  -DCOMPILE_SKI10_EVALUATION_CODE:BOOL=${COMPILE_SKI10_EVALUATION_CODE}
  -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
  -DCMAKE_CXX_COMPILER:PATH=${CMAKE_CXX_COMPILER}
  -DCMAKE_CXX_FLAGS_RELEASE:STRING=${CMAKE_CXX_FLAGS_RELEASE}
  -DCMAKE_CXX_FLAGS_DEBUG:STRING=${CMAKE_CXX_FLAGS_DEBUG}
  -DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
  -DCMAKE_C_COMPILER:PATH=${CMAKE_C_COMPILER}
  -DCMAKE_C_FLAGS_RELEASE:STRING=${CMAKE_C_FLAGS_RELEASE}
  -DCMAKE_C_FLAGS_DEBUG:STRING=${CMAKE_C_FLAGS_DEBUG}
  -DCMAKE_C_FLAGS:STRING=${CMAKE_C_FLAGS}
  -DCMAKE_SHARED_LINKER_FLAGS:STRING=${CMAKE_SHARED_LINKER_FLAGS}
  -DCMAKE_EXE_LINKER_FLAGS:STRING=${CMAKE_EXE_LINKER_FLAGS}
  -DCMAKE_MODULE_LINKER_FLAGS:STRING=${CMAKE_MODULE_LINKER_FLAGS}
  -DCMAKE_GENERATOR:STRING=${CMAKE_GENERATOR}
  -DCMAKE_EXTRA_GENERATOR:STRING=${CMAKE_EXTRA_GENERATOR}
  -DCMAKE_VERBOSE_MAKEFILE:BOOL=${CMAKE_VERBOSE_MAKEFILE}
)

if ( COMPILE_SLICER_FROM_SOURCE )
  set( proj Slicer )
  ExternalProject_Add( ${proj}
    SVN_REPOSITORY "http://svn.slicer.org/Slicer4/trunk"
    SVN_REVISION -r "23774"
    SOURCE_DIR "${CMAKE_BINARY_DIR}/Slicer"
    BINARY_DIR Slicer-Build
    CMAKE_GENERATOR ${gen}
    CMAKE_ARGS
      ${COMMON_PROJECT_COMPILER_FLAGS}
    INSTALL_COMMAND ""
)
endif ( COMPILE_SLICER_FROM_SOURCE )

##
## Check if sytem ITK or superbuild ITK
##
if( NOT USE_SYSTEM_ITK )

  if( NOT GIT_EXECUTABLE )
    find_package( Git REQUIRED )
  endif( NOT GIT_EXECUTABLE )

  option( GIT_PROTOCOL_HTTP
    "Use HTTP for git access (useful if behind a firewall)" OFF )
  if( GIT_PROTOCOL_HTTP )
    set( GIT_PROTOCOL "http" CACHE STRING "Git protocol for file transfer" )
  else( GIT_PROTOCOL_HTTP )
    set( GIT_PROTOCOL "git" CACHE STRING "Git protocol for file transfer" )
  endif( GIT_PROTOCOL_HTTP )
  mark_as_advanced( GIT_PROTOCOL )

  ##
  ## ITK
  ##
  set( proj ITK )
  ExternalProject_Add( ${proj}
    GIT_REPOSITORY "${GIT_PROTOCOL}://itk.org/ITK.git"
    GIT_TAG "v4.8.0"
    SOURCE_DIR "${CMAKE_BINARY_DIR}/ITK"
    BINARY_DIR ITK-Build
    CMAKE_GENERATOR ${gen}
    CMAKE_ARGS
      ${COMMON_PROJECT_COMPILER_FLAGS}
      -DBUILD_SHARED_LIBS:BOOL=${shared}
      -DBUILD_EXAMPLES:BOOL=OFF
      -DBUILD_TESTING:BOOL=OFF
      -DUSE_FFTWF:BOOL=OFF
      -DUSE_FFTWD:BOOL=OFF
      -DUSE_SYSTEM_FFTW:BOOL=OFF
      -DITKGroup_IO:BOOL=ON
      -DITKGroup_Filtering:BOOL=ON
      -DITKGroup_Nonunit:BOOL=ON
      -DITKGroup_Registration:BOOL=ON
      -DITKGroup_Segmentation:BOOL=ON
      -DITKGroup_Numerics:BOOL=ON
      -DITKGroup_ThirdParty:BOOL=ON
      -DITKGroup_Bridge:BOOL=ON
      -DITKGroup_Compatibility:BOOL=ON
      -DITK_BUILD_ALL_MODULES:BOOL=OFF
      -DModule_ITKTestKernel:BOOL=ON
      -DITKV3_COMPATIBILITY:BOOL=ON
      -DITK_LEGACY_REMOVE:BOOL=OFF
      -DITK_USE_REVIEW:BOOL=ON
    INSTALL_COMMAND ""
    )
  set( ITK_DIR "${base}/ITK-Build" )
  set( KSRT_DEPENDS ${KSRT_DEPENDS} "ITK" )
endif( NOT USE_SYSTEM_ITK )

##
## Compile external project ANN
##
set( COMPILE_ANN FALSE )
set( ANN_MAKE_COMMAND "" )

if( ${UNIX} )
  # then section.
  set( COMPILE_ANN TRUE )
#    message( WARNING "ANN is being compiled. This is GPL. Remove dependency to obtain Apache 2.0 compatibility." )
  if ( ${CMAKE_SYSTEM_NAME} MATCHES "Darwin" )
    set( ANN_MAKE_COMMAND make macosx-g++ )
  else ( ${CMAKE_SYSTEM_NAME} MATCHES "Darwin" )
    set( ANN_MAKE_COMMAND make linux-g++ )
  endif( ${CMAKE_SYSTEM_NAME} MATCHES "Darwin" )
elseif( ${WIN32} )
  # elseif section.
  message( WARNING "Automatic compilation of ANN is not supported on windows yet. Compile it manually." )
else( ${UNIX} )
  # else section.
  message( WARNING "Unknown operating system. Cannot compile ANN automatically yet." )
endif( ${UNIX} )


set( proj ANN )
ExternalProject_Add( ${proj}
  DOWNLOAD_COMMAND ""
  SOURCE_DIR "${CMAKE_SOURCE_DIR}/lib/ann_1.1.2"
  CMAKE_GENERATOR ${gen}
  CMAKE_ARGS
      ${COMMON_PROJECT_COMPILER_FLAGS}
    -DBUILD_TESTING:BOOL=${testing}
    -DBUILD_DOCUMENTATION:BOOL=${BUILD_DOCUMENTATION}
    -DKSRT_USE_SUPERBUILD:BOOL=FALSE
  BUILD_IN_SOURCE 1
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ${ANN_MAKE_COMMAND}
  INSTALL_COMMAND ""
  DEPENDS
    ${KSRT_DEPENDS}
  )

set( proj LIBSVM )
set( libSVM_MAKE_COMMAND make all )
ExternalProject_Add( ${proj}
  DOWNLOAD_COMMAND ""
  SOURCE_DIR "${CMAKE_SOURCE_DIR}/lib/libsvm-3.18"
  CMAKE_GENERATOR ${gen}
  CMAKE_ARGS
      ${COMMON_PROJECT_COMPILER_FLAGS}
    -DBUILD_TESTING:BOOL=${testing}
    -DBUILD_DOCUMENTATION:BOOL=${BUILD_DOCUMENTATION}
    -DKSRT_USE_SUPERBUILD:BOOL=FALSE
  BUILD_IN_SOURCE 1
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ${libSVM_MAKE_COMMAND}
  INSTALL_COMMAND ""
  DEPENDS
    ${KSRT_DEPENDS}
  )

##
## ImageViewer Build
##

if ( COMPILE_IMAGEVIEWER_FROM_SOURCE )
  set( proj ImageViewer )
  ExternalProject_Add( ${proj}
    GIT_REPOSITORY "${GIT_PROTOCOL}://github.com/TubeTK/ImageViewer.git"
    SOURCE_DIR "${CMAKE_BINARY_DIR}/ImageViewer"
    BINARY_DIR ImageViewer-Build
    CMAKE_GENERATOR ${gen}
    CMAKE_ARGS
      ${COMMON_PROJECT_COMPILER_FLAGS}
    INSTALL_COMMAND ""
    DEPENDS
      ${KSRT_DEPENDS}
)
endif ( COMPILE_IMAGEVIEWER_FROM_SOURCE )

##
## KSRT - Normal Build
##
set( proj KSRT )
ExternalProject_Add( ${proj}
  DOWNLOAD_COMMAND ""
  SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}"
  BINARY_DIR KSRT-Build
  CMAKE_GENERATOR ${gen}
  CMAKE_ARGS
      ${COMMON_PROJECT_COMPILER_FLAGS}
    -DBUILD_TESTING:BOOL=${testing}
    -DBUILD_DOCUMENTATION:BOOL=${BUILD_DOCUMENTATION}
    -DKSRT_USE_SUPERBUILD:BOOL=FALSE
    -DITK_DIR:PATH=${ITK_DIR}
  INSTALL_COMMAND ""
  DEPENDS
    ${KSRT_DEPENDS}
  )

#    -DFFTWF_LIB:FILEPATH=${CMAKE_BINARY_DIR}/ITK-Build/fftw/lib/libfftw3f.a
#    -DFFTW_LIB:FILEPATH=${CMAKE_BINARY_DIR}/ITK-Build/fftw/lib/libfftw3.a
#    -DFFTW_PATH:PATH=${CMAKE_BINARY_DIR}/ITK-Build/fftw/include
