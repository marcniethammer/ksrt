# Knee Segmentation and Registration Toolkit (KSRT) #

KSRT allows for the automatic segmentation and statistical analysis of femoral and tibial cartilage from magnetic resonance images of the knee. The software has been tested on the full Pfizer Longitudinal Dataset as well as the SKI10 dataset.

### What is this repository for? ###

This repository contains the source code for all the analysis algorithms required to perform cartilage segmentation and registration. 

### How do I get set up? ###

See the [wiki page](https://bitbucket.org/marcniethammer/ksrt/wiki/Home) for detailed installation instructions as well as for a tutorial on how to use the software.

A high-level overview of the algorithms used can be found in [Liang Shan's PhD defense slides](http://wwwx.cs.unc.edu/~mn/KSRT/KSRT_liang_shan_PhD_defense.pdf).