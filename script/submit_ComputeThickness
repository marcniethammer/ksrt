# Special comment lines to the grid engine start
# with #$ and they should contain the command-line arguments for the
# qsub command.  See 'man qsub' for more options.
#
#$ -S /bin/bash
#$ -t 1-5
#$ -tc 100
#$ -pe smp 16
#$ -o $HOME/ThicknessComputation_$JOB_ID_$TASK_ID
#$ -j y
#$ -cwd
#$ -l background
#$ -P CS-bg
# The above arguments mean:
#       -S /bin/bash : Run this set of jobs using /bin/bash
#       -t 1-10 : Run 100 separate instances with the SGE_TASK_ID set from 1 through 100
#       -o : Put the output files in ~/tmp, named by job name and ID, and task ID
#       -j y : Join the error and output files for each job

# The following are among the useful environment variables set when each
# job is run:
#       $SGE_TASK_ID : Which job I am from the above range
#       $SGE_LAST_TASK : Last number from the above range
#               (Equal to the number of tasks if range starts with 1
#                and has a stride of 1.)

# Go to the location you want each job to run.
# The executable to run should be in your path or named explicitly.
#cd ~

source ~/.bash_profile

SEEDFILE="$DataDIR/list.txt"
line=$(cat $SEEDFILE | head -n $SGE_TASK_ID | tail -n 1)
echo "This is job $SGE_TASK_ID"

AppDIR=$ksrtAppDIR

SITE=$(echo $line|cut -d' ' -f1)
ID=$(echo $line|cut -d' ' -f2)
VISIT=$(echo $line|cut -d' ' -f3)
## this is chosen by us
ChosenAtlas="$DataDIR"/site1002/10021014/0

## creating the output folder for 3D thickness map
ThickOutDIR="$DataDIR/thick"
if [ ! -d "$ThickOutDIR" ]
then
    echo "Creating output folder $ThickOutDIR"
    mkdir "$ThickOutDIR/"
fi

## thickness from longitudinal segmentation of femoral cartilage
"$AppDIR"/ksrtComputeCartilageThickness "$DataDIR"/"$SITE"/"$ID"/joint4D/segmentation/"$VISIT"_seg_fem_long.nhdr "$DataDIR"/"$SITE"/"$ID"/joint4D/segmentation/"$VISIT"_thick_fem_long.nhdr
## copying the thickness map to different location 
"$AppDIR"/ksrtCopy "$DataDIR"/"$SITE"/"$ID"/joint4D/segmentation/"$VISIT"_thick_fem_long.nhdr "$ThickOutDIR"/fem_"$ID"_"$VISIT".nhdr

## thickness from longitudinal segmentation of tibial cartilage
"$AppDIR"/ksrtComputeCartilageThickness "$DataDIR"/"$SITE"/"$ID"/joint4D/segmentation/"$VISIT"_seg_tib_long.nhdr "$DataDIR"/"$SITE"/"$ID"/joint4D/segmentation/"$VISIT"_thick_tib_long.nhdr
## copying the thickness map to different location
"$AppDIR"/ksrtCopy "$DataDIR"/"$SITE"/"$ID"/joint4D/segmentation/"$VISIT"_thick_tib_long.nhdr "$ThickOutDIR"/tib_"$ID"_"$VISIT".nhdr

## creating the registration output folder
ThickTransformOutDIR="$ThickOutDIR/transform"
if [ ! -d "$ThickTransformOutDIR" ]
then
    echo "Creating output folder $ThickOutDIR"
    mkdir "$ThickTransformOutDIR/"
fi

## compute deformation to common chosen atlas space

"$AppDIR"/ksrtComputeAffineRegistration "$ChosenAtlas"/joint/femur.nhdr "$DataDIR"/"$SITE"/"$ID"/"$VISIT"/joint/seg_femur.nhdr "$ThickTransformOutDIR"/affine_"$ID"_"$VISIT"_femur.nhdr "$ThickTransformOutDIR"/affine_"$ID"_"$VISIT"_femur.tfm --metric SSD
"$AppDIR"/ksrtComputeBSplineRegistration "$ChosenAtlas"/joint/femur.nhdr "$ThickTransformOutDIR"/affine_"$ID"_"$VISIT"_femur.nhdr "$ThickTransformOutDIR"/bspline_"$ID"_"$VISIT"_femur.nhdr "$ThickTransformOutDIR"/bspline_"$ID"_"$VISIT"_femur.tfm
"$AppDIR"/ksrtComputeAffineRegistration "$ChosenAtlas"/joint/tibia.nhdr "$DataDIR"/"$SITE"/"$ID"/"$VISIT"/joint/seg_tibia.nhdr "$ThickTransformOutDIR"/affine_"$ID"_"$VISIT"_tibia.nhdr "$ThickTransformOutDIR"/affine_"$ID"_"$VISIT"_tibia.tfm --metric SSD
"$AppDIR"/ksrtComputeBSplineRegistration "$ChosenAtlas"/joint/tibia.nhdr "$ThickTransformOutDIR"/affine_"$ID"_"$VISIT"_tibia.nhdr "$ThickTransformOutDIR"/bspline_"$ID"_"$VISIT"_tibia.nhdr "$ThickTransformOutDIR"/bspline_"$ID"_"$VISIT"_tibia.tfm

## apply deformation to thickness from longitudinal segmentation

MovedThickOutDIR="$ThickOutDIR/moved"
if [ ! -d "$ThickOutDIR/moved" ]
then
    echo "Creating output folder $MovedThickOutDIR"
    mkdir "$MovedThickOutDIR/"
fi

"$AppDIR"/ksrtApplyAffineTransform "$ThickTransformOutDIR"/affine_"$ID"_0_femur.tfm "$ThickOutDIR"/fem_"$ID"_"$VISIT".nhdr "$ChosenAtlas"/joint/fem.nhdr "$MovedThickOutDIR"/fem_"$ID"_"$VISIT".nhdr
"$AppDIR"/ksrtApplyBSplineTransform "$ThickTransformOutDIR"/bspline_"$ID"_0_femur.tfm "$MovedThickOutDIR"/fem_"$ID"_"$VISIT".nhdr "$ChosenAtlas"/joint/fem.nhdr "$MovedThickOutDIR"/fem_"$ID"_"$VISIT".nhdr
"$AppDIR"/ksrtApplyAffineTransform "$ThickTransformOutDIR"/affine_"$ID"_0_tibia.tfm "$ThickOutDIR"/tib_"$ID"_"$VISIT".nhdr "$ChosenAtlas"/joint/tib.nhdr "$MovedThickOutDIR"/tib_"$ID"_"$VISIT".nhdr
"$AppDIR"/ksrtApplyBSplineTransform "$ThickTransformOutDIR"/bspline_"$ID"_0_tibia.tfm "$MovedThickOutDIR"/tib_"$ID"_"$VISIT".nhdr "$ChosenAtlas"/joint/tib.nhdr "$MovedThickOutDIR"/tib_"$ID"_"$VISIT".nhdr

MovedThick2DOutDIR="$ThickOutDIR/moved_2d"
if [ ! -d "$ThickOutDIR/moved_2d" ]
then
    echo "Creating output folder $MovedThick2DOutDIR"
    mkdir "$MovedThick2DOutDIR/"
fi

## create 2D thickness map

"$AppDIR"/ksrtCompressCartilageThickness "$MovedThickOutDIR"/fem_"$ID"_"$VISIT".nhdr "$MovedThick2DOutDIR"/fem_"$ID"_"$VISIT".nhdr -c 0
"$AppDIR"/ksrtCompressCartilageThickness "$MovedThickOutDIR"/tib_"$ID"_"$VISIT".nhdr "$MovedThick2DOutDIR"/tib_"$ID"_"$VISIT".nhdr -c 0

MovedThick2DHDROutDIR="$ThickOutDIR/moved_2d_hdr"
if [ ! -d "$ThickOutDIR/moved_2d_hdr" ]
then
    echo "Creating output folder $MovedThick2DHDROutDIR"
    mkdir "$MovedThick2DHDROutDIR/"
fi

## create hdr format
"$AppDIR"/ksrtCopy "$MovedThick2DOutDIR"/fem_"$ID"_"$VISIT".nhdr "$MovedThick2DHDROutDIR"/fem_"$ID"_"$VISIT".hdr
"$AppDIR"/ksrtCopy "$MovedThick2DOutDIR"/tib_"$ID"_"$VISIT".nhdr "$MovedThick2DHDROutDIR"/tib_"$ID"_"$VISIT".hdr
