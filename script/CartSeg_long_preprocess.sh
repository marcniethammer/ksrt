#!/bin/bash
source ~/.bash_profile
FILE="$DataDIR/list.txt"
if [ -f "$DataDIR/list_subject.txt" ]
then
    echo "removing old list text file"
    rm "$DataDIR/list_subject.txt"
fi

while read line
do
    SITE=$(echo $line|cut -d' ' -f1)
    ID=$(echo $line|cut -d' ' -f2)
    VISIT=$(echo $line|cut -d' ' -f3)
    QueryDIR="$DataDIR/$SITE/$ID/$VISIT"

    echo $QueryDIR
    if [ $VISIT -eq 0 ]
    then
        if [ ! -d "$DataDIR/$SITE/$ID/joint4D" ]
        then
            mkdir "$DataDIR/$SITE/$ID/joint4D"
        fi
        if [ ! -d "$DataDIR/$SITE/$ID/joint4D/segmentation" ]
        then
            mkdir "$DataDIR/$SITE/$ID/joint4D/segmentation"
        fi        
        if [ ! -d "$DataDIR/$SITE/$ID/joint4D/transform" ]
        then
            mkdir "$DataDIR/$SITE/$ID/joint4D/transform"
        fi
        if [ -f "$DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt" ]
        then
            echo "Removing old in_aniso.txt"
            rm "$DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt"
        fi
        if [ -f "$DataDIR/$SITE/$ID/joint4D/segmentation/out_aniso.txt" ]
        then
            echo "Removing old out_aniso.txt"
            rm "$DataDIR/$SITE/$ID/joint4D/segmentation/out_aniso.txt"
        fi
        echo "$SITE $ID" >> "$DataDIR/list_subject.txt"
    fi
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_fem_cost.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_bg_cost.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_tib_cost.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_nx.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_ny.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_nz.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt
    echo "$VISIT" >> $DataDIR/$SITE/$ID/joint4D/segmentation/in_aniso.txt

    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_seg_fem_long.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/out_aniso.txt
    echo -n "$DataDIR"/"$SITE"/"$ID""/joint4D/segmentation/""$VISIT""_seg_tib_long.nhdr " >> $DataDIR/$SITE/$ID/joint4D/segmentation/out_aniso.txt
    echo "$VISIT" >> $DataDIR/$SITE/$ID/joint4D/segmentation/out_aniso.txt
done < $FILE
