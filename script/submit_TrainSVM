# Special comment lines to the grid engine start
# with #$ and they should contain the command-line arguments for the
# qsub command.  See 'man qsub' for more options.
#
#$ -S /bin/bash
#$ -t 1
#$ -l gpus=1
#$ -tc 100
#$ -pe smp 1
#$ -o $HOME/SVM_Train_$JOB_NAME.$JOB_ID.$TASK_ID
#$ -j y
#$ -cwd
# The above arguments mean:
#       -S /bin/bash : Run this set of jobs using /bin/bash
#       -t 1-1 : Run single instance with the SGE_TASK_ID set to 1
#       -tc 1 : Run only at most 4 jobs at a time.  ** Use -tc 100 if you submit > 100
#       -o : Put the output files in ~/tmp, named by job name and ID, and task ID
#       -j y : Join the error and output files for each job
#       -cwd : Run the job in the Current Working Directory (where the script is)
# The following are among the useful environment variables set when each
# job is run:
#       $SGE_TASK_ID : Which job I am from the above range
#       $SGE_TASK_LAST : Last number from the above range
#               (Equal to the number of tasks if range starts with 1
#                and has a stride of 1.)
# This will be run once on each of the compute nodes selected, with the variable "$SGE_TASK_ID" set
# to the correct instance.
echo "This is job $SGE_TASK_ID on training SVM classifier"

source ~/.bash_profile

AppDIR=$ksrtAppDIR
FILE="$DataDIR"/list_atlas.txt
ImgMathDIR=$ksrtAppDIR
svmDIR="$LibDIR/libsvm-3.18"
# creating SVM output directory
if [ ! -d "$DataDIR/SVM" ]
then
    echo "Creating SVM output directory"
    mkdir "$DataDIR/SVM"
fi
svmOutDIR="$DataDIR/SVM"
# removing old training files
if [ -f "$svmOutDIR/train_small" ]
then
    echo "Removing old training file: train_small and train_small.model"
    rm "$svmOutDIR/train_small"
    rm "$svmOutDIR/train_small.model"
fi

if [ -f "$svmOutDIR/train_1" ]
then
    echo "Removing old training file: train_1 and train_1_small"
    rm "$svmOutDIR/train_1"
    rm "$svmOutDIR/train_1_small"
fi

if [ -f "$svmOutDIR/train_2" ]
then
    echo "Removing old training file: train_2 and train_2_small"
    rm "$svmOutDIR/train_2"
    rm "$svmOutDIR/train_2_small"
fi

if [ -f "$svmOutDIR/train_3" ]
then
    echo "Removing old training file: train_3 and train_3_small"
    rm "$svmOutDIR/train_3"
    rm "$svmOutDIR/train_3_small"
fi

n=0
while read line
do
    SITE=$(echo $line|cut -d' ' -f1)
    ID=$(echo $line|cut -d' ' -f2)
    VISIT=$(echo $line|cut -d' ' -f3)
    Atlas="$SITE/$ID/$VISIT"

    n=`expr $n + 1`
    "$AppDIR"/N4ITKBiasFieldCorrection --inputimage "$DataDIR"/"$Atlas"/cor_t1.nhdr --outputimage "$DataDIR"/"$Atlas"/cor_t1_correct.nhdr --outputbiasfield "$DataDIR"/"$Atlas"/bias.nhdr
    "$AppDIR"/ksrtScaleIntensities "$DataDIR"/"$Atlas"/cor_t1_correct.nhdr "$DataDIR"/"$Atlas"/cor_t1_correct_scale.nhdr -m 100

#    "$ImgMathDIR"/ImageMath "$DataDIR"/"$Atlas"/cor_t1_correct_scale.nhdr -smooth -curveEvol -iter 20 -outfile "$DataDIR"/"$Atlas"/cor_t1_correct_scale_smooth.nhdr
     $AppDIR/CurvatureAnisotropicDiffusion --timeStep 0.01 --iterations 50 "$DataDIR"/"$Atlas"/cor_t1_correct_scale.nhdr "$DataDIR"/"$Atlas"/cor_t1_correct_scale_smooth.nhdr

    "$AppDIR"/ksrtResample "$DataDIR"/"$Atlas"/cor_t1_correct_scale_smooth.nhdr -r downsamplingFactor -v 4,4,1 "$DataDIR"/"$Atlas"/cor_t1_correct_scale_smooth_small.nhdr
    "$AppDIR"/ksrtResample "$DataDIR"/"$Atlas"/femur.nhdr -r downsamplingFactor -v 4,4,1 "$DataDIR"/"$Atlas"/femur_small.nhdr
    "$AppDIR"/ksrtResample "$DataDIR"/"$Atlas"/tibia.nhdr -r downsamplingFactor -v 4,4,1 "$DataDIR"/"$Atlas"/tibia_small.nhdr

    if [ ! -d "$DataDIR/$Atlas/joint" ]
    then
        echo "Creating Joint Region Directory"
        mkdir "$DataDIR/$Atlas/joint"
    fi
    echo "Extracting Joint Region for MRI"
    "$AppDIR"/ksrtExtractJointRegion "$DataDIR"/"$Atlas"/cor_t1_correct_scale.nhdr "$DataDIR"/"$Atlas"/femur.nhdr "$DataDIR"/"$Atlas"/tibia.nhdr "$DataDIR"/"$Atlas"/joint/cor_t1_correct_scale.nhdr --size 256,128,80
    echo "Extracting Joint Region for femur segmentation"
    "$AppDIR"/ksrtExtractJointRegion "$DataDIR"/"$Atlas"/femur.nhdr "$DataDIR"/"$Atlas"/femur.nhdr "$DataDIR"/"$Atlas"/tibia.nhdr "$DataDIR"/"$Atlas"/joint/femur.nhdr --size 256,128,80
    echo "Extracting Joint Region for tibia segmentation"
    "$AppDIR"/ksrtExtractJointRegion "$DataDIR"/"$Atlas"/tibia.nhdr "$DataDIR"/"$Atlas"/femur.nhdr "$DataDIR"/"$Atlas"/tibia.nhdr "$DataDIR"/"$Atlas"/joint/tibia.nhdr --size 256,128,80
    echo "Extracting Joint Region for femoral segmentation"
    "$AppDIR"/ksrtExtractJointRegion "$DataDIR"/"$Atlas"/fem.nhdr "$DataDIR"/"$Atlas"/femur.nhdr "$DataDIR"/"$Atlas"/tibia.nhdr "$DataDIR"/"$Atlas"/joint/fem.nhdr --size 256,128,80
    echo "Extracting Joint Region for tibial segmentation"
    "$AppDIR"/ksrtExtractJointRegion "$DataDIR"/"$Atlas"/tib.nhdr "$DataDIR"/"$Atlas"/femur.nhdr "$DataDIR"/"$Atlas"/tibia.nhdr "$DataDIR"/"$Atlas"/joint/tib.nhdr --size 256,128,80

    echo "Rescaling Intensity ranges"
    "$ImgMathDIR"/ImageMath "$DataDIR"/"$Atlas"/joint/cor_t1_correct_scale.nhdr -constOper 2,300 -outfile "$DataDIR"/"$Atlas"/joint/cor_t1_correct_scale_30000.nhdr

    echo "Computing Training Label Images"
    "$AppDIR"/ksrtComputeTrainingLabelImage "$DataDIR"/"$Atlas"/joint/fem.nhdr "$DataDIR"/"$Atlas"/joint/tib.nhdr "$DataDIR"/"$Atlas"/joint/trainLabel.nhdr -r 4 
    echo "Extracting Training Feature"
    "$AppDIR"/ksrtExtractTrainingFeaturesGroups_SVM "$DataDIR"/"$Atlas"/joint/cor_t1_correct_scale_30000.nhdr "$DataDIR"/"$Atlas"/joint/trainLabel.nhdr -s 0.3125,0.625,1.0 "$svmOutDIR"/train_1 "$svmOutDIR"/train_2 "$svmOutDIR"/train_3
done < $FILE
echo "Selecting a subset of features to be used in training"
python "$svmDIR"/tools/subset.py "$svmOutDIR"/train_1 66667 "$svmOutDIR"/train_1_small
python "$svmDIR"/tools/subset.py "$svmOutDIR"/train_2 66667 "$svmOutDIR"/train_2_small
python "$svmDIR"/tools/subset.py "$svmOutDIR"/train_3 66667 "$svmOutDIR"/train_3_small

echo "Concatenating all of smaller subset of features together"
cat "$svmOutDIR"/train_3_small >> "$svmOutDIR"/train_small
cat "$svmOutDIR"/train_2_small >> "$svmOutDIR"/train_small
cat "$svmOutDIR"/train_1_small >> "$svmOutDIR"/train_small

echo "Training classifier"
"$svmDIR"/svm-train -c 4 -e 0.1 -b 1 -m 12000 -h 0 "$svmOutDIR"/train_small "$svmOutDIR"/train_small.model

