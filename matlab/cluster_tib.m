close all
clear all
addpath ../

load('mat/mask_tib.mat', 'mask_tib')
load('mat/tib.mat', 'tib')
% load('mat/lme_cell_tib.mat', 'lme_cell_tib')

% list = tdfread('/data/list_new.txt', ' ');
list = tdfread( 'list_new.txt', ' ' );
N = 706;
N_nc = 368;
N_subject = 155;
N_nc_subject = 81;
sz = [256, 80];

unique_subject = unique(list.subject);
map = containers.Map(unique_subject, 1:size(unique_subject, 1));
unique_oa_subject = unique(list.subject(list.klg>0));
unique_nc_subject = unique(list.subject(list.klg==0));

f0 = zeros(sz); % NC intercept (fixed effect)
f1 = zeros(sz); % NC slope (fixed effect)
f2 = zeros(sz); % OA-NC intercept (fixed effect)
f3 = zeros(sz); % OA-NC slope (fixed effect)

se0 = zeros(sz);
se1 = zeros(sz);
se2 = zeros(sz);
se3 = zeros(sz);

p_f0 = zeros(sz); % pvalue of f0 not being zero
p_f1 = zeros(sz); % pvalue of f1 not being zero
p_f2 = zeros(sz); % pvalue of f2 not being zero
p_f3 = zeros(sz); % pvalue of f3 not being zero

r0 = zeros(sz(1), sz(2), N_subject); % NC intercept (random effects)
r1 = zeros(sz(1), sz(2), N_subject); % NC slope (random effects)
r2 = zeros(sz(1), sz(2), N_subject); % OA intercept (random effects)
r3 = zeros(sz(1), sz(2), N_subject); % OA slope (random effects)

r0n = zeros(sz(1), sz(2), N_subject); % normalized r0
r1n = zeros(sz(1), sz(2), N_subject); % normalized r1
r2n = zeros(sz(1), sz(2), N_subject); % normalized r2
r3n = zeros(sz(1), sz(2), N_subject); % normalized r3

loc = 1;
for i = 1:sz(1)
    for j = 1:sz(2)
        if mask_tib(i,j) > 0
            fprintf(' i= %d, j = %d\n', i, j)
            y = squeeze(tib(i,j,:));
            missing = y==0;
            tmp_subject = list.subject;
            tmp_subject(missing) = [];
            
            r0(i,j,:) = NaN;
            r1(i,j,:) = NaN;
            r2(i,j,:) = NaN;
            r3(i,j,:) = NaN;
            
            lme = lme_cell_tib{loc};
            loc = loc + 1;
            f = fixedEffects(lme); % get fixed effects
            f0(i,j) = f(1); 
            f1(i,j) = f(2);
            f2(i,j) = f(3);
            f3(i,j) = f(4);
            
            se0(i,j) = lme.Coefficients(1,3).SE;
            se1(i,j) = lme.Coefficients(2,3).SE;
            se2(i,j) = lme.Coefficients(3,3).SE;
            se3(i,j) = lme.Coefficients(4,3).SE;
            
            p_f0(i,j) = lme.Coefficients.pValue(1);
            p_f1(i,j) = lme.Coefficients.pValue(2);
            p_f2(i,j) = lme.Coefficients.pValue(3);
            p_f3(i,j) = lme.Coefficients.pValue(4);
            
            r = randomEffects(lme);
            idx_tib = 1;
            uni_tmp_subject = unique(tmp_subject);
            [psi,mse,stats] = covarianceParameters(lme);
            std0 = stats{1}.Estimate(1);
            std1 = stats{1}.Estimate(5);
            std2 = stats{1}.Estimate(8);
            std3 = stats{1}.Estimate(10);
            for k = 1:size(uni_tmp_subject,1)
                r0(i,j,map(uni_tmp_subject(k))) = r(idx_tib);
                r1(i,j,map(uni_tmp_subject(k))) = r(idx_tib+1);
                r2(i,j,map(uni_tmp_subject(k))) = r(idx_tib+2);
                r3(i,j,map(uni_tmp_subject(k))) = r(idx_tib+3);
                r0n(i,j,map(uni_tmp_subject(k))) = r(idx_tib) / std0;
                r1n(i,j,map(uni_tmp_subject(k))) = r(idx_tib+1) / std1;
                r2n(i,j,map(uni_tmp_subject(k))) = r(idx_tib+2) / std2;
                r3n(i,j,map(uni_tmp_subject(k))) = r(idx_tib+3) / std3;
                idx_tib = idx_tib + 4;                
            end
        end
    end
end

save('mat/all_tib_pf0_raw.mat', 'p_f0')
save('mat/all_tib_pf1_raw.mat', 'p_f1')
save('mat/all_tib_pf2_raw.mat', 'p_f2')
save('mat/all_tib_pf3_raw.mat', 'p_f3')

[p_f0_thr, p_f0_cor, p_f0_adj] = fdr(p_f0(mask_tib>0));
[p_f1_thr, p_f1_cor, p_f1_adj] = fdr(p_f1(mask_tib>0));
[p_f2_thr, p_f2_cor, p_f2_adj] = fdr(p_f2(mask_tib>0));
[p_f3_thr, p_f3_cor, p_f3_adj] = fdr(p_f3(mask_tib>0));

p_f0(mask_tib>0) = -log10(p_f0_adj);
p_f1(mask_tib>0) = -log10(p_f1_adj);
p_f2(mask_tib>0) = -log10(p_f2_adj);
p_f3(mask_tib>0) = -log10(p_f3_adj);

figure
subplot(2,4,1), imagesc(f0), colorbar, title('f0')
subplot(2,4,2), imagesc(f1), colorbar, title('f1')
subplot(2,4,3), imagesc(f2), colorbar, title('f2')
subplot(2,4,4), imagesc(f3), colorbar, title('f3')
subplot(2,4,5), imagesc(p_f0), colorbar, title('pf0')
subplot(2,4,6), imagesc(p_f1), colorbar, title('pf1')
subplot(2,4,7), imagesc(p_f2), colorbar, title('pf2')
subplot(2,4,8), imagesc(p_f3), colorbar, title('pf3')

save('mat/all_tib_f0.mat', 'f0')
save('mat/all_tib_f1.mat', 'f1')
save('mat/all_tib_f2.mat', 'f2')
save('mat/all_tib_f3.mat', 'f3')

save('mat/all_tib_se0.mat', 'se0')
save('mat/all_tib_se1.mat', 'se1')
save('mat/all_tib_se2.mat', 'se2')
save('mat/all_tib_se3.mat', 'se3')

save('mat/all_tib_pf0.mat', 'p_f0')
save('mat/all_tib_pf1.mat', 'p_f1')
save('mat/all_tib_pf2.mat', 'p_f2')
save('mat/all_tib_pf3.mat', 'p_f3')
% oa_idx = [];
% for i = 1:size(unique_oa_subject,1)
%     oa_idx = [oa_idx; map(unique_oa_subject(i))];
% end
% nc_idx = [];
% for i = 1:size(unique_nc_subject,1)
%     nc_idx = [nc_idx; map(unique_nc_subject(i))];
% end
% 
% r2v = reshape(r2(:,:,oa_idx), [], size(oa_idx,1));
% r3v = reshape(r3(:,:,oa_idx), [], size(oa_idx,1));
% r2nv = reshape(r2n(:,:,oa_idx), [], size(oa_idx,1));
% r3nv = reshape(r3n(:,:,oa_idx), [], size(oa_idx,1));
% 
% r2v = r2v';
% r3v = r3v';
% r2nv = r2nv';
% r3nv = r3nv';
% 
% c = [r2nv, r3nv];
% c(isnan(c)) = 0;
% k = 4;
% [idx_tib, centers_tib] = kmeans(c, k, 'replicates', 500);
% save('mat/idx_tib.mat', 'idx_tib');
% % load('mat/idx_tib.mat', 'idx_tib');
% % 
% avg_r2v_cluster = zeros(k, prod(sz));
% avg_r3v_cluster = zeros(k, prod(sz));
% r2v(isnan(r2v)) = 0;
% r3v(isnan(r3v)) = 0;
% for i = 1:k
%     avg_r2v_cluster(i,:) = sum(r2v(idx_tib==i,:), 1)/sum(idx_tib==i);
%     avg_r3v_cluster(i,:) = sum(r3v(idx_tib==i,:), 1)/sum(idx_tib==i);
% end
% figure
% for i = 1:k
%     subplot(2,k, i), imagesc(reshape(avg_r2v_cluster(i,:), sz)), colorbar, title(['avg r2 cluster ', int2str(i)])
% end
% for i = 1:k
%     subplot(2,k,k+i), imagesc(reshape(avg_r3v_cluster(i,:), sz)), colorbar, title(['avg r3 cluster ', int2str(i)])
% end


% % r2_cluster1 = reshape(centers_tib(1,1:prod(sz)), sz);
% % r3_cluster1 = reshape(centers_tib(1,1+prod(sz):end), sz);
% % r2_cluster2 = reshape(centers_tib(2,1:prod(sz)), sz);
% % r3_cluster2 = reshape(centers_tib(2,1+prod(sz):end), sz);
% % r2_cluster3 = reshape(centers_tib(3,1:prod(sz)), sz);
% % r3_cluster3 = reshape(centers_tib(3,1+prod(sz):end), sz);
% % r2_cluster4 = reshape(centers_tib(4,1:prod(sz)), sz);
% % r3_cluster4 = reshape(centers_tib(4,1+prod(sz):end), sz);
% % 
% % figure
% % subplot(2,4,1), imagesc(r2_cluster1), colorbar, title('r2 center 1')
% % subplot(2,4,2), imagesc(r2_cluster2), colorbar, title('r2 center 2')
% % subplot(2,4,3), imagesc(r2_cluster3), colorbar, title('r2 center 3')
% % subplot(2,4,4), imagesc(r2_cluster4), colorbar, title('r2 center 4')
% % subplot(2,4,5), imagesc(r3_cluster1), colorbar, title('r3 center 1')
% % subplot(2,4,6), imagesc(r3_cluster2), colorbar, title('r3 center 2')
% % subplot(2,4,7), imagesc(r3_cluster3), colorbar, title('r3 center 3')
% % subplot(2,4,8), imagesc(r3_cluster4), colorbar, title('r3 center 4')
