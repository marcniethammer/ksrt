close all
clear all

load('mat/mask_fem.mat', 'mask_fem');
load('mat/fem.mat', 'fem');

list = tdfread('/data/list_new.txt', ' ');

sz = size(mask_fem);
N = 706;

lme_cell_fem = cell(sum(mask_fem(:)), 1);
loc = 1;
for i = 1:sz(1)
    for j = 1:sz(2)
        if mask_fem(i,j) > 0
            fprintf(' i= %d, j = %d\n', i, j)
            X = [ones(N, 1) , list.visit, ones(N,1) .* (list.klg>0), list.visit .* (list.klg>0)];
            Z = [ones(N, 1) .* (list.klg==0), list.visit .* (list.klg==0), ones(N,1) .* (list.klg>0), list.visit .* (list.klg>0)];
            y = squeeze(fem(i,j,:));
            missing = y==0;
            y(missing) = [];
            X(missing,:) = [];
            Z(missing,:) = [];
            tmp_subject = list.subject;
            tmp_subject(missing) = [];
            lme = fitlmematrix(X, y(:), Z, tmp_subject);
            lme_cell_fem{loc} = lme;
            loc = loc+1;            
        end
    end
end
save('mat/lme_cell_fem.mat', 'lme_cell_fem')