close all
clear all

load('mat/mask_fem.mat', 'mask_fem')
load('mat/fem.mat', 'fem')
load('mat/idx_fem.mat', 'idx_fem')

list = tdfread('/data/list_new.txt', ' ');
N = 706;
N_nc = 368;
N_subject = 155;
N_nc_subject = 81;
sz = [256, 80];

unique_subject = unique(list.subject);
map = containers.Map(unique_subject, 1:size(unique_subject, 1));
unique_oa_subject = unique(list.subject(list.klg>0));
unique_nc_subject = unique(list.subject(list.klg==0));

oa_idx = [];
for i = 1:size(unique_oa_subject,1)
    oa_idx = [oa_idx; map(unique_oa_subject(i))];
end
nc_idx = [];
for i = 1:size(unique_nc_subject,1)
    nc_idx = [nc_idx; map(unique_nc_subject(i))];
end

unique_oa_subject_cluster = unique_subject(oa_idx(idx_fem==1));
subjectC = [];
visitC = [];
femC = zeros(sz(1),sz(2),0);
klgC = [];
for i = 1:706
    if any(list.subject(i) == unique_oa_subject_cluster) || any(list.subject(i) == unique_nc_subject)
        subjectC = [subjectC; list.subject(i)];
        visitC = [visitC; list.visit(i)];
        klgC = [klgC; list.klg(i)];
        femC(:,:,end+1) = fem(:,:,i);
    end
end

NN = size(klgC,1);

loc = 1;
lme_cell_femC1 = cell(sum(mask_fem(:)), 1);
for i = 1:sz(1)
    for j = 1:sz(2)
        if mask_fem(i,j) > 0
            fprintf(' i= %d, j = %d\n', i, j)
            X = [ones(NN, 1) , visitC, ones(NN,1) .* (klgC>0), visitC .* (klgC>0)];
            Z = [ones(NN, 1) .* (klgC==0), visitC .* (klgC==0), ones(NN,1) .* (klgC>0), visitC .* (klgC>0)];
            y = squeeze(femC(i,j,:));
            missing = y==0;
            y(missing) = [];
            X(missing,:) = [];
            Z(missing,:) = [];
            tmp_subject = subjectC;
            tmp_subject(missing) = [];
            lme = fitlmematrix(X, y(:), Z, tmp_subject);
            lme_cell_femC1{loc} = lme;
            loc = loc+1;            
        end
    end
end
save('mat/lme_cell_femC1.mat', 'lme_cell_femC1')

% load('mat/lme_cell_femC1.mat', 'lme_cell_femC1')

f0 = zeros(sz); % NC intercept (fixed effect)
f1 = zeros(sz); % NC slope (fixed effect)
f2 = zeros(sz); % OA-NC intercept (fixed effect)
f3 = zeros(sz); % OA-NC slope (fixed effect)

p_f0 = zeros(sz); % pvalue of f0 not being zero
p_f1 = zeros(sz); % pvalue of f1 not being zero
p_f2 = zeros(sz); % pvalue of f2 not being zero
p_f3 = zeros(sz); % pvalue of f3 not being zero

se0 = zeros(sz);
se1 = zeros(sz);
se2 = zeros(sz);
se3 = zeros(sz);

loc = 1;
for i = 1:sz(1)
    fprintf(' i= %d\n', i)
    for j = 1:sz(2)
        if mask_fem(i,j) > 0
            lme = lme_cell_femC1{loc};
            loc = loc + 1;
            f = fixedEffects(lme); % get fixed effects
            f0(i,j) = f(1); 
            f1(i,j) = f(2);
            f2(i,j) = f(3);
            f3(i,j) = f(4);
            p_f0(i,j) = lme.Coefficients.pValue(1);
            p_f1(i,j) = lme.Coefficients.pValue(2);
            p_f2(i,j) = lme.Coefficients.pValue(3);
            p_f3(i,j) = lme.Coefficients.pValue(4);
            se0(i,j) = lme.Coefficients(1,3).SE;
            se1(i,j) = lme.Coefficients(2,3).SE;
            se2(i,j) = lme.Coefficients(3,3).SE;
            se3(i,j) = lme.Coefficients(4,3).SE;
        end
    end
end

save('mat/C1_fem_pf0_raw.mat', 'p_f0')
save('mat/C1_fem_pf1_raw.mat', 'p_f1')
save('mat/C1_fem_pf2_raw.mat', 'p_f2')
save('mat/C1_fem_pf3_raw.mat', 'p_f3')

[p_f0_thr, p_f0_cor, p_f0_adj] = fdr(p_f0(mask_fem>0));
[p_f1_thr, p_f1_cor, p_f1_adj] = fdr(p_f1(mask_fem>0));
[p_f2_thr, p_f2_cor, p_f2_adj] = fdr(p_f2(mask_fem>0));
[p_f3_thr, p_f3_cor, p_f3_adj] = fdr(p_f3(mask_fem>0));

p_f0(mask_fem>0) = -log10(p_f0_adj);
p_f1(mask_fem>0) = -log10(p_f1_adj);
p_f2(mask_fem>0) = -log10(p_f2_adj);
p_f3(mask_fem>0) = -log10(p_f3_adj);

save('mat/C1_fem_f0.mat', 'f0')
save('mat/C1_fem_f1.mat', 'f1')
save('mat/C1_fem_f2.mat', 'f2')
save('mat/C1_fem_f3.mat', 'f3')

save('mat/C1_fem_se0.mat', 'se0')
save('mat/C1_fem_se1.mat', 'se1')
save('mat/C1_fem_se2.mat', 'se2')
save('mat/C1_fem_se3.mat', 'se3')

save('mat/C1_fem_pf0.mat', 'p_f0')
save('mat/C1_fem_pf1.mat', 'p_f1')
save('mat/C1_fem_pf2.mat', 'p_f2')
save('mat/C1_fem_pf3.mat', 'p_f3')
% 
% f0_up = upsample(f0',3) + upsample(f0',3,1) + upsample(f0',3,2);
% f1_up = upsample(f1',3) + upsample(f1',3,1) + upsample(f1',3,2);
% f2_up = upsample(f2',3) + upsample(f2',3,1) + upsample(f2',3,2);
% f3_up = upsample(f3',3) + upsample(f3',3,1) + upsample(f3',3,2);
% 
% p_f0_up = upsample(p_f0',3) + upsample(p_f0',3,1) + upsample(p_f0',3,2);
% p_f1_up = upsample(p_f1',3) + upsample(p_f1',3,1) + upsample(p_f1',3,2);
% p_f2_up = upsample(p_f2',3) + upsample(p_f2',3,1) + upsample(p_f2',3,2);
% p_f3_up = upsample(p_f3',3) + upsample(p_f3',3,1) + upsample(p_f3',3,2);

% figure
% imagesc(f0_up)
% set(gcf, 'color', [1,1,1])
% set(gca, 'FontSize', 20)
% axis off
% axis equal
% colorbar('FontSize', 20)
% export_fig -m2 pdf/fem_intpt_nc_vs_c1_expt.pdf

% figure
% subplot(2,4,1), imagesc(f0), colorbar, title('f0')
% subplot(2,4,2), imagesc(f1), colorbar, title('f1')
% subplot(2,4,3), imagesc(f2), colorbar, title('f2')
% subplot(2,4,4), imagesc(f3), colorbar, title('f3')
% subplot(2,4,5), imagesc(p_f0), colorbar, title('pf0')
% subplot(2,4,6), imagesc(p_f1), colorbar, title('pf1')
% subplot(2,4,7), imagesc(p_f2), colorbar, title('pf2')
% subplot(2,4,8), imagesc(p_f3), colorbar, title('pf3')
% 
% thresh = -log(0.05)+log(sum(mask_fem(:)));
% figure
% subplot(2,4,1), imagesc(f0), colorbar, title('f0')
% subplot(2,4,2), imagesc(f1), colorbar, title('f1')
% subplot(2,4,3), imagesc(f2), colorbar, title('f2')
% subplot(2,4,4), imagesc(f3), colorbar, title('f3')
% subplot(2,4,5), imagesc(p_f0>thresh), colorbar, title('pf0')
% subplot(2,4,6), imagesc(p_f1>thresh), colorbar, title('pf1')
% subplot(2,4,7), imagesc(p_f2>thresh), colorbar, title('pf2')
% subplot(2,4,8), imagesc(p_f3>thresh), colorbar, title('pf3')
