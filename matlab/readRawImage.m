function I = readRawImage( imName, imSize, imType )
% function [ I, x, y, z ] = readRawImage( imName, imSize, imSpacing, imType )
%% Reads an raw image with specified image name, image size, image spacing and image typte. Returns the image data, together with world coordinates.
    fid = fopen( imName );
    I = fread( fid, prod( imSize ), imType );
    fclose( fid );
    
    I = reshape( I, imSize );
    
%     [x,y,z] = ndgrid( 1:imSize(1), 1:imSize(2), 1:imSize(3) );
%     x = x * imSpacing(1);
%     y = y * imSpacing(2);
%     z = z * imSpacing(3);
    
end
