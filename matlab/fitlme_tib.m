close all
clear all

load('mat/mask_tib.mat', 'mask_tib');
load('mat/tib.mat', 'tib');

% list = tdfread('/data/list_new.txt', ' ');
list = tdfread( 'list_new.txt', ' ' );

sz = size(mask_tib);
N = 706;

lme_cell_tib = cell(sum(mask_tib(:)), 1);
loc = 1;
%%
for i = 1:sz(1)
    for j = 1:sz(2)
        if mask_tib(i,j) > 0
            fprintf(' i= %d, j = %d\n', i, j)
            X = [ones(N, 1) , list.visit, ones(N,1) .* (list.klg>0), list.visit .* (list.klg>0)];
            Z = [ones(N, 1) .* (list.klg==0), list.visit .* (list.klg==0), ones(N,1) .* (list.klg>0), list.visit .* (list.klg>0)];
            y = squeeze(tib(i,j,:));
            missing = y==0;
            y(missing) = [];
            X(missing,:) = [];
            Z(missing,:) = [];
            tmp_subject = list.subject;
            tmp_subject(missing) = [];
            lme = fitlmematrix(X, y(:), Z, tmp_subject);
            lme_cell_tib{loc} = lme;
            loc = loc+1;            
        end
    end
end
save('mat/lme_cell_tib.mat', 'lme_cell_tib', '-v7.3')