close all
clear all
addpath ../

list = tdfread('/data/list_new.txt', ' ');

thresh = 680;
%% compute the mask
sz = [256, 80];
N = 706;
% mask_fem = zeros(sz);
% mask_tib = zeros(sz);

% for i = 1:size(list.subject,1)
%     imName = ['/data/long_moved_2d/fem_',int2str(list.subject(i)),'_',int2str(list.visit(i)),'.raw'];
%     fem = readRawImage(imName, sz, 'single');
%     mask_fem = mask_fem + (fem > 0);
%     
%     imName = ['/data/long_moved_2d/tib_',int2str(list.subject(i)),'_',int2str(list.visit(i)),'.raw'];
%     tib = readRawImage(imName, sz, 'single');
%     mask_tib = mask_tib + (tib > 0);
% end
% 
% mask_fem = uint16(mask_fem > thresh);
% mask_tib = uint16(mask_tib > thresh);
% % writeRawImage('/data/mask_fem_short.raw', mask_fem, 'uint16');
% % writeRawImage('/data/mask_tib_short.raw', mask_tib, 'uint16');
% save('mat/mask_fem.mat', 'mask_fem');
% save('mat/mask_tib.mat', 'mask_tib');

%% store thickness into a 3D array
fem = zeros(sz(1), sz(2), N);
for i = 1:size(list.subject,1)
    imName = ['/data/long_moved_2d/fem_',int2str(list.subject(i)),'_',int2str(list.visit(i)),'.raw'];
    fem(:,:,i) = readRawImage(imName, sz, 'single');
end

tib = zeros(sz(1), sz(2), N);
for i = 1:size(list.subject,1)
    imName = ['/data/long_moved_2d/tib_',int2str(list.subject(i)),'_',int2str(list.visit(i)),'.raw'];
    tib(:,:,i) = readRawImage(imName, sz, 'single');
end
save('mat/fem.mat', 'fem');
save('mat/tib.mat', 'tib');
