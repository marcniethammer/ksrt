# Performs the segmentations of femoral and the tibial cartilage for the id $ID

echo "Running DoCartilageSegmentationForID for ID = $ID"

if [ -f "$CartilageSegOutDIR"/result-orig-"$ID".mhd ]
then
  echo "File $CartilageSegOutDIR/result-orig-$ID.mhd already exists."
  echo "Not recomputing."
  echo "Delete the file if you want to force recomputation."
else

# create two zero images

echo "Creating empty image for femoral cartilage fusion: $CartilageSegOutDIR/fem-fusion-$ID.nhdr"
$ksrtAppDIR/ImageMath "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct.nhdr -sub "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct.nhdr -outfile "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr -type float

echo "Creating empty image for tibial cartilage fusion: $CartilageSegOutDIR/tib-fusion-$ID.nhdr"
$ksrtAppDIR/ImageMath "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct.nhdr -sub "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct.nhdr -outfile "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr -type float

n=0
n_all=0

# if there is already patch data, remove it
if [ -f "$CartilageSegOutDIR"/patch-"$ID".txt ]
then
       rm "$CartilageSegOutDIR"/patch-"$ID".txt
fi

# loop over all the atlas files to do cartilage segmentations
while read line
do
    if [[ ! -z "${line// }" ]]
    then
        ID_A=$(echo $line|cut -d' ' -f1)              
	BoneSegOutDIR_A="$DataDIROut/$ID_A/bone_segmentation"

	# computing affine registration
	echo "Affinely registering femur bone $ID_A to $ID"

	# TODO: not sure if this cutting is such a good idea for the registration, needs to be revisited
	$ksrtAppDIR/ksrtComputeAffineRegistration "$BoneSegOutDIR"/femur-seg-"$ID"-cut.nhdr "$BoneSegOutDIR_A"/femur-seg-"$ID_A"-cut.nhdr --metric SSD "$BoneRegOutDIR"/femur-"$ID"-"$ID_A".nhdr "$BoneRegOutDIR"/affine-femur-"$ID"-"$ID_A".tfm

        $ksrtAppDIR/ksrtComputeAffineRegistration "$BoneSegOutDIR"/tibia-seg-"$ID"-cut.nhdr "$BoneSegOutDIR_A"/tibia-seg-"$ID_A"-cut.nhdr --metric SSD "$BoneRegOutDIR"/tibia-"$ID"-"$ID_A".nhdr "$BoneRegOutDIR"/affine-tibia-"$ID"-"$ID_A".tfm

	# TODO: Maybe add B-spline here??

	n_all=`expr $n_all + 1`
        if [ -f "$BoneRegOutDIR"/affine-femur-"$ID"-"$ID_A".tfm -a -f "$BoneRegOutDIR"/affine-tibia-"$ID"-"$ID_A".tfm ]
        then
                n=`expr $n + 1`
		
		# for femur
                $ksrtAppDIR/ksrtApplyAffineTransform "$BoneRegOutDIR"/affine-femur-"$ID"-"$ID_A".tfm "$DataDIROut"/"$ID_A"/preprocess/fem-"$ID_A".nhdr "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale-smooth.nhdr "$BoneRegOutDIR"/fem-"$ID"-"$ID_A".nhdr

		# for tibia
                $ksrtAppDIR/ksrtApplyAffineTransform "$BoneRegOutDIR"/affine-tibia-"$ID"-"$ID_A".tfm "$DataDIROut"/"$ID_A"/preprocess/tib-"$ID_A".nhdr "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale-smooth.nhdr "$BoneRegOutDIR"/tib-"$ID"-"$ID_A".nhdr

		# now add it to the fusion file
                $ksrtAppDIR/ImageMath "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr -add "$BoneRegOutDIR"/fem-"$ID"-"$ID_A".nhdr -outfile "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr -type float
                $ksrtAppDIR/ImageMath "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr -add "$BoneRegOutDIR"/tib-"$ID"-"$ID_A".nhdr -outfile "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr -type float

		# creating patch information
		$ksrtAppDIR/ksrtApplyAffineTransform "$BoneRegOutDIR"/affine-femur-"$ID"-"$ID_A".tfm "$DataDIROut"/"$ID_A"/preprocess/image-"$ID_A"-correct-scale.nhdr "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale-smooth.nhdr "$BoneRegOutDIR"/mri-femur-"$ID"-"$ID_A".nhdr
                $ksrtAppDIR/ksrtApplyAffineTransform "$BoneRegOutDIR"/affine-tibia-"$ID"-"$ID_A".tfm "$DataDIROut"/"$ID_A"/preprocess/image-"$ID_A"-correct-scale.nhdr "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale-smooth.nhdr "$BoneRegOutDIR"/mri-tibia-"$ID"-"$ID_A".nhdr
               S1="$BoneRegOutDIR/mri-femur-$ID-$ID_A.nhdr"
               S2="$BoneRegOutDIR/fem-$ID-$ID_A.nhdr"
               S3="$BoneRegOutDIR/mri-tibia-$ID-$ID_A.nhdr"
               S4="$BoneRegOutDIR/tib-$ID-$ID_A.nhdr"

               echo "$S1 $S2 $S3 $S4" >> "$CartilageSegOutDIR"/patch-"$ID".txt       
	fi
    fi
done < $atlasFILE
echo $n

echo "Registered $n/$n_all images"

# computing the probability maps for femoral and for tibial cartilage
echo "Computing the probability maps for femoral and tibial cartilage"
$ksrtAppDIR/ImageMath "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr -constOper 3,"$n" -outfile "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr -type float
$ksrtAppDIR/ImageMath "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr -constOper 3,"$n" -outfile "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr -type float

# computing the patch information
echo "Computing the patch information"
$ksrtAppDIR/ksrtPatchBasedLabelFusion "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale.nhdr "$CartilageSegOutDIR"/patch-"$ID".txt "$CartilageSegOutDIR"/fem-fusion-"$ID"-patch.nhdr "$CartilageSegOutDIR"/tib-fusion-"$ID"-patch.nhdr --patchSize 2 --neighborhoodSize 2 --nearestNeighbors 1

# scaling the image for SVM
echo "Scaling the image"
$ksrtAppDIR/ImageMath "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale.nhdr -constOper 2,300 -outfile "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale-30000.nhdr

# extract the features used for the SVM classification
echo "Extracting features for SVM"
$ksrtAppDIR/ksrtExtractTestingFeatures_SVM "$DataDIROut"/"$ID"/preprocess/image-"$ID"-correct-scale-30000.nhdr "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr --scales 0.39,0.78,1.0 --threshold 0.0 "$svmOutDIR"/f-"$ID"

# do prediction using svm
echo "Performing SVM prediction"
$svmDIR/svm-predict -b 1 "$svmOutDIR"/f-"$ID" "$svmOutDIR"/train_new_small.model "$svmOutDIR"/p-"$ID"

# compute probabilites based on SVM
echo "Computing probabilities based on SVM"
$ksrtAppDIR/ksrtComputeClassificationProbabilities_SVM "$CartilageSegOutDIR"/fem-fusion-"$ID".nhdr "$CartilageSegOutDIR"/tib-fusion-"$ID".nhdr "$svmOutDIR"/p-"$ID" "$CartilageSegOutDIR"/pFem-"$ID"-svm.nhdr "$CartilageSegOutDIR"/pTib-"$ID"-svm.nhdr -t 0.0

# compute normals
echo "Computing normals"
$ksrtAppDIR/ksrtComputeNormalDirection "$BoneSegOutDIR"/femur-seg-"$ID".nhdr "$BoneSegOutDIR"/tibia-seg-"$ID".nhdr "$BoneSegOutDIR"/nx-"$ID".nhdr "$BoneSegOutDIR"/ny-"$ID".nhdr "$BoneSegOutDIR"/nz-"$ID".nhdr -u 1,1,1

# compute the overall labeling cost to be used for the cartilage segmentation
echo "Computing overall labeling cost"
$ksrtAppDIR/ksrtComputeCartilageLabelingCost "$CartilageSegOutDIR"/pFem-"$ID"-svm.nhdr "$CartilageSegOutDIR"/pTib-"$ID"-svm.nhdr "$CartilageSegOutDIR"/fem-fusion-"$ID"-patch.nhdr "$CartilageSegOutDIR"/tib-fusion-"$ID"-patch.nhdr "$CartilageSegOutDIR"/fem-cost-"$ID".nhdr "$CartilageSegOutDIR"/bkg-cost-"$ID".nhdr "$CartilageSegOutDIR"/tib-cost-"$ID".nhdr

# perform the anisotropic segmentation
echo "Performing anisotropic 3 label segmentation"
$ksrtAppDIR/ksrtSegmentation_3label_anisotropic "$CartilageSegOutDIR"/fem-cost-"$ID".nhdr "$CartilageSegOutDIR"/bkg-cost-"$ID".nhdr "$CartilageSegOutDIR"/tib-cost-"$ID".nhdr "$BoneSegOutDIR"/nx-"$ID".nhdr "$BoneSegOutDIR"/ny-"$ID".nhdr "$BoneSegOutDIR"/nz-"$ID".nhdr "$CartilageSegOutDIR"/fem-seg-"$ID"-svm-patch-1.0-0.1.nhdr "$CartilageSegOutDIR"/tib-seg-"$ID"-svm-patch-1.0-0.1.nhdr -g 1.0 -a 0.1

# merge the segmentation labels
echo "Merging the segmentation labels"
$ksrtAppDIR/ksrtMergeSegmentations "$BoneSegOutDIR"/femur-seg-"$ID".nhdr "$CartilageSegOutDIR"/fem-seg-"$ID"-svm-patch-1.0-0.1.nhdr "$BoneSegOutDIR"/tibia-seg-"$ID".nhdr "$CartilageSegOutDIR"/tib-seg-"$ID"-svm-patch-1.0-0.1.nhdr "$CartilageSegOutDIR"/result-"$ID"-svm-patch-1.0-0.1.nhdr 

# flip the result back if needed
echo "Flipping the segmentation result back if needed"
$ksrtAppDIR/ksrtFlip "$CartilageSegOutDIR"/result-"$ID"-svm-patch-1.0-0.1.nhdr -f 0,0,"$FLIP" "$CartilageSegOutDIR"/result-orig-"$ID".mhd -c 0

fi


