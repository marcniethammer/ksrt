Installation:

1) mkdir ksrt-build
2) cd ksrt-build
3) ccmake [directory_containing_ksrt_source]
4) make
5) make doc

Making a binary installer
6) cpack -C CPackConfig.cmake

Making a source installer
7) cpack -C CPackSourceConfig.cmake


